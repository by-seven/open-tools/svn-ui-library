// .storybook/AlephTheme.js
// import { useTheme } from 'vuetify';
import { create } from '@storybook/theming/create';

// const currentTheme = useTheme().global.name.value
// const themeColors = useTheme().themes.value?.[currentTheme].colors

export default create({
  base: 'light',
  brandTitle: 'Aleph',
  brandUrl: 'https://learn.byseven.co',
  brandImage: 'https://learn.byseven.co/logos/seven.svg',
  brandTarget: '_self',

  // //
  // colorPrimary: '#3A10E5',
  // colorSecondary: '#585C6D',

  // // UI
  // appBg: '#ffffff',
  // appContentBg: '#ffffff',
  // appBorderColor: '#585C6D',
  // appBorderRadius: 4,

  // // Text colors
  // textColor: '#10162F',
  // textInverseColor: '#ffffff',

  // // Toolbar default and active colors
  // barTextColor: '#9E9E9E',
  // barSelectedColor: '#585C6D',
  // barBg: '#ffffff',

  // // Form colors
  // inputBg: '#ffffff',
  // inputBorder: '#10162F',
  // inputTextColor: '#10162F',
  // inputBorderRadius: 2,
});