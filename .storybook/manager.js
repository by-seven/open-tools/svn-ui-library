// .storybook/manager.js
import { addons } from '@storybook/manager-api';
import AlephTheme from "./AlephTheme";

addons.setConfig({
  theme: AlephTheme,
});