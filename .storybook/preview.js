/** @type { import('@storybook/vue3').Preview } */
import '../src/index.css'
import 'vuetify/styles';
import '../src/variables.scss'
import loadFonts from '../src/plugins/webfontloader.js';
import { setup } from '@storybook/vue3';
import { themes } from '@storybook/theming';
import vuetify from '../src/plugins/vuetify';
import { withVuetifyTheme } from './withVuetifyTheme.decorator.js';
import { Intersect } from 'vuetify/directives';

setup((app) => {
  loadFonts()
  // Registers your app's plugins into Storybook
  app.directive('intersect', Intersect)
  app.use(vuetify)
});

const customViewports = {
  xs: {
    name: 'xs',
    styles: {
      width: '360px',
      height: '800px',
    },
  },
  sm: {
    name: 'sm',
    styles: {
      width: '640px',
      height: '960px',
    },
  },
  md: {
    name: 'md',
    styles: {
      width: '768px',
      height: '1024px',
    },
  },
  lg: {
    name: 'lg',
    styles: {
      width: '1024px',
      height: '768px',
    },
  },
  smallScreen: {
    name: 'small Screen',
    styles: {
      width: '1111px',
      height: '633px',
    },
  },
  xl: {
    name: 'xl',
    styles: {
      width: '1280px',
      height: '1024px',
    },
  },
  xxl: {
    name: 'xxl',
    styles: {
      width: '1536px',
      height: '864px',
    },
  },
  xxxl: {
    name: 'xxxl',
    styles: {
      width: '1920px',
      height: '1080px',
    },
  },
};

export const parameters = {
  actions: { argTypesRegex: "^on[A-Z].*" },
  controls: {
    matchers: {
      color: /(background|color)$/i,
      date: /Date$/,
    },
  },
  docs: {
    theme: themes.normal
  },
  viewport: {
    viewports: {
      ...customViewports,
    },
    defaultViewport: 'xl',
  }
};

export const decorators = [withVuetifyTheme];

export const globalTypes = {
  theme: {
    name: 'Theme',
    description: 'Global theme for components',
    defaultValue: 'alephLight',
    toolbar: {
      icon: 'paintbrush',
      // Array of plain string values or MenuItem shape
      items: [
        { value: 'alephLight', title: 'Light' },
        { value: 'studio', title: 'Studio' },
        { value: 'neutral', title: 'Neutral' },
      ],
      // Change title based on selected value
      dynamicTitle: true,
    },
  },
  locale: {
    name: 'Locale',
    description: 'Internationalization locale',
    defaultValue: 'en',
    toolbar: {
      icon: 'globe',
      items: [
        { value: 'en', right: '🇺🇸', title: 'Anglais' },
        { value: 'fr', right: '🇫🇷', title: 'Francais' },
      ],
      // Change title based on selected value
      dynamicTitle: true,
    },
  },
};