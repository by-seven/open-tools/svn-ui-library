# svn-ui-library

Svn Ui is a vuejs plugin uses in multiple projects.

## Project setup
- [Install vuetify](https://v15.vuetifyjs.com/en/getting-started/quick-start/#existing-applications)
- [Install tailwindcss](https://tailwindcss.com/docs/guides/vite#vue)
- `npm install @iconify/icons-mdi --save-dev`

Then
``` 
npm install
```

## Add Library
Get the last commit to the [library project](https://gitlab.com/by-seven/open-tools/svn-ui-library.git)

Add in dependencies
```
"dependencies": {
    "svn-ui-library": "https://gitlab.com/by-seven/open-tools/svn-ui-library.git#50cbd3bcc039e49aa6f9e303927e2428b2fa27d2",
  },
```

Add in main.js
```
import svnUiLibrary from './plugins/svn-ui-library';

app.use(svnUiLibrary)
```

### Compiles and hot-reloads for development
```
yarn dev
```

### Compiles and minifies for production
```
yarn build
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

## Recommended IDE Setup

- [VS Code](https://code.visualstudio.com/) + [Volar](https://marketplace.visualstudio.com/items?itemName=Vue.volar) (and disable Vetur) + [TypeScript Vue Plugin (Volar)](https://marketplace.visualstudio.com/items?itemName=Vue.vscode-typescript-vue-plugin).
