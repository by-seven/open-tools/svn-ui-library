import vueEslintParser from 'vue-eslint-parser';
import typescriptEslintParser from '@typescript-eslint/parser';
import vuePlugin from 'eslint-plugin-vue'; // Import the Vue plugin directly
import typescriptPlugin from '@typescript-eslint/eslint-plugin'; // Import TypeScript plugin
import prettierPlugin from 'eslint-plugin-prettier'; // Import Prettier plugin
import prettierConfig from 'eslint-config-prettier'; // Prettier config

export default [
  {
    files: ['**/*.vue', '**/*.ts'],
    languageOptions: {
      parser: vueEslintParser,
      parserOptions: {
        parser: typescriptEslintParser,
        ecmaVersion: 2020,
        sourceType: 'module',
      },
    },
    plugins: {
      vue: vuePlugin, // Use the Vue plugin
      '@typescript-eslint': typescriptPlugin, // Use the TypeScript plugin
      prettier: prettierPlugin, // Use the Prettier plugin
    },
    rules: {
      ...vuePlugin.configs['vue3-recommended'].rules, // Vue 3 recommended rules
      ...typescriptPlugin.configs.recommended.rules, // TypeScript recommended rules
      ...prettierConfig.rules, // Prettier recommended rules
      'vue/multi-word-component-names': 'off', // Example rule
    },
  },
];

