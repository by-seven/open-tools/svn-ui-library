// prettier.config.js
export default {
  semi: true,                   // Add semicolons at the end of statements
  singleQuote: true,            // Use single quotes instead of double quotes
  printWidth: 80,               // Maximum line length before wrapping
  tabWidth: 2,                  // Number of spaces per tab
  useTabs: false,               // Indent with spaces instead of tabs
  trailingComma: 'es5',         // Add trailing commas where valid in ES5 (e.g., objects, arrays)
  bracketSpacing: true,         // Print spaces between brackets in object literals
  jsxBracketSameLine: false,    // Put the `>` of a multi-line JSX element at the end of the last line
  arrowParens: 'always',        // Always include parentheses around arrow function arguments
  endOfLine: 'lf',              // Use line-feed only for line endings
};