//const express = require('express');
import express from 'express';
import path from 'path';
import { fileURLToPath } from 'url';

const __filename = fileURLToPath(import.meta.url); // get the resolved path to the file
const __dirname = path.dirname(__filename); // get the name of the directory

const app = express();
const port = process.env.PORT || 3000;

// Serve the static Storybook files from the "storybook-static" directory
app.use(express.static(path.join(__dirname, 'storybook-static')));

// For any route, serve the index.html file from Storybook
app.get('*', (req, res) => {
  res.sendFile(path.join(__dirname, 'storybook-static', 'index.html'));
});

// Start the server
app.listen(port, () => {
  console.log(`Storybook is running on http://localhost:${port}`);
});

