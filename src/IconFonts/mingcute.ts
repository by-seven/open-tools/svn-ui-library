import { Icon } from '@iconify/vue/dist/iconify.js';
import { h } from 'vue';
import type { IconSet } from 'vuetify';
// import { Icon } from "@iconify/vue/dist/iconify.js";
import { VSvgIcon } from 'vuetify/components';

const mingcuteAliases = {
  menu: 'svg:M3,6H21V8H3V6M3,11H21V13H3V11M3,16H21V18H3V16Z',
  book: 'svg:M10 3c.667 0 1.333.253 2 .76c.667-.507 1.333-.76 2-.76h6a2 2 0 0 1 2 2v13a2 2 0 0 1-2 2h-7c0 .552-.45 1-1 1s-1-.45-1-1H4a2 2 0 0 1-2-2V5a2 2 0 0 1 2-2z',
}

const mingcute = {
  component: VSvgIcon
};
export { mingcuteAliases, mingcute };