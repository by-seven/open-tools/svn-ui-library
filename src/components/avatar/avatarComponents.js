import {
  createSvnProAvatar,
  AvatarType,
  AvatarSize,
} from '@/factories/avatar.factory';

/**
 * SvnProAvatarCard
 *
 * @component
 * @prop {String} image - The URL of the avatar image. If it fails to load, the monogram will be displayed.
 * @prop {String} firstname - Name or string used to generate the initial for the monogram.
 *
 * @default
 * type: AvatarType.PHOTO
 * size: AvatarSize.SMALL
 * start: false
 */

export const SvnProAvatarCard = createSvnProAvatar(
  {
    type: AvatarType.PHOTO,
    size: AvatarSize.SMALL,
    start: false,
    firstname: 'A',
    image: undefined,
  },
  {
    name: 'SvnProAvatarCard',
  }
);

// SvnProAvatarCard.beforeCreate = function () {
//   this.$options.propsData.type = AvatarType.PHOTO;
//   this.$options.propsData.size = AvatarSize.SMALL;
//   this.$options.propsData.start = false;
// };
