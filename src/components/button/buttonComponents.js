import { createButtonComponent } from '@/factories/button.factory';

export const TextButtonPadding = createButtonComponent({
  text: 'Text Button',
  variant: 'text',
  disabled: false,
  prependIcon: undefined,
  padding: 'px-10 py-2',
});

export const TextButton = createButtonComponent(
  {
    text: 'Text Button',
    variant: 'text',
    disabled: false,
    prependIcon: undefined,
  },
  {
    name: 'TextButton',
  }
);
