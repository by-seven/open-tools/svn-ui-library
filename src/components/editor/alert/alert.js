/**
 * Alert block for the Editor.js.
 *
 * @author Vishal Telangre
 * @license MIT
 */

/**
 * Build styles
 */
import './index.scss';

/**
 * Import Tool's icons
 */
import ToolboxIcon from '../alert/assets/icon.svg';
import SettingsIcon from '../alert/assets/settings-icon.svg';
import AlignLeftIcon from '../alert/assets/align-left-icon.svg';
import AlignCenterIcon from '../alert/assets/align-center-icon.svg';
import AlignRightIcon from '../alert/assets/align-right-icon.svg';

/**
 * @class Alert
 * @classdesc Alert Tool for Editor.js
 * @property {AlertData} data - Alert Tool`s input and output data
 * @property {object} api - Editor.js API instance
 *
 * @typedef {object} AlertData
 * @description Alert Tool`s input and output data
 * @property {string} type - Alert type
 * @property {string} alignType - Alert align type
 * @property {string} message - Alert message
 *
 * @typedef {object} AlertConfig
 * @description Alert Tool`s initial configuration
 * @property {string} defaultType - default Alert type
 * @property {string} defaultAlignType - default align Alert type
 * @property {string} messagePlaceholder - placeholder to show in Alert`s message input
 */
export default class Alert {
  /**
   * Get Toolbox settings
   *
   * @public
   * @returns {string}
   */
  static get toolbox() {
    return {
      icon: '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" stroke-width="0" stroke="currentColor" fill="none"><path d="M11 15H13V17H11V15ZM11 7H13V13H11V7ZM12 2C6.47 2 2 6.5 2 12C2 14.6522 3.05357 17.1957 4.92893 19.0711C5.85752 19.9997 6.95991 20.7362 8.17317 21.2388C9.38642 21.7413 10.6868 22 12 22C14.6522 22 17.1957 20.9464 19.0711 19.0711C20.9464 17.1957 22 14.6522 22 12C22 10.6868 21.7413 9.38642 21.2388 8.17317C20.7362 6.95991 19.9997 5.85752 19.0711 4.92893C18.1425 4.00035 17.0401 3.26375 15.8268 2.7612C14.6136 2.25866 13.3132 2 12 2ZM12 20C9.87827 20 7.84344 19.1571 6.34315 17.6569C4.84285 16.1566 4 14.1217 4 12C4 9.87827 4.84285 7.84344 6.34315 6.34315C7.84344 4.84285 9.87827 4 12 4C14.1217 4 16.1566 4.84285 17.6569 6.34315C19.1571 7.84344 20 9.87827 20 12C20 14.1217 19.1571 16.1566 17.6569 17.6569C16.1566 19.1571 14.1217 20 12 20Z" fill="#000000"/></svg>',
      title: 'Alert text',
    };
  }

  /**
   * Allow to press Enter inside the Alert block
   * @public
   * @returns {boolean}
   */
  static get enableLineBreaks() {
    return true;
  }

  /**
   * Default Alert type
   *
   * @public
   * @returns {string}
   */
  static get DEFAULT_TYPE() {
    return 'info';
  }

  /**
   * Default Alert align type
   *
   * @public
   * @returns {string}
   */
  static get DEFAULT_ALIGN_TYPE() {
    return 'left';
  }

  /**
   * Default placeholder for Alert message
   *
   * @public
   * @returns {string}
   */
  static get DEFAULT_MESSAGE_PLACEHOLDER() {
    return 'Type here...';
  }

  /**
   * Supported Alert types
   *
   * @public
   * @returns {array}
   */
  static get ALERT_TYPES() {
    return [
      'primary',
      'secondary',
      'info',
      'success',
      'warning',
      'danger',
      'light',
      'dark',
    ];
  }

  /**
   * Supported Align types
   *
   * @public
   * @returns {array}
   */
  static get ALIGN_TYPES() {
    return ['left', 'center', 'right'];
  }

  /**
   * Alert Tool`s styles
   *
   * @returns {Object}
   */
  get CSS() {
    return {
      wrapper: 'cdx-alert',
      wrapperForType: (type) => `cdx-alert-${type}`,
      wrapperForAlignType: (alignType) => `cdx-alert-align-${alignType}`,
      message: 'cdx-alert__message',
    };
  }

  /**
   * Render plugin`s main Element and fill it with saved data
   *
   * @param {AlertData} data — previously saved data
   * @param {AlertConfig} config — user config for Tool
   * @param {Object} api - Editor.js API
   * @param {boolean} readOnly - read only mode flag
   */
  constructor({ data, config, api, readOnly }) {
    this.api = api;

    this.defaultType = config.defaultType || Alert.DEFAULT_TYPE;
    this.defaultAlign = config.defaultAlign || Alert.DEFAULT_ALIGN_TYPE;
    this.messagePlaceholder =
      config.messagePlaceholder || Alert.DEFAULT_MESSAGE_PLACEHOLDER;

    this.data = {
      type: Alert.ALERT_TYPES.includes(data.type)
        ? data.type
        : this.defaultType,
      align: Alert.ALIGN_TYPES.includes(data.align)
        ? data.align
        : this.defaultAlign,
      message: data.message || '',
    };

    this.container = undefined;

    this.readOnly = readOnly;
  }

  /**
   * Returns true to notify the core that read-only mode is supported
   *
   * @return {boolean}
   */
  static get isReadOnlySupported() {
    return true;
  }

  /**
   * Create Alert Tool container
   *
   * @returns {Element}
   */
  render() {
    const containerClasses = [
      this.CSS.wrapper,
      this.CSS.wrapperForType(this.data.type),
      this.CSS.wrapperForAlignType(this.data.align),
    ];

    this.container = this._make('div', containerClasses);

    const messageEl = this._make('div', [this.CSS.message], {
      contentEditable: !this.readOnly,
      innerHTML: this.data.message,
    });
    messageEl.dataset.placeholder = this.messagePlaceholder;

    this.container.appendChild(messageEl);

    return this.container;
  }

  /**
   * Create Block's settings block
   *
   * @returns {array}
   */
  renderSettings() {
    const alertTypes = Alert.ALERT_TYPES.map((type) => ({
      icon: SettingsIcon,
      name: `alert-${type}`,
      label: this._getFormattedName(type),
      toggle: 'alert',
      isActive: this.data.type === type,
      onActivate: () => {
        this._changeAlertType(type);
      },
    }));

    const alignTypes = Alert.ALIGN_TYPES.map((align) => ({
      icon:
        align == 'left'
          ? AlignLeftIcon
          : align == 'center'
          ? AlignCenterIcon
          : align == 'right'
          ? AlignRightIcon
          : IconAlign_left,
      name: `align-${align}`,
      label: this._getFormattedName(align),
      toggle: 'align',
      isActive: this.data.align === align,
      onActivate: () => {
        this._changeAlignType(align);
      },
    }));
    return [...alertTypes, ...alignTypes];
  }

  /**
   * Helper for formatting Alert Type / Align Type
   *
   * @param {string} type - Alert type or Align type
   * @returns {string}
   */
  _getFormattedName(name) {
    return this.api.i18n.t(name.charAt(0).toUpperCase() + name.slice(1));
  }

  /**
   * Helper for changing style of Alert block with the selected Alert type
   *
   * @param {string} newType - new Alert type to be applied to the block
   * @private
   */
  _changeAlertType(newType) {
    // Save new type
    this.data.type = newType;

    Alert.ALERT_TYPES.forEach((type) => {
      const alertClass = this.CSS.wrapperForType(type);

      // Remove the old Alert type class
      this.container.classList.remove(alertClass);

      if (newType === type) {
        // Add an Alert class for the selected Alert type
        this.container.classList.add(alertClass);
      }
    });
  }

  /**
   * Helper for changing align of Alert block with the selected Align type
   *
   * @param {string} newAlign - new align type to be applied to the block
   * @private
   */
  _changeAlignType(newAlign) {
    // Save new type
    this.data.align = newAlign;

    Alert.ALIGN_TYPES.forEach((align) => {
      const alignClass = this.CSS.wrapperForAlignType(align);

      // Remove the old Alert type class
      this.container.classList.remove(alignClass);

      if (newAlign === align) {
        // Add an Alert class for the selected Alert type
        this.container.classList.add(alignClass);
      }
    });
  }

  /**
   * Extract Alert data from Alert Tool element
   *
   * @param {HTMLDivElement} alertElement - element to save
   * @returns {AlertData}
   */
  save(alertElement) {
    const messageEl = alertElement.querySelector(`.${this.CSS.message}`);

    return { ...this.data, message: messageEl.innerHTML };
  }

  /**
   * Helper for making Elements with attributes
   *
   * @param  {string} tagName           - new Element tag name
   * @param  {array|string} classNames  - list or name of CSS classname(s)
   * @param  {Object} attributes        - any attributes
   * @returns {Element}
   * @private
   */
  _make(tagName, classNames = null, attributes = {}) {
    let el = document.createElement(tagName);

    if (Array.isArray(classNames)) {
      el.classList.add(...classNames);
    } else if (classNames) {
      el.classList.add(classNames);
    }

    for (let attrName in attributes) {
      el[attrName] = attributes[attrName];
    }

    return el;
  }

  /**
   * Fill Alert's message with the pasted content
   *
   * @param {PasteEvent} event - event with pasted content
   */
  onPaste(event) {
    const { data } = event.detail;

    this.data = {
      type: this.defaultType,
      message: data.innerHTML || '',
    };
  }

  /**
   * Allow Alert to be converted to/from other blocks
   */
  static get conversionConfig() {
    return {
      // export Alert's message for other blocks
      export: (data) => data.message,
      // fill Alert's message from other block's export string
      import: (string) => {
        return {
          message: string,
          type: this.DEFAULT_TYPE,
          alignType: this.DEFAULT_ALIGN_TYPE,
        };
      },
    };
  }

  /**
   * Sanitizer config for Alert Tool saved data
   * @returns {Object}
   */
  static get sanitize() {
    return {
      message: true,
      type: false,
      alignType: false,
    };
  }
}