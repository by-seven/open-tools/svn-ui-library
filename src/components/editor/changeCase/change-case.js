import { upperCase, lowerCase, titleCase, sentenceCase, toggleCase, localeLowerCase, localeUpperCase } from "./change-case-util";
import './change-case.css';

export default class ChangeCase {

    static get isInline() {
        return true;
    }

    get state() {
        return this._state;
    }

    constructor({ config, api }) {
        this.api = api;
        this.button = null;
        this.optionButtons = [];
        this._state = true;
        this.selectedText = null;
        this.range = null;
        this._settings = config;

        this.CSS = {
            actions: 'change-case-action',
            toolbarLabel: 'change-case-toolbar__label',
            tool: 'change-case-tool',
            toolbarBtnActive: this.api.styles.settingsButtonActive,
            inlineButton: this.api.styles.inlineToolButton
        };

        this.caseOptions = {
            'titleCase': 'Title Case',
            'lowerCase': 'lower case',
            'upperCase': 'UPPER CASE',
            'localeLowerCase': 'localé lower casé',
            'localeUpperCase': 'LöCALE UPPER CASE',
            'sentenceCase': 'Sentence case',
            'toggleCase': 'tOGGLE cASE'
        }
    }

    set state(state) {
        this._state = state;
        this.button.classList.toggle(this.CSS.toolbarBtnActive, state);
    }

    get title() {
        return 'Change Case';
    }

    render() {
        this.button = document.createElement('button');
        this.button.type = 'button';
        this.button.innerHTML = `<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none"><path d="M3.9751 17L7.7251 7H9.5251L13.2751 17H11.5501L10.6501 14.45H6.6001L5.7001 17H3.9751ZM7.1251 13H10.1251L8.6751 8.85H8.5751L7.1251 13ZM16.3501 17.275C15.5334 17.275 14.8918 17.0623 14.4251 16.637C13.9584 16.2117 13.7251 15.6327 13.7251 14.9C13.7251 14.2 13.9961 13.629 14.5381 13.187C15.0801 12.745 15.7758 12.5243 16.6251 12.525C17.0084 12.525 17.3628 12.5543 17.6881 12.613C18.0134 12.6717 18.2924 12.7673 18.5251 12.9V12.55C18.5251 12.1 18.3711 11.7417 18.0631 11.475C17.7551 11.2083 17.3341 11.075 16.8001 11.075C16.4501 11.075 16.1211 11.15 15.8131 11.3C15.5051 11.45 15.2424 11.6667 15.0251 11.95L13.9501 11.15C14.2668 10.7 14.6668 10.3583 15.1501 10.125C15.6334 9.89167 16.1918 9.775 16.8251 9.775C17.8584 9.775 18.6501 10.021 19.2001 10.513C19.7501 11.005 20.0251 11.7173 20.0251 12.65V17.05H18.5501V16.2H18.4751C18.2584 16.5333 17.9668 16.796 17.6001 16.988C17.2334 17.18 16.8168 17.2757 16.3501 17.275ZM16.6001 16.025C17.1334 16.025 17.5878 15.8377 17.9631 15.463C18.3384 15.0883 18.5258 14.634 18.5251 14.1C18.2918 13.9667 18.0251 13.8667 17.7251 13.8C17.4251 13.7333 17.1501 13.7 16.9001 13.7C16.3668 13.7 15.9584 13.8043 15.6751 14.013C15.3918 14.2217 15.2501 14.5173 15.2501 14.9C15.2501 15.2333 15.3751 15.5043 15.6251 15.713C15.8751 15.9217 16.2001 16.0257 16.6001 16.025Z" fill="black"/></svg>`;
        this.button.classList.add(this.CSS.inlineButton);

        return this.button;
    }

    checkState(selection) {
        const text = selection.anchorNode;
        if (!text) return;
    }

    convertCase(range, option) {
        if (!range) return
        const clone = range.cloneContents();
        if (!clone) return
        clone.childNodes.forEach(node => {
            if (node.nodeName !== '#text') return;

            switch (option) {
                case 'titleCase':
                    node.textContent = titleCase(node.textContent);
                    break;

                case 'lowerCase':
                    node.textContent = lowerCase(node.textContent);
                    break;

                case 'upperCase':
                    node.textContent = upperCase(node.textContent);
                    break;

                case 'localeLowerCase':
                    node.textContent = localeLowerCase(node.textContent, this._settings.locale);
                    break;

                case 'localeUpperCase':
                    node.textContent = localeUpperCase(node.textContent, this._settings.locale);
                    break;

                case 'sentenceCase':
                    node.textContent = sentenceCase(node.textContent);
                    break;

                case 'toggleCase':
                    node.textContent = toggleCase(node.textContent);
                    break;

                default:
                    break;
            }
        });

        range.extractContents();
        range.insertNode(clone);
        this.api.inlineToolbar.close();
    }

    surround(range) {
        this.selectedText = range.cloneContents();
        this.actions.hidden = !this.actions.hidden;
        this.range = !this.actions.hidden ? range : null;
        this.state = !this.actions.hidden;
    }

    renderActions() {
        this.actions = document.createElement('div');
        this.actions.classList.add(this.CSS.actions);
        const actionsToolbar = document.createElement('div');
        actionsToolbar.classList.add(this.CSS.toolbarLabel);
        actionsToolbar.innerHTML = 'Change Case';

        this.actions.appendChild(actionsToolbar);

        if (!this._settings.showLocaleOption) {
            delete this.caseOptions.localeLowerCase;
            delete this.caseOptions.localeUpperCase;
        }        
        
        this.optionButtons = Object.keys(this.caseOptions).map(option => {
            const btnOption = document.createElement('div');
            btnOption.classList.add(this.CSS.tool);
            btnOption.dataset.mode = option;
            btnOption.innerHTML = this.caseOptions[option];
            return btnOption
        })

        for (const btnOption of this.optionButtons) {
            this.actions.appendChild(btnOption);
            this.api.listeners.on(btnOption, 'click', () => {
                this.convertCase(this.range, btnOption.dataset.mode)
            });
        }

        this.actions.hidden = true;
        return this.actions;
    }

    destroy() {
        for (const btnOption of this.optionButtons) {
            this.api.listeners.off(btnOption, 'click');
        }
    }
}
