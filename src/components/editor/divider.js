class Divider {
  static get toolbox() {
    return {
      title: "Divider",
      icon: "<div class='w-4 h-0.5 bg-[#000000]' />"
    }
  }

  constructor({ api, data, config, readOnly, block }) {
    this.api = api
    this.data = {
      alignment: data.alignment || config.alignment || "left",
      fullWidth: data.fullWidth || config.fullWidth || true,
    }
    this.config = config
    this.readOnly = readOnly
    this.block = block
    this.wrapper = undefined
    this.divider = undefined
    this.settings = [
      {
        name: "left",
        icon: "<svg xmlns=\"http://www.w3.org/2000/svg\" id=\"Layer\" enable-background=\"new 0 0 64 64\" height=\"20\" viewBox=\"0 0 64 64\" width=\"20\"><path d=\"m54 8h-44c-1.104 0-2 .896-2 2s.896 2 2 2h44c1.104 0 2-.896 2-2s-.896-2-2-2z\"/><path d=\"m54 52h-44c-1.104 0-2 .896-2 2s.896 2 2 2h44c1.104 0 2-.896 2-2s-.896-2-2-2z\"/><path d=\"m10 23h28c1.104 0 2-.896 2-2s-.896-2-2-2h-28c-1.104 0-2 .896-2 2s.896 2 2 2z\"/><path d=\"m54 30h-44c-1.104 0-2 .896-2 2s.896 2 2 2h44c1.104 0 2-.896 2-2s-.896-2-2-2z\"/><path d=\"m10 45h28c1.104 0 2-.896 2-2s-.896-2-2-2h-28c-1.104 0-2 .896-2 2s.896 2 2 2z\"/></svg>",
        classes: ["justify-start"]
      },
      {
        name: "center",
        icon: "<svg xmlns=\"http://www.w3.org/2000/svg\" id=\"Layer\" enable-background=\"new 0 0 64 64\" height=\"20\" viewBox=\"0 0 64 64\" width=\"20\"><path d=\"m54 8h-44c-1.104 0-2 .896-2 2s.896 2 2 2h44c1.104 0 2-.896 2-2s-.896-2-2-2z\"/><path d=\"m54 52h-44c-1.104 0-2 .896-2 2s.896 2 2 2h44c1.104 0 2-.896 2-2s-.896-2-2-2z\"/><path d=\"m46 23c1.104 0 2-.896 2-2s-.896-2-2-2h-28c-1.104 0-2 .896-2 2s.896 2 2 2z\"/><path d=\"m54 30h-44c-1.104 0-2 .896-2 2s.896 2 2 2h44c1.104 0 2-.896 2-2s-.896-2-2-2z\"/><path d=\"m46 45c1.104 0 2-.896 2-2s-.896-2-2-2h-28c-1.104 0-2 .896-2 2s.896 2 2 2z\"/></svg>",
        classes: ["justify-center"]
      },
      {
        name: "right",
        icon: "<svg xmlns=\"http://www.w3.org/2000/svg\" id=\"Layer\" enable-background=\"new 0 0 64 64\" height=\"20\" viewBox=\"0 0 64 64\" width=\"20\"><path d=\"m54 8h-44c-1.104 0-2 .896-2 2s.896 2 2 2h44c1.104 0 2-.896 2-2s-.896-2-2-2z\"/><path d=\"m54 52h-44c-1.104 0-2 .896-2 2s.896 2 2 2h44c1.104 0 2-.896 2-2s-.896-2-2-2z\"/><path d=\"m54 19h-28c-1.104 0-2 .896-2 2s.896 2 2 2h28c1.104 0 2-.896 2-2s-.896-2-2-2z\"/><path d=\"m54 30h-44c-1.104 0-2 .896-2 2s.896 2 2 2h44c1.104 0 2-.896 2-2s-.896-2-2-2z\"/><path d=\"m54 41h-28c-1.104 0-2 .896-2 2s.896 2 2 2h28c1.104 0 2-.896 2-2s-.896-2-2-2z\"/></svg>",
        classes: ["justify-end"]
      }
    ]
  }

  static get isReadOnlySupported() {
    return true;
  }

  render() {
    this.wrapper = document.createElement("hr")

    this.wrapper.classList.add(this.data.fullWidth ? "w-full" : "w-1/2")
    this.wrapper.classList.add("my-4", "flex", "items-center", "justify-center")
    this.wrapper.classList.add("h-0.5", "bg-middleGrey", "transition-all", "duration-200")
    return this.wrapper
  }

  renderSettings() {
    const wrapper = document.createElement("div")
    const alignWrapper = document.createElement("div")
    const fullWidthWrapper = document.createElement("div")

    wrapper.classList.add("flex", "flex-col", "items-start", "justify-start")
    wrapper.appendChild(alignWrapper)
    wrapper.appendChild(fullWidthWrapper)

    alignWrapper.classList.add("flex", "p-1", "gap-x-1")
    this.settings.map(setting => {
      const button = document.createElement("button")

      button.classList.add(this.api.styles.settingsButton, "transtion-all", "duration-300");
      button.innerHTML = setting.icon;
      button.type = 'button';
      button.classList.toggle("!bg-[#abc3dc]", setting.name === this.data.alignment);

      alignWrapper.appendChild(button)
      return button
    }).forEach((setting, index, buttons) => {
      setting.addEventListener("click", () => {
        this.wrapper.classList.remove("justify-start", "justify-center", "justify-end")
        this.data.alignment = this.settings[index].name
        this.wrapper.classList.add(...this.settings[index].classes)

        buttons.forEach((button, index) => {
          const { name } = this.settings[index]
          button.classList.toggle("!bg-[#abc3dc]", name === this.data.alignment)
        })
      })
    })

    const checkbox = document.createElement("input")
    checkbox.classList.add("cursor-pointer")

    const text = document.createElement("p")

    checkbox.type = "checkbox"
    checkbox.classList.add("w-4", "h-4", "m-1")
    text.classList.add("ce-popover-item__title")
    text.innerText = "Full width"

    if (this.data) {
      checkbox.checked = this.data.fullWidth
    }

    fullWidthWrapper.classList.add("items-center", "justify-start", "p-1", "gap-x-3", "ce-popover-item", "w-full")
    fullWidthWrapper.appendChild(checkbox)
    fullWidthWrapper.appendChild(text)
    fullWidthWrapper.addEventListener('click', () => {
      checkbox.checked = !this.data.fullWidth
      this.divider.classList.toggle("w-full")
      this.divider.classList.toggle("w-1/2")
      this.data.fullWidth = !this.data.fullWidth
    })

    checkbox.addEventListener('click', () => {
      checkbox.checked = !this.data.fullWidth
    })

    return wrapper
  }

  save(blockContent) {
    return this.data
  }
}

export default Divider
