import { lowlight } from 'lowlight';
import { CodeBlockLowlight } from '@tiptap/extension-code-block-lowlight';

export const CodeBlock = CodeBlockLowlight.configure({
  lowlight,
  defaultLanguage: null,
})

export default CodeBlock