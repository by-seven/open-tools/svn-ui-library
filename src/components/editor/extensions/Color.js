import TiptapColor from '@tiptap/extension-color';

export const Color = TiptapColor.configure({
  types: ['textStyle'],
})

export default Color