import emojiSuggestion from './EmojiSuggestion.js';
import TiptapEmoji from '@tiptap-pro/extension-emoji';

export const Emoji = TiptapEmoji.configure({
  enableEmoticons: true,
  suggestion: emojiSuggestion,
})

export default Emoji