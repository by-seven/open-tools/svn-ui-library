import tippy from 'tippy.js';
import { VueRenderer } from '@tiptap/vue-3';

import EmojiList from '../menu/EmojiList.vue';

export const emojiSuggestion = {
  items: ({ editor, query }) =>
    editor.storage.emoji.emojis
      .filter(({ shortcodes, tags }) =>
        shortcodes.find(shortcode => shortcode.startsWith(query.toLowerCase())) ||
        tags.find(tag => tag.startsWith(query.toLowerCase()))).slice(0, 250),
  allowSpaces: false,
  
  render: () => {
    let component;
    let popup;

    return {
      onStart: props => {
        component = new VueRenderer(EmojiList, {
          props,
          editor: props.editor,
        });

        popup = tippy('body', {
          getReferenceClientRect: props.clientRect,
          appendTo: () => document.body,
          content: component.element,
          showOnCreate: true,
          interactive: true,
          trigger: 'manual',
          placement: 'bottom-start',
        });
      },
      onUpdate: props => {
        component.updateProps(props);
        popup[0].setProps({
          getReferenceClientRect: props.clientRect,
        });
      },
      onKeyDown: props => {
        if (props.event.key === 'Escape') {
          popup[0].hide();
          component.destroy();
          return true;
        }
        return component.refs?.onKeyDown(props);
      },
      onExit: () => {
        popup[0].destroy();
        component.destroy();
      },
    };
  },
};

export default emojiSuggestion;
