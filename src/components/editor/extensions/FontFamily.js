import TipTapFontFamily from '@tiptap/extension-font-family';

export const FontFamily = TipTapFontFamily.configure({
  types: ['textStyle'],
})

export default FontFamily