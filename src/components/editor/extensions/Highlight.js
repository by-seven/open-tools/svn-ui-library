import HighlightTiptap from '@tiptap/extension-highlight';

export const Highlight = HighlightTiptap.configure({
  multicolor: true,
})

export default Highlight