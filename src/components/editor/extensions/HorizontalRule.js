import { mergeAttributes } from '@tiptap/core'
import { VueNodeViewRenderer } from '@tiptap/vue-3';
import Divider from '../divider/Divider.vue';
import TiptapHorizontalRule from '@tiptap/extension-horizontal-rule'

export const HorizontalRule = TiptapHorizontalRule.extend({
  renderHTML() {
    return ['div', mergeAttributes(this.options.HTMLAttributes, { 'data-type': this.name }), ['hr']]
  },
  addNodeView() {
    return VueNodeViewRenderer(Divider);
  },
})

export default HorizontalRule