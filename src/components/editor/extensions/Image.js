import { Image as TiptapImage } from '@tiptap/extension-image'

export const Image = TiptapImage.extend({
  group: 'block',
})

export default Image