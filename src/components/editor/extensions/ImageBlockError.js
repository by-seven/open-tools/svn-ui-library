import { VueNodeViewRenderer } from '@tiptap/vue-3';
import { mergeAttributes, Node } from '@tiptap/core';

import ImageBlockViewError from '../component/ImageBlockViewError.vue';

export const ImageBlockError = Node.create({
  name: 'imageBlockError',
  group: 'block',
  defining: true,
  isolating: true,

  addAttributes() {
    return {}
  },

  parseHTML() {
    return [
      {
        tag: 'img[src]:not([src^="data:"])',
      },
    ];
  },

  renderHTML({ HTMLAttributes }) {
    return ['div', mergeAttributes(this.options.HTMLAttributes, HTMLAttributes)];
  },

  addCommands() {
    return {
      setImageBlockError: (attrs) => ({ commands }) => {
        return commands.insertContent({ type: 'imageBlockError' });
      },

      setImageBlockErrorAt: (attrs) => ({ commands }) => {
        return commands.insertContentAt(attrs.pos, { type: 'imageBlockError' });
      },
    };
  },

  addNodeView() {
    return VueNodeViewRenderer(ImageBlockViewError);
  },
});

export default ImageBlockError;