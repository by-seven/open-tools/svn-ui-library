import { VueNodeViewRenderer } from '@tiptap/vue-3';
import { mergeAttributes } from '@tiptap/core';

import ImageBlockViewLoading from '../component/ImageBlockViewLoading.vue';
import { Image } from './Image.js';

export const ImageBlockLoading = Image.extend({
  name: 'imageBlockLoading',
  group: 'block',
  defining: true,
  isolating: true,

  addOptions() {
    return {
      inline: false,
      allowBase64: true,
      HTMLAttributes: {},
    }
  },

  addAttributes() {
    return {
      src: {
        default: '',
        parseHTML: element => element.getAttribute('src'),
        renderHTML: attributes => ({
          src: attributes.src,
        }),
      },
      progress: {
        default: 0,
        parseHTML: element => element.getAttribute('data-progress'),
        renderHTML: attributes => ({
          'data-progress': attributes.progress,
        }),
      },
    };
  },

  parseHTML() {
    return [
      {
        tag: 'img[src]:not([src^="data:"])',
      },
    ];
  },

  renderHTML({ HTMLAttributes }) {
    return ['div', mergeAttributes(this.options.HTMLAttributes, HTMLAttributes)];
  },

  addCommands() {
    return {
      setImageBlockLoading: (attrs) => ({ commands }) => {
        return commands.insertContent({ type: 'imageBlockLoading', attrs: { src: attrs.src, progress: attrs.progress } });
      },

      setImageBlockLoadingAt: (attrs) => ({ commands }) => {
        return commands.insertContentAt(attrs.pos, { type: 'imageBlockLoading', attrs: { src: attrs.src, progress: attrs.progress } });
      },
    };
  },

  addNodeView() {
    return VueNodeViewRenderer(ImageBlockViewLoading);
  },
});

export default ImageBlockLoading;