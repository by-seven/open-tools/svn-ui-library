import { Node, VueNodeViewRenderer } from '@tiptap/vue-3';
import ImageUploadComponent from '../component/ImageUpload.vue';

export const ImageUpload = Node.create({
  name: 'imageUpload',
  isolating: true,
  defining: true,
  group: 'block',
  draggable: true,
  selectable: true,
  inline: false,

  parseHTML() {
    return [
      { tag: `div[data-type="${this.name}"]` },
    ];
  },

  renderHTML() {
    return ['div', { 'data-type': this.name }];
  },

  addCommands() {
    return {
      setImageUpload: () => {
        return ({ commands }) => {
          commands.insertContent(`<div data-type="${this.name}"></div>`);
        };
      },
    };
  },

  addNodeView() {
    return VueNodeViewRenderer(ImageUploadComponent);
  },
});

export default ImageUpload;
