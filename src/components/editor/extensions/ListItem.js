import ListItemTiptap from '@tiptap/extension-list-item';

export const ListItem = ListItemTiptap.extend({
  group: 'rootblock'
})

export default ListItem