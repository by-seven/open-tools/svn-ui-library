import ParagraphTiptap from '@tiptap/extension-paragraph';

export const Paragraph = ParagraphTiptap.configure({
  HTMLAttributes: {
    class: 'svn-tiptap-paragraph',
  },
})

export default Paragraph