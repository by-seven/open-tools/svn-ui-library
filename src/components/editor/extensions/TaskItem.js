import TaskItemTiptap from '@tiptap/extension-task-item';

export const TaskItem = TaskItemTiptap.configure({
  nested: false,
})

export default TaskItem