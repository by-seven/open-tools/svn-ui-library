import TiptapTextAlign from '@tiptap/extension-text-align';

export const TextAlign = TiptapTextAlign.configure({
  types: ['heading', 'paragraph', 'table', 'bulletList', 'orderedList', 'taskList', 'youtube'],
})

export default TextAlign