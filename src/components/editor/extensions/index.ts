// List of Extensions
import Bold from './Bold.js';
export {Bold}
import Text from './Text.js';
export {Text}
import Link from './Link.js';
export {Link}
import Color from './Color.js';
export {Color}
import Emoji from './Emoji.js';
export {Emoji}
import Focus from './Focus.js';
export {Focus}
import Keymap from './Keymap.js';
export {Keymap}
import Strike from './Strike.js';
export {Strike}
import Italic from './Italic.js';
export {Italic}
import Youtube from './Youtube.js';
export {Youtube}
import History from './History.js';
export {History}
import Heading from './Heading.js';
export {Heading}
import TaskItem from './TaskItem.js';
export {TaskItem}
import TaskList from './TaskList.js';
export {TaskList}
import Document from './Document.js';
export {Document}
import FontSize from './FontSize.js';
export {FontSize}
import ListItem from './ListItem.js';
export {ListItem}
import TableRow from './Table/Row.js';
export {TableRow}
import HardBreak from './HardBreak.js';
export {HardBreak}
import Underline from './Underline.js';
export {Underline}
import TextStyle from './TextStyle.js';
export {TextStyle}
import Paragraph from './Paragraph.js';
export {Paragraph}
import CodeBlock from './CodeBlock.js';
export {CodeBlock}
import RootBlock from './RootBlock.js';
export {RootBlock}
import Selection from './Selection.js';
export {Selection}
import Highlight from './Highlight.js';
export {Highlight}
import TextAlign from './TextAlign.js';
export {TextAlign}
import Gapcursor from './Gapcursor.js';
export {Gapcursor}
import TableCell from './Table/Cell.js';
export {TableCell}
import Typography from './Typography.js';
export {Typography}
import Blockquote from './Blockquote.js';
export {Blockquote}
import FontFamily from './FontFamily.js';
export {FontFamily}
import ImageBlock from './ImageBlock.js';
export {ImageBlock}
import BulletList from './BulletList.js';
export {BulletList}
import Divider from './HorizontalRule.js';
export {Divider}
import OrderedList from './OrderedList.js';
export {OrderedList}
import Placeholder from './Placeholder.js';
export {Placeholder}
import TableTiptap from './Table/Table.js';
export {TableTiptap}
import ImageUpload from './ImageUpload.js';
export {ImageUpload}
import TableHeader from './Table/Header.js';
export {TableHeader}
import BaseDocument from './BaseDocument.js';
export {BaseDocument}
import TrailingNode from './TrailingNode.js';
export {TrailingNode}
import BaseListItem from './BaseListItem.js';
export {BaseListItem}
import SlashCommand from './SlashCommand.js';
export {SlashCommand}
import ImageBlockError from './ImageBlockError.js';
export {ImageBlockError}
import ImageBlockLoading from './ImageBlockLoading.js';
export {ImageBlockLoading}

export const CheckList = [TaskList, TaskItem]
export const Image = [ImageUpload, ImageBlock, ImageBlockError, ImageBlockLoading]
export const Table = [TableTiptap, TableHeader, TableCell, TableRow]
export const LeftMenu = [RootBlock, Document, TrailingNode, ListItem, Text, Paragraph, Gapcursor, TextStyle]

export const AllTipTapPlugins = [
  Text,
  Paragraph,
  Bold,
  Italic,
  Underline,
  Strike,
  Color,
  Highlight,
  Link,
  TextStyle,
  Keymap,
  History,
  Heading,
  Selection,
  Divider,
  Blockquote,
  Youtube,
  FontSize,
  Focus,
  FontFamily,
  TextAlign,
  Gapcursor,
  BulletList,
  OrderedList,
  CodeBlock,
  Typography,
  HardBreak,
  Document,
  BaseDocument,
  TrailingNode,
  BaseListItem,
  ListItem,
  Emoji,
  Placeholder,
  SlashCommand,
  TaskItem,
  RootBlock,
  LeftMenu,
  Image,
  CheckList,
  Table,
]