import { Node } from '@tiptap/pm/model'
import { NodeSelection } from '@tiptap/pm/state'
import { Editor } from '@tiptap/vue-3'

const useContentItemMenuActions = (editor, currentNode, currentNodePos) => {

  const resetTextFormatting = () => {
    const chain = editor.chain()

    chain.setNodeSelection(currentNodePos).unsetAllMarks()

    if (currentNode?.type.name !== 'paragraph') {
      chain.setParagraph()
    }

    chain.run()
  }

  const duplicateNode = () => {
    editor.commands.setNodeSelection(currentNodePos)

    const { $anchor } = editor.state.selection
    const selectedNode = $anchor.node(1) || (editor.state.selection).node

    editor
      .chain()
      .setMeta('hideDragHandle', true)
      .insertContentAt(currentNodePos + (currentNode?.nodeSize || 0), selectedNode.toJSON())
      .run()
  }

  const copyNodeToClipboard = () => {
    editor.chain().setMeta('hideDragHandle', true).setNodeSelection(currentNodePos).run()
    
    const copiedText = currentNode.textContent
    if (copiedText) {
      navigator.clipboard.writeText(copiedText)
    }
  }

  const deleteNode = () => {
    editor.chain().setMeta('hideDragHandle', true).setNodeSelection(currentNodePos).deleteSelection().run()
  }

  const handleAdd = () => {
    if (currentNodePos !== -1) {
      editor.chain().insertContentAt(currentNodePos, {
        type: "rootblock",
        content: [
          {
            type: "paragraph",
            attrs: {
              class: null,
              textAlign: "left"
            },
            content: [
              {
                text: "/",
                type: "text"
              }
            ]
          }
        ]
      }).focus(currentNodePos + 3).run()
    }
  }

  return {
    resetTextFormatting,
    duplicateNode,
    copyNodeToClipboard,
    deleteNode,
    handleAdd,
  }
}

export default useContentItemMenuActions