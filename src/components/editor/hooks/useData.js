import { Node } from '@tiptap/pm/model';
import { Editor } from '@tiptap/core';
import { ref, computed } from 'vue';

export const useData = () => {
  const currentNode = ref(null);
  const currentNodePos = ref(-1);

  const handleNodeChange = (data) => {
    if (data.node) {
      currentNode.value = data.node;
    }
    currentNodePos.value = data.pos;
  };

  return {
    currentNode: computed(() => currentNode.value),
    currentNodePos: computed(() => currentNodePos.value),
    setCurrentNode: (node) => {
      currentNode.value = node;
    },
    setCurrentNodePos: (pos) => {
      currentNodePos.value = pos;
    },
    handleNodeChange,
  };
};
