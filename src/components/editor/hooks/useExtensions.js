import {
  Document,
  Text,
  BaseDocument,
  Bold,
  Italic,
  LeftMenu,
  History,
  Paragraph,
  Heading,
  Link,
  Selection,
  Divider,
  Blockquote,
  Youtube,
  Strike,
  TextStyle,
  FontSize,
  Focus,
  FontFamily,
  Color,
  TextAlign,
  BaseListItem,
  Emoji,
  Image,
  BulletList,
  OrderedList,
  CodeBlock,
  Typography,
  Highlight,
  Underline,
  CheckList,
  Table,
  Gapcursor,
  Keymap,
  SlashCommand,
  HardBreak,
  Placeholder,
  TaskItem,
  RootBlock,
  TrailingNode,
  ListItem,
} from '../extensions'

const BaseExtensions = [
  Text,
  TextStyle,
  FontSize,
  FontFamily,
  Color,
  Highlight,
  Bold,
  Italic,
  Underline,
  Strike,
  Link,
  Paragraph,
  Keymap,
  History,
  Heading,
  Selection,
  Divider,
  Blockquote,
  Youtube,
  Focus,
  TextAlign,
  Gapcursor,
  BulletList,
  OrderedList,
  CodeBlock,
  Typography,
  HardBreak,
  ...Image,
  ...CheckList,
  ...Table,
]

const ExtensionsWithLeftMenu = [
  Document,
  RootBlock,
  TrailingNode,
  ...BaseExtensions,
  ListItem,
]

const ExtensionsWithoutLeftMenu = [
  BaseDocument,
  BaseListItem,
  ...BaseExtensions,
]

export const useExtensions = (leftMenu, slashCommand, extensionGlobal) => {
  let extensions = []

  extensions = leftMenu ? [...ExtensionsWithLeftMenu] : [...ExtensionsWithoutLeftMenu]

  if (slashCommand?.length) {
    extensions = [...extensions, Placeholder, SlashCommand]
  }

  if (extensionGlobal?.length) {
    extensions = [...extensions, ...extensionGlobal]
  }

  return extensions
}

export default useExtensions