export const useTextMenuCommands = (editor) => {
  const onBold = () => {
    editor.chain().focus().toggleBold().run()
  }

  const onItalic = () => {
    editor.chain().focus().toggleItalic().run()
  }

  const onUnderline = () => {
    editor.chain().focus().toggleUnderline().run()
  }

  const onStrike = () => {
    editor.chain().focus().toggleStrike().run()
  }

  const onAlignLeft = () => {
    editor.chain().focus().setTextAlign('left').run()
  }

  const onAlignCenter = () => {
    editor.chain().focus().setTextAlign('center').run()
  }

  const onAlignRight = () => {
    editor.chain().focus().setTextAlign('right').run()
  }

  const onAlignJustify = () => {
    editor.chain().focus().setTextAlign('justify').run()
  }

  const onLink = (url, inNewTab) => {
    editor.chain().focus().setLink({ href: url, target: inNewTab ? '_blank' : '' }).run()
  }

  const onSetFont = (font) => {
    if (!font || font.length === 0) {
      return editor.chain().focus().unsetFontFamily().run()
    }
    return editor.chain().focus().setFontFamily(font).run()
  }

  const onSetFontSize = (fontSize) => {
    if (!fontSize || fontSize.length === 0) {
      return editor.chain().focus().setMark('textStyle', { fontSize: null }).removeEmptyTextStyle().run()
    }
    return editor.chain().focus().setMark('textStyle', { fontSize }).run()
  }

  const onChangeHighlight = (color) => {
    editor.chain().setHighlight({ color }).run()
  }

  const onClearHighlight = () => {
    editor.chain().focus().unsetHighlight().run()
  }

  const onChangeColor = (color) => {
    editor.chain().setColor(color).run()
  }

  const onClearColor = () => {
    editor.chain().focus().unsetColor().run()
  }

  return {
    onBold,
    onItalic,
    onUnderline,
    onStrike,
    onSetFont,
    onSetFontSize,
    onAlignLeft,
    onAlignCenter,
    onAlignRight,
    onAlignJustify,
    onLink,
    onChangeHighlight,
    onClearHighlight,
    onChangeColor,
    onClearColor
  }
}

export default useTextMenuCommands