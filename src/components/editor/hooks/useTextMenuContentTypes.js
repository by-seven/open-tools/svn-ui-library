export const useTextMenuContentTypes = (editor) => {
  return [
    {
      type: 'category',
      title: 'HIERARCHY',
      id: 'hierarchy',
    },
    {
      icon: 'ph:paragraph',
      title: 'Paragraph',
      id: 'paragraph',
      disabled: () => !editor.can().setParagraph(),
      isActive: () => 
        editor.isActive('paragraph') &&
        editor.isActive('paragraph') &&
        !editor.isActive('orderedList') &&
        !editor.isActive('bulletList') &&
        !editor.isActive('taskList'),
      onClick: () => editor.chain().focus().setParagraph().run(),
      type: 'option',
    },
    {
      icon: 'icon-park-outline:h1',
      title: 'Heading 1',
      id: 'heading1',
      disabled: () => !editor.can().setHeading({ level: 1 }),
      isActive: () => editor.isActive('heading', { level: 1 }),
      onClick: () => editor.chain().focus().setHeading({ level: 1 }).run(),
      type: 'option',
    },
    {
      icon: 'icon-park-outline:h2',
      title: 'Heading 2',
      id: 'heading2',
      disabled: () => !editor.can().setHeading({ level: 2 }),
      isActive: () => editor.isActive('heading', { level: 2 }),
      onClick: () => editor.chain().focus().setHeading({ level: 2 }).run(),
      type: 'option',
    },
    {
      icon: 'icon-park-outline:h3',
      title: 'Heading 3',
      id: 'heading3',
      disabled: () => !editor.can().setHeading({ level: 3 }),
      isActive: () => editor.isActive('heading', { level: 3 }),
      onClick: () => editor.chain().focus().setHeading({ level: 3 }).run(),
      type: 'option',
    },
    {
      type: 'category',
      title: 'LISTS',
      id: 'lists',
    },
    {
      icon: 'gravity-ui:list-ul',
      title: 'Bulleted list',
      id: 'bulletList',
      disabled: () => !editor.can().toggleBulletList(),
      isActive: () => editor.isActive('bulletList'),
      onClick: () => editor.chain().focus().toggleBulletList().run(),
      type: 'option',
    },
    {
      icon: 'gravity-ui:list-ol',
      title: 'Numbered list',
      id: 'orderedList',
      disabled: () => !editor.can().toggleOrderedList(),
      isActive: () => editor.isActive('orderedList'),
      onClick: () => editor.chain().focus().toggleOrderedList().run(),
      type: 'option',
    },
    {
      icon: 'lucide:list-todo',
      title: 'Todo list',
      id: 'todoList',
      disabled: () => !editor.can().toggleTaskList(),
      isActive: () => editor.isActive('taskList'),
      onClick: () => editor.chain().focus().toggleTaskList().run(),
      type: 'option',
    },
  ]
}