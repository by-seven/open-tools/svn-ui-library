import { isTextSelected } from '../utils/isTextSelected.js';
import { isCustomNodeSelected } from '../utils/isCustomNodeSelected.js';

export const useTextStates = (editor) => {
  const shouldShow = ({ view, from }) => {
    if (!view) {
      return false
    }
    
    const domAtPos = view.domAtPos(from || 0).node
    const nodeDOM = view.nodeDOM(from || 0)
    const node = nodeDOM || domAtPos
    const columnMenu = document.getElementById('tableColumnMenu')
    const rowMenu = document.getElementById('tableRowMenu')

    if (isCustomNodeSelected(editor, node) || columnMenu || rowMenu) {
      return false
    }

    return isTextSelected({ editor })
  }

  // const shouldShowMenu = () => {
  //   const columnMenu = document.getElementById('tableColumnMenu')
  //   const rowMenu = document.getElementById('tableRowMenu')

  //   if (columnMenu || rowMenu) {
  //     return false
  //   }
  //   return true
  // }

  return {
    isBold: editor.isActive('bold'),
    isItalic: editor.isActive('italic'),
    isStrike: editor.isActive('strike'),
    isUnderline: editor.isActive('underline'),
    isCode: editor.isActive('code'),
    isSubscript: editor.isActive('subscript'),
    isSuperscript: editor.isActive('superscript'),
    isAlignLeft: editor.isActive({ textAlign: 'left' }),
    isAlignCenter: editor.isActive({ textAlign: 'center' }),
    isAlignRight: editor.isActive({ textAlign: 'right' }),
    isAlignJustify: editor.isActive({ textAlign: 'justify' }),
    currentColor: editor.getAttributes('textStyle')?.color || null,
    currentHighlight: editor.getAttributes('highlight')?.color || null,
    currentFont: editor.getAttributes('textStyle')?.fontFamily || 'Work Sans',
    currentSize: editor.getAttributes('textStyle')?.fontSize || '16px',
    shouldShow,
    // shouldShowMenu,
  }
}

export default useTextStates