class Spacer {
  static get toolbox() {
    return {
      name: "Spacer",
      icon: "<svg xmlns=\"http://www.w3.org/2000/svg\" width=\"14\" height=\"14\" viewBox=\"0 0 14 14\"><path fill=\"none\" stroke=\"currentColor\" stroke-linecap=\"round\" stroke-linejoin=\"round\" d=\"M7 11.5v-9M9.5 5L7 2.5L4.5 5m5 4L7 11.5L4.5 9m9-8.5H.5m13 13H.5\"/></svg>"
    }
  }

  constructor({ api, readOnly, data }) {
    this.api = api
    this.readOnly = readOnly
    this.data = { lines: data.lines || 1 }
    this.text = undefined
    this.current = undefined
  }

  // static get shortcut() {
  //   return 'CTRL+SHIFT+ENTER';
  // }

  static get isReadOnlySupported() {
    return true;
  }

  render() {
    this.text = document.createElement("p")

    this.text.innerText = ""
    for (let i = 0; i < this.data.lines; ++i) {
      this.text.innerText += "\n"
    }

    return this.text
  }

  renderSettings() {
    const wrapper = document.createElement("div")
    const selectedValue = document.createElement("p")
    const selectIcon = document.createElement("div")
    const dropdown = document.createElement("div")
    const text = document.createElement("p")
    const options = [
      { title: "Small", lines: 1 },
      { title: "Medium", lines: 3 },
      { title: "Large", lines: 5 },
    ]

    selectedValue.innerText = options.find(item => item.lines === this.data.lines).title
    selectedValue.classList.add("ml-2")
    dropdown.classList.add('hidden', "absolute", "top-8", "left-1/2", "-translate-x-1/2", "bg-white", "px-2", "border")
    options.map((o) => {
      const option = document.createElement("p")

      option.innerText = o.title
      option.classList.add(this.api.styles.settingsButton, "w-full", "self-start")
      option.classList.toggle(this.api.styles.settingsButtonActive, o.lines === this.data.lines)

      dropdown.appendChild(option)
      return option
    }).forEach((button, index, buttons) => {
      button.addEventListener('click', () => {
        buttons.forEach((el) => {
          el.classList.toggle(this.api.styles.settingsButtonActive, el.innerText === button.innerText)
        })
        selectedValue.innerText = button.innerText
        this.data.lines = options[index].lines
        this.text.innerText = ""
        for (let i = 0; i < this.data.lines; ++i) {
          this.text.innerText += "\n"
        }
      })
    })
    text.classList.add("ce-popover-item__title")
    text.innerText = "Size: "

    selectIcon.classList.add("w-4", "h-4", "transition-all", "duration-200")
    selectIcon.innerHTML = "<svg xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 24 24\"><title>chevron-down</title><path d=\"M7.41,8.58L12,13.17L16.59,8.58L18,10L12,16L6,10L7.41,8.58Z\" /></svg>"

    wrapper.classList.add("items-center", "justify-start", "p-1", "gap-x-3", "ce-popover-item", "w-full", "cursor-pointer", "relative")
    wrapper.appendChild(text)
    wrapper.appendChild(dropdown)
    wrapper.appendChild(selectedValue)
    wrapper.appendChild(selectIcon)
    wrapper.addEventListener('click', () => {
      dropdown.classList.toggle("hidden")
      dropdown.classList.toggle("block")
      selectIcon.classList.toggle("-rotate-180")
    })

    return wrapper
  }

  save(blockContent) {
    return this.data
  }
}

export default Spacer