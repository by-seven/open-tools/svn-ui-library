const ALLOWED_SLASH_COMMMANDS = [
  'heading1',
  'heading2',
  'heading3',
  'bulletList',
  'orderedList',
  'taskList',
  'blockquote',
  'codeBlock',
  'table',
  'imageUpload',
  'youtube',
  'horizontalRule',
]

const ALLOWED_SELECTION_COMMANDS = [
  'paragraph',
  'heading1',
  'heading2',
  'heading3',
  'bulletList',
  'orderedList',
  'taskList',
  'bold',
  'italic',
  'underline',
  'strike',
  'textAlign',
  'link',
  'color',
  'highlight',
  'fontFamily',
  'fontSize',
  'fontSize',
]

export {
  ALLOWED_SLASH_COMMMANDS,
  ALLOWED_SELECTION_COMMANDS,
}