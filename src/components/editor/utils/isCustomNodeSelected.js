import Link from "@tiptap/extension-link";
import Table from "../extensions/Table/Table.js";
import CodeBlock from "../extensions/CodeBlock.js";
import ImageBlock from "../extensions/ImageBlock.js";
import ImageUpload from "../extensions/ImageUpload.js";
import HorizontalRule from '.././extensions/HorizontalRule.js';
import { CodeBlockLowlight } from '@tiptap/extension-code-block-lowlight';

export const isCustomNodeSelected = (editor, node) => {
  const customNodes = [
    Link.name,
    CodeBlock.name,
    ImageBlock.name,
    ImageUpload.name,
    HorizontalRule.name,
    CodeBlockLowlight.name,
    // Table.name,
  ]

  return customNodes.some(type => editor.isActive(type))
}

export default isCustomNodeSelected