import Paragraph from "@tiptap/extension-paragraph";

export const isParagraphActive = (editor) => {
  const customNodes = [
    Paragraph.name
  ]

  return customNodes.some(type => editor.isActive(type))
}

export default isParagraphActive