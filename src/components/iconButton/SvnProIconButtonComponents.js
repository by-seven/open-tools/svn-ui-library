import { createSvnProIconButtonComponent } from '../../factories/iconButton.factory';

export const SvnProIconButtonSave = createSvnProIconButtonComponent({
  icon: 'mdi-content-save',
  style: 'filled',
  density: 'compact',
  ariaLabel: 'Save',
  state: 'enabled',
});

export const SvnProIconButtonDelete = createSvnProIconButtonComponent({
  icon: 'mdi-delete',
  style: 'standard',
  density: 'default',
  ariaLabel: 'Delete',
});

export const SvnProIconButtonEdit = createSvnProIconButtonComponent({
  icon: 'mdi-pencil',
  style: 'tonal',
  density: 'comfortable',
  ariaLabel: 'Edit',
});

export const SvnProIconButtonCardDelete = createSvnProIconButtonComponent(
  {
    icon: 'custom:mingcute:delete-2-line',
    variant: 'standard',
    enable: true,
    density: 'default',
    ariaLabel: 'Delete',
  },
  {
    name: 'SvnProIconButtonCardDelete',
  }
);

export const SvnProIconButtonLoading = createSvnProIconButtonComponent({
  icon: 'mdi-sync',
  style: 'elevated',
  density: 'default',
  loading: true,
  ariaLabel: 'Loading',
});

export const SvnProIconButtonDisabled = createSvnProIconButtonComponent({
  icon: 'mdi-block-helper',
  style: 'standard',
  density: 'compact',
  disabled: true, // Disabled state
  ariaLabel: 'Disabled',
});
