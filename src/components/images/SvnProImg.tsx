import { defineComponent } from 'vue';
import { useRender } from '@/tools/useRender.js';

export default defineComponent({
  name: 'SvnProImg',
  props: {
    src: {
      type: String,
      required: true,
    },
    cover: {
      type: Boolean,
      default: false,
    },
  },
  setup(props) {
    return useRender(() => {
      return (
        <v-img className={'w-full h-full'} src={props.src} cover={props.cover}>
          {{
            placeholder: () => (
              <div
                className={
                  'd-flex align-center justify-center fill-height bg-neutral-100'
                }
              >
                <svn-loader loading-size="lg" />
              </div>
            ),
            error: () => (
              <div
                className={
                  'w-full h-full flex flex-col justify-center items-center'
                }
              >
                <p className={'sm medium text-center text-onSurfaceVariant'}>
                  Link expired
                </p>
              </div>
            ),
          }}
        </v-img>
      );
    });
  },
});
