import SvnFloatingActionButton from '../components/SvnFloatingActionButton.vue';
import SvnDotsButton from '../components/SvnDotsButton.vue';
import SvnDialogSkeleton from '../components/skeleton/SvnDialogSkeleton.vue';
import SvnButton from '../components/button/SvnButton.vue';
import SvnIconButton from '../components/button/SvnIconButton.vue';
import SvnLoader from '../components/animation/SvnLoader.vue';
import SvnTooltip from '../components/SvnTooltip.vue';
import SvnTimeline from '../components/SvnTimeline.vue';
import SvnModalManagePeople from '../components/dialog/SvnModalManagePeople.vue';
import SvnAutoCompleteDropdown from '../components/SvnAutoCompleteDropdown.vue';
import SvnTitle from '../components/styles/SvnTitle.vue';
import SvnText from '../components/styles/SvnText.vue';
import SvnAvatar from '../components/avatar/SvnAvatar.vue';
import SvnCenterContent from '../components/SvnCenterContent.vue';
import SvnProfile from '../components/profile/SvnProfile.vue';
import SvnProfileTableCell from '../components/profile/SvnProfileTableCell.vue';
import SvnAutocomplete from '../components/input/SvnAutocomplete.vue';
import SvnIndexSkeleton from '../components/skeleton/SvnIndexSkeleton.vue';
import SvnStudioTrainingCard from '../components/card/SvnStudioTrainingCard.vue';
import SvnStudioTrainerList from '../components/list/SvnStudioTrainerList.vue';
import SvnNavbarMenu from '../components/navbar/SvnNavbarMenu.vue';
import SvnModalSkeleton from '../components/skeleton/SvnModalSkeleton.vue';
import SvnDropdown from '../components/button/SvnDropdown.vue';
import SvnDatePicker from '../components/SvnDatePicker.vue';
import SvnNavbar from '../components/navbar/SvnNavbar.vue';
import SvnLoginCard from '../components/login/SvnLoginCard.vue';
import SvnStatusChip from '../components/chip/SvnStatusChip.vue';
import SvnChip from '../components/chip/SvnChip.vue';
import SvnSwitch from '../components/input/SvnSwitch.vue';
import SvnPagination from '../components/pagination/SvnPagination.vue';
import SvnDataTable from '../components/table/SvnDataTable.vue';
import SvnTagsCell from '../components/table_cell/SvnTagsCell.vue';
import SvnHeaderCell from '../components/table_cell/SvnHeaderCell.vue';
import SvnUpdateCell from '../components/table_cell/SvnUpdateCell.vue';
import SvnProfileCell from '../components/table_cell/SvnProfileCell.vue';
import SvnDropdownCell from '../components/table_cell/SvnDropdownCell.vue';
import SvnStatusCell from '../components/table_cell/SvnStatusCell.vue';
import SvnFilter from '../components/filters/SvnFilter.vue';
import SvnHelloBanner from '../components/banner/SvnHelloBanner.vue';
import SvnEditor from '../components/editor/SvnEditor.vue';
import SvnTiptap from '../components/editor/SvnTiptap.vue';
import SvnAlephNavbar from '../components/navbar/SvnAlephNavbar.vue';
import SvnAlephNavbarMenu from '../components/navbar/SvnAlephNavbarMenu.vue';
import SvnHeaderChip from '../components/chip/SvnHeaderChip.vue';

export default {
  SvnFloatingActionButton,
  SvnDotsButton,
  SvnDialogSkeleton,
  SvnButton,
  SvnIconButton,
  SvnLoader,
  SvnTooltip,
  SvnTimeline,
  SvnModalManagePeople,
  SvnAutoCompleteDropdown,
  SvnTitle,
  SvnText,
  SvnAvatar,
  SvnCenterContent,
  SvnProfile,
  SvnProfileTableCell,
  SvnAutocomplete,
  SvnIndexSkeleton,
  SvnStudioTrainingCard,
  SvnStudioTrainerList,
  SvnNavbarMenu,
  SvnModalSkeleton,
  SvnDropdown,
  SvnDatePicker,
  SvnNavbar,
  SvnLoginCard,
  SvnStatusChip,
  SvnChip,
  SvnSwitch,
  SvnPagination,
  SvnDataTable,
  SvnTagsCell,
  SvnHeaderCell,
  SvnUpdateCell,
  SvnProfileCell,
  SvnDropdownCell,
  SvnStatusCell,
  SvnFilter,
  SvnHelloBanner,
  SvnEditor,
  SvnTiptap,
  SvnAlephNavbar,
  SvnAlephNavbarMenu,
  SvnHeaderChip,
};
