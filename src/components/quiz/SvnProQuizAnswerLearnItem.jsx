import { capitalize, defineComponent } from 'vue';
import SvnProCard from '@/components/card/SvnProCard.vue';
import SvnProInfoTag from '@/components/chip/SvnProInfoTag.vue';
import SvnProText from '@/components/styles/SvnProText.vue';

export const SvnProQuizAnswerLearnItemStates = Object.freeze({
  partiallyCorrect: 'partiallyCorrect',
  wrong: 'wrong',
  correct: 'correct',
  enable: 'enable',
  selected: 'selected',
});

const StatesChildAttr = {
  partiallyCorrect: {
    tagType: 'warning',
    txtColor: 'warning',
  },
  wrong: {
    tagType: 'error',
    txtColor: 'error',
  },
  correct: {
    tagType: 'success',
    txtColor: 'success',
  },
  enable: {
    tagType: 'info',
    txtColor: 'onSurface',
  },
  selected: {
    tagType: 'primary',
    txtColor: 'primary',
  },
};

export default defineComponent({
  name: 'SvnProQuizAnswerLearnItem',
  props: {
    leftLetter: String,
    optionText: String,
    state: {
      type: String,
      default: () => {
        return SvnProQuizAnswerLearnItemStates.partiallyCorrect;
      },
    },
    link: {
      type: Boolean,
      default: () => {
        return true;
      },
    },
  },
  render(props) {
    if (
      Object.values(SvnProQuizAnswerLearnItemStates).includes(props.state) ===
      false
    ) {
      throw 'state not permitted SvnProQuizAnswerLearnItemStates';
    }

    return (
      <SvnProCard
        class={'px-4 py-4 flex flex-row items-center w-full'}
        link={props.link}
      >
        <div className={'pr-4'}>
          <SvnProInfoTag
            text={capitalize(props?.leftLetter?.[0])}
            tagType={StatesChildAttr[props.state].tagType}
            tagStyle={'filled'}
            tagSize={'default'}
            leadingItem={null}
          />
        </div>
        <SvnProText
          bodyLarge={true}
          regular={true}
          color={StatesChildAttr[props.state].txtColor}
        >
          {props?.optionText || ''}
        </SvnProText>
      </SvnProCard>
    );
  },
});
