import { defineComponent } from 'vue';
import SvnProCard from '@/components/card/SvnProCard.vue';
import SvnProText from '@/components/styles/SvnProText.vue';
import SvnProTitle from '@/components/styles/SvnProTitle.vue';
import SvnProQuizAnswerLearnItem from '@/components/quiz/SvnProQuizAnswerLearnItem.jsx';
import { useRender } from '@/tools/useRender.js';

export const SvnProSectionQuizLearnCorrectionType = Object.freeze({
  wrongAnswer: 'wrongAnswer',
  partiallyRight: 'partiallyRight',
  rightAnswer: 'rightAnswer',
  none: 'none',
});

const mapperCorrectionType = {
  wrongAnswer: {
    color: 'error',
    headText: 'Wrong answer',
  },
  partiallyRight: {
    color: 'warning',
    headText: 'Partially right',
  },
  rightAnswer: {
    color: 'success',
    headText: 'Right answer',
  },
  none: null,
};

export default defineComponent({
  name: 'SvnProSectionQuizLearnBase',
  props: {
    correctionType: {
      type: String,
      default() {
        return SvnProSectionQuizLearnCorrectionType.none;
      },
      validator(value, props) {
        return Object.values(SvnProSectionQuizLearnCorrectionType).includes(
          value
        );
      },
    },
    title: {
      type: String,
    },
    list: {
      type: Array,
      default() {
        return [];
      },
    },
  },
  setup(props, { slots }) {
    useRender(() => {
      return (
        <SvnProCard class={'w-full'} link={false}>
          <div className={'px-6 py-6 flex flex-col w-full'}>
            {props.correctionType !==
              SvnProSectionQuizLearnCorrectionType.none && (
              <div className={'pb-2'}>
                <SvnProText
                  subtitleLarge
                  medium
                  color={mapperCorrectionType[props.correctionType].color}
                >
                  {mapperCorrectionType[props.correctionType].headText}
                </SvnProText>
              </div>
            )}

            <div className={'pb-8'}>
              <SvnProTitle h5 medium color={'onSurface'}>
                {props.title || ''}
              </SvnProTitle>
            </div>

            <div className={'space-y-4'}>
              {props.list.map((item, index) => (
                <div key={index}>{slots.default?.({ item, index })}</div>
              ))}
            </div>
          </div>
        </SvnProCard>
      );
    });
  },
});
