import { defineComponent } from 'vue';
import { useRender } from '../../../tools/useRender';
import SvnProCard from '../../card/SvnProCard.vue';

export default defineComponent({
  name: 'SvnProSectionQuizLearnSkeleton',
  setup(props, { slots }) {
    useRender(() => {
      return (
        <SvnProCard class={'w-full'} link={false}>
          <div className={'px-6 py-6 flex flex-col w-full'}>
            <div className={'pb-8'}>
              <v-skeleton-loader type="text"></v-skeleton-loader>
            </div>

            <div className={'space-y-4'}>
              <v-skeleton-loader type="text"></v-skeleton-loader>
              <v-skeleton-loader type="text"></v-skeleton-loader>
              <v-skeleton-loader type="text"></v-skeleton-loader>
            </div>
          </div>
        </SvnProCard>
      );
    });
  },
});
