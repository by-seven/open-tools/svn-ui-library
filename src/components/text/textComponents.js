import {
  createSvnProText,
  TextSize,
  TextWeight,
} from '@/factories/text.factory';

// Instance pour Subtitle1 avec Medium weight
export const SvnProSubtitle1Medium = createSvnProText(
  {
    size: TextSize.SUBTITLE1,
    weight: TextWeight.MEDIUM,
    color: 'onSurface',
  },
  {
    name: 'SvnProSubtitle1Medium',
  }
);

// Instance pour Body text Medium
export const SvnProBodyText = createSvnProText(
  {
    size: TextSize.BODY2,
    weight: TextWeight.REGULAR,
    color: 'onSurface',
  },
  {
    name: 'SvnProBodyText',
  }
);

// Instance pour Caption text (date avec onSurfaceVariant color)
export const SvnProCaptionDateText = createSvnProText(
  {
    size: TextSize.CAPTION,
    weight: TextWeight.REGULAR,
    color: 'onSurfaceVariant',
  },
  {
    name: 'SvnProCaptionDateText',
  }
);

// Instance pour Caption text (souligne un texte avec Regular weight)
export const SvnProCaptionText = createSvnProText({
  size: TextSize.CAPTION,
  weight: TextWeight.REGULAR,
  color: 'onSurface',
});

// Instance pour Overline (petit texte en gras)
export const SvnProOverlineText = createSvnProText({
  size: TextSize.OVERLINE,
  weight: TextWeight.BOLD,
  color: 'onSurface',
});

// Test text avec Body1 et Bold
export const SvnProTest = createSvnProText({
  text: 'Texte en Body1, Bold',
  size: TextSize.BODY1,
  weight: TextWeight.BOLD,
  color: 'primary',
});
