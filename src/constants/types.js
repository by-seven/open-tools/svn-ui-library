export const TopBarLearnModuleTypes = Object.freeze({
  draft: 'draft',
  show: 'show',
  edit: 'edit',
  quiz: 'quiz',
  view_mode: 'view_mode',
  face_to_face: 'face_to_face',
});

export const TopBarLearnModuleSaveTypes = Object.freeze({
  manual: 'manual',
  auto: 'auto',
});

export const ComponentSizes = Object.freeze({
  compact: 'compact',
  default: 'default',
});

export const CardInterviewTargetTypes = Object.freeze({
  create: 'create',
  create_proposal: 'create_proposal',
  update_show: 'update_show',
});

export const ButtonVariants = Object.freeze({
  elevated: 'elevated',
  flat: 'flat',
  text: 'text',
  tonal: 'tonal',
  outlined: 'outlined',
  plain: 'plain',
});

export const InfotagStyles = Object.freeze({
  text: 'text',
  filled: 'filled',
  outlined: 'outlined',
});

export const InfotagTypes = Object.freeze({
  info: 'info',
  error: 'error',
  success: 'success',
  warning: 'warning',
  primary: 'primary',
});

export const InputVariants = Object.freeze({
  solo: 'solo',
  plain: 'plain',
  outlined: 'outlined',
  underlined: 'underlined',
  solo_filled: 'solo-filled',
  solo_inverted: 'solo-inverted',
});

export const TargetIndicatorStatuses = Object.freeze({
  completed: 'completed',
  in_progress: 'in_progress',
  uncompleted: 'uncompleted',
});

export const TargetObjectiveIndicatorTypes = Object.freeze({
  rating: 'rating',
  boolean: 'boolean',
  numeric: 'numeric_value',
  percentage: 'percentage',
  multi_choice: 'multi_choice',
});

export const InterviewCompletionStatuses = Object.freeze({
  submitted: 'submitted',
  in_progress: 'in_progress',
  not_started: 'not_started',
  not_available: 'not_available',
});

export const InterviewAppQuestionTypes = Object.freeze({
  chapter: 'InterviewApp::Questions::Chapter',
  paragraph: 'InterviewApp::Questions::Paragraph',
  open: 'InterviewApp::Questions::Open',
  rating: 'InterviewApp::Questions::Rating',
  mcq: 'InterviewApp::Questions::Mcq',
  roadmap_create: 'InterviewApp::Questions::CreateRoadmap',
  roadmap_update: 'InterviewApp::Questions::UpdateRoadmap',
});

export const FormBlockTypes = Object.freeze({
  icon_buttons: 'icon buttons',
  text_area: 'Text area',
  autocomplete: 'Autocomplete',
  radio_buttons: 'Radio buttons',
})

export const InterviewAppTemplateTypes = Object.freeze({
  employee: 'interviewee',
  manager: 'interviewer',
  both: 'both',
  cross: 'cross',
});

export const EvaluationTypes = Object.freeze({
  SELF_ASSESSMENT: 'SelfAssessment',
  QUIZ: 'Quiz',
  FACE_TO_FACE: 'FaceToFace',
});

export const chartTypes = Object.freeze({
  doughnut: 'doughnut',
  bar: 'bar',
  line: 'line',
});

export const LearnInputType = {
  CHECKBOX: 'Learn::Inputs::CheckboxQuestion',
  CONFIRMATION: 'Learn::Inputs::Confirmation',
  OPEN: 'Learn::Inputs::OpenQuestion',
  PARAGRAPH: 'Learn::Inputs::Paragraph',
  SIMPLE_TEXT: 'Learn::Inputs::SimpleText',
};

export const DataTableVariant = Object.freeze({
  EVERYONE: 'everyone',
  MANUAL_SELECTION: 'manual-selection',
  MANUAL_DESELECTION: 'manual-deselection',
  HIDDEN_SELECTION: 'hidden-selection',
});

export const FilterOperatorOptions = Object.freeze({
  BEFORE: "before",
  AFTER: "after",
  EQUAL: "equal",
  NOT_EQUAL: "not_equal",
  GREATER_THAN: "greater_than",
  LESS_THAN: "less_than",
  CONTAINS: "contains",
});

export const FilterType = Object.freeze({
  NUMBER: "number",
  DATE: "date",
  MULTI_SELECT: "multi_select",
  TEXT: "text",
})

export const SizeEnum = Object.freeze({
  X_SMALL: "x-small",
  SMALL: "small",
  DEFAULT: "default",
  LARGE: "large",
  X_LARGE: "x-large"
});

export const ContainerVariants = Object.freeze({
  WIDE: 'wide',
  NARROW: 'narrow',
});

export const FilterAutocompleteProperties = Object.freeze({
  FULLNAME: 'fullname',
  FIRSTNAME: 'firstname',
  NAME: 'name',
  TITLE: 'title',
  LABEL: 'label',
  TEXT: 'text',
});

export const ComponentDensities = Object.freeze({
  DEFAULT: 'default',
  COMPACT: 'compact',
  COMFORTABLE: 'comfortable',
});

export const ComponentBorderRadiuses = Object.freeze({
  ZERO: '0',
  XS: 'xs',
  SM: 'sm',
  TRUE: 'true',
  LG: 'lg',
  XL: 'xl',
  PILL: 'pill',
  CIRCLE: 'circle',
  SHAPED: 'shaped',
});

export const ComponentBorders = Object.freeze({
  FALSE: 'false',
  XS: 'xs',
  SM: 'sm',
  MD: 'md',
  LG: 'lg',
  XL: 'xl',
});

export const TabAlignTypes = Object.freeze({
  TITLE: 'title',
  START: 'start',
  END: 'end',
  CENTER: 'center',
});

export const SvnProCardSizes = Object.freeze({
  DEFAULT: 'default',
  COMPACT_STACKED: 'compact-stacked',
  COMPACT_SIDE: 'compact-side',
});

export const ViewTypes = Object.freeze({
  GRID: 'grid',
  LIST: 'list',
});