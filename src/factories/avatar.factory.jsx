import anonymImage from '../assets/anonym.svg';
import { Icon } from '@iconify/vue';
import checkImageStatus from '../tools/imageStatus';
import { defineComponent } from 'vue';

export const AvatarType = Object.freeze({
  PHOTO: 'photo',
  MONOGRAM: 'monogram',
  ANONYM: 'anonym',
  CHECK: 'check',
});

export const AvatarSize = Object.freeze({
  SMALL: 32,
  MEDIUM: 64,
  LARGE: 88,
  EXTRA_LARGE: 120,
});

export const createSvnProAvatar = (defaultProps = {}, componentOptions = {}) =>
  defineComponent({
    name: componentOptions.name,
    props: {
      type: {
        type: String,
        default: defaultProps.type || AvatarType.MONOGRAM,
        validator: (value) => Object.values(AvatarType).includes(value),
      },
      image: {
        type: String,
        default: defaultProps.image || undefined,
      },
      anonym: {
        type: String,
        default: anonymImage,
      },
      firstname: {
        type: String,
        default: defaultProps.firstname || '',
      },
      size: {
        type: Number,
        default: defaultProps.size || AvatarSize.MEDIUM,
        validator: (value) =>
          Object.values(AvatarSize).includes(value) || value > 0,
      },
      start: {
        type: Boolean,
        default: defaultProps.start || false,
      },
    },
    data() {
      return {
        displayImage: false,
      };
    },
    mounted() {
      this.checkImage();
    },
    methods: {
      checkImage() {
        if (!this.image) {
          this.displayImage = false;
          return;
        }
        checkImageStatus(this.image, (exists) => {
          this.displayImage = exists;
        });
      },
      getColor() {
        const startingLetter = this.firstname?.substring(0, 1)?.toUpperCase();
        if (!startingLetter) return 'avatarBg8';

        const ranges = {
          avatarBg1: 'ABC',
          avatarBg2: 'DEF',
          avatarBg3: 'GHI',
          avatarBg4: 'JKL',
          avatarBg5: 'MNO',
          avatarBg6: 'PQR',
          avatarBg7: 'STUV',
          avatarBg8: 'WXYZ',
        };

        for (const [bgClass, letters] of Object.entries(ranges)) {
          if (letters.includes(startingLetter)) return bgClass;
        }

        return 'avatarBg8';
      },
      getFontSize() {
        const avatarSize = this.size;
        const sizeMapping = {
          16: 'text-[10px]',
          20: 'text-xs',
          24: 'text-xs',
          32: 'text-sm',
          40: 'text-base',
          48: 'text-lg',
          56: 'text-xl',
          64: 'text-2xl',
          72: 'text-3xl',
          88: 'text-[34px]',
          96: 'text-4xl',
          120: 'text-[40px]',
        };
        return sizeMapping[avatarSize] || 'text-base';
      },
    },
    watch: {
      image(newImage) {
        this.checkImage();
      },
    },
    render() {
      return (
        <>
          {this.type === AvatarType.PHOTO && this.displayImage && (
            <v-avatar image={this.image} size={this.size} start={this.start} />
          )}

          {this.type === AvatarType.ANONYM && (
            <v-avatar image={this.anonym} size={this.size} start={this.start} />
          )}

          {this.type === AvatarType.CHECK && (
            <v-avatar
              size={this.size}
              start={this.start}
              rounded="circle"
              color="primary"
            >
              <Icon
                class="text-onPrimary"
                width={(this.size * 2) / 3}
                height={(this.size * 2) / 3}
                icon="mdi-check"
              />
            </v-avatar>
          )}

          {(this.type === AvatarType.MONOGRAM ||
            (this.type === AvatarType.PHOTO && !this.displayImage)) && (
            <v-avatar
              size={this.size}
              start={this.start}
              color={this.getColor()}
            >
              <span class={`${this.getFontSize()} font-medium text-onSurface`}>
                {this.firstname?.[0]?.toUpperCase() || '?'}
              </span>
            </v-avatar>
          )}
        </>
      );
    },
  });
