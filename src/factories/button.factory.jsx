import { defineComponent, h } from 'vue';
import { useRender } from '@/tools/useRender.js';

export const createButtonComponent = (
  defaultProps = {},
  componentOptions = {}
) =>
  defineComponent({
    name: componentOptions.name,
    props: {
      text: {
        type: String,
        default: defaultProps.text || '',
      },
      color: {
        type: String,
        default: defaultProps.color || 'primary',
      },
      variant: {
        type: String,
        default: defaultProps.variant || 'elevated',
      },
      prependIcon: {
        type: String,
        default: defaultProps.prependIcon || undefined,
      },
      disabled: {
        type: Boolean,
        default: defaultProps.disabled || false,
      },
      loading: {
        type: Boolean,
        default: defaultProps.loading || false,
      },
      append: {
        type: Boolean,
        default: defaultProps.append || false,
      },
      block: {
        type: Boolean,
        default: defaultProps.block || false,
      },
    },

    // Setup
    setup(props, { slots }) {
      useRender(() => {
        return (
          <v-btn
            text={props.text}
            rounded="pill"
            height="40"
            color={props.color}
            block={props.block}
            loading={props.loading}
            variant={props.variant}
            disabled={props.disabled}
            prepend-icon={props.prependIcon}
          ></v-btn>
        );
      });
    },
  });
