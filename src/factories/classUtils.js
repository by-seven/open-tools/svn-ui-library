/**
 * Génère les classes CSS pour le bouton en fonction des états et des props
 * @param {Object} props
 * @returns {String} classes - Classes CSS calculées pour le bouton
 */

export const generateButtonClasses = (props) => {
  const { variant, state, density, disabled, loading, rounded } = props;

  const stateClasses = {
    hovered: "v-btn--hovered",
    focused: "v-btn--focused",
    pressed: "v-btn--pressed",
    disabled: "v-btn--disabled",
  };

  return [
    `v-btn--${variant}`,
    `v-btn--${rounded}`,
    `v-btn--${density}`,
    disabled || state === "disabled" ? "v-btn--disabled" : "",
    loading ? "v-btn--loading" : "",
    stateClasses[state] || "",
  ]
    .filter(Boolean)
    .join(" ");
};
