import { defineComponent, h } from 'vue';
import { generateButtonClasses } from './classUtils';

export const createSvnProIconButtonComponent = (
  defaultProps = {},
  componentOptions = {}
) =>
  defineComponent({
    name: componentOptions.name,
    props: {
      icon: {
        type: String,
        default: defaultProps.icon || 'mdi-check',
        required: true,
      },
      variant: {
        type: String,
        default: defaultProps.variant || 'standard',
        validator: (value) =>
          ['filled', 'outlined', 'standard', 'tonal', 'elevated'].includes(
            value
          ),
      },
      color: {
        type: String,
        default: defaultProps.color || 'onSurfaceVariant',
      },
      size: {
        type: String,
        default: defaultProps.size || 'default',
        validator: (value) =>
          ['x-small', 'small', 'default', 'large', 'x-large'].includes(value),
      },
      density: {
        type: String,
        default: defaultProps.density || 'default',
        validator: (value) =>
          ['default', 'comfortable', 'compact'].includes(value),
      },
      state: {
        type: String,
        default: defaultProps.state || 'enabled',
        validator: (value) =>
          ['enabled', 'hovered', 'focused', 'pressed', 'disabled'].includes(
            value
          ),
      },
      rounded: {
        type: String,
        default: defaultProps.rounded || 'circle',
        validator: (value) =>
          ['none', 'sm', 'md', 'lg', 'xl', 'circle', 'pill'].includes(value),
      },
      elevation: {
        type: Number,
        default: defaultProps.elevation || 0,
      },
      block: {
        type: Boolean,
        default: defaultProps.block || false,
      },
      ripple: {
        type: Boolean,
        default: defaultProps.ripple || true,
      },
      loading: {
        type: Boolean,
        default: defaultProps.loading || false,
      },
      disabled: {
        type: Boolean,
        default: defaultProps.disabled || false,
      },
      ariaLabel: {
        type: String,
        default: defaultProps.ariaLabel || 'Button',
      },
    },

    computed: {
      buttonClasses() {
        return generateButtonClasses({
          variant: this.variant,
          state: this.state,
          density: this.density,
          disabled: this.disabled,
          loading: this.loading,
          rounded: this.rounded,
        });
      },
    },

    render() {
      return (
        <v-btn
          class={this.buttonClasses}
          disabled={this.disabled || this.state === 'disabled'}
          aria-label={this.ariaLabel}
          icon={this.icon}
          variant={this.variant}
          color={this.color}
          size={this.size}
          density={this.density}
          elevation={this.elevation}
          block={this.block}
          ripple={this.ripple}
          loading={this.loading}
          rounded={this.rounded}
        />
      );
    },
  });
