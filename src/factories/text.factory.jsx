import { defineComponent, h } from 'vue';

// Enums for text sizes
export const TextSize = Object.freeze({
  BODY1: 'text-body-1',
  BODY2: 'text-body-2',
  SUBTITLE1: 'text-subtitle-1',
  SUBTITLE2: 'text-subtitle-2',
  CAPTION: 'text-caption',
  BUTTON: 'text-button',
  OVERLINE: 'text-overline',
});

// Enums for text weights
export const TextWeight = Object.freeze({
  REGULAR: 'font-weight-regular',
  MEDIUM: 'font-weight-medium',
  BOLD: 'font-weight-bold',
});

export const createSvnProText = (defaultProps = {}, componentOptions = {}) =>
  defineComponent({
    name: componentOptions.name,
    props: {
      text: {
        type: String,
        default: defaultProps.text || '',
      },
      color: {
        type: String,
        default: defaultProps.color || 'onSurface',
      },
      size: {
        type: String,
        default: defaultProps.size || '',
        validator: (value) =>
          Object.values(TextSize).includes(value) || value === '',
      },
      weight: {
        type: String,
        default: defaultProps.weight || '',
        validator: (value) =>
          Object.values(TextWeight).includes(value) || value === '',
      },
      bodyLarge: {
        type: Boolean,
        default: defaultProps.bodyLarge || false,
      },
      bodyMedium: {
        type: Boolean,
        default: defaultProps.bodyMedium || false,
      },
      subtitleLarge: {
        type: Boolean,
        default: defaultProps.subtitleLarge || false,
      },
      subtitleMedium: {
        type: Boolean,
        default: defaultProps.subtitleMedium || false,
      },
      textarea: {
        type: Boolean,
        default: defaultProps.textarea || false,
      },
    },

    computed: {
      // Font size determination with fallback to older props
      getFontSize() {
        if (this.size) return this.size;
        if (this.bodyLarge) return TextSize.BODY1;
        if (this.bodyMedium) return TextSize.BODY2;
        if (this.subtitleLarge) return TextSize.SUBTITLE1;
        if (this.subtitleMedium) return TextSize.SUBTITLE2;
        return ''; // Default if no size is set
      },

      // Font weight determination with fallback to older props
      getFontWeight() {
        if (this.weight) return this.weight;
        if (this.bold) return TextWeight.BOLD;
        if (this.medium) return TextWeight.MEDIUM;
        return TextWeight.REGULAR; // Default weight
      },
    },

    render() {
      const classes = [
        `text-${this.color}`,
        this.getFontSize,
        this.getFontWeight,
      ];

      // Render editable textarea or standard text
      return this.textarea ? (
        <div class="w-full h-full flex items-center justify-center p-4">
          <div v-html={this.text} class={classes} />
        </div>
      ) : (
        <p class={classes}>{this.text}</p>
      );
    },
  });
