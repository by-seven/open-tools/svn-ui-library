import { createApp } from 'vue'
import App from './App.vue'
import svnUiLibrary from './plugins/svn-ui-library';
import loadFonts from './plugins/webfontloader.js';
// import 'vuetify/styles';
// import "../src/index.css";
import vuetify from "./plugins/vuetify.js";
import Basics from "@/views/Basics.vue";
import Aleph from "@/views/Aleph.vue";
import Studio from "@/views/Studio.vue";
import {createRouter, createWebHistory} from "vue-router";
import Index from "@/views/Index.vue";
import Animation from "@/views/Components/Animation.vue";
import Table from "@/views/Components/Table.vue";
import Skeleton from "@/views/Components/Skeleton.vue";
import Profile from "@/views/Components/Profile.vue";
import Other from "@/views/Components/Other.vue";
import Navbar from "@/views/Components/Navbar.vue";
import Login from "@/views/Components/Login.vue";
import List from "@/views/Components/List.vue";
import Input from "@/views/Components/Input.vue";
import Dialog from "@/views/Components/Dialog.vue";
import Chip from "@/views/Components/Chip.vue";
import Card from "@/views/Components/Card.vue";
import Button from "@/views/Components/Button.vue";
import Avatar from "@/views/Components/Avatar.vue";
import ComponentsIndex from "@/views/Components/ComponentsIndex.vue";
import Pagination from "@/views/Components/Pagination.vue";
import TableCell from "@/views/Components/TableCell.vue";
import Editor from "@/views/Components/Editor.vue";
import Filter from "@/views/Components/Filter.vue";
import { Intersect } from "vuetify/directives";
import './variables.scss';

const app = createApp(App)

loadFonts()

const routes = [
    {
        path: '/',
        name: 'index',
        component: Index,
        children: [
            { path: '', name: 'Seven User Interface Library', component: ComponentsIndex },
            { path: 'animation', name: 'animation', component: Animation} ,
            { path: 'avatar', name: 'avatar', component: Avatar },
            { path: 'button', name: 'button', component: Button },
            { path: 'card', name: 'card', component: Card },
            { path: 'chip', name: 'chip', component: Chip },
            { path: 'dialog', name: 'dialog', component: Dialog },
            { path: 'filters', name: 'filters', component: Filter },
            { path: 'input', name: 'input', component: Input },
            { path: 'list', name: 'list', component: List },
            { path: 'login', name: 'login', component: Login },
            { path: 'navbar', name: 'navbar', component: Navbar },
            { path: 'other', name: 'other', component: Other },
            { path: 'profile', name: 'profile', component: Profile },
            { path: 'skeleton', name: 'skeleton', component: Skeleton },
            { path: 'table', name: 'table', component: Table },
            { path: 'pagination', name: 'pagination', component: Pagination },
            { path: 'table-cell', name: 'table-cell', component: TableCell },
            { path: 'editor', name: 'editor', component: Editor },
        ]
    },
    { path: '/basics', component: Basics },
    { path: '/aleph', component: Aleph },
    { path: '/studio', component: Studio}
]

const router = createRouter({
    history: createWebHistory(),
    routes,
})

app.use(router)
app.use(vuetify)
app.directive('intersect', Intersect)
app.use(svnUiLibrary)
app.mount('#app')
