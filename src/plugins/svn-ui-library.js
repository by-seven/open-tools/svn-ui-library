// Helper
import _ from 'lodash';

// V1 Components
import SvnFloatingActionButton from '../components/SvnFloatingActionButton.vue';
import SvnDotsButton from '../components/SvnDotsButton.vue';
import SvnDialogSkeleton from '../components/skeleton/SvnDialogSkeleton.vue';
import SvnButton from '../components/button/SvnButton.vue';
import SvnIconButton from '../components/button/SvnIconButton.vue';
import SvnLoader from '../components/animation/SvnLoader.vue';
import SvnTooltip from '../components/SvnTooltip.vue';
import SvnTimeline from '../components/SvnTimeline.vue';
import SvnProTimeline from '../components/SvnProTimeline.vue';
import SvnModalManagePeople from '../components/dialog/SvnModalManagePeople.vue';
import SvnAutoCompleteDropdown from '../components/SvnAutoCompleteDropdown.vue';
import SvnTitle from '../components/styles/SvnTitle.vue';
import SvnText from '../components/styles/SvnText.vue';
import SvnAvatar from '../components/avatar/SvnAvatar.vue';
import SvnCenterContent from '../components/SvnCenterContent.vue';
import SvnProfile from '../components/profile/SvnProfile.vue';
import SvnProfileTableCell from '../components/profile/SvnProfileTableCell.vue';
import SvnAutocomplete from '../components/input/SvnAutocomplete.vue';
import SvnIndexSkeleton from '../components/skeleton/SvnIndexSkeleton.vue';
import SvnStudioTrainingCard from '../components/card/SvnStudioTrainingCard.vue';
import SvnStudioTrainerList from '../components/list/SvnStudioTrainerList.vue';
import SvnNavbarMenu from '../components/navbar/SvnNavbarMenu.vue';
import SvnModalSkeleton from '../components/skeleton/SvnModalSkeleton.vue';
import SvnDropdown from '../components/button/SvnDropdown.vue';
import SvnDatePicker from '../components/SvnDatePicker.vue';
import SvnNavbar from '../components/navbar/SvnNavbar.vue';
import SvnLoginCard from '../components/login/SvnLoginCard.vue';
import SvnStatusChip from '@/components/chip/SvnStatusChip.vue';
import SvnChip from '@/components/chip/SvnChip.vue';
import SvnSwitch from '@/components/input/SvnSwitch.vue';
import SvnPagination from '@/components/pagination/SvnPagination.vue';
import SvnDataTable from '../components/table/SvnDataTable.vue';
import SvnTagsCell from '../components/table_cell/SvnTagsCell.vue';
import SvnHeaderCell from '../components/table_cell/SvnHeaderCell.vue';
import SvnUpdateCell from '../components/table_cell/SvnUpdateCell.vue';
import SvnProfileCell from '../components/table_cell/SvnProfileCell.vue';
import SvnDropdownCell from '../components/table_cell/SvnDropdownCell.vue';
import SvnStatusCell from '../components/table_cell/SvnStatusCell.vue';
import SvnFilter from '../components/filters/SvnFilter.vue';
import SvnHelloBanner from '../components/banner/SvnHelloBanner.vue';
import SvnEditor from '../components/editor/SvnEditor.vue';
import SvnTiptap from '../components/editor/SvnTiptap.vue';
import SvnAlephNavbar from '../components/navbar/SvnAlephNavbar.vue';
import SvnAlephNavbarMenu from '../components/navbar/SvnAlephNavbarMenu.vue';
import SvnHeaderChip from '../components/chip/SvnHeaderChip.vue';

// V2 Components
import SvnProAvatar from '../components/avatar/SvnProAvatar.vue';
import SvnProBadge from '../components/chip/SvnProBadge.vue';
import SvnProButton from '../components/button/SvnProButton.vue';
import SvnProCard from '../components/card/SvnProCard.vue';
import SvnProCheckbox from '../components/input/SvnProCheckbox.vue';
import SvnProChip from '../components/chip/SvnProChip.vue';
import SvnProDatePicker from '../components/picker/SvnProDatePicker.vue';
import SvnProDialog from '../components/dialog/SvnProDialog.vue';
import SvnProDivider from '../components/divider/SvnProDivider.vue';
import SvnProFloatingActionButton from '../components/button/SvnProFloatingActionButton.vue';
import SvnProIconButton from '../components/button/SvnProIconButton.vue';
import SvnProProgressCircular from '../components/progress/SvnProProgressCircular.vue';
import SvnProProgressLinear from '../components/progress/SvnProProgressLinear.vue';
import SvnProRadioButton from '../components/input/SvnProRadioButton.vue';
import SvnProSlider from '../components/input/SvnProSlider.vue';
import SvnProSnackbar from '../components/snackbar/SvnProSnackbar.vue';
import SvnProSwitch from '../components/input/SvnProSwitch.vue';
import SvnProText from '../components/styles/SvnProText.vue';
import SvnProTextField from '../components/input/SvnProTextField.vue';
import SvnProTimePicker from '../components/picker/SvnProTimePicker.vue';
import SvnProTitle from '../components/styles/SvnProTitle.vue';
import SvnProTooltip from '../components/tooltip/SvnProTooltip.vue';
import SvnProModal from '../components/modal/SvnProModal.vue';
import SvnProListItem from '../components/list/SvnProListItem.vue';
import SvnProCommentList from '../components/list/SvnProCommentList.vue';
import SvnProInfoTag from '../components/chip/SvnProInfoTag.vue';
import SvnProButtonStickyContainer from '../components/button/SvnProButtonStickyContainer.vue';
import SvnProSelect from '../components/input/SvnProSelect.vue';
import SvnProTextArea from '../components/input/SvnProTextArea.vue';
import SvnProAutocomplete from '../components/input/SvnProAutocomplete.vue';
import SvnProCombobox from '../components/input/SvnProCombobox.vue';
import SvnProCardHomepage from '../components/card/SvnProCardHomepage.vue';
import SvnProHeaderSection from '../components/card/SvnProHeaderSection.vue';
import SvnProPagination from '../components/pagination/SvnProPagination.vue';
import SvnProEmptyStates from '../components/states/SvnProEmptyStates.vue';
import SvnProExtendedRadioButton from '../components/button/SvnProExtendedRadioButton.vue';
import SvnProCardRoadmapTemplate from '../components/card/SvnProCardRoadmapTemplate.vue';
import SvnProCardSectionQuizResultLearn from '../components/card/SvnProCardSectionQuizResultLearn.vue';
import SvnProCardSectionEvaluationShow from '../components/card/SvnProCardSectionEvaluationShow.vue';
import SvnProBreadcrumbs from '../components/button/SvnProBreadcrumbs.vue';
import SvnProCardLearn from '../components/card/SvnProCardLearn.vue';
import SvnProReactionTag from '../components/chip/SvnProReactionTag.vue';
import SvnProProfile from '../components/profile/SvnProProfile.vue';
import SvnProGraphTarget from '../components/progress/SvnProGraphTarget.vue';
import SvnProDialogUploadImage from '../components/dialog/SvnProDialogUploadImage.vue';
import SvnProDataTable from '../components/table/SvnProDataTable.vue';
import SvnProAdvancedTable from '../components/table/SvnProAdvancedTable.vue';
import SvnProFilter from '../components/filters/SvnProFilter.vue';
import SvnProEditDate from '../components/dialog/SvnProEditDate.vue';
import SvnProDataTableCell from '../components/table_cell/SvnProDataTableCell.vue';
import SvnProToast from '../components/snackbar/SvnProToast.vue';
import SvnProMenu from '../components/menu/SvnProMenu.vue';
import SvnProLearnBadge from '../components/chip/SvnProLearnBadge.vue';
import SvnProTrainingCard from '../components/card/SvnProTrainingCard.vue';
import SvnProCardReaction from '../components/card/SvnProCardReaction.vue';
import SvnProQuizAnswerLearnItem from '@/components/quiz/SvnProQuizAnswerLearnItem.jsx';
import SvnProCardInterviewTarget from '@/components/card/SvnProCardInterviewTarget.vue';
import SvnProAnswerBubbleInterview from '@/components/card/SvnProAnswerBubbleInterview.vue';
import SvnProTopBarLearnModule from '@/components/menu/SvnProTopBarLearnModule.vue';
import SvnProCardComment from '@/components/card/SvnProCardComment.vue';
import SvnProChart from '../components/chart/SvnProChart.vue';
import SvnProSectionQuizLearnBase from '@/components/quiz/SvnProSectionQuizLearnBase.jsx';
import SvnProFormBlock from '../components/card/SvnProFormBlock.vue';
import SvnProDialogValidation from '../components/dialog/SvnProDialogValidation.vue';
import SvnProSectionQuizLearnSkeleton from '@/components/quiz/skeleton/SvnProSectionQuizLearnSkeleton';
import SvnProImg from '@/components/images/SvnProImg';
import SvnProCardEvaluation from '../components/card/SvnProCardEvaluation.vue';
import {
  SvnProBodyText,
  SvnProCaptionDateText,
  SvnProSubtitle1Medium,
} from '../components/text/textComponents';
import { SvnProIconButtonCardDelete } from '../components/iconButton/SvnProIconButtonComponents';
import { SvnProAvatarCard } from '../components/avatar/avatarComponents';
import { TextButton } from '../components/button/buttonComponents';
import SvnProTopBar from '@/components/menu/SvnProTopBar.vue';
import SvnProContainer from '@/components/layout/SvnProContainer.vue';
import SvnProButtonToggle from '../components/button/SvnProButtonToggle.vue';
import SvnProTabs from '../components/tab/SvnProTabs.vue';
import SvnProCardUser from '../components/card/SvnProCardUser.vue';

// constants
export {
  TopBarLearnModuleTypes,
  TopBarLearnModuleSaveTypes,
  ComponentSizes,
  CardInterviewTargetTypes,
  ButtonVariants,
  InfotagStyles,
  TargetIndicatorStatuses,
  TargetObjectiveIndicatorTypes,
  InterviewCompletionStatuses,
  InterviewAppQuestionTypes,
  LearnInputType,
  DataTableVariant,
  FilterOperatorOptions,
  FilterType,
  FormBlockTypes,
  ContainerVariants,
  ComponentDensities,
  ComponentBorderRadiuses,
  ComponentBorders,
  TabAlignTypes,
  SvnProCardSizes,
  InfotagTypes,
} from '@/constants/types.js';

export { SvnProSectionQuizLearnCorrectionType } from '@/components/quiz/SvnProSectionQuizLearnBase.jsx';
export { SvnProQuizAnswerLearnItemStates } from '@/components/quiz/SvnProQuizAnswerLearnItem.jsx';
export { SvnProCardSectionQuizResultLearnType } from '@/components/card/SvnProCardSectionQuizResultLearn.vue';
export { SvnProGraphTargetProgress } from '@/components/progress/SvnProGraphTarget.vue';
export { SvnProGraphTargetIndicatorType } from '@/components/progress/SvnProGraphTarget.vue';

function registerComponentToKebabCase(app, component) {
  app.component(_.kebabCase(component.__name || component.name), component);
}

export default {
  install: (app, options = {}) => {
    // Add a global property or provide the environment variable
    app.provide('unsplashAccessKey', options.unsplashAccessKey);

    // Version 1
    registerComponentToKebabCase(app, SvnFloatingActionButton);
    registerComponentToKebabCase(app, SvnDotsButton);
    registerComponentToKebabCase(app, SvnDialogSkeleton);
    registerComponentToKebabCase(app, SvnButton);
    registerComponentToKebabCase(app, SvnIconButton);
    registerComponentToKebabCase(app, SvnLoader);
    registerComponentToKebabCase(app, SvnTooltip);
    registerComponentToKebabCase(app, SvnTimeline);
    registerComponentToKebabCase(app, SvnProTimeline);
    registerComponentToKebabCase(app, SvnModalManagePeople);
    registerComponentToKebabCase(app, SvnAutoCompleteDropdown);
    registerComponentToKebabCase(app, SvnTitle);
    registerComponentToKebabCase(app, SvnText);
    registerComponentToKebabCase(app, SvnAvatar);
    registerComponentToKebabCase(app, SvnCenterContent);
    registerComponentToKebabCase(app, SvnProfile);
    registerComponentToKebabCase(app, SvnProfileTableCell);
    registerComponentToKebabCase(app, SvnAutocomplete);
    registerComponentToKebabCase(app, SvnIndexSkeleton);
    registerComponentToKebabCase(app, SvnStudioTrainingCard);
    registerComponentToKebabCase(app, SvnStudioTrainerList);
    registerComponentToKebabCase(app, SvnNavbarMenu);
    registerComponentToKebabCase(app, SvnModalSkeleton);
    registerComponentToKebabCase(app, SvnDropdown);
    registerComponentToKebabCase(app, SvnDatePicker);
    registerComponentToKebabCase(app, SvnNavbar);
    registerComponentToKebabCase(app, SvnLoginCard);
    registerComponentToKebabCase(app, SvnStatusChip);
    registerComponentToKebabCase(app, SvnChip);
    registerComponentToKebabCase(app, SvnSwitch);
    registerComponentToKebabCase(app, SvnPagination);
    registerComponentToKebabCase(app, SvnDataTable);
    registerComponentToKebabCase(app, SvnTagsCell);
    registerComponentToKebabCase(app, SvnHeaderCell);
    registerComponentToKebabCase(app, SvnUpdateCell);
    registerComponentToKebabCase(app, SvnProfileCell);
    registerComponentToKebabCase(app, SvnDropdownCell);
    registerComponentToKebabCase(app, SvnStatusCell);
    registerComponentToKebabCase(app, SvnFilter);
    registerComponentToKebabCase(app, SvnEditor);
    registerComponentToKebabCase(app, SvnTiptap);
    registerComponentToKebabCase(app, SvnAlephNavbar);
    registerComponentToKebabCase(app, SvnAlephNavbarMenu);
    registerComponentToKebabCase(app, SvnHelloBanner);
    registerComponentToKebabCase(app, SvnHeaderChip);

    // Version 2 Components
    registerComponentToKebabCase(app, SvnProAvatar);
    registerComponentToKebabCase(app, SvnProBadge);
    registerComponentToKebabCase(app, SvnProButton);
    registerComponentToKebabCase(app, SvnProCard);
    registerComponentToKebabCase(app, SvnProCheckbox);
    registerComponentToKebabCase(app, SvnProChip);
    registerComponentToKebabCase(app, SvnProDatePicker);
    registerComponentToKebabCase(app, SvnProDialog);
    registerComponentToKebabCase(app, SvnProDivider);
    registerComponentToKebabCase(app, SvnProFloatingActionButton);
    registerComponentToKebabCase(app, SvnProIconButton);
    registerComponentToKebabCase(app, SvnProProgressCircular);
    registerComponentToKebabCase(app, SvnProProgressLinear);
    registerComponentToKebabCase(app, SvnProRadioButton);
    registerComponentToKebabCase(app, SvnProSlider);
    registerComponentToKebabCase(app, SvnProSnackbar);
    registerComponentToKebabCase(app, SvnProSwitch);
    registerComponentToKebabCase(app, SvnProText);
    registerComponentToKebabCase(app, SvnProTextField);
    registerComponentToKebabCase(app, SvnProTimePicker);
    registerComponentToKebabCase(app, SvnProTitle);
    registerComponentToKebabCase(app, SvnProTooltip);
    registerComponentToKebabCase(app, SvnProModal);
    registerComponentToKebabCase(app, SvnProListItem);
    registerComponentToKebabCase(app, SvnProCommentList);
    registerComponentToKebabCase(app, SvnProInfoTag);
    registerComponentToKebabCase(app, SvnProButtonStickyContainer);
    registerComponentToKebabCase(app, SvnProSelect);
    registerComponentToKebabCase(app, SvnProTextArea);
    registerComponentToKebabCase(app, SvnProAutocomplete);
    registerComponentToKebabCase(app, SvnProCombobox);
    registerComponentToKebabCase(app, SvnProCardHomepage);
    registerComponentToKebabCase(app, SvnProHeaderSection);
    registerComponentToKebabCase(app, SvnProPagination);
    registerComponentToKebabCase(app, SvnProEmptyStates);
    registerComponentToKebabCase(app, SvnProExtendedRadioButton);
    registerComponentToKebabCase(app, SvnProCardRoadmapTemplate);
    registerComponentToKebabCase(app, SvnProCardSectionQuizResultLearn);
    registerComponentToKebabCase(app, SvnProCardSectionEvaluationShow);
    registerComponentToKebabCase(app, SvnProBreadcrumbs);
    registerComponentToKebabCase(app, SvnProCardLearn);
    registerComponentToKebabCase(app, SvnProReactionTag);
    registerComponentToKebabCase(app, SvnProProfile);
    registerComponentToKebabCase(app, SvnProGraphTarget);
    registerComponentToKebabCase(app, SvnProDialogUploadImage);
    registerComponentToKebabCase(app, SvnProDataTable);
    registerComponentToKebabCase(app, SvnProAdvancedTable);
    registerComponentToKebabCase(app, SvnProFilter);
    registerComponentToKebabCase(app, SvnProEditDate);
    registerComponentToKebabCase(app, SvnProDataTableCell);
    registerComponentToKebabCase(app, SvnProToast);
    registerComponentToKebabCase(app, SvnProMenu);
    registerComponentToKebabCase(app, SvnProLearnBadge);
    registerComponentToKebabCase(app, SvnProTrainingCard);
    registerComponentToKebabCase(app, SvnProCardReaction);
    registerComponentToKebabCase(app, SvnProQuizAnswerLearnItem);
    registerComponentToKebabCase(app, SvnProTopBarLearnModule);
    registerComponentToKebabCase(app, SvnProCardInterviewTarget);
    registerComponentToKebabCase(app, SvnProAnswerBubbleInterview);
    registerComponentToKebabCase(app, SvnProCardComment);
    registerComponentToKebabCase(app, SvnProChart);
    registerComponentToKebabCase(app, SvnProCardEvaluation);
    registerComponentToKebabCase(app, SvnProSectionQuizLearnBase);
    registerComponentToKebabCase(app, SvnProFormBlock);
    registerComponentToKebabCase(app, TextButton);
    registerComponentToKebabCase(app, SvnProBodyText);
    registerComponentToKebabCase(app, SvnProIconButtonCardDelete);
    registerComponentToKebabCase(app, SvnProCaptionDateText);
    registerComponentToKebabCase(app, SvnProSubtitle1Medium);
    registerComponentToKebabCase(app, SvnProAvatarCard);
    registerComponentToKebabCase(app, SvnProDialogValidation);
    registerComponentToKebabCase(app, SvnProSectionQuizLearnSkeleton);
    registerComponentToKebabCase(app, SvnProImg);
    registerComponentToKebabCase(app, SvnProTopBar);
    registerComponentToKebabCase(app, SvnProContainer);
    registerComponentToKebabCase(app, SvnProButtonToggle);
    registerComponentToKebabCase(app, SvnProTabs);
    registerComponentToKebabCase(app, SvnProCardUser);
  },
};
