// Styles
import '../../node_modules/@mdi/font/css/materialdesignicons.css';
// import 'vuetify/styles';

// Vuetify
import { Icon } from '@iconify/vue';
import { createVuetify } from 'vuetify';
import { VuetifyDateAdapter } from 'vuetify/lib/composables/date/adapters/vuetify';
import * as components from 'vuetify/components';
import * as labs from 'vuetify/labs/components';

const alephLight = {
  dark: false,
  colors: {
    background: '#FDFCFC',
    backgroundOverlayMultiplier: 1,
    surface: '#FBF9F9',
    surfaceOverlayMultiplier: 1,
    surfaceBright: '#FBF9F9',
    surfaceBrightOverlayMultiplier: 1,
    surfaceLight: '#F1EEEE',
    surfaceLightOverlayMultiplier: 1,
    surfaceVariant: '#E3E1EC',
    surfaceVariantOverlayMultiplier: 2,
    onSurfaceVariant: '#46464F',
    primary: '#3E52CA',
    primaryOverlayMultiplier: 2,
    primaryDarken1: '#2237B1',
    primaryDarken1OverlayMultiplier: 2,
    secondary: '#535C87',
    secondaryOverlayMultiplier: 1,
    secondaryDarken1: '#3B446D',
    secondaryDarken1OverlayMultiplier: 1,
    error: '#BA1A1A',
    errorOverlayMuliplier: 2,
    info: '#1C1B1B',
    infoOverlayMuliplier: 1,
    success: '#1C6D1E',
    successOverlayMuliplier: 1,
    warning: '#CA8100',
    warningOverlayMuliplier: 1,
    onBackground: '#1C1B1B',
    onSurface: '#1C1B1B',
    onSurfaceBright: '#1C1B1B',
    onSurfaceLight: '#1C1B1B',
    onPrimary: '#FFFFFF',
    onPrimaryDarken1: '#FFFFFF',
    onSecondary: '#FFFFFF',
    onSecondaryDarken1: '#FFFFFF',
    onError: '#FFFFFF',
    onInfo: '#FFFFFF',
    onSuccess: '#FFFFFF',
    onWarning: '#FFFFFF',
    kbd: '#212529',
    onKbd: '#FFFFFF',
    code: '#F5F5F5',
    onCode: '#000000',
    borderColor: '#767680',
    avatarBg1: '#F9C25D',
    avatarBg2: '#85E0DB',
    avatarBg3: '#FFBBBB',
    avatarBg4: '#ABDB84',
    avatarBg5: '#F8A677',
    avatarBg6: '#E1BBFF',
    avatarBg7: '#C9D5E0',
    avatarBg8: '#EFE176',
  },
  variables: {
    'border-color': '#767680',
    'border-error': '#BA1A1A',
    'border-opacity': 0.12,
    'high-emphasis-opacity': 0.87,
    'medium-emphasis-opacity': 0.6,
    'disabled-opacity': 0.38,
    'idle-opacity': 0.04,
    'hover-opacity': 0.04,
    'focus-opacity': 0.12,
    'selected-opacity': 0.08,
    'activated-opacity': 0.12,
    'pressed-opacity': 0.12,
    'dragged-opacity': 0.08,
  },
};

// OLD COLORS FOR STUDIO
const studio = {
  dark: false,
  colors: {
    background: '#FFFFFF',
    surface: '#FFFFFF',
    primary: "#FDCF79",
    primaryHover: "#FDD994",
    primaryActive: "#FFC452",
    primaryLighten3: "#FEF4F0",
    tooltipBackground: "#FDD994",
    onSurfaceVariant: '#46464F',
    tooltipText: "#FFFFFF",
    accent: "#000000",
    secondary: "#656565",
    success: "#86AF3F",
    info: "#EED055",
    warning: "#FB8C00",
    grey: '#CDCDCD',
    error: "#FF5252",
    neutral: "#333333",
    fakeBlack: "#333333",
    darkGrey: "#787878",
    middleGrey: "#CDCDCD",
    lightGrey: "#ECECEC",
    veryLightGrey: "#F6F6F6",
    positiveGreen: "#7BC079",
    mediumOrange: "#FFC85C",
    negativeRed: "#FF5656",
    mediumOrangeOpacity: "#FFFAEF",
    negativeRedOpacity: "#FFEFEF",
    positiveGreenOpacity: "#F2F9F2",
    transparentPositiveGreen: "#D8EDD7",
    transparentMediumOrange: "#FFF3DB",
    transparentNegativeRed: "#FAD4D4",
    positiveGreenLighten2: "#E7F4E7",
    mediumOrangeLighten2: "#FFFCE6",
    negativeRedLighten2: "#FFECEF",
  }
}

// OLD COLORS FOR STUDIO
const neutral = {
  dark: false,
  colors: {
    background: '#FFFFFF',
    surface: '#FFFFFF',
    primary: "#333333",
    primaryHover: "#8c8c8c",
    primaryActive: "#151515",
    primaryLighten3: "#ECECEC",
    tooltipBackground: "#787878",
    onSurfaceVariant: '#46464F',
    tooltipText: "#FFFFFF",
    accent: "#000000",
    secondary: "#656565",
    success: "#86af3f",
    info: "#eed055",
    warning: "#FB8C00",
    grey: '#CDCDCD',
    error: "#FF5252",
    neutral: "#333333",
    fakeBlack: "#333333",
    darkGrey: "#787878",
    middleGrey: "#CDCDCD",
    lightGrey: "#ECECEC",
    veryLightGrey: "#F6F6F6",
    positiveGreen: "#7BC079",
    mediumOrange: "#FFC85C",
    negativeRed: "#FF5656",
    mediumOrangeOpacity: "#FFFAEF",
    negativeRedOpacity: "#FFEFEF",
    positiveGreenOpacity: "#F2F9F2",
    transparentPositiveGreen: "#D8EDD7",
    transparentMediumOrange: "#fff3db",
    transparentNegativeRed: "#FAD4D4",
    positiveGreenLighten2: "#E7F4E7",
    mediumOrangeLighten2: "#FFFCE6",
    negativeRedLighten2: "#FFECEF",
  }
}

// Custom IconSet to use iconify Icons
const customIconSet = {
  component: Icon,
  aliases: {
    custom: {
      component: Icon,
    },
  },
  props: {
    icon: '',
  },
};

export default createVuetify(
  // https://vuetifyjs.com/en/introduction/why-vuetify/#feature-guides
  {
    icons: {
      defaultSet: 'mdi',
      sets: {
        custom: customIconSet,
      },
    },
    date: {
      adapter: VuetifyDateAdapter,
    },
    theme: {
      defaultTheme: 'alephLight',
      themes: {
        alephLight,
        studio,
        neutral
      },
      variations: {
        colors: [
          'primary',
          'primaryHover',
          'secondary',
          'fakeBlack',
          'darkGrey',
          'middleGrey',
          'lightGrey',
          'veryLightGrey',
          'positiveGreen',
          'mediumOrange',
          'negativeRed',
          'onSurfaceVariant',
        ],
        lighten: 5,
        darken: 5,
      },
    },
    components: {
      ...components,
      ...labs,
    },
  }
);
