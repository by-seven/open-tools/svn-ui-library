import SvnAutocomplete from "../components/input/SvnAutocomplete.vue";

const items = [
  {
    id: 0,
    avatar: null,
    firstname: "Darell",
    lastname: "Steward",
    fullname: "Darell Steward",
  },
  {
    id: 1,
    avatar: null,
    firstname: "Esther",
    lastname: "Howard",
    fullname: "Esther Howard",
  },
  {
    id: 2,
    avatar: null,
    firstname: "Jenny",
    lastname: "Wilson",
    fullname: "Jenny Wilson",
  },
  {
    id: 3,
    avatar: null,
    firstname: "Robert",
    lastname: "Garcia",
    fullname: "Robert Garcia",
  },
  {
    id: 4,
    avatar: null,
    firstname: "Harold",
    lastname: "Léon",
    fullname: "Harold Léon",
  },
  {
    id: 5,
    avatar: null,
    firstname: "William",
    lastname: "Favreau",
    fullname: "William Favreau",
  },
  {
    id: 6,
    avatar: null,
    firstname: "Hammayoun",
    lastname: "Safi",
    fullname: "Hammayoun Safi",
  },
];

export default {
  title: "Design System/Components/V1/SvnAutocomplete",
  component: SvnAutocomplete,
  tags: ["autodocs"],
  parameters: {
    docs: {
      description: {
        component:
          "Component for Autocomplete, shows an item or a list of items.",
      },
    },
  },
};

export const Playground = {
  render: (args) => ({
    components: { SvnAutocomplete },
    setup() {
      return { args };
    },
    template: '<SvnAutocomplete v-bind="args" />',
  }),
  args: {
    items: items,
    label: "Label",
    multiple: false,
    checkbox: false,
    closableChips: false,
    disabled: false,
    avatarChip: false,
    clearOnSelect: false,
    resetSearchOnUnfocus: false,
    returnObject: false,
  },
};
