import SvnAvatar from "../components/avatar/SvnAvatar.vue";

export default {
  title: "Design System/Components/V1/SvnAvatar",
  component: SvnAvatar,
  tags: ["autodocs"],
  argTypes: {
    size: {
      control: "select",
      options: ["xs", "20", "sm", "md", "lg", "xl", "2xl"],
    },
    avatar: {
      control: "text",
    },
  },
  parameters: {
    docs: {
      description: {
        component:
          "Component for avatar, showing a circular picture, or just initials.",
      },
    },
  },
};

export const Playground = {
  render: (args) => ({
    components: { SvnAvatar },
    setup() {
      return { args };
    },
    template: '<SvnAvatar v-bind="args" />',
  }),
  args: {
    firstname: "Dazai",
    lastname: "Osamu",
    size: "2xl",
    alt: "",
    avatar: "avatar",
    rounded: false,
  },
};
