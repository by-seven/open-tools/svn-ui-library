import SvnButton from "../components/button/SvnButton.vue";

export default {
  title: "Design System/Components/V1/SvnButton",
  component: SvnButton,
  tags: ["autodocs"],
  argTypes: {
    variant: {
      control: "select",
      options: ["default", "outlined", "plain", "neutral"],
    },
    text: {
      control: "text",
    },
  },
  parameters: {
    docs: {
      description: {
        component: "Component for button, with different configurations.",
      },
    },
  },
};

export const Playground = {
  render: (args) => ({
    components: { SvnButton },
    setup() {
      return { args };
    },
    template: '<SvnButton v-bind="args" />',
  }),
  args: {
    disable: false,
    variant: "default",
    danger: false,
    loading: false,
    block: false,
    text: "button",
  },
};
