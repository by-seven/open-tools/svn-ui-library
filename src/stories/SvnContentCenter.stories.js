import SvnCenterContent from "../components/SvnCenterContent.vue";

export default {
  title: "Design System/Components/V1/SvnCenterContent",
  component: SvnCenterContent,
  tags: ["autodocs"],
  argTypes: {
    backgroundColorClass: {
      control: "text",
    },
  },
  parameters: {
    docs: {
      description: {
        component:
          "Component thats displays everything inside its tags at the center of the page.",
      },
    },
  },
};

export const Playground = {
  render: (args) => ({
    components: { SvnCenterContent },
    setup() {
      return { args };
    },
    template: '<SvnCenterContent v-bind="args" />',
  }),
  args: {
    backgroundColorClass: "",
  },
};
