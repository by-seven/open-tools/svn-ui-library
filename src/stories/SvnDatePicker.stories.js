import SvnDatePicker from "../components/SvnDatePicker.vue";

export default {
  title: "Design System/Components/V1/SvnDatePicker",
  component: SvnDatePicker,
  tags: ["autodocs"],
  argTypes: {
    density: {
      control: "select",
      options: ["default", "compact", "comfortable"],
    },
  },
  parameters: {
    docs: {
      description: {
        component:
          "Component for Date Picker, shows a clickable input which toggles a calendar.",
      },
    },
  },
};

export const Playground = {
  render: (args) => ({
    components: { SvnDatePicker },
    setup() {
      return { args };
    },
    template: '<SvnDatePicker v-bind="args" />',
  }),
  args: {
    disable: false,
    density: "compact",
  },
};
