import SvnDialogSkeleton from "../components/skeleton/SvnDialogSkeleton.vue";

export default {
  title: "Design System/Components/V1/SvnDialogSkeleton",
  component: SvnDialogSkeleton,
  tags: ["autodocs"],
  argTypes: {
    activatorVariant: {
      control: "select",
      options: ["default", "outlined", "plain", "neutral"],
    },
  },
  parameters: {
    docs: {
      description: {
        component: "Component for our Modal, with many variants.",
      },
    },
  },
};

export const Playground = {
  render: (args) => ({
    components: { SvnDialogSkeleton },
    setup() {
      return { args };
    },
    template: '<SvnDialogSkeleton v-bind="args" />',
  }),
  args: {
    primaryButtonText: "Button",
    secondaryButtonText: "Button",
    title: "Dialog title",
    icon: "ph:globe-bold",
    subtitle: "Dialog subtitle",
    description:
      "Lorem ipsum dolor sit amet consectetur. Quis aliquet eget id quis lectus pellentesque. Porttitor.",
    dialogCustomClass: "w-[400px]",
    closeIcon: "material-symbols:close",
    activatorText: "Open Dialog",
    primaryButtonClass: "",
    secondaryButtonClass:
      "border border-middle-grey bg-white text-fake-black text-xs font-medium",
    primaryButtonDisabled: false,
    iconSize: 32,
    primaryButtonDanger: false,
    secondaryButtonDanger: false,
    primaryButtonCloseOnClick: true,
    secondaryButtonCloseOnClick: true,
    withInputs: true,
  },
};
