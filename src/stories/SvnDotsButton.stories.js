import SvnDotsButton from "../components/SvnDotsButton.vue";

export default {
  title: "Design System/Components/V1/SvnDotsButton",
  component: SvnDotsButton,
  tags: ["autodocs"],
  argTypes: {
    disable: {
      control: "boolean",
    },
    table: {
      control: "boolean",
    },
    variant: {
      control: "select",
      options: ["default", "outlined", "plain", "noOutlined"],
    },
    buttonSize: {
      control: "select",
      options: ["md", "lg"],
    },
  },
  parameters: {
    docs: {
      description: {
        component: "Button used to display a list of elements in a menu",
      },
    },
  },
};

export const Playground = {
  render: (args) => ({
    components: { SvnDotsButton },
    setup() {
      return { args };
    },
    template: '<SvnDotsButton v-bind="args" />',
  }),
  args: {
    disable: false,
    variant: "plain",
    icon: "mdi:dots-vertical",
    buttonSize: "md",
    to: "",
    danger: false,
    circle: false,
    table: false,
    colorClass: "",
  },
};
