import SvnDropdown from "../components/button/SvnDropdown.vue";

const items = [
  {
    id: 0,
    title: "Darell Steward",
  },
  {
    id: 1,
    title: "Esther Howard",
  },
  {
    id: 2,
    title: "Jenny Wilson",
  },
];

export default {
  title: "Design System/Components/V1/SvnDropdown",
  component: SvnDropdown,
  tags: ["autodocs"],
  parameters: {
    docs: {
      description: {
        component: "Component for Dropdown, shows an item or a list of items.",
      },
    },
  },
};

export const Playground = {
  render: (args) => ({
    components: { SvnDropdown },
    setup() {
      return { args };
    },
    template: '<SvnDropdown v-bind="args" />',
  }),
  args: {
    items: items,
    closeOnContentCLick: true,
  },
};
