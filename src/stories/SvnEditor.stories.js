import SvnEditor from "../components/editor/SvnEditor.vue";

export default {
  title: "Design System/Components/V1/SvnEditor",
  component: SvnEditor,
  tags: ["autodocs"],
  parameters: {
    docs: {
      description: {
        component: "Component for the text Editor, the core of Learn App.",
      },
    },
  },
};

export const Playground = {
  render: (args) => ({
    components: { SvnEditor },
    setup() {
      return { args };
    },
    template: '<SvnEditor v-bind="args" />',
  }),
  args: {
    readOnly: false,
    editorId: "editor-test",
    createImageUrl: "null",
    aleph: false,
  },
};
