import SvnFilter from "../views/Components/Filter.vue";

const data = [
  {
    id: 3,
    campaign_draft_id: 3,
    campaign_type: "one_to_one",
    can_set_copilot: true,
    categories: [
      {
        id: 1,
        title: "Tag1",
      },
      {
        id: 2,
        title: "Tag2",
      },
    ],
    completion: 0,
    deadline: "2023-12-24",
    deleted_at: null,
    employees_count: 1,
    startline: "2023-12-22",
    status: "published",
    title: "NEW",
  },
  {
    id: 2,
    campaign_draft_id: 2,
    campaign_type: "one_to_one",
    can_set_copilot: true,
    categories: [],
    completion: 100,
    deadline: "2023-12-24",
    deleted_at: null,
    employees_count: 1,
    startline: "2023-12-22",
    status: "published",
    title: "qsd",
  },
  {
    id: 1,
    campaign_draft_id: 1,
    campaign_type: "one_to_one",
    can_set_copilot: true,
    categories: [],
    completion: 0,
    deadline: "2023-12-15",
    deleted_at: null,
    employees_count: 1,
    startline: "2023-12-14",
    status: "published",
    title: "Campagne Interview X Roadmap",
  },
];

export default {
  title: "Design System/Components/V1/SvnFilter",
  component: SvnFilter,
  tags: ["autodocs"],
  argTypes: {
    activatorVariant: {
      control: "select",
      options: ["default", "outlined", "tonal", "text", "plain"],
    },
  },
  parameters: {
    docs: {
      description: {
        component: "Filter button used on many index pages",
      },
    },
  },
};

export const Playground = {
  render: (args) => ({
    components: { SvnFilter },
    setup() {
      return { args };
    },
    template: '<SvnFilter v-bind="args" />',
  }),
  args: {
    mockData: data,
    activatorText: "Filters",
    activatorIcon: "mdi-filter-outline",
    activatorVariant: "outlined",
    activatorColor: "darkGrey",
    activatorHeight: 48,
  },
};
