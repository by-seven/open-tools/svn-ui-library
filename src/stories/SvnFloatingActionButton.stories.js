import SvnFloatingActionButton from "../components/SvnFloatingActionButton.vue";

export default {
  title: "Design System/Components/V1/SvnFloatingActionButton",
  component: SvnFloatingActionButton,
  tags: ["autodocs"],
  parameters: {
    docs: {
      description: {
        component: "Button used to navigate at the top of the page",
      },
    },
  },
};

export const Playground = {
  render: (args) => ({
    components: { SvnFloatingActionButton },
    setup() {
      return { args };
    },
    template: '<SvnFloatingActionButton v-bind="args" />',
  }),
  args: {
    default: true,
    square: false,
    round: false,
    mini: false,
    menu: false,
    text: "Action",
    icon: "ic:round-vertical-align-top",
  },
};
