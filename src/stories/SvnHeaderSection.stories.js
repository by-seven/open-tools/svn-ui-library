import SvnProHeaderSection from '../components/card/SvnProHeaderSection.vue';

export default {
  title: 'Design System/Components/V2/SvnProHeaderSection',
  component: SvnProHeaderSection,
  tags: ['autodocs'],
  argTypes: {
    title: {
      control: 'text',
    },
    subtitle: {
      control: 'text',
    },
    size: {
      control: 'select',
      options: ['default', 'compact'],
    },
    emoji: {
      control: 'text',
    },
    interviewsSubmitted: {
      control: 'text',
    },
    modulesAcquired: {
      control: 'text',
    },
    currentTargets: {
      control: 'text',
    },
    interview: {
      control: 'boolean',
    },
    training: {
      control: 'boolean',
    },
    roadmap: {
      control: 'boolean',
    },
  },
  parameters: {
    docs: {
      description: {
        component:
          'Component for the Header Section in the Homepage, displaying a title, a subtitle and some other details.',
      },
    },
  },
};

export const Playground = {
  render: (args) => ({
    components: { SvnProHeaderSection },
    setup() {
      return { args };
    },
    template: '<SvnProHeaderSection v-bind="args" />',
  }),
  args: {
    title: 'Hello, Annalisa',
    subtitle:
      'Lorem ipsum dolor sit amet consectetur. Ut quam quis ullamcorper ultrices purus.',
    size: 'default',
    emoji: 'noto:waving-hand',
    interview: false,
    training: false,
    roadmap: false,
    interviewsSubmitted: '3 interviews submitted',
    modulesAcquired: '5 modules acquired',
    currentTargets: '2 current targets',
  },
};
