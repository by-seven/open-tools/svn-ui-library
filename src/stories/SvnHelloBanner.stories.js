import SvnHelloBanner from "../components/banner/SvnHelloBanner.vue";

export default {
  title: "Design System/Components/V1/SvnHelloBanner",
  component: SvnHelloBanner,
  tags: ["autodocs"],
  argTypes: {
    app: {
      control: "select",
      options: ["interview", "learn", "roadmap", "home"],
    },
  },
  parameters: {
    docs: {
      description: {
        component: "Hello banner component for the different apps.",
      },
    },
  },
};

export const Playground = {
  render: (args) => ({
    components: { SvnHelloBanner },
    setup() {
      return { args };
    },
    template: '<SvnHelloBanner v-bind="args" />',
  }),
  args: {
    app: "interview",
    url: "https://cdn.pixabay.com/photo/2019/03/13/09/24/simson-4052520_960_720.jpg",
    firstname: "John Wick",
    subtitle: "Here are your interviews as interviewee.",
    completedTrainings: 0,
    modulesAcquired: 0,
    completedTargets: 0,
    currentTargets: 0,
    textChipFirst: "Trainings completed",
    textChipSecond: "Modules acquired",
  },
};
