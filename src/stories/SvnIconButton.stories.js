import SvnIconButton from "../components/button/SvnIconButton.vue";

export default {
  title: "Design System/Components/V1/SvnIconButton",
  component: SvnIconButton,
  tags: ["autodocs"],
  argTypes: {
    variant: {
      control: "select",
      options: ["default", "outlined", "plain"],
    },
    buttonSize: {
      control: "select",
      options: ["sm", "md", "lg", "lg2", "xl"],
    },
  },
  parameters: {
    docs: {
      description: {
        component: "Component for Icon button, with different.",
      },
    },
  },
};

export const Playground = {
  render: (args) => ({
    components: { SvnIconButton },
    setup() {
      return { args };
    },
    template: '<SvnIconButton v-bind="args" />',
  }),
  args: {
    disable: false,
    variant: "default",
    icon: "material-symbols:add",
    buttonSize: "md",
    danger: false,
    circle: false,
  },
};
