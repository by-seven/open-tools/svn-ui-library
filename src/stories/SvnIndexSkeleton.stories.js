import SvnIndexSkeleton from "../components/skeleton/SvnIndexSkeleton.vue";

const headers = [
  {
    align: "start",
    key: "name",
    sortable: true,
    title: "Name",
  },
  {
    align: "start",
    key: "access_level",
    sortable: true,
    title: "Access Level",
  },
  {
    align: "start",
    key: "email",
    sortable: true,
    title: "Email",
  },
  {
    align: "start",
    key: "job_title",
    sortable: true,
    title: "Job title",
  },
  {
    align: "start",
    key: " ",
    sortable: true,
    title: "",
  },
];

const items = [
  {
    id: 41,
    access_level: "employee",
    avatar:
      "http://localhost:3000/rails/active_storage/blobs/redirect/eyJfcmFpbHMiOnsibWVzc2FnZSI6IkJBaHBBdnNCIiwiZXhwIjpudWxsLCJwdXIiOiJibG9iX2lkIn19--e35cc5e943cd5def4475080a87d4b37b3d18bd4f/accepter-le-changement.jpg",
    email: "tim_treutel@heller.net",
    firstname: "Beulah",
    fullname: "Beulah ABSHIRE",
    hire_date: "2022-03-26",
    job_title: null,
    lastname: "Abshire",
  },
  {
    id: 256,
    access_level: "employee",
    avatar:
      "http://localhost:3000/rails/active_storage/blobs/redirect/eyJfcmFpbHMiOnsibWVzc2FnZSI6IkJBaHBBdnNCIiwiZXhwIjpudWxsLCJwdXIiOiJibG9iX2lkIn19--e35cc5e943cd5def4475080a87d4b37b3d18bd4f/accepter-le-changement.jpg",
    email: "tim_treutel@heller.net",
    firstname: "Beulah",
    fullname: "Beulah ABSHIRE",
    hire_date: "2022-03-26",
    job_title: null,
    lastname: "Abshire",
  },
  {
    id: 445,
    access_level: "employee",
    avatar:
      "http://localhost:3000/rails/active_storage/blobs/redirect/eyJfcmFpbHMiOnsibWVzc2FnZSI6IkJBaHBBdnNCIiwiZXhwIjpudWxsLCJwdXIiOiJibG9iX2lkIn19--e35cc5e943cd5def4475080a87d4b37b3d18bd4f/accepter-le-changement.jpg",
    email: "tim_treutel@heller.net",
    firstname: "Beulah",
    fullname: "Beulah ABSHIRE",
    hire_date: "2022-03-26",
    job_title: null,
    lastname: "Abshire",
  },
  {
    id: 421,
    access_level: "employee",
    avatar:
      "http://localhost:3000/rails/active_storage/blobs/redirect/eyJfcmFpbHMiOnsibWVzc2FnZSI6IkJBaHBBdnNCIiwiZXhwIjpudWxsLCJwdXIiOiJibG9iX2lkIn19--e35cc5e943cd5def4475080a87d4b37b3d18bd4f/accepter-le-changement.jpg",
    email: "tim_treutel@heller.net",
    firstname: "Beulah",
    fullname: "Beulah ABSHIRE",
    hire_date: "2022-03-26",
    job_title: null,
    lastname: "Abshire",
  },
  {
    id: 78,
    access_level: "employee",
    avatar:
      "http://localhost:3000/rails/active_storage/blobs/redirect/eyJfcmFpbHMiOnsibWVzc2FnZSI6IkJBaHBBdnNCIiwiZXhwIjpudWxsLCJwdXIiOiJibG9iX2lkIn19--e35cc5e943cd5def4475080a87d4b37b3d18bd4f/accepter-le-changement.jpg",
    email: "tim_treutel@heller.net",
    firstname: "qsdq",
    fullname: "Dfef BABOIUM",
    hire_date: "2022-03-26",
    job_title: null,
    lastname: "BABOIUM",
  },
  {
    id: 396,
    access_level: "employee",
    avatar:
      "http://localhost:3000/rails/active_storage/blobs/redirect/eyJfcmFpbHMiOnsibWVzc2FnZSI6IkJBaHBBdnNCIiwiZXhwIjpudWxsLCJwdXIiOiJibG9iX2lkIn19--e35cc5e943cd5def4475080a87d4b37b3d18bd4f/accepter-le-changement.jpg",
    email: "tim_treutel@heller.net",
    firstname: "JKQF",
    fullname: "JKQF qdqsdsqd",
    hire_date: "2022-03-26",
    job_title: null,
    lastname: "Abshire",
  },
];

export default {
  title: "Design System/Components/V1/SvnIndexSkeleton",
  component: SvnIndexSkeleton,
  tags: ["autodocs"],
  argTypes: {
    entity: {
      control: "select",
      options: [
        "default",
        "learn_catalog",
        "studio_training",
        "roadmap",
        "interviews",
        "roadmap_targets",
      ],
    },
  },
  parameters: {
    docs: {
      description: {
        component: "Component for Index with many slots.",
      },
    },
  },
};

export const Playground = {
  render: (args) => ({
    components: { SvnIndexSkeleton },
    setup() {
      return { args };
    },
    template: '<SvnIndexSkeleton v-bind="args" />',
  }),
  args: {
    withOptions: false,
    tableShowSelect: true,
    entity: "default",
    indexTitle: "Title",
    indexSubtitle: "Here is a short sentence",
    indexIcon: "noto:books",
    tableHeaders: headers,
    tableItems: items,
  },
};
