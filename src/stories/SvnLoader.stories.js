import SvnLoader from "../components/animation/SvnLoader.vue";

export default {
  title: "Design System/Components/V1/SvnLoader",
  component: SvnLoader,
  tags: ["autodocs"],
  argTypes: {
    loadingSize: {
      control: "select",
      options: ["xs", "md", "lg", "xl"],
    },
  },
  parameters: {
    docs: {
      description: {
        component: "Component for Loader, just a circular progress bar.",
      },
    },
  },
};

export const Playground = {
  render: (args) => ({
    components: { SvnLoader },
    setup() {
      return { args };
    },
    template: '<SvnLoader v-bind="args" />',
  }),
};
