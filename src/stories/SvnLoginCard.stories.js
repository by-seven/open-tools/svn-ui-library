import SvnLoginCard from "../components/login/SvnLoginCard.vue";
import logo from "@/stories/assets/logo_studio.svg";

export default {
  title: "Design System/Components/V1/SvnLoginCard",
  component: SvnLoginCard,
  tags: ["autodocs"],
  parameters: {
    docs: {
      description: {
        component: "Component for the white login card ",
      },
    },
  },
};

export const Playground = {
  render: (args) => ({
    components: { SvnLoginCard },
    setup() {
      return { args };
    },
    template: '<SvnLoginCard v-bind="args" />',
  }),
  args: {
    logo: logo,
  },
};
