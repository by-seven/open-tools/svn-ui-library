import SvnModalManagePeople from "../components/dialog/SvnModalManagePeople.vue";

const mockUsers = [
  {
    id: 0,
    avatar: null,
    firstname: "Darell",
    lastname: "Steward",
    fullname: "Darell Steward",
  },
  {
    id: 1,
    avatar: null,
    firstname: "Esther",
    lastname: "Howard",
    fullname: "Esther Howard",
  },
  {
    id: 2,
    avatar: null,
    firstname: "Jenny",
    lastname: "Wilson",
    fullname: "Jenny Wilson",
  },
  {
    id: 3,
    avatar: null,
    firstname: "Robert",
    lastname: "Garcia",
    fullname: "Robert Garcia",
  },
  {
    id: 4,
    avatar: null,
    firstname: "Harold",
    lastname: "Léon",
    fullname: "Harold Léon",
  },
  {
    id: 5,
    avatar: null,
    firstname: "William",
    lastname: "Favreau",
    fullname: "William Favreau",
  },
  {
    id: 6,
    avatar: null,
    firstname: "Hammayoun",
    lastname: "Safi",
    fullname: "Hammayoun Safi",
  },
];

export default {
  title: "Design System/Components/V1/SvnModalManagePeople",
  component: SvnModalManagePeople,
  tags: ["autodocs"],
  parameters: {
    docs: {
      description: {
        component:
          "Component for a modal with a list of elemnts, an Autocomplete to add more elemenst to the actual list.",
      },
    },
  },
};

export const Playground = {
  render: (args) => ({
    components: { SvnModalManagePeople },
    setup() {
      return { args };
    },
    template: '<SvnModalManagePeople v-bind="args" />',
  }),
  args: {
    title: "Modal title",
    searchUsers: mockUsers,
    persistent: false,
    modalTitleCenter: false,
    vertical: false,
    vertialHeader: false,
    appendIconCloseModal: false,
    activatorText: "Open Modal",
    prependHeaderIcon: "",
    appendHeaderIcon: "mdi:close",
    headerCustomClass: "",
    modalCustomClass: "!w-[585px]",
    activatorVariant: "plain",
  },
};
