import SvnModalSkeleton from "../components/skeleton/SvnModalSkeleton.vue";

export default {
  title: "Design System/Components/V1/SvnModalSkeleton",
  component: SvnModalSkeleton,
  tags: ["autodocs"],
  argTypes: {
    activatorVariant: {
      control: "select",
      options: ["default", "outlined", "plain", "neutral"],
    },
  },
  parameters: {
    docs: {
      description: {
        component: "Component for our Modal, with many variants.",
      },
    },
  },
};

export const Playground = {
  render: (args) => ({
    components: { SvnModalSkeleton },
    setup() {
      return { args };
    },
    template: '<SvnModalSkeleton v-bind="args" />',
  }),
  args: {
    activatorVariant: "neutral",
    activatorText: "Open Modal",
    fullscreen: false,
    persistent: false,
    modalTitle: "Create a new template",
    modalTitleCenter: false,
    verticalHeader: false,
    appendIconCloseModal: true,
    transition: "dialog-bottom-transition",
  },
};
