import SvnNavbarMenu from "../components/navbar/SvnNavbarMenu.vue";

const menuItems = [
  {
    title: "my interviews",
    to: "",
  },
  {
    title: "my team interviews",
    to: "",
  },
  {
    title: "my campaigns",
    to: "",
  },
  {
    title: "templates",
    to: "",
  },
  {
    title: "reports",
    to: "",
  },
];

export default {
  title: "Design System/Components/V1/SvnNavBarMenu",
  component: SvnNavbarMenu,
  tags: ["autodocs"],
  argTypes: {
    variant: {
      control: "select",
      options: ["modal", "select", "navbar"],
    },
  },
  parameters: {
    docs: {
      description: {
        component: "Component for The NavBar.",
      },
    },
  },
};

export const Playground = {
  render: (args) => ({
    components: { SvnNavbarMenu },
    setup() {
      return { args };
    },
    template: '<SvnNavbarMenu v-bind="args" />',
  }),
  args: {
    items: menuItems,
    variant: "modal",
  },
};
