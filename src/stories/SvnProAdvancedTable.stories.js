import { ref } from 'vue';
import SvnProButton from '@/components/button/SvnProButton.vue';
import SvnProFilter from '@/components/filters/SvnProFilter.vue';
import SvnProAdvancedTable from '@/components/table/SvnProAdvancedTable.vue';
import SvnProDataTableCell from '@/components/table_cell/SvnProDataTableCell.vue';
import svnProCardLearn from '@/components/card/SvnProCardLearn.vue';

const backendFilterItems = [
  {
    title: 'Campaign title',
    type: 'text',
    operatorOptions: ['contains'],
    value: '',
    operator: null,
    name: 'title',
  },
  {
    title: 'Manager',
    type: 'multi_select',
    operatorOptions: ['contains'],
    value: [],
    name: 'manager_id',
    items: [
      { id: 4277, fullname: 'Harold ARMIJO LEON' },
      { id: 7569, fullname: 'Edouard BOURÉLY' },
      { id: 7268, fullname: 'Lisa DOYEN' },
      { id: 3, fullname: 'Yahya FALLAH' },
      { id: 4278, fullname: 'Virginie FAURÉ' },
      { id: 3993, fullname: 'William FAVREAU' },
      { id: 4276, fullname: 'Thomas FRAUDET' },
      { id: 6207, fullname: 'Maxime GRZESIEWSKI' },
      { id: 6314, fullname: 'Estelle MARET' },
      { id: 6286, fullname: 'Daniel Stéphane MBAPPE' },
    ],
  },
  {
    title: 'Period',
    type: 'date',
    operatorOptions: ['before', 'after'],
    value: '',
    operator: null,
    name: 'startline',
  },
  {
    title: 'Participants',
    type: 'number',
    operatorOptions: ['equal', 'not_equal', 'greater_than', 'less_than'],
    value: '',
    operator: null,
    name: 'employees_count',
  },
  {
    title: 'Campaign type',
    type: 'multi_select',
    operatorOptions: ['contains'],
    value: '',
    operator: null,
    name: 'campaign_type',
    items: ['OneToOne', 'Feedback', 'Survey'],
  },
  {
    title: 'Completion',
    type: 'number',
    operatorOptions: ['equal', 'not_equal', 'greater_than', 'less_than'],
    value: '',
    operator: null,
    name: 'completion',
  },
];
const tabHeaders = [
  {
    align: 'start',
    key: 'firstname',
    sortable: true,
    title: 'User',
  },
  {
    align: 'start',
    key: 'jobTitle',
    sortable: true,
    title: 'Job',
  },
  {
    align: 'start',
    key: 'email',
    sortable: true,
    title: 'Email',
  },
  {
    align: 'start',
    key: 'salary',
    sortable: true,
    title: 'Salary',
  },
  {
    align: 'start',
    key: 'emoji',
    sortable: true,
    title: 'Emoji',
  },
  {
    align: 'start',
    key: 'skills',
    sortable: false,
    title: 'Skills',
  },
  {
    align: 'start',
    key: 'button',
    sortable: false,
    title: '',
  },
]
const users = [
  {
    id: 1,
    title: 'ID001',
    avatar: 'https://randomuser.me/api/portraits/men/1.jpg',
    firstname: 'John',
    lastname: 'Doe',
    jobTitle: 'Software Engineer',
    salary: '$85,000',
    skills: ['JavaScript', 'React', 'Node.js'],
    email: 'john.doe@example.com',
    emoji: 'mdi:account',
    icon: '$vuetify',
  },
  {
    id: 2,
    title: 'ID002',
    avatar: 'https://randomuser.me/api/portraits/women/2.jpg',
    firstname: 'Jane',
    lastname: 'Smith',
    jobTitle: 'Data Analyst',
    salary: '$70,000',
    skills: ['Python', 'SQL'],
    email: 'jane.smith@example.com',
    emoji: 'mdi:chart-bar',
    icon: '$vuetify',
  },
  {
    id: 3,
    title: 'ID003',
    avatar: 'https://randomuser.me/api/portraits/men/3.jpg',
    firstname: 'James',
    lastname: 'Brown',
    jobTitle: 'Project Manager',
    salary: '$90,000',
    skills: ['Agile', 'Scrum', 'Leadership'],
    email: 'james.brown@example.com',
    emoji: 'mdi:clipboard-outline',
    icon: '$vuetify',
  },
  {
    id: 4,
    title: 'ID004',
    avatar: 'https://randomuser.me/api/portraits/women/4.jpg',
    firstname: 'Emily',
    lastname: 'Johnson',
    jobTitle: 'Graphic Designer',
    salary: '$60,000',
    skills: ['Photoshop', 'Illustrator'],
    email: 'emily.johnson@example.com',
    emoji: 'mdi:palette',
    icon: '$vuetify',
  },
  {
    id: 5,
    title: 'ID005',
    avatar: 'https://randomuser.me/api/portraits/men/5.jpg',
    firstname: 'Michael',
    lastname: 'Williams',
    jobTitle: 'Marketing Manager',
    salary: '$95,000',
    skills: ['SEO', 'Content Marketing', 'Social Media'],
    email: 'michael.williams@example.com',
    emoji: 'mdi:bullhorn',
    icon: '$vuetify',
  },
  {
    id: 6,
    title: 'ID006',
    avatar: 'https://randomuser.me/api/portraits/women/6.jpg',
    firstname: 'Sarah',
    lastname: 'Davis',
    jobTitle: 'Product Manager',
    salary: '$100,000',
    skills: ['Product Development', 'Roadmap Planning'],
    email: 'sarah.davis@example.com',
    emoji: 'mdi:clipboard-check-outline',
    icon: '$vuetify',
  },
  {
    id: 7,
    title: 'ID007',
    avatar: 'https://randomuser.me/api/portraits/men/7.jpg',
    firstname: 'David',
    lastname: 'Miller',
    jobTitle: 'Network Engineer',
    salary: '$80,000',
    skills: ['Networking', 'Cisco', 'Firewall'],
    email: 'david.miller@example.com',
    emoji: 'mdi:network',
    icon: '$vuetify',
  },
  {
    id: 8,
    title: 'ID008',
    avatar: 'https://randomuser.me/api/portraits/women/8.jpg',
    firstname: 'Laura',
    lastname: 'Garcia',
    jobTitle: 'HR Specialist',
    salary: '$65,000',
    skills: ['Recruitment', 'Employee Relations'],
    email: 'laura.garcia@example.com',
    emoji: 'mdi:account-group',
    icon: '$vuetify',
  },
  {
    id: 9,
    title: 'ID009',
    avatar: 'https://randomuser.me/api/portraits/men/9.jpg',
    firstname: 'Robert',
    lastname: 'Martinez',
    jobTitle: 'Database Administrator',
    salary: '$75,000',
    skills: ['SQL', 'Oracle', 'Database Management'],
    email: 'robert.martinez@example.com',
    emoji: 'mdi:database',
    icon: '$vuetify',
  },
  {
    id: 10,
    title: 'ID010',
    avatar: 'https://randomuser.me/api/portraits/women/10.jpg',
    firstname: 'Karen',
    lastname: 'Robinson',
    jobTitle: 'Content Writer',
    salary: '$55,000',
    skills: ['Writing', 'Editing'],
    email: 'karen.robinson@example.com',
    emoji: 'mdi:pencil',
    icon: '$vuetify',
  },
  {
    id: 11,
    title: 'ID011',
    avatar: 'https://randomuser.me/api/portraits/men/11.jpg',
    firstname: 'William',
    lastname: 'Clark',
    jobTitle: 'UX Designer',
    salary: '$85,000',
    skills: ['Wireframing', 'User Research'],
    email: 'william.clark@example.com',
    emoji: 'mdi:draw',
    icon: '$vuetify',
  },
]

export default {
  title: 'Design System/Components/V2/SvnProAdvancedTable',
  component: SvnProAdvancedTable,
  tags: ['autodocs'],
  parameters: {
    docs: {
      description: {
        component: `
          **SvnProAdvancedTable** is a flexible and feature-rich table component that supports:
          - Customizable headers and rows
          - Filters and pagination
          - Multiple row selection behaviors via the \`variant\` prop
          - Real-time updates via events

          ### Slots:
          - **\`filter-items\`**: Slot for injecting custom filter components.
          - **\`item\`**: Slot for customizing individual rows. Exposes:
            - \`item\`: The current row's data.
            - \`index\`: The row's index.
            - \`handleRowSelection\`: Function to toggle row selection.
            - \`isChecked\`: Function to check if a row is selected.
          - **\`loading\`**: Slot for a custom loading state when data is being fetched.
          - **\`no-data\`**: Slot to display custom content when no data is available.
          - **\`bottom\`**: Slot for additional content or pagination below the table.
        `,
      },
    },
  },
  argTypes: {
    filterItemsDev: {
      description:
        'This prop is created only for testing purposes, allowing users to dynamically choose the number of filter items.',
      control: 'select',
      options: [0, 1, 2, 3, 4, 5, 6, 7],
    },
    itemsDev: {
      description:
        'This prop is created only for testing purposes, allowing users to dynamically change limit of table row.',
      control: 'select',
      options: [0, 5, 10, 20, 50],
      defaultValue: 10,
    },
    headersDev: {
      description:
        'This prop is created only for testing purposes, allowing users to dynamically change table header.',
      control: 'select',
      options: [0, 1, 2, 3, 4, 5],
      defaultValue: 14,
    },
    variant: {
      description: 'Selection behavior for the table rows selection.',
    },
    items: {
      description: 'List of items to display in the table.',
      control: 'array',
    },
    itemsPerPage: {
      description: 'Number of items displayed per page.',
      control: 'number',
      defaultValue: 10,
    },
    showSelect: {
      description: 'Whether to show checkboxes for row selection.',
      control: 'boolean',
      defaultValue: false,
    },
    showFilter: {
      description: 'Whether to display the filter button and panel.',
      control: 'boolean',
      defaultValue: true,
    },
    allowViewChange: {
      description: 'Whether to display view types toggle options buttons.',
      control: 'boolean',
      defaultValue: true,
    },
    activeFilterCount: {
      description: 'Number of active filters displayed on the filter button.',
      control: 'number',
      defaultValue: 0,
    },
    paginationTotalPages: {
      control: 'number',
      defaultValue: 1,
    },
    selectedOnResultCount: {
      description: 'Number of selected rows displayed above the table.',
      control: 'number',
      defaultValue: 0,
    },
    totalCount: {
      description: 'Total number of rows in the dataset.',
      control: 'number',
      defaultValue: 0,
    },
    onRefetch: {
      description: 'Triggered when the table requests data refetch.',
    },
    onUpdateModelValue: {
      description: 'Triggered when the selected rows change.',
    },
    onToggleFilterVisibility: {
      description: 'Triggered when the filter visibility changes.',
    },
    onSelectedItems: {
      description: 'Triggered when row selection behavior changes.',
    },
    onUpdateSelectedDataFilters: {
      description: 'Emitted when selected filter data changes.',
    },
    onSetDataAfterSearch: {
      description:
        'Emitted for dynamic searches when the search text changes for multi_select.',
    },
  },
};

export const Playground = {
  render: (args, { argTypes }) => ({
    components: { SvnProAdvancedTable, SvnProFilter, SvnProDataTableCell, SvnProButton, svnProCardLearn },

    props: Object.keys(argTypes),
    setup() {
      // Function to dynamically update items
      const updateItems = (count, items) => {
        // Validate input: ensure items is an array and count is valid
        if (!Array.isArray(items)) {
          console.warn("Invalid input: 'items' must be an array.");
          return [];
        }

        if (count <= 0) {
          console.warn("Invalid input: 'count' must be greater than 0.");
          return [];
        }

        // If the items array has fewer elements than the count, return all items
        if (items.length <= count) {
          console.log(
            'Items count is less than or equal to the requested count. Returning all items.'
          );
          return [...items]; // Return all items without modifying the original array
        }

        // Otherwise, slice the array to return exactly `count` items
        const list = items.slice(0, count); // Adjusted to return exactly `count` items
        console.log('Updated items list:', list);

        return list;
      };

      if (args.filterItems?.length && args.filterItemsDev !== undefined) {
        const list = updateItems(args.filterItemsDev, args.filterItems);

        args = {
          ...args,
          filterItems: list, // Overwrite `filterItems` dynamically
        };
      }

      if (args.itemsDev !== undefined) {
        console.log('ItemsDev changed:', args.itemsDev);
        const list = updateItems(args.itemsDev, args.items);
        args = {
          ...args,
          items: list, // Overwrite `items` dynamically
        };
      }

      if (args.headersDev !== undefined) {
        console.log('headersDev changed:', args.headersDev);
        const list = updateItems(args.headersDev, args.headers);
        args = {
          ...args,
          headers: list, // Overwrite `headers` dynamically
        };
      }

      const selected = ref([]);
      const shouldShow = ref(true);
      const selectedFilters = ref([]);

      const variant = ref('manual-selection');

      const updateVariant = (newVariant, ids) => {
        variant.value = newVariant;
        selected.value = ids;
      };

      const updateSelectedDataFilters = (filters) => {
        selectedFilters.value = filters;
        args['onUpdateSelectedDataFilters'](filters); // Call the action to show in Storybook
      };

      const toggleFilterVisibility = () => {
        shouldShow.value = !shouldShow.value; // Toggle `shouldShow` state
      };

      return {
        args,
        selected,
        shouldShow,
        variant,
        updateVariant,
        selectedFilters,
        toggleFilterVisibility,
        updateSelectedDataFilters,
      };
    },
    template: `
      <svn-pro-advanced-table
        v-bind="args"
        v-model="selected"
        :activeFilterCount="selectedFilters?.length"
        :variant="variant"
        @selected-items="updateVariant"
        @toggle-filter-visibility="toggleFilterVisibility"
      >
        <template #actions>
          <div class="flex flex-wrap items-center">
            <SvnProButton variant="text" prepend-icon="custom:mingcute:settings-5-line" text="Button 1" />
            <SvnProButton variant="text" prepend-icon="custom:mingcute:settings-5-line" text="Button 2" />
            <SvnProButton variant="text" prepend-icon="custom:mingcute:settings-5-line" text="Button 3" />
          </div>
        </template>

        <template #filter-items>
          <SvnProFilter
            v-show="shouldShow && args.filterItems?.length"
            :filterItems="args.filterItems"
            class="flex"
            @update-selected-data-filters="updateSelectedDataFilters($event)"
            @set-data-after-search="args['onSetDataAfterSearch']"
          />
        </template>

        <template #item="{ item, handleRowSelection, isChecked }">
          <tr class="hover:bg-[#46464F14]/[0.08] active:bg-[#46464F1F]/[0.12]">
            <td v-if="args.showSelect" class="px-2">
              <v-checkbox
                :model-value="isChecked(item.id)"
                hide-details
                @update:model-value="handleRowSelection(item.id)"
              />
            </td>

            <td>
              <svn-pro-data-table-cell
                cell-type="avatar_text_supporting_text"
                :avatar="item?.avatar"
                :text="item?.firstname"
                :supporting-text="item?.lastname"
              />
            </td>

            <td>
              <svn-pro-data-table-cell
                cell-type="text"
                :text="item?.jobTitle"
              />
            </td>

            <td>
              <svn-pro-data-table-cell
                cell-type="text_supporting_text"
                :text="item?.firstname"
                :supporting-text="item?.email"
              />
            </td>

            <td>
              <svn-pro-data-table-cell
                cell-type="number"
                :text="item?.salary"
              />
            </td>

            <td>
              <svn-pro-data-table-cell
                cell-type="emoji_text"
                :emoji="item?.emoji"
                :text="item?.emoji"
              />
            </td>

            <td>
              <svn-pro-data-table-cell
                cell-type="info_tag"
                :tag-items="item?.skills"
              />
            </td>

            <td>
              <svn-pro-data-table-cell
                cell-type="icon_button"
                :icon="item?.icon"
                :cell-width="50"
              />
            </td>
          </tr>
        </template>

        <template #grid-item="{ item }">
          <svn-pro-card-learn card-type="module">
          </svn-pro-card-learn>
        </template>
      </svn-pro-advanced-table>
    `,
  }),
};

Playground.args = {
  itemsDev: 10,
  filterItemsDev: 10,
  headersDev: 7,
  headers: tabHeaders,
  items: users,
  filterItems: backendFilterItems,
  itemsPerPage: 10,
  showSelect: true,
  showFilter: true,
  paginationTotalPages: 5,
  selectedOnResultCount: 1,
  totalCount: 50,
};

export const NoFilterExample = { ...Playground };

NoFilterExample.args = {
  filterItemsDev: 10,
  itemsDev: 10,
  headersDev: 7,
  headers: tabHeaders,
  items: users,
  itemsPerPage: 10,
  showSelect: true,
  showFilter: false, // Disable the filter
  paginationTotalPages: 5,
  selectedOnResultCount: 0,
  totalCount: 50,
};

export const NoSelectionExample = { ...Playground };
NoSelectionExample.args = {
  filterItemsDev: 10,
  itemsDev: 10,
  headersDev: 7,
  headers: tabHeaders,
  items: users,
  itemsPerPage: 10,
  showSelect: false, // Disable row selection
  showFilter: false,
  paginationTotalPages: 5,
  selectedOnResultCount: 0,
  totalCount: 50,
};

export const selectViewExample = { ...Playground };
selectViewExample.args = {
  filterItemsDev: 10,
  itemsDev: 10,
  headersDev: 7,
  headers: tabHeaders,
  items: users,
  itemsPerPage: 10,
  showSelect: false, // Disable row selection
  showFilter: false,
  paginationTotalPages: 5,
  selectedOnResultCount: 0,
  totalCount: 50,
  allowViewChange: true
};