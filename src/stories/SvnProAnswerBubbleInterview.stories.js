import { InterviewAppQuestionTypes } from '../constants/types.js';
import SvnProAnswerBubbleInterview from '../components/card/SvnProAnswerBubbleInterview.vue';

export default {
  title: 'Design System/Components/V2/SvnProAnswerBubbleInterview',
  component: SvnProAnswerBubbleInterview,
  tags: ['autodocs'],
  argTypes: {
    side: {
      control: 'boolean',
    },
    comment: {
      control: 'text',
    },
    user: {
      control: 'object',
    },
    crossed: {
      control: 'boolean',
    },
    selectedValue: {
      control: 'text',
    },
    questionType: {
      control: 'select',
      options: Object.values(InterviewAppQuestionTypes),
    },
    ratingLength: {
      control: 'text',
    },
    bubbleTitle: {
      control: 'text',
    },
    isLastInterviewCompletedAndLocked: {
      control: 'boolean',
    },
  },
};

export const Playground = {
  render: (args) => ({
    components: { SvnProAnswerBubbleInterview },
    setup() {
      return { args };
    },
    template: '<SvnProAnswerBubbleInterview v-bind="args" />',
  }),
  args: {
    side: false,
    comment: '',
    bubbleTitle: 'Adam Silver',
    user: {
      firstname: 'Dazai',
      lastname: 'Osamu',
      avatar: {
        50: 'https://sm.ign.com/ign_fr/cover/a/avatar-gen/avatar-generations_bssq.jpg',
      },
    },
    crossed: false,
    selectedValue: '4',
    questionType: InterviewAppQuestionTypes.open,
    ratingLength: '5',
    isLastInterviewCompletedAndLocked: false,
  },
};
