import SvnProAutocomplete from '../components/input/SvnProAutocomplete.vue';

const items = [
  {
    title: 'Element One',
    value: 'value_one',
  },
  {
    title: 'Element Two',
    value: 'value_two',
  },
  {
    title: 'Element Three',
    value: 'value_three',
  },
  {
    title: 'Element Four',
    value: 'value_four',
  },
  {
    title: 'Element Five',
    value: 'value_five',
  },
];

export default {
  title: 'Design System/Components/V2/SvnProAutocomplete',
  component: SvnProAutocomplete,
  tags: ['autodocs'],
  argTypes: {
    modelValue: {
      control: 'text',
    },
    menuProps: {
      control: 'object',
    },
    items: {
      control: 'object',
    },
    itemTitle: {
      control: 'text',
    },
    itemValue: {
      control: 'text',
    },
    prependInnerIcon: {
      control: 'text',
    },
    label: {
      control: 'text',
    },
    multiple: {
      control: 'boolean',
    },
    chips: {
      control: 'boolean',
    },
    disabled: {
      control: 'boolean',
    },
    messages: {
      control: 'text',
    },
    withSelectAll: {
      control: 'boolean',
    },
    error: {
      control: 'boolean',
    },
  },
  parameters: {
    docs: {
      description: {
        component:
          'Component for Autocomplete, shows an item or a list of items.',
      },
    },
  },
};

export const Playground = {
  render: (args) => ({
    components: { SvnProAutocomplete },
    setup() {
      return { args };
    },
    template: '<SvnProAutocomplete v-bind="args" />',
  }),
  args: {
    modelValue: undefined,
    menuProps: undefined,
    items: items,
    itemTitle: 'title',
    itemValue: 'value',
    prependInnerIcon: undefined,
    label: 'Label',
    chips: false,
    multiple: false,
    disabled: false,
    messages: undefined,
    withSelectAll: false,
    error: false,
  },
};
