import {
  createSvnProAvatar,
  AvatarType,
  AvatarSize,
} from '../factories/avatar.factory';
import anonymImage from '../assets/anonym.svg'; // Assure-toi que l'import est correct

// Créer le composant Avatar à partir de la factory
const SvnProAvatar = createSvnProAvatar({ anonymImage });

export default {
  title: 'Design System/Factories/SvnProAvatarFactory',
  component: SvnProAvatar,
  argTypes: {
    // Type d'avatar (Enum AvatarType)
    type: {
      control: 'select',
      options: Object.values(AvatarType),
      description: "Type d'avatar : photo, monogram, anonym, check",
      defaultValue: AvatarType.MONOGRAM,
    },

    // Image à afficher (pour le type photo)
    image: {
      control: 'text',
      description:
        'URL de l\'image à afficher (utilisé uniquement pour type "photo"). Si l\'image échoue ou est absente, le monogramme sera affiché.',
      defaultValue: 'https://picsum.photos/200', // Peut-être nul ou une image invalide pour tester
    },

    // Prénom (pour les monogrammes)
    firstname: {
      control: 'text',
      description:
        "Prénom pour générer l'initiale du monogramme (affiché si pas d'image).",
      defaultValue: 'John',
    },

    // Taille de l'avatar (Enum AvatarSize)
    size: {
      control: 'select',
      options: Object.values(AvatarSize),
      description: "Taille de l'avatar (en pixels)",
      defaultValue: AvatarSize.MEDIUM,
    },

    // Taille manuelle rétrocompatible
    sizeManual: {
      control: 'number',
      description: 'Taille manuelle pour rétrocompatibilité',
      defaultValue: 88,
    },

    // Position de départ
    start: {
      control: 'boolean',
      description: "Positionner l'avatar à gauche",
      defaultValue: false,
    },
  },
};

// Template pour Storybook
const Template = (args) => ({
  components: { SvnProAvatar },
  setup() {
    return { args };
  },
  template: '<SvnProAvatar v-bind="args" />',
});

// Playground pour tester toutes les combinaisons
export const Playground = Template.bind({});
Playground.args = {
  type: AvatarType.MONOGRAM,
  firstname: 'John',
  image: 'https://picsum.photos/200', // Valeur de test avec image valide
  size: AvatarSize.MEDIUM,
  start: false,
};

// Test pour l'absence d'image (fallback vers monogramme)
export const NoImageFallback = Template.bind({});
NoImageFallback.args = {
  type: AvatarType.PHOTO,
  image: '', // Cas où l'image est vide, cela doit fallback vers le monogramme
  firstname: 'Jane', // Le monogramme "J" doit être affiché
  size: AvatarSize.MEDIUM,
  start: false,
};

// Exemples spécifiques avec rétrocompatibilité
export const RetroCompatibleAvatar = Template.bind({});
RetroCompatibleAvatar.args = {
  type: AvatarType.MONOGRAM,
  firstname: 'Anna',
  sizeManual: 88, // Taille manuelle (rétrocompatibilité)
  start: false,
};

export const AnonymAvatar = Template.bind({});
AnonymAvatar.args = {
  type: AvatarType.ANONYM,
  size: AvatarSize.MEDIUM,
  start: false,
};
