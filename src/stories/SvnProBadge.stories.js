import SvnProBadge from '../components/chip/SvnProBadge.vue';

export default {
  title: 'Design System/Components/V2/SvnProBadge',
  component: SvnProBadge,
  tags: ['autodocs'],
  argTypes: {
    dot: {
      control: 'boolean',
    },
    content: {
      control: 'number',
    },
    max: {
      control: 'number',
    },
  },
  parameters: {
    docs: {
      description: {
        component:
          'Component for the Badges, The small dots around cards, avatar and others.',
      },
    },
  },
};

export const Playground = {
  render: (args) => ({
    components: { SvnProBadge },
    setup() {
      return { args };
    },
    template: '<SvnProBadge v-bind="args" />',
  }),
  args: {
    dot: false,
    max: undefined,
    content: 8,
  },
};
