import SvnProBreadcrumbs from '../components/button/SvnProBreadcrumbs.vue';

const breadcrumbItems = [
  {
    title: "Lorem Ipsum is simply dummy text of the printing and typesetting industry.",
    ellipsis: false,
    disabled: false,
    to: { name: '' },
  },
  {
    title: 'Link 2',
    ellipsis: false,
    disabled: false,
    to: { name: '' },
  },
  {
    title: 'Link 3',
    ellipsis: true,
    disabled: false,
    to: { name: '' },
  },
  {
    title: 'Link 4',
    ellipsis: false,
    disabled: true,
    to: { name: '' },
  },
  {
    title: 'Active Link',
    ellipsis: false,
    disabled: false,
    to: '',
  },
]

export default {
  title: 'Design System/Components/V2/SvnProBreadcrumbs',
  component: SvnProBreadcrumbs,
  tags: ['autodocs'],
  parameters: {
    docs: {
      description: {
        component: '**SvnProBreadcrumbs** allow you to navigate between places of your App in an instant.<br>It renders a list of SvnProBreadcrumbsItem according to the values inside the *items* argType',
      },
    },
  },
  argTypes: {
    items: {
      control: 'array',
      description: 'Defines the list of items with their props.<br> Each element should have these properties: <br>- **title:** The title of the Breadcrumb item<br>- **disabled:** Puts the BreadcrumbItem in a disabed state (grayed out & not click events)<br>- **ellipsis:** Whether or not the BreadcrumbsItem should be wrapped with "..."<br>- **to:** An object representing the destination where we want to navigate, of type RouteLocationRaw.<br><u>Example:</u> { name: "home" }. You can also add query parameters as you would normally with a classic "router.push()"',
    },
    divider: {
      control: 'text',
      description: 'Defines the divider used between two BreadcrumbsItem.',
    },
    activeColor: {
      control: 'text',
      description: 'Defines the color of the active BreadcrumbsItem.',
    },
  },
};

export const Playground = {
  render: (args) => ({
    components: { SvnProBreadcrumbs },
    setup() {
      return { args };
    },
    template: '<SvnProBreadcrumbs v-bind="args" />',
  }),
  args: {
    divider: '/',
    activeColor: 'primary',
    items: breadcrumbItems,
  },
};
