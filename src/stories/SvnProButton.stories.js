import SvnProButton from '../components/button/SvnProButton.vue';

export default {
  title: 'Design System/Components/V2/SvnProButton',
  component: SvnProButton,
  tags: ['autodocs'],
  argTypes: {
    text: {
      control: 'text',
    },
    color: {
      control: 'text',
    },
    variant: {
      control: 'select',
      options: ['elevated', 'flat', 'text', 'tonal', 'outlined', 'plain'],
    },
    prependIcon: {
      control: 'text',
    },
    disabled: {
      control: 'boolean',
    },
    loading: {
      control: 'boolean',
    },
    block: {
      control: 'boolean',
    },
  },
  parameters: {
    docs: {
      description: {
        component: 'Component for the Button, with many variants.',
      },
    },
  },
};

export const Playground = {
  render: (args) => ({
    components: { SvnProButton },
    setup() {
      return { args };
    },
    template: '<SvnProButton v-bind="args" />',
  }),
  args: {
    text: 'Button',
    color: 'primary',
    variant: 'elevated',
    prependIcon: undefined,
    disabled: false,
    loading: false,
    block: false,
  },
};
