import SvnProButtonStickyContainer from '../components/button/SvnProButtonStickyContainer.vue';

export default {
  title: 'Design System/Components/V2/SvnProButtonStickyContainer',
  component: SvnProButtonStickyContainer,
  tags: ['autodocs'],
  argTypes: {
    configuration: {
      control: 'select',
      options: ['horizontal', 'stacked'],
    },
    primaryActionText: {
      control: 'text',
    },
    secondaryActionText: {
      control: 'text',
    },
    hasPrimaryAction: {
      control: 'boolean',
    },
    hasSecondaryAction: {
      control: 'boolean',
    },
  },
  parameters: {
    docs: {
      description: {
        component:
          'Component for the Sticky container usually at the bottom of Modals/Dialogs.',
      },
    },
  },
};

export const Playground = {
  render: (args) => ({
    components: { SvnProButtonStickyContainer },
    setup() {
      return { args };
    },
    template: '<SvnProButtonStickyContainer v-bind="args" />',
  }),
  args: {
    configuration: 'horizontal',
    primaryActionText: 'Label',
    secondaryActionText: 'Label',
    hasPrimaryAction: true,
    hasSecondaryAction: true,
  },
};
