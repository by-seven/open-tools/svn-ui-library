import SvnProButtonToggle from "../components/button/SvnProButtonToggle.vue";
import { ButtonVariants, ComponentDensities, SizeEnum, ComponentBorderRadiuses, ComponentBorders } from '../constants/types';

const SvnProButtonItems = [
  {
    value: 'one',
    icon: 'custom:mingcute:settings-5-line',
    disabled: false,
  },
  {
    value: 'two',
    icon: 'custom:mingcute:settings-5-line',
    disabled: false,
  },
  {
    value: 'three',
    icon: 'custom:mingcute:settings-5-line',
    disabled: false,
  },
  {
    value: 'four',
    icon: 'custom:mingcute:settings-5-line',
    disabled: false,
  },
]

export default {
  title: "Design System/Components/V2/SvnProButtonToggle",
  component: SvnProButtonToggle,
  tags: ["autodocs"],
  parameters: {
    docs: {
      description: {
        component: '**Toggle buttons** allow you to create a styled group of buttons that can be selected or toggled.',
      },
    },
  },
  argTypes: {
    items: {
      control: 'array',
      description: 'Defines the list of SvnProIconButtons with their props.<br> Each element should have a **"value"** property to identify each button.',
    },
    multiple: {
      control: 'boolean',
      description: 'Defines if the user can select more than one SvnProIconButton.',
    },
    divided: {
      control: 'boolean',
      description: 'Defines if the toggle button have a divider between each button.',
    },
    mandatory: {
      control: 'boolean',
      description: 'Defines if the user has to absolutely select a button.',
    },
    variant: {
      control: 'select',
      description: 'Defines the variant of the SvnProIconButtonToggle.',
      options: Object.values(ButtonVariants),
    },
    border: {
      control: 'select',
      description: 'Designates the border-radius applied to the component. This can be xs, sm, md, lg, xl.',
      options: Object.values(ComponentBorders),
    },
    rounded: {
      control: 'select',
      description: 'Defines the border radius of the Toggle Button.',
      options: Object.values(ComponentBorderRadiuses),
    },
    color: {
      control: 'text',
      description: 'Defines the color of the active(s) SvnProIconButton.',
    },
    density: {
      control: 'select',
      description: 'Defines the spacing<br>(default, compact, comfortable).',
      options: Object.values(ComponentDensities),
    },
    size: {
      control: 'select',
      description: 'Defines the size of each SvnProIconButton<br>(x-small, small, default, large, x-large).',
      options: Object.values(SizeEnum),
    },
  },
};

export const Playground = {
  render: (args) => ({
    components: { SvnProButtonToggle },
    setup() {
      return { args };
    },
    template: '<SvnProButtonToggle v-bind="args" />',
  }),
  args: {
    items: SvnProButtonItems,
    multiple: false,
    divided: true,
    mandatory: true,
    variant: ButtonVariants.flat,
    border: ComponentBorders.FALSE,
    rounded: ComponentBorderRadiuses.SM,
    color: 'primary',
    density: ComponentDensities.DEFAULT,
    size: SizeEnum.DEFAULT,
  },
};
