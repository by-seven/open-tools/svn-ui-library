import SvnCardTest from './Components/SvnCardTest.vue';

export default {
  title: 'Design System/Components/V2/SvnProCard',
  component: SvnCardTest,
  tags: ['autodocs'],
  argTypes: {
    variant: {
      control: 'select',
      options: ['outlined', 'elevated', 'flat'],
    },
  },
  parameters: {
    docs: {
      description: {
        component:
          'Component for the Card, a container where you put all and everything.',
      },
    },
  },
};

export const Playground = {
  render: (args) => ({
    components: { SvnCardTest },
    setup() {
      return { args };
    },
    template: '<SvnCardTest v-bind="args" />',
  }),
  args: {
    variant: 'outlined',
  },
};
