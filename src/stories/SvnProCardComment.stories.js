import SvnProCardComment from '../components/card/SvnProCardComment.vue';

export default {
  title: 'Design System/Components/V2/SvnProCardComment',
  component: SvnProCardComment,
  tags: ['autodocs'],
  argTypes: {
    commentLoading: { control: 'boolean', description: 'Indicator if the comment is loading (used in AI)' },
    username: { control: 'text', description: "Le nom d'utilisateur affiché" },
    date: { control: 'date', description: 'Date du commentaire' },
    comment: { control: 'text', description: 'Contenu du commentaire' },
    avatar: {
      control: 'text',
      description:
        'URL de l\'image à afficher (utilisé uniquement pour type "photo"). Si l\'image échoue ou est absente, le monogramme sera affiché.',
      defaultValue: 'https://picsum.photos/200',
    },
    textComponentType: {
      control: 'select',
      options: ['subtitle', 'body'], // Limite les options
      description:
        "Type de composant de texte utilisé pour afficher le nom d'utilisateur",
    },
    replies: {
      control: 'number',
      description:
        "Nombre de réponses au commentaire. Change le texte du bouton 'Reply' en fonction.",
      defaultValue: 0,
    },
    onClickAppButton: {
      action: 'clicked',
      description: 'Événement au clic du bouton REPLY',
    },
  },
  parameters: {
    docs: {
      description: {
        component:
          "Composant pour afficher un commentaire avec avatar, nom d'utilisateur, date, texte, et bouton 'Reply'. Le type de texte utilisé pour le nom d'utilisateur peut être limité à 'subtitle' ou 'body'. Le nombre de réponses ('replies') modifie dynamiquement le texte du bouton.",
      },
    },
  },
};

const Template = (args) => ({
  components: { SvnProCardComment },
  setup() {
    return { args };
  },
  template: '<SvnProCardComment v-bind="args" />',
});

export const Playground = Template.bind({});

Playground.args = {
  username: 'User name',
  date: '2023-07-12T19:55:00',
  commentLoading: false,
  comment:
    'House glory aragog wormtail azkaban no bagman armchairs disciplinary. Aragog under mr lights boggarts smile grim spell forest. Doe snape robes tail locket ickle teacup.',
  avatar: 'https://picsum.photos/200', // Exemple d'avatar
  textComponentType: 'subtitle', // Valeur par défaut pour le type de texte
  replies: 0, // Valeur par défaut pour les réponses
};

// Test pour l'absence d'image (fallback vers monogramme)
export const NoImage = Template.bind({});
NoImage.args = {
  username: 'User name',
  avatar: undefined, // Cas où l'image est vide, cela doit fallback vers le monogramme
  date: '2023-07-12T19:55:00',
  textComponentType: 'subtitle', // Valeur par défaut pour le type de texte
  comment:
    'House glory aragog wormtail azkaban no bagman armchairs disciplinary. Aragog under mr lights boggarts smile grim spell forest. Doe snape robes tail locket ickle teacup.',
  replies: 0, // Valeur par défaut pour les réponses
};
