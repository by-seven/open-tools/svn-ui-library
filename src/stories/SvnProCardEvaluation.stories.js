import SvnProCardEvaluation from '../components/card/SvnProCardEvaluation.vue';
import { EvaluationTypes, ComponentSizes } from '../constants/types';

export default {
  title: 'Design System/Components/V2/SvnProCardEvaluation',
  component: SvnProCardEvaluation,
  tags: ['autodocs'],
  argTypes: {
    evaluationTitle: {
      control: 'text',
      description: 'Title of the evaluation section',
    },
    evaluationItems: {
      control: 'array',
      description: 'Array of evaluation types',
    },
    size: {
      control: { type: 'select' },
      options: Object.values(ComponentSizes),
      description: 'Select the size of the component (compact or default)',
    },
    error: {
      control: 'boolean',
      description: 'Show error message if true',
    },
  },
  parameters: {
    docs: {
      description: {
        component:
          'Component to select different types of evaluation like Self-assessment, Quiz, or Face-to-face.',
      },
    },
  },
};

// Template without slot (default)
const Template = (args) => ({
  components: { SvnProCardEvaluation },
  setup() {
    return { args };
  },
  template:
    '<SvnProCardEvaluation v-bind="args" @update:selectedEvaluationType="args.updateEvaluationType" />',
});

// Playground story (default usage)
export const Playground = Template.bind({});
Playground.args = {
  evaluationTitle: 'Choose Evaluation Type',
  evaluationItems: [
    {
      type: EvaluationTypes.SELF_ASSESSMENT,
      icon: 'noto:check-mark-button',
      title: 'Self-assessment',
      subtitle:
        'The learner must confirm they have fully understood the module before proceeding.',
    },
    {
      type: EvaluationTypes.QUIZ,
      icon: 'noto:red-question-mark',
      title: 'Quiz',
      subtitle:
        'The learner must pass the quiz with a score of 100% correct answers to successfully complete this section.',
    },
    {
      type: EvaluationTypes.FACE_TO_FACE,
      icon: 'noto:busts-in-silhouette',
      title: 'Face-to-face',
      subtitle:
        "The learner's answers will be evaluated in real time by an expert, ensuring accurate assessment and personalized feedback.",
    },
  ],
  size: ComponentSizes.default,
  error: false,
};

// Compact size story
export const Compact = Template.bind({});
Compact.args = {
  ...Playground.args,
  size: ComponentSizes.compact,
};

// Story with an error state
export const WithError = Template.bind({});
WithError.args = {
  ...Playground.args,
  error: true,
};

// Story using a slot to add additional content
export const WithSlot = (args) => ({
  components: { SvnProCardEvaluation },
  setup() {
    return { args };
  },
  template: `
    <SvnProCardEvaluation v-bind="args" @update:selectedEvaluationType="args.updateEvaluationType">
      <!-- Slot content to be rendered below the evaluation options -->
      <div class="mt-4 p-4 border rounded bg-gray-100">
        <h4>Additional Slot Content</h4>
        <p>This content is injected into the slot of SvnProCardEvaluation.</p>
        <button @click="handleSlotAction">Click Me</button>
      </div>
    </SvnProCardEvaluation>
  `,
  methods: {
    handleSlotAction() {
      alert('Slot button clicked!');
    },
  },
});

// Define default args for the story with a slot
WithSlot.args = {
  ...Playground.args,
};
