import SvnProCardHomepage from '../components/card/SvnProCardHomepage.vue';

export default {
  title: 'Design System/Components/V2/SvnProCardHomepage',
  component: SvnProCardHomepage,
  tags: ['autodocs'],
  argTypes: {
    appIcon: {
      control: 'text',
    },
    appColor: {
      control: 'text',
    },
    appName: {
      control: 'text',
    },
    appDescription: {
      control: 'text',
    },
    appLabel: {
      control: 'text',
    },
  },
  parameters: {
    docs: {
      description: {
        component:
          'Component for the Homepage Card, displaying the App Name, and a button.',
      },
    },
  },
};

export const Playground = {
  render: (args) => ({
    components: { SvnProCardHomepage },
    setup() {
      return { args };
    },
    template: '<SvnProCardHomepage v-bind="args" />',
  }),
  args: {
    appIcon: 'mingcute:chat-3-line',
    appColor: 'primary',
    appName: 'Interview',
    appDescription:
      'Lorem ipsum dolor sit amet consectetur. Dignissim eget integer.',
    appLabel: '',
  },
};
