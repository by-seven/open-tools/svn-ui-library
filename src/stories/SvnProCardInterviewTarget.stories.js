import SvnProCardInterviewTarget from '../components/card/SvnProCardInterviewTarget.vue';
import {
  ComponentSizes,
  CardInterviewTargetTypes,
  ButtonVariants,
  InfotagStyles,
  TargetIndicatorStatuses,
  InterviewCompletionStatuses,
  TargetObjectiveIndicatorTypes,
} from '../constants/types.js';

export default {
  title: 'Design System/Components/V2/SvnProCardInterviewTarget',
  component: SvnProCardInterviewTarget,
  tags: ['autodocs'],
  argTypes: {
    size: {
      control: 'select',
      options: Object.values(ComponentSizes),
    },
    type: {
      control: 'select',
      options: Object.values(CardInterviewTargetTypes),
    },
    comments: {
      control: 'text',
    },
    answerLabel: {
      control: 'text',
    },
    createPrependicon: {
      control: 'text',
    },
    deleteIcon: {
      control: 'text',
    },
    buttonText: {
      control: 'text',
    },
    infoText: {
      control: 'text',
    },
    noDeadlineText: {
      control: 'text',
    },
    interviewLocked: {
      control: 'boolean',
    },
    isFinalUpdate: {
      control: 'boolean',
    },
    hasEmployeeSuggestion: {
      control: 'boolean',
    },
    hasManagerOrCrossedSuggestion: {
      control: 'boolean',
    },
    interviewStatus: {
      control: 'select',
      options: Object.values(InterviewCompletionStatuses),
    },
    tagStyle: {
      control: 'select',
      options: Object.values(InfotagStyles),
    },
    buttonVariant: {
      control: 'select',
      options: Object.values(ButtonVariants),
    },
    deleteButtonVariant: {
      control: 'select',
      options: Object.values(ButtonVariants),
    },
    targetTitle: {
      control: 'text',
    },
    targetDeadline: {
      control: 'text',
    },
    targetDescription: {
      control: 'text',
    },
    variableLabelOne: {
      control: 'text',
    },
    variableLabelTwo: {
      control: 'text',
    },
    comments: {
      contorl: 'text',
    },
    tooltipOneContent: {
      control: 'text',
    },
    tooltipTwoContent: {
      control: 'text',
    },
    hasManagerOrCrossedSuggestionAndSubmitted: {
      control: 'boolean',
    },
    targetObjectiveIndicator: {
      control: 'object',
    },
  },
};

export const Playground = {
  render: (args) => ({
    components: { SvnProCardInterviewTarget },
    setup() {
      return { args };
    },
    template: '<SvnProCardInterviewTarget v-bind="args" />',
  }),
  args: {
    type: 'create',
    size: 'compact',
    deleteIcon: 'custom:mingcute:delete-2-line',
    createPrependicon: 'custom:mingcute:add-line',
    buttonText: 'Create new target',
    buttonVariant: 'flat',
    tagStyle: 'filled',
    answerLabel: 'Answer',
    commentLabel: 'Comment',
    comments: 'Comments',
    isFinalUpdate: false,
    interviewLocked: false,
    hasEmployeeSuggestion: false,
    hasManagerOrCrossedSuggestion: false,
    hasManagerOrCrossedSuggestionAndSubmitted: false,
    interviewStatus: 'in_progress',
    deleteButtonVariant: 'outlined',
    noDeadlineText: 'Invalid date',
    targetTitle: 'New target',
    targetDeadline: '25 oct.',
    targetDescription:
      'Targets added will be create and add to Roadmap when the Cross Review will be submitted.',
    infoText:
      'This target is still a draft. Complete all interviews of the set to create it.',
    variableLabelOne: 'Suggested updates',
    variableLabelTwo: 'Your suggested update',
    tooltipOneContent: 'Tooltip one',
    tooltipTwoContent: 'Tooltip two',
    targetObjectiveIndicator: {
      status: 'completed',
      indicator_type: 'boolean',
      options: {
        starting_value: '3',
        current_value: '17',
        target_value: '45',
        multi_choice_list: [
          {
            option: 'Singapore',
            checkbox: false,
          },
          {
            option: 'Paris',
            checkbox: true,
          },
          {
            option: 'London',
            checkbox: false,
          },
          {
            option: 'New-York',
            checkbox: false,
          },
          {
            option: 'Beijing',
            checkbox: false,
          },
        ],
      },
    },
  },
};
