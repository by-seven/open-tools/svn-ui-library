import SvnProCardLearn from '../components/card/SvnProCardLearn.vue';

const playlistModules = [
  {
    title: 'Feast norwegian willow years glass',
    cover_url: {
      50: 'https://sm.ign.com/ign_fr/cover/a/avatar-gen/avatar-generations_bssq.jpg',
    },
    duration: '1h30min',
    submission_status: 'acquired',
    themes: [
      { name: 'Learn' },
      { name: 'Aleph' },
      { name: 'Hotêsse' },
      { name: 'Sevener' },
      { name: 'Modules Seven' },
      { name: 'onboarding big mamma' },
      { name: 'hygiène niveau 1' },
      { name: 'valeurs big mamma' },
      { name: 'biomatériaux' },
      { name: 'tissium' },
      { name: 'Communication' },
      { name: 'Leadership' },
      { name: 'Prise de parole' },
      { name: 'BeCoS' },
      { name: 'cxwwx' },
    ]
  },
  {
    title: 'Drops fantastic which die many',
    cover_url: {
      50: 'https://sm.ign.com/ign_fr/cover/a/avatar-gen/avatar-generations_bssq.jpg',
    },
    duration: '45min',
    submission_status: 'not_acquired',
    themes: [{ name: 'Theme 1' }, { name: 'Theme long 2' }],
  },
  {
    title: 'Chess portrait who lived wizard',
    cover_url: {
      50: 'https://sm.ign.com/ign_fr/cover/a/avatar-gen/avatar-generations_bssq.jpg',
    },
    duration: '45min',
    submission_status: 'not_acquired',
    themes: [
      { name: 'Theme 1' },
      { name: 'Theme long 2' },
      { name: 'Theme long 3 qsghdvjqs dqsdhqd' },
    ],
  },
];

export default {
  title: 'Design System/Components/V2/SvnProCardLearn',
  component: SvnProCardLearn,
  tags: ['autodocs'],
  argTypes: {
    state: {
      control: 'select',
      options: ['not_started', 'in_progress', 'completed'],
    },
    cardType: {
      control: 'select',
      options: [
        'playlist',
        'module',
        'module_in_playlist',
        'playlist_in_training_edit',
        'playlist_in_training_show',
      ],
    },
    cardImage: {
      control: 'text',
    },
    editText: {
      control: 'text',
    },
    deleteText: {
      control: 'text',
    },
    cardTitle: {
      control: 'text',
    },
    duration: {
      control: 'text',
    },
    remaining: {
      control: 'text',
    },
    count: {
      control: 'number',
    },
    progress: {
      control: 'number',
    },
    isFavorited: {
      control: 'boolean',
    },
    editable: {
      control: 'boolean',
    },
    shareable: {
      control: 'boolean',
    },
    playlistImages: {
      control: 'array',
    },
    themes: {
      control: 'array',
    },
    acquiredText: {
      control: 'text',
    },
    acquiredIcon: {
      control: 'text',
    },
    reactionCount: {
      control: 'number',
    },
    isAcquired: {
      control: 'boolean',
    },
    cardSize: {
      control: 'select',
      options: ['default', 'compact'],
    },
    reactionList: {
      control: 'array',
    },
  },
  parameters: {
    docs: {
      description: {
        component: 'Component for the cards of Learn',
      },
    },
  },
};

export const Playground = {
  render: (args) => ({
    components: { SvnProCardLearn },
    setup() {
      return { args };
    },
    template: '<SvnProCardLearn v-bind="args" />',
  }),
  args: {
    state: 'not_started',
    cardType: 'playlist',
    cardImage:
      'https://sm.ign.com/ign_fr/cover/a/avatar-gen/avatar-generations_bssq.jpg',
    cardTitle:
      'Formation hygiène niveau 1 pour pizzaiolos niveau 1 pour pizzaiolosert',
    duration: '2h35',
    remaining: '50 min left',
    count: 23,
    progress: 50,
    isFavorited: false,
    editable: true,
    editText: 'Edit module',
    deleteText: 'Delete module',
    playlistImages: [
      'https://sm.ign.com/ign_fr/cover/a/avatar-gen/avatar-generations_bssq.jpg',
      'https://sm.ign.com/ign_fr/cover/a/avatar-gen/avatar-generations_bssq.jpg',
      'https://sm.ign.com/ign_fr/cover/a/avatar-gen/avatar-generations_bssq.jpg',
      'https://sm.ign.com/ign_fr/cover/a/avatar-gen/avatar-generations_bssq.jpg',
    ],
    themes: [
      'Theme 1',
      'Theme long 2',
      'Theme long 3 qsghdvjqs dqsdhqd',
      'Theme 4',
      'Theme 5',
      'Theme 41',
      'Theme 50',
    ],
    acquiredText: 'Acquired',
    acquiredIcon: 'mingcute:check-circle-fill',
    reactionCount: 12,
    reactionList: ['like', 'recommend', 'question', 'remark'],
    shareable: true,
    isAcquired: false,
    cardSize: 'default',
    playlistModules: playlistModules,
  },
};
