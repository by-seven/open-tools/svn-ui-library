import SvnProCardReaction from '../components/card/SvnProCardReaction.vue';

export default {
  title: 'Design System/Components/V2/SvnProCardReaction',
  component: SvnProCardReaction,
  tags: ['autodocs'],
  argTypes: {
    feedback: {
      control: 'boolean',
    },
    feedbackTitle: {
      control: 'text',
    },
    cardSize: {
      control: 'select',
      options: ['default', 'compact'],
    },
    feedbackDescription: {
      control: 'text',
    },
  },
  parameters: {
    docs: {
      description: {
        component: 'Component for the Reaction Card, used in Learn.',
      },
    },
  },
};

export const Playground = {
  render: (args) => ({
    components: { SvnProCardReaction },
    setup() {
      return { args };
    },
    template: '<SvnProCardReaction v-bind="args" />',
  }),
  args: {
    feedback: false,
    cardSize: 'default',
    feedbackTitle: 'What did you think of the module?',
    feedbackDescription: 'Your feedback helps us improve this module.',
    reactions: [
      {
        value: 'like',
        icon: 'https://lottie.host/?file=1d730887-ff07-4bcb-916a-1c47a1fc2191/tBJxPeERxp.json',
        text: 'I like it',
        active: false,
      },
      {
        value: 'recommend',
        icon: 'https://lottie.host/?file=f503d535-88db-4e36-b0b3-a061d09424f7/XWharmF7Hb.json',
        text: 'I recommend',
        active: false,
      },
    ],
  },
};
