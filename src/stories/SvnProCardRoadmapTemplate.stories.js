import SvnProCardRoadmapTemplate from '../components/card/SvnProCardRoadmapTemplate.vue';

export default {
  title: 'Design System/Components/V2/SvnProCardRoadmapTemplate',
  component: SvnProCardRoadmapTemplate,
  tags: ['autodocs'],
  argTypes: {
    icon: {
      control: 'text',
    },
    title: {
      control: 'text',
    },
    subtitle: {
      control: 'text',
    },
    disabled: {
      control: 'boolean',
    },
    value: {
      control: 'text',
    },
    items: {
      control: 'array',
    },
  },
  parameters: {
    docs: {
      description: {
        component: 'Component for the Card of Roadmap template',
      },
    },
  },
};

export const Playground = {
  render: (args) => ({
    components: { SvnProCardRoadmapTemplate },
    setup() {
      return { args };
    },
    template: '<SvnProCardRoadmapTemplate v-bind="args" />',
  }),
  args: {
    indicatorIcon: 'mingcute:sale-line',
    indicatorName: 'Percent',
    targetTitle:
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua',
    targetDeadline: '2023-04-24',
  },
};
