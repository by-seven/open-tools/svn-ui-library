import SvnProCardSectionEvaluationShow from '../components/card/SvnProCardSectionEvaluationShow.vue';

export default {
  title: 'Design System/Components/V2/SvnProCardSectionEvaluationShow',
  component: SvnProCardSectionEvaluationShow,
  tags: ['autodocs'],
  argTypes: {
    cardType: {
      control: 'select',
      options: ['default', 'compact'],
    },
    cardTitle: {
      control: 'text',
    },
    cardDescription: {
      control: 'text',
    },
    cardButtonText: {
      control: 'text',
    },
  },
  parameters: {
    docs: {
      description: {
        component: 'Component for the Card Evaluation Show',
      },
    },
  },
};

export const Playground = {
  render: (args) => ({
    components: { SvnProCardSectionEvaluationShow },
    setup() {
      return { args };
    },
    template: '<SvnProCardSectionEvaluationShow v-bind="args" />',
  }),
  args: {
    cardType: 'default',
    cardTitle: "🙌 You've reached the end! Ready for the quiz ?",
    cardDescription: 'To complete this module, evaluate yourself with a quiz.',
    cardButtonText: 'Start quiz',
  },
};
