import SvnProCardSectionQuizResultLearn from '../components/card/SvnProCardSectionQuizResultLearn.vue';
import { ComponentSizes } from '@/constants/types.js';
import { SvnProCardSectionQuizResultLearnType } from '@/components/card/SvnProCardSectionQuizResultLearn.vue';
import { action } from '@storybook/addon-actions';

export default {
  title: 'Design System/Components/V2/SvnProCardSectionQuizResultLearn',
  component: SvnProCardSectionQuizResultLearn,
  tags: ['autodocs'],
  argTypes: {
    type: {
      control: 'select',
      options: Object.values(SvnProCardSectionQuizResultLearnType),
    },
    size: {
      control: 'select',
      options: Object.values(ComponentSizes),
    },
    title: {
      control: 'text',
    },
    description: {
      control: 'text',
    },
    hasSecondaryAction: {
      control: 'boolean',
    },
    hasTertiaryAction: {
      control: 'boolean',
    },
    primaryText: {
      control: 'text',
    },
    secondaryText: {
      control: 'text',
    },
    tertiaryText: {
      control: 'text',
    },
  },
  parameters: {
    docs: {
      description: {
        component: 'Component for the Card Section Quiz Learn',
      },
    },
  },
};

export const Playground = {
  render: (args) => ({
    components: { SvnProCardSectionQuizResultLearn },
    setup() {
      return { args };
    },
    template: `
      <SvnProCardSectionQuizResultLearn
        v-bind="args"
        @primary-click="primaryClick"
        @secondary-click="secondaryClick"
        @tertiary-click="tertiaryClick"
      />`,
    methods: {
      primaryClick: action('primaryClick', (event) => {
        return event;
      }),
      secondaryClick: action('primaryClick', (event) => {
        return event;
      }),
      tertiaryClick: action('primaryClick', (event) => {
        return event;
      }),
    },
  }),
  args: {
    type: 'fail',
    size: 'compact',
    title: '👌 Here is one more module acquired',
    description: 'Congratulations, you just acquired a new module, keep it up!',
  },
};
