import SvnProCardUser from "../components/card/SvnProCardUser.vue";
import { SvnProCardSizes } from '../constants/types';

export default {
  title: "Design System/Components/V2/SvnProCardUser",
  component: SvnProCardUser,
  tags: ["autodocs"],
  parameters: {
    docs: {
      description: {
        component: '**SvnProCardUser** is a modular user card component that can be used in many pages across the app.',
      },
    },
  },
  argTypes: {
    size: {
      control: 'select',
      options: Object.values(SvnProCardSizes),
      description: 'Defines the size of the component.',
    },
    userName: {
      control: 'text',
      description: 'Defines the username of the user.',
    },
    userAvatar: {
      control: 'text',
      description: 'Defines the avatar of the user.',
    },
    secondaryInfo: {
      control: 'text',
      description: 'Defines a secondary info field fot the user.',
    },
    hasAvatar: {
      control: 'boolean',
      description: 'Defines if an avatar is shown in the top section.',
    },
    hasActions: {
      control: 'boolean',
      description: 'Defines if the card has buttons in the bottom section.',
    },
    showDivider: {
      control: 'boolean',
      description: 'Defines if a divider is shown between the top and bottom sections.',
    },
    hasMoreActions: {
      control: 'boolean',
      description: 'Defines if the card has a more actions buttons menu.',
    },
    showInfoTags: {
      control: 'boolean',
      description: 'Defines if info tags are shown in the card.',
    },
    infotagItems: {
      control: 'array',
      description: 'Defines the list of apps, they will be shown if `showInfoTags` is set to true. Each item has the following properties:<br>- **text:** the value inside the infotag<br>- **icon:** the icon to be prepended before the text of the infotag.',
    },
    menuItems: {
      control: 'array',
      description: 'Defines the list of items of the menu. Each item can take the following properties:<br>- **show:** displays or not the component (displays by default)<br>- **to:** route destination to go to (VueRouterLocation)<br>- **icon:** icon to prepend before the text<br>- **disabled:** tells if the menu item is disabled<br>- **onClick:** click event when the menu item is clicked<br>- **error:** if true, the menu item is displayed with the error color<br>- **title:** the title of the menu item.',
    },
    primaryButton: {
      control: 'object',
      description: 'Defines the primary button details. An Object with the following properties:<br>- **icon:** the prepend icon of the button<br>- **text:** the main text of the button<br>- **onClick:** the event fired when the button is clicked.',
    },
    secondaryButton: {
      control: 'object',
      description: 'Defines the secondary button details. An Object with the following properties:<br>- **icon:** the prepend icon of the button<br>- **text:** the main text of the button<br>- **onClick:** the event fired when the button is clicked.',
    }
  },
};

export const Playground = {
  render: (args) => ({
    components: { SvnProCardUser },
    setup() {
      return { args };
    },
    template: '<SvnProCardUser v-bind="args" />',
  }),
  args: {
    size: SvnProCardSizes.DEFAULT,
    userName: 'Annalisa Papagna',
    userAvatar: '',
    secondaryInfo: 'Designer UX/UI',
    hasAvatar: true,
    hasActions: true,
    showDivider: true,
    hasMoreActions: true,
    showInfoTags: true,
    menuItems: [
      {
        value: 'update',
        title: 'Update user',
        error: false,
      },
      {
        value: 'delete',
        title: 'Delete user',
        error: true,
      },
    ],
    infotagItems: [
      {
        text: '6 Campaigns',
        icon: 'mingcute:chat-3-line'
      },
      {
        text: '4 Trainings',
        icon: 'mingcute:mortarboard-line'
      },
      {
        text: '20 Campaigns',
        icon: 'mingcute:target-line'
      },
    ],
    primaryButton: {
      icon: 'custom:mingcute:group-line',
      text: 'View team (6)',
      onClick: () => {}
    },
    secondaryButton: {
      icon: 'custom:mingcute:eye-2-line',
      text: 'View profile',
      onClick: () => {}
    },
  },
};
