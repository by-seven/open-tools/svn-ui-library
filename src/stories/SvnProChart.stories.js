import SvnProChart from '../components/chart/SvnProChart.vue';
import { chartTypes, TabAlignTypes } from '../constants/types';

const chartDataBarAndDoughnut = {
  labels: ['France', 'Spain', 'United States'],
  datasets: [
    {
      cutout: '50%',
      label: 'Not started',
      backgroundColor: ['#BA1A1A', '#CA8100', '#1C6D1E'],
      data: [32, 14, 21],
    },
  ],
}

const chartDataLine = {
  labels: ['France', 'Spain', 'United States', 'UK', 'Iran', 'Congo', 'Iceland'],
  datasets: [{
    label: 'My First Dataset',
    data: [65, 59, 80, 81, 56, 55, 40],
    fill: true,
    backgroundColor: '#3E52CA',
    borderColor: '#3E52CA',
    borderJoinStyle: 'round',
    borderWidth: 3,
    pointHoverBorderWidth: 6,
    pointBackgroundColor: '#3E52CA',
    pointBorderWidth: 2,
    pointStyle: 'circle',
    pointRadius: 3,
    tension: 0,
  }],
}

const tabItems = [
  {
    value: 'one',
    icon: 'custom:mingcute:settings-5-line',
    text: 'Tab 1',
    class: '',
    chartPlugins: {},
    chartType: 'bar',
    chartData: chartDataBarAndDoughnut,
  },
  {
    value: 'two',
    icon: 'custom:mingcute:settings-5-line',
    text: 'Tab 2',
    class: '',
    chartPlugins: {},
    chartType: 'doughnut',
    chartData: chartDataBarAndDoughnut,
  },
  {
    value: 'three',
    icon: 'custom:mingcute:settings-5-line',
    text: 'Tab 3',
    class: '',
    chartPlugins: {},
    chartType: 'line',
    chartData: chartDataLine,
  },
]

export default {
  title: 'Design System/Components/V2/SvnProChart',
  component: SvnProChart,
  tags: ['autodocs'],
  parameters: {
    docs: {
      description: {
        component: '**SvnProChart** allow you to visualize your data in multiple ways.',
      },
    },
  },
  argTypes: {
    itemValue: {
      control: 'text',
      description: 'Defines if the key vuetify should look for to identify an item of the select.',
    },
    categories: {
      control: 'array',
      description: 'Defines if the list of items of the select',
    },
    chartData: {
      control: 'object',
      description: 'Defines the data of the chart to plot.',
    },
    withCategory: {
      control: 'boolean',
      description: 'Defines if the select is shown.',
    },
    chartType: {
      control: 'select',
      options: Object.values(chartTypes),
      description: 'Defines the chart type that is going to be rendered.',
    },
    itemTitle: {
      control: 'text',
      description: 'Defines if the title vuetify should look for to identify an item of the select.',
    },
    removeXMarkers: {
      control: 'boolean',
      description: 'Defines if the horizontal markers are deleted.',
    },
    removeYMarkers: {
      control: 'boolean',
      description: 'Defines if the vertical markers are deleted.',
    },
    stepSize: {
      control: 'number',
      description: 'Defines if the vertical markers are deleted.',
    },
    beginAtZero: {
      control: 'boolean',
      description: 'Defines if the y legend values are forced to begin at 0 (The default behaviour is to begin at the lower found value).',
    },
    chartPlugins: {
      control: 'object',
      description: 'Defines the plugins object passed to the chart.',
    },
    chartTitle: {
      control: 'text',
      description: 'Defines the title of the chart, inside the card.',
    },
    chartDescription: {
      control: 'text',
      description: 'Defines the description of the chart, inside the card.',
    },
    categoryLabel: {
      control: 'text',
      description: 'Defines the label inside the select.',
    },
    withTabs: {
      control: 'boolean',
      description: 'Defines whether tabs should be displayed in the component. Goes in accordance with the prop tabItems.'
    },
    tabItems: {
      control: 'array',
      description: 'Defines the list of v-tabs with their props.<br> Each element should have a **"value"** property to identify each button.<br>The list of the properties are: <br>-icon<br>-value<br>-text<br>-class',
    },
    showArrows: {
      control: 'boolean',
      description: 'Defines if pagination arrows are shown if the tab items overflow their container.<br><u>DISCLAIMER:</u> If the tab items overflow their container, pagination controls will appear on desktop. For mobile devices, arrows will only display with the **show-arrows** prop.',
    },
    growTabs: {
      control: 'boolean',
      description: 'Defines if the tabs should take up all available space.',
    },
    alignTabs: {
      control: 'select',
      description: 'Defines how to align the tabs relative to their container.<br>Possible values are: title, start, end, center.',
      options: Object.values(TabAlignTypes),
    },
  },
};

export const Playground = {
  render: (args) => ({
    components: { SvnProChart },
    setup() {
      return { args };
    },
    template: '<SvnProChart v-bind="args" />',
  }),
  args: {
    itemValue: 'id',
    categories: [
      {
        id: 'france',
        title: 'France',
      },
      {
        id: 'spain',
        title: 'Spain',
      },
      {
        id: 'united_states',
        title: 'United States',
      },
    ],
    chartData: chartDataBarAndDoughnut,
    withCategory: false,
    chartType: chartTypes.doughnut,
    itemTitle: 'title',
    removeXMarkers: false,
    removeYMarkers: false,
    beginAtZero: false,
    stepSize: undefined,
    chartPlugins: {},
    chartTitle: 'Chart title',
    chartDescription: 'Chart description',
    categoryLabel: 'Category',
    growTabs: false,
    tabItems: tabItems,
    showArrows: false,
    alignTabs: 'start',
    withTabs: false,
  },
};
