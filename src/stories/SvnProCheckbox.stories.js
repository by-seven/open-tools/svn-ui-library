import SvnProCheckbox from '../components/input/SvnProCheckbox.vue';

export default {
  title: 'Design System/Components/V2/SvnProCheckbox',
  component: SvnProCheckbox,
  tags: ['autodocs'],
  argTypes: {
    color: {
      control: 'text',
    },
    label: {
      control: 'text',
    },
    error: {
      control: 'boolean',
    },
    disabled: {
      control: 'boolean',
    },
    indeterminate: {
      control: 'boolean',
    },
    rules: {
      control: 'array',
    },
  },
  parameters: {
    docs: {
      description: {
        component:
          'Component for the Checkbox, A box that you click to toggle on/off.',
      },
    },
  },
};

export const Playground = {
  render: (args) => ({
    components: { SvnProCheckbox },
    setup() {
      return { args };
    },
    template: '<SvnProCheckbox v-bind="args" />',
  }),
  args: {
    color: 'primary',
    label: undefined,
    error: false,
    disabled: false,
    indeterminate: false,
    rules: [],
  },
};
