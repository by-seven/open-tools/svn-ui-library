import SvnProChip from '../components/chip/SvnProChip.vue';
import SvnChipTest from './Components/SvnChipTest.vue';

export default {
  title: 'Design System/Components/V2/SvnProChip',
  component: SvnProChip,
  tags: ['autodocs'],
  argTypes: {
    variant: {
      control: 'select',
      options: ['input', 'assist', 'filter', 'suggestion'],
    },
    text: {
      control: 'text',
    },
    prependIcon: {
      control: 'text',
    },
    appendIcon: {
      control: 'text',
    },
    closable: {
      control: 'boolean',
    },
    avatar: {
      control: 'boolean',
    },
    filterIcon: {
      control: 'text',
    },
    avatarType: {
      control: 'select',
      options: ['photo', 'monogram', 'anonym', 'check'],
    },
    avatarImage: {
      control: 'text',
    },
    disabled: {
      control: 'boolean',
    },
    elevation: {
      control: 'number',
    },
  },
  parameters: {
    docs: {
      description: {
        component: 'Component for the Chip, usually used within a chip group.',
      },
    },
  },
};

export const Playground = {
  render: (args) => ({
    components: { SvnProChip },
    setup() {
      return { args };
    },
    template: '<SvnProChip v-bind="args" />',
  }),
  args: {
    variant: 'input',
    text: 'Label',
    prependIcon: undefined,
    appendIcon: undefined,
    closable: false,
    avatar: false,
    filterIcon: undefined,
    disabled: false,
    elevation: undefined,
    avatarType: 'monogram',
    avatarImage: undefined,
  },
};

export const ChipGroup = {
  render: (args) => ({
    components: { SvnChipTest },
    setup() {
      return { args };
    },
    template: '<SvnChipTest v-bind="args" />',
  }),
  args: {
    variant: 'input',
    chips: ['One', 'Two', 'Three'],
    prependIcon: undefined,
    appendIcon: undefined,
    closable: false,
    avatar: false,
    filterIcon: undefined,
    disabled: false,
    elevation: undefined,
    avatarType: 'monogram',
    avatarImage: undefined,
  },
};
