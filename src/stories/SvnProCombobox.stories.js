import SvnProCombobox from '../components/input/SvnProCombobox.vue';

const items = [
  {
    title: 'Element One',
    value: 'value_one',
  },
  {
    title: 'Element Two',
    value: 'value_two',
  },
  {
    title: 'Element Three',
    value: 'value_three',
  },
  {
    title: 'Element Four',
    value: 'value_four',
  },
  {
    title: 'Element Five',
    value: 'value_five',
  },
];

export default {
  title: 'Design System/Components/V2/SvnProCombobox',
  component: SvnProCombobox,
  tags: ['autodocs'],
  argTypes: {
    modelValue: {
      control: 'text',
    },
    menuProps: {
      control: 'object',
    },
    items: {
      control: 'object',
    },
    itemTitle: {
      control: 'text',
    },
    itemValue: {
      control: 'text',
    },
    prependInnerIcon: {
      control: 'text',
    },
    label: {
      control: 'text',
    },
    multiple: {
      control: 'boolean',
    },
    chips: {
      control: 'boolean',
    },
    disabled: {
      control: 'boolean',
    },
    messages: {
      control: 'text',
    },
    withSelectAll: {
      control: 'boolean',
    },
    error: {
      control: 'boolean',
    },
  },
  parameters: {
    docs: {
      description: {
        component:
          'Component for the Combobox, shows an item or a list of items, and you can add your own items.',
      },
    },
  },
};

export const Playground = {
  render: (args) => ({
    components: { SvnProCombobox },
    setup() {
      return { args };
    },
    template: '<SvnProCombobox v-bind="args" />',
  }),
  args: {
    modelValue: undefined,
    menuProps: undefined,
    items: items,
    itemTitle: 'title',
    itemValue: 'value',
    prependInnerIcon: undefined,
    label: 'Label',
    multiple: false,
    disabled: false,
    chips: false,
    messages: undefined,
    withSelectAll: false,
    error: false,
  },
};
