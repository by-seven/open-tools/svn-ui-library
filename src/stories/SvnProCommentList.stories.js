import SvnProCommentList from "../components/list/SvnProCommentList.vue";

export default {
  title: "Design System/Components/V2/SvnProCommentList",
  component: SvnProCommentList,
  tags: ["autodocs"],
  argTypes: {
    HeaderTitle: {
      control: "text",
    },
    selectedComment: {
      control: "object",
    },
    isMobile: {
      control: "boolean",
    },
    drawerLarge: {
      control: "boolean",
    },
    isInThread: {
      control: "boolean",
    },
  },
  parameters: {
    docs: {
      description: {
        component: "Component for the List comments",
      },
    },
  },
};

export const Playground = {
  render: (args) => ({
    components: { SvnProCommentList },
    setup() {
      return { args };
    },
    template: '<SvnProCommentList v-bind="args" />',
  }),
  args: {
    HeaderTitle: undefined,
    selectedComment: {},
    isMobile: false,
    drawerLarge: false,
    isInThread: false,
  },
};
