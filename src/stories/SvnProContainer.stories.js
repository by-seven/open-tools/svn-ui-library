import { ContainerVariants } from "../constants/types.js";
import SvnProContainer from '../components/layout/SvnProContainer.vue';

export default {
  title: 'Design System/Components/V2/SvnProContainer',
  component: SvnProContainer,
  tags: ['autodocs'],
  parameters: {
    docs: {
      description: {
        component: '**SvnProContainer** is the component used in many pages for the layout to center the content.<br>It supports 2 different variants: "narrow" and "wide".<br>The default variant is <b>wide</b>',
      },
    },
  },
  argTypes: {
    variant: {
      control: 'select',
      options: [ContainerVariants.WIDE, ContainerVariants.NARROW],
      description: 'The variant of the Container',
    },
  }
}

export const Playground = {
  render: (args, { argTypes }) => ({
    components: { SvnProContainer },
    props: Object.keys(argTypes),
    setup() {
      return { args };
    },
    template: `
      <SvnProContainer
        v-bind="args"
      />
    `,
  }),
  args: {
    variant: ContainerVariants.WIDE
  }
}