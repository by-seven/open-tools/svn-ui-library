import SvnDataTableTest from './Components/SvnDataTableTest.vue';

export default {
  title: 'Design System/Components/V2/SvnProDataTable',
  component: SvnDataTableTest,
  tags: ['autodocs'],
  argTypes: {
    headers: {
      control: 'array',
    },
    items: {
      control: 'array',
    },
    density: {
      control: 'select',
      options: ['default', 'compact', 'comfortable'],
    },
    itemValue: {
      control: 'text',
    },
    showSelect: {
      control: 'boolean',
    },
    page: {
      control: 'number',
    },
    itemsPerPage: {
      control: 'number',
    },
    paginationTotalPages: {
      control: 'number',
    },
  },
  parameters: {
    docs: {
      description: {
        component: 'Component for the Data Table.',
      },
    },
  },
};

export const Playground = {
  render: (args) => ({
    components: { SvnDataTableTest },
    setup() {
      return { args };
    },
    template: '<SvnDataTableTest v-bind="args" />',
  }),
  args: {
    itemValue: 'title',
    density: 'default',
    showSelect: true,
    page: 1,
    itemsPerPage: 5,
    paginationTotalPages: 9,
    headers: [
      {
        align: 'start',
        key: 'firstname',
        sortable: true,
        title: 'User',
      },
      {
        align: 'start',
        key: 'jobTitle',
        sortable: true,
        title: 'Job',
      },
      {
        align: 'start',
        key: 'email',
        sortable: true,
        title: 'Email',
      },
      {
        align: 'start',
        key: 'salary',
        sortable: true,
        title: 'Salary',
      },
      {
        align: 'start',
        key: 'emoji',
        sortable: true,
        title: 'Emoji',
      },
      {
        align: 'start',
        key: 'skills',
        sortable: false,
        title: 'Skills',
      },
      {
        align: 'start',
        key: 'button',
        sortable: false,
        title: '',
      },
    ],
    items: [
      {
        title: 'ID001',
        avatar: 'https://randomuser.me/api/portraits/men/1.jpg',
        firstname: 'John',
        lastname: 'Doe',
        jobTitle: 'Software Engineer',
        salary: '$85,000',
        skills: ['JavaScript', 'React', 'Node.js'],
        email: 'john.doe@example.com',
        emoji: 'mdi:account',
        icon: '$vuetify',
      },
      {
        title: 'ID002',
        avatar: 'https://randomuser.me/api/portraits/women/2.jpg',
        firstname: 'Jane',
        lastname: 'Smith',
        jobTitle: 'Data Analyst',
        salary: '$70,000',
        skills: ['Python', 'SQL'],
        email: 'jane.smith@example.com',
        emoji: 'mdi:chart-bar',
        icon: '$vuetify',
      },
      {
        title: 'ID003',
        avatar: 'https://randomuser.me/api/portraits/men/3.jpg',
        firstname: 'James',
        lastname: 'Brown',
        jobTitle: 'Project Manager',
        salary: '$90,000',
        skills: ['Agile', 'Scrum', 'Leadership'],
        email: 'james.brown@example.com',
        emoji: 'mdi:clipboard-outline',
        icon: '$vuetify',
      },
      {
        title: 'ID004',
        avatar: 'https://randomuser.me/api/portraits/women/4.jpg',
        firstname: 'Emily',
        lastname: 'Johnson',
        jobTitle: 'Graphic Designer',
        salary: '$60,000',
        skills: ['Photoshop', 'Illustrator'],
        email: 'emily.johnson@example.com',
        emoji: 'mdi:palette',
        icon: '$vuetify',
      },
      {
        title: 'ID005',
        avatar: 'https://randomuser.me/api/portraits/men/5.jpg',
        firstname: 'Michael',
        lastname: 'Williams',
        jobTitle: 'Marketing Manager',
        salary: '$95,000',
        skills: ['SEO', 'Content Marketing', 'Social Media'],
        email: 'michael.williams@example.com',
        emoji: 'mdi:bullhorn',
        icon: '$vuetify',
      },
      {
        title: 'ID006',
        avatar: 'https://randomuser.me/api/portraits/women/6.jpg',
        firstname: 'Sarah',
        lastname: 'Davis',
        jobTitle: 'Product Manager',
        salary: '$100,000',
        skills: ['Product Development', 'Roadmap Planning'],
        email: 'sarah.davis@example.com',
        emoji: 'mdi:clipboard-check-outline',
        icon: '$vuetify',
      },
      {
        title: 'ID007',
        avatar: 'https://randomuser.me/api/portraits/men/7.jpg',
        firstname: 'David',
        lastname: 'Miller',
        jobTitle: 'Network Engineer',
        salary: '$80,000',
        skills: ['Networking', 'Cisco', 'Firewall'],
        email: 'david.miller@example.com',
        emoji: 'mdi:network',
        icon: '$vuetify',
      },
      {
        title: 'ID008',
        avatar: 'https://randomuser.me/api/portraits/women/8.jpg',
        firstname: 'Laura',
        lastname: 'Garcia',
        jobTitle: 'HR Specialist',
        salary: '$65,000',
        skills: ['Recruitment', 'Employee Relations'],
        email: 'laura.garcia@example.com',
        emoji: 'mdi:account-group',
        icon: '$vuetify',
      },
      {
        title: 'ID009',
        avatar: 'https://randomuser.me/api/portraits/men/9.jpg',
        firstname: 'Robert',
        lastname: 'Martinez',
        jobTitle: 'Database Administrator',
        salary: '$75,000',
        skills: ['SQL', 'Oracle', 'Database Management'],
        email: 'robert.martinez@example.com',
        emoji: 'mdi:database',
        icon: '$vuetify',
      },
      {
        title: 'ID010',
        avatar: 'https://randomuser.me/api/portraits/women/10.jpg',
        firstname: 'Karen',
        lastname: 'Robinson',
        jobTitle: 'Content Writer',
        salary: '$55,000',
        skills: ['Writing', 'Editing'],
        email: 'karen.robinson@example.com',
        emoji: 'mdi:pencil',
        icon: '$vuetify',
      },
      {
        title: 'ID011',
        avatar: 'https://randomuser.me/api/portraits/men/11.jpg',
        firstname: 'William',
        lastname: 'Clark',
        jobTitle: 'UX Designer',
        salary: '$85,000',
        skills: ['Wireframing', 'User Research'],
        email: 'william.clark@example.com',
        emoji: 'mdi:draw',
        icon: '$vuetify',
      },
      {
        title: 'ID012',
        avatar: 'https://randomuser.me/api/portraits/women/12.jpg',
        firstname: 'Lisa',
        lastname: 'Lewis',
        jobTitle: 'Business Analyst',
        salary: '$77,000',
        skills: ['Business Analysis', 'Requirements Gathering'],
        email: 'lisa.lewis@example.com',
        emoji: 'mdi:chart-line',
        icon: '$vuetify',
      },
      {
        title: 'ID013',
        avatar: 'https://randomuser.me/api/portraits/men/13.jpg',
        firstname: 'Thomas',
        lastname: 'Walker',
        jobTitle: 'System Administrator',
        salary: '$70,000',
        skills: ['Linux', 'Windows Server'],
        email: 'thomas.walker@example.com',
        emoji: 'mdi:server',
        icon: '$vuetify',
      },
      {
        title: 'ID014',
        avatar: 'https://randomuser.me/api/portraits/women/14.jpg',
        firstname: 'Angela',
        lastname: 'Young',
        jobTitle: 'Sales Manager',
        salary: '$90,000',
        skills: ['Salesforce', 'Lead Generation'],
        email: 'angela.young@example.com',
        emoji: 'mdi:chart-areaspline',
        icon: '$vuetify',
      },
      {
        title: 'ID015',
        avatar: 'https://randomuser.me/api/portraits/men/15.jpg',
        firstname: 'Daniel',
        lastname: 'Hall',
        jobTitle: 'Operations Manager',
        salary: '$88,000',
        skills: ['Operations Management', 'Logistics'],
        email: 'daniel.hall@example.com',
        emoji: 'mdi:cogs',
        icon: '$vuetify',
      },
      {
        title: 'ID016',
        avatar: 'https://randomuser.me/api/portraits/women/16.jpg',
        firstname: 'Patricia',
        lastname: 'Allen',
        jobTitle: 'Customer Service Representative',
        salary: '$45,000',
        skills: ['Customer Service', 'CRM'],
        email: 'patricia.allen@example.com',
        emoji: 'mdi:headset',
        icon: '$vuetify',
      },
      {
        title: 'ID017',
        avatar: 'https://randomuser.me/api/portraits/men/17.jpg',
        firstname: 'Matthew',
        lastname: 'Hernandez',
        jobTitle: 'Mobile Developer',
        salary: '$85,000',
        skills: ['Android', 'iOS'],
        email: 'matthew.hernandez@example.com',
        emoji: 'mdi:cellphone',
        icon: '$vuetify',
      },
      {
        title: 'ID018',
        avatar: 'https://randomuser.me/api/portraits/women/18.jpg',
        firstname: 'Nancy',
        lastname: 'King',
        jobTitle: 'Legal Advisor',
        salary: '$105,000',
        skills: ['Contract Law', 'Corporate Law'],
        email: 'nancy.king@example.com',
        emoji: 'mdi:balance-scale',
        icon: '$vuetify',
      },
      {
        title: 'ID019',
        avatar: 'https://randomuser.me/api/portraits/men/19.jpg',
        firstname: 'Brian',
        lastname: 'Lopez',
        jobTitle: 'Financial Analyst',
        salary: '$75,000',
        skills: ['Financial Modeling', 'Excel'],
        email: 'brian.lopez@example.com',
        emoji: 'mdi:finance',
        icon: '$vuetify',
      },
      {
        title: 'ID020',
        avatar: 'https://randomuser.me/api/portraits/women/20.jpg',
        firstname: 'Barbara',
        lastname: 'Scott',
        jobTitle: 'Digital Marketer',
        salary: '$60,000',
        skills: ['SEO', 'PPC', 'Analytics'],
        email: 'barbara.scott@example.com',
        emoji: 'mdi:google-analytics',
        icon: '$vuetify',
      },
      {
        title: 'ID021',
        avatar: 'https://randomuser.me/api/portraits/men/21.jpg',
        firstname: 'Steven',
        lastname: 'Green',
        jobTitle: 'Cloud Engineer',
        salary: '$110,000',
        skills: ['AWS', 'Azure', 'Google Cloud'],
        email: 'steven.green@example.com',
        emoji: 'mdi:cloud',
        icon: '$vuetify',
      },
      {
        title: 'ID022',
        avatar: 'https://randomuser.me/api/portraits/women/22.jpg',
        firstname: 'Jessica',
        lastname: 'Adams',
        jobTitle: 'Office Manager',
        salary: '$50,000',
        skills: ['Administration', 'Office Management'],
        email: 'jessica.adams@example.com',
        emoji: 'mdi:office-building',
        icon: '$vuetify',
      },
      {
        title: 'ID023',
        avatar: 'https://randomuser.me/api/portraits/men/23.jpg',
        firstname: 'Christopher',
        lastname: 'Nelson',
        jobTitle: 'Quality Assurance Engineer',
        salary: '$78,000',
        skills: ['Testing', 'Automation'],
        email: 'christopher.nelson@example.com',
        emoji: 'mdi:check-circle-outline',
        icon: '$vuetify',
      },
      {
        title: 'ID024',
        avatar: 'https://randomuser.me/api/portraits/women/24.jpg',
        firstname: 'Mary',
        lastname: 'Baker',
        jobTitle: 'Technical Writer',
        salary: '$65,000',
        skills: ['Technical Writing', 'Documentation'],
        email: 'mary.baker@example.com',
        emoji: 'mdi:file-document-outline',
        icon: '$vuetify',
      },
      {
        title: 'ID025',
        avatar: 'https://randomuser.me/api/portraits/men/25.jpg',
        firstname: 'Andrew',
        lastname: 'Perez',
        jobTitle: 'Security Analyst',
        salary: '$82,000',
        skills: ['Cybersecurity', 'Risk Assessment'],
        email: 'andrew.perez@example.com',
        emoji: 'mdi:shield-outline',
        icon: '$vuetify',
      },
    ],
  },
};
