import SvnProDatePicker from '../components/picker/SvnProDatePicker.vue';

export default {
  title: 'Design System/Components/V2/SvnProDatePicker',
  component: SvnProDatePicker,
  tags: ['autodocs'],
  argTypes: {
    modelValue: {
      control: 'text',
    },
    color: {
      control: 'text',
    },
    title: {
      control: 'text',
    },
    disabled: {
      control: 'boolean',
    },
    actions: {
      control: 'boolean',
    },
    range: {
      control: 'boolean',
    },
  },
  parameters: {
    docs: {
      description: {
        component:
          'Component for the Date Picker, you can select month, week, day. You can also select multiple dates, a range of dates',
      },
    },
  },
};

export const Playground = {
  render: (args) => ({
    components: { SvnProDatePicker },
    setup() {
      return { args };
    },
    template: '<SvnProDatePicker v-bind="args" />',
  }),
  args: {
    modelValue: undefined,
    color: 'primary',
    title: 'Select time',
    range: false,
    actions: false,
    disabled: false,
  },
};
