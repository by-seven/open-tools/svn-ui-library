import SvnProDialog from '../components/dialog/SvnProDialog.vue';

export default {
  title: 'Design System/Components/V2/SvnProDialog',
  component: SvnProDialog,
  tags: ['autodocs'],
  argTypes: {
    title: {
      control: 'text',
    },
    subtitle: {
      control: 'text',
    },
    contentHeight: {
      control: 'number',
    },
    icon: {
      control: 'text',
    },
    actionOneTitle: {
      control: 'text',
    },
    actionTwoTitle: {
      control: 'text',
    },
    contentText: {
      control: 'text',
    },
  },
  parameters: {
    docs: {
      description: {
        component: 'Component for the Dialog.',
      },
    },
  },
};

export const Playground = {
  render: (args) => ({
    components: { SvnProDialog },
    setup() {
      return { args };
    },
    template: '<SvnProDialog v-bind="args" />',
  }),
  args: {
    title: 'Basic dialog title',
    contentText:
      'A modal is a type of dilaog that appears in front of app content to provide critical information, or prompt for a decision to be made.',
    subtitle: undefined,
    icon: undefined,
    contentHeight: undefined,
    actionOneTitle: undefined,
    actionTwoTitle: undefined,
  },
};
