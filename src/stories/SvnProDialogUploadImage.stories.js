import SvnProDialogUploadImage from '../components/dialog/SvnProDialogUploadImage.vue';

export default {
  title: 'Design System/Components/V2/SvnProDialogUploadImage',
  component: SvnProDialogUploadImage,
  tags: ['autodocs'],
  argTypes: {},
  parameters: {
    docs: {
      description: {
        component: 'Component for the Upload Imag ePannel, with 3 tabs.',
      },
    },
  },
};

export const Playground = {
  render: (args) => ({
    components: { SvnProDialogUploadImage },
    setup() {
      return { args };
    },
    template: '<SvnProDialogUploadImage v-bind="args" />',
  }),
  args: {},
};
