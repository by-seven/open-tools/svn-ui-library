import SvnProDivider from '../components/divider/SvnProDivider.vue';

export default {
  title: 'Design System/Components/V2/SvnProDivider',
  component: SvnProDivider,
  tags: ['autodocs'],
  argTypes: {
    color: {
      control: 'text',
    },
    vertical: {
      control: 'boolean',
    },
    inset: {
      control: 'boolean',
    },
    thickness: {
      control: 'number',
    },
  },
  parameters: {
    docs: {
      description: {
        component: 'Component for the Divider, just a long line.',
      },
    },
  },
};

export const Playground = {
  render: (args) => ({
    components: { SvnProDivider },
    setup() {
      return { args };
    },
    template: '<SvnProDivider v-bind="args" />',
  }),
  args: {
    color: 'primary',
    vertical: false,
    inset: false,
    thickness: 1,
  },
};
