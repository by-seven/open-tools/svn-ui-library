import SvnProEmptyStates from '../components/states/SvnProEmptyStates.vue';

export default {
  title: 'Design System/Components/V2/SvnProEmptyStates',
  component: SvnProEmptyStates,
  tags: ['autodocs'],
  argTypes: {
    variant: {
      control: 'select',
      options: ['index', 'results'],
    },
    size: {
      control: 'select',
      options: ['default', 'compact'],
    },
    title: {
      control: 'text',
    },
    supportingText: {
      control: 'text',
    },
    actionPrimaryTitle: {
      control: 'text',
    },
    actionSecondaryTitle: {
      control: 'text',
    },
    actions: {
      control: 'boolean',
    },
  },
  parameters: {
    docs: {
      description: {
        component: 'Component for the Empty states, usually inside indexes.',
      },
    },
  },
};

export const Playground = {
  render: (args) => ({
    components: { SvnProEmptyStates },
    setup() {
      return { args };
    },
    template: '<SvnProEmptyStates v-bind="args" />',
  }),
  args: {
    variant: 'index',
    size: 'default',
    title: undefined,
    supportingText: undefined,
    actionPrimaryTitle: undefined,
    actionSecondaryTitle: undefined,
    actions: false,
  },
};
