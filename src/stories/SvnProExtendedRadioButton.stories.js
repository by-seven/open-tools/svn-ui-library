import SvnProExtendedRadioButton from '../components/button/SvnProExtendedRadioButton.vue';
import SvnProExtendedRadioButtonTest from './Components/SvnProExtendedRadioButtonTest.vue';

const items = [
  {
    value: 'item_one',
    icon: 'noto:handshake',
    title: 'Item One',
    subtitle: 'Lorem ipsum dolor sit amet consectetur. Dignissim eget integer.',
    disabled: false,
  },
  {
    value: 'item_two',
    icon: 'noto:books',
    title: 'Item Two',
    subtitle: 'Lorem ipsum dolor sit amet consectetur. Dignissim eget integer.',
    disabled: false,
  },
];

export default {
  title: 'Design System/Components/V2/SvnProExtendedRadioButton',
  component: SvnProExtendedRadioButton,
  tags: ['autodocs'],
  argTypes: {
    icon: {
      control: 'text',
    },
    title: {
      control: 'text',
    },
    subtitle: {
      control: 'text',
    },
    disabled: {
      control: 'boolean',
    },
    value: {
      control: 'text',
    },
    items: {
      control: 'array',
    },
  },
  parameters: {
    docs: {
      description: {
        component:
          'Component for the Extended Radio Button. TO BE USED EXCLUSIVELY WITH V-ITEM-GROUP OTHERWISE COMPILING ERROR',
      },
    },
  },
};

export const Playground = {
  render: (args) => ({
    components: { SvnProExtendedRadioButton },
    setup() {
      return { args };
    },
    template: '<SvnProExtendedRadioButton v-bind="args" />',
  }),
  args: {
    icon: 'noto:handshake',
    title: 'Label title',
    subtitle: 'Lorem ipsum dolor sit amet consectetur. Dignissim eget integer.',
    disabled: false,
    value: undefined,
  },
};

export const MultipleExtendedRadioButton = {
  render: (args) => ({
    components: { SvnProExtendedRadioButtonTest },
    setup() {
      return { args };
    },
    template: '<SvnProExtendedRadioButtonTest v-bind="args" />',
  }),
  args: {
    items: items,
  },
};
