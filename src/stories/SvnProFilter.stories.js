import { expect, within } from '@storybook/test';
import SvnProFilter from '@/components/filters/SvnProFilter.vue';

export default {
  title: 'Design System/Components/V2/SvnProFilter',
  component: SvnProFilter,
  tags: ['autodocs'],
  parameters: {
    docs: {
      description: {
        component: `
          **SvnProFilter** is a versatile filter component used on index pages.
          It integrates with filter chips and supports dynamic behavior for
          managing filters, clearing all filters, and updating filter states.

          ### Events
          - **\`update-selected-data-filters(payload)\`**:
            Emitted whenever the selected filter data changes.
            - **Payload**: \`[{ name: string, type: string, value: any, operator: string }]\`
          - **\`set-data-after-search(filterName, searchText)\`**:
            Emitted during dynamic searches (e.g., autocomplete).
            - **Payload**: \`filterName: string, searchText: string\`
          - **\`load-more-data(filterName)\`**:
            Emitted when a request is made to load more data for a specific filter (infinite scrolling).
            - **Payload**: \`filterName: string\`        `,
      },
    },
  },
  argTypes: {
    filterItems: {
      control: 'array',
      description: 'List of filter definitions passed to the component.',
    },
    onUpdateSelectedDataFilters: {
      description: 'Emitted when selected filter data changes.',
    },
    onSetDataAfterSearch: {
      description:
        'Emitted for dynamic searches when the search text changes for multi_select.',
    },
    onLoadMoreData: {
      description:
        'Emitted when the user scrolls to the end of the content, triggering a dynamic data load for infinite or lazy loading scenarios.',
    },
  },
};

const backendFilterItems = [
  {
    title: 'Campaign title',
    type: 'text',
    operatorOptions: ['contains'],
    value: '',
    operator: null,
    name: 'title',
  },
  {
    title: 'Manager',
    type: 'multi_select',
    operatorOptions: ['contains'],
    value: [],
    name: 'manager_id',
    items: [
      { id: 4277, fullname: 'Harold ARMIJO LEON' },
      { id: 7569, fullname: 'Edouard BOURÉLY' },
      { id: 7268, fullname: 'Lisa DOYEN' },
      { id: 3, fullname: 'Yahya FALLAH' },
      { id: 4278, fullname: 'Virginie FAURÉ' },
      { id: 3993, fullname: 'William FAVREAU' },
      { id: 4276, fullname: 'Thomas FRAUDET' },
      { id: 6207, fullname: 'Maxime GRZESIEWSKI' },
      { id: 6314, fullname: 'Estelle MARET' },
      { id: 6286, fullname: 'Daniel Stéphane MBAPPE' },
    ],
  },
  {
    title: 'Period',
    type: 'date',
    operatorOptions: ['before', 'after'],
    value: '',
    operator: null,
    name: 'startline',
  },
  {
    title: 'Participants',
    type: 'number',
    operatorOptions: ['equal', 'not_equal', 'greater_than', 'less_than'],
    value: '',
    operator: null,
    name: 'employees_count',
  },
  {
    title: 'Campaign type',
    type: 'multi_select',
    operatorOptions: ['contains'],
    value: '',
    operator: null,
    name: 'campaign_type',
    items: ['OneToOne', 'Feedback', 'Survey'],
  },
  {
    title: 'Completion',
    type: 'number',
    operatorOptions: ['equal', 'not_equal', 'greater_than', 'less_than'],
    value: '',
    operator: null,
    name: 'completion',
  },
];

export const Playground = {
  render: (args, { argTypes }) => ({
    components: { SvnProFilter },
    props: Object.keys(argTypes),
    setup() {
      return { args };
    },
    template: `
      <SvnProFilter
        v-bind="args"
      />
    `,
  }),
  args: {
    filterItems: backendFilterItems,
  },
};

export const WithAdditionalFilters = {
  render: (args, { argTypes }) => ({
    components: { SvnProFilter },
    props: Object.keys(argTypes),
    setup() {
      return { args };
    },
    template: `
      <SvnProFilter v-bind="args">
        <template #additional-filters>
          <div data-testid="additional-filters">
            <v-chip-group selected-class="bg-primary text-surface" multiple>
              <v-chip
                v-for="(chip, index) in ['One', 'Two', 'Three']"
                :key="index"
                draggable
                :filter="variant !== 'assist'"
                variant="outlined"
                :text="chip"
                :value="chip"
                rounded="lg"
                :disabled="false"
                :closable="false"
              />
            </v-chip-group>
          </div>
        </template>
      </SvnProFilter>
    `,
  }),
  args: {
    filterItems: backendFilterItems
  },
  play: async ({ canvasElement }) => {
    const canvas = within(canvasElement);

    const additionalFilters = await canvas.findByTestId('additional-filters');
    expect(additionalFilters).toBeTruthy();

    const chipOne = await canvas.findByText('One');
    const chipTwo = await canvas.findByText('Two');
    const chipThree = await canvas.findByText('Three');

    expect(chipOne).toBeTruthy();
    expect(chipTwo).toBeTruthy();
    expect(chipThree).toBeTruthy();
  },
};