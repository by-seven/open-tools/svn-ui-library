import SvnProFilterItemChip from '@/components/filters/SvnProFilterItemChip.vue';
import { FilterType } from '@/constants/types';

export default {
  title: 'Design System/Components/V2/SvnProFilterItemChip',
  component: SvnProFilterItemChip,
  tags: ['autodocs'],
  parameters: {
    docs: {
      description: {
        component: `
          **SvnProFilterItemChip** is a filter chip component for representing and managing filters.
          It supports various filter types, operator options, and dynamic data handling.

          ### Events
          - **\`update-selected-data-filters(payload)\`**:
            Emitted when filter data changes.
            - **Payload**: \`{ value: any, operator: string }\`
          - **\`set-data-after-search(searchText)\`**:
            Emitted for dynamic searches (e.g., autocomplete).
            - **Payload**: \`string\`
          - **\`click(event)\`**:
            Emitted when the filter chip is clicked.
            - **Payload**: \`MouseEvent\`

          ### Props
          - **title**: Title of the filter.
          - **type**: Type of the filter (\`text\`, \`number\`, \`date\`, \`multi_select\`).
          - **operatorOptions**: Available operator options (e.g., \`equals\`, \`contains\`).
          - **items**: Options for \`multi_select\` type.
        `,
      },
    },
  },
  argTypes: {
    title: {
      control: 'text',
      description: 'Filter title displayed on the chip.',
    },
    type: {
      control: 'select',
      options: [
        FilterType.TEXT,
        FilterType.NUMBER,
        FilterType.DATE,
        FilterType.MULTI_SELECT,
      ],
      description: 'Type of the filter.',
    },
    operatorOptions: {
      control: 'array',
      description: 'List of operator options for the filter.',
    },
    items: {
      control: 'array',
      description: 'Available options for multi-select filter type.',
    },
    onUpdateSelectedDataFilters: {
      description: 'Emitted when selected filter data changes.',
    },
    onSetDataAfterSearch: {
      description:
        'Emitted for dynamic searches when the search text changes for multi_select.',
    },
  },
};

const items = [
  { id: 1, fullname: 'John Doe' },
  { id: 2, fullname: 'Jane Smith' },
  { id: 3, fullname: 'Alice Johnson' },
];

export const Playground = {
  render: (args, { argTypes }) => ({
    components: { SvnProFilterItemChip },
    props: Object.keys(argTypes),
    setup() {
      return { args };
    },
    template: `
      <SvnProFilterItemChip
        v-bind="args"
      />
    `,
  }),
  args: {
    title: 'Dynamic Filter',
    type: FilterType.TEXT,
    operatorOptions: ['equals', 'contains', 'starts_with'],
  },
};

// Specific Filter Examples
export const TextFilter = {
  render: (args, { argTypes }) => ({
    components: { SvnProFilterItemChip },
    props: Object.keys(argTypes),
    setup() {
      return { args };
    },
    template: `
      <SvnProFilterItemChip
        v-bind="args"
      />
    `,
  }),
  args: {
    title: 'Campaign Title',
    type: FilterType.TEXT,
    operatorOptions: ['equals', 'contains', 'starts_with'],
  },
};

export const NumberFilter = {
  render: (args, { argTypes }) => ({
    components: { SvnProFilterItemChip },
    props: Object.keys(argTypes),
    setup() {
      return { args };
    },
    template: `
      <SvnProFilterItemChip
        v-bind="args"
      />
    `,
  }),
  args: {
    title: 'Employee Count',
    type: FilterType.NUMBER,
    operatorOptions: ['equals', 'greater_than', 'less_than'],
  },
};

export const DateFilter = {
  render: (args, { argTypes }) => ({
    components: { SvnProFilterItemChip },
    props: Object.keys(argTypes),
    setup() {
      return { args };
    },
    template: `
      <SvnProFilterItemChip
        v-bind="args"
      />
    `,
  }),
  args: {
    title: 'Start Date',
    type: FilterType.DATE,
    operatorOptions: ['before', 'after', 'equals'],
  },
};

export const MultiSelectFilter = {
  render: (args, { argTypes }) => ({
    components: { SvnProFilterItemChip },
    props: Object.keys(argTypes),
    setup() {
      return { args };
    },
    template: `
      <SvnProFilterItemChip
        v-bind="args"
      />
    `,
  }),
  args: {
    title: 'Assigned Users',
    type: FilterType.MULTI_SELECT,
    items: [
      { id: 1, fullname: 'John Doe' },
      { id: 2, fullname: 'Jane Smith' },
      { id: 3, fullname: 'Alice Johnson' },
    ],
    operatorOptions: ['contains', 'not_contains'],
  },
};
