import SvnProFloatingActionButton from '../components/button/SvnProFloatingActionButton.vue';

export default {
  title: 'Design System/Components/V2/SvnProFloatingActionButton',
  component: SvnProFloatingActionButton,
  tags: ['autodocs'],
  argTypes: {
    extended: {
      control: 'boolean',
    },
    toTop: {
      control: 'boolean',
    },
    text: {
      control: 'text',
    },
    variant: {
      control: 'select',
      options: ['surface', 'primary', 'secondary'],
    },
    icon: {
      control: 'text',
    },
    size: {
      control: 'select',
      options: ['default', 'small', 'large'],
    },
  },
  parameters: {
    docs: {
      description: {
        component:
          'Component for the Floating Action Button, a button positionned at the bottom right of your page, representing your main page action.',
      },
    },
  },
};

export const Playground = {
  render: (args) => ({
    components: { SvnProFloatingActionButton },
    setup() {
      return { args };
    },
    template: '<SvnProFloatingActionButton v-bind="args" />',
  }),
  args: {
    extended: false,
    toTop: false,
    text: 'Label',
    variant: 'surface',
    icon: 'mdi-plus',
    size: 'default',
  },
};
