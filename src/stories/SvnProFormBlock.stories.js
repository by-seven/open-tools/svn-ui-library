import { FormBlockTypes, ComponentSizes } from '../constants/types';
import SvnProFormBlock from '../components/card/SvnProFormBlock.vue';

export default {
  title: 'Design System/Components/V2/SvnProFormBlock',
  component: SvnProFormBlock,
  tags: ['autodocs'],
  argTypes: {
    title: {
      control: 'text',
    },
    hasTitle: {
      control: 'boolean',
    },
    size: {
      control: 'select',
      options: Object.values(ComponentSizes),
    },
    hasDivider: {
      control: 'boolean',
    },
    description: {
      control: 'text',
    },
    state: {
      control: 'select',
      options: ['default', 'error'],
    },
    inputLabel: {
      control: 'text',
    },
    selectedValue: {
      control: 'text',
    },
    MCQOptions: {
      control: 'array',
    },
    ratingLength: {
      control: 'text',
    },
    hasComment: {
      control: 'boolean',
    },
    hasVariableContent: {
      control: 'boolean',
    },
    type: {
      control: 'select',
      options: Object.values(FormBlockTypes),
    },
    comments: {
      control: 'text',
    },
    hasEditableArea: {
      control: 'boolean',
    },
    hasDescription: {
      control: 'boolean',
    },
    ratingSelectLabel: {
      control: 'text',
    },
  },
};

export const Playground = {
  render: (args) => ({
    components: { SvnProFormBlock },
    setup() {
      return { args };
    },
    template: '<SvnProFormBlock v-bind="args" />',
  }),
  args: {
    hasTitle: true,
    title: 'What is your why?',
    size: 'compact',
    type: FormBlockTypes.autocomplete,
    hasDivider: false,
    hasDescription: true,
    hasVariableContent: false,
    hasEditableArea: true,
    hasComment: false,
    description:
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Lacinia tempor neque vitae dictumst luctus ornare id pretium diam. Amet cras amet habitasse cursus. Urna, sed rhoncus morbi proin. Eu sapien, maecenas lorem pellentesque non eget viverra. Amet, volutpat odio diam feugiat. Et varius aliquam rhoncus urna.',
    state: 'default',
    inputLabel: 'Answer',
    selectedValue: '4',
    MCQOptions: ['Yes', 'No', 'Maybe'],
    comments: 'Comments',
    ratingLength: '5',
    ratingSelectLabel: 'Choose option',
  },
};
