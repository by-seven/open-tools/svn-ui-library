import SvnProGraphTarget from '../components/progress/SvnProGraphTarget.vue';
import {
  SvnProGraphTargetProgress,
  SvnProGraphTargetIndicatorType,
} from '@/components/progress/SvnProGraphTarget.vue';

export default {
  title: 'Design System/Components/V2/SvnProGraphTarget',
  component: SvnProGraphTarget,
  tags: ['autodocs'],
  argTypes: {
    progress: {
      control: 'select',
      options: Object.values(SvnProGraphTargetProgress),
    },
    indicatorType: {
      control: 'select',
      options: Object.values(SvnProGraphTargetIndicatorType),
    },
    currentValue: {
      control: 'text',
    },
    targetValue: {
      control: 'text',
    },
    notSetText: {
      control: 'text',
    },
  },
  parameters: {
    docs: {
      description: {
        component: 'Component for the Profile.',
      },
    },
  },
};

export const Playground = {
  render: (args) => ({
    components: { SvnProGraphTarget },
    setup() {
      return { args };
    },
    template: '<SvnProGraphTarget v-bind="args" />',
  }),
  args: {
    progress: SvnProGraphTargetProgress.notSet,
    currentValue: '1500000',
    targetValue: '3000000',
    notSetText: 'Not set',
    indicatorType: SvnProGraphTargetIndicatorType.numeric,
  },
};
