import { SizeEnum } from '../constants/types';
import { createSvnProIconButtonComponent } from '../factories/iconButton.factory';

// Factory instance pour Storybook
const SvnProIconButtonFactory = (args) => createSvnProIconButtonComponent(args);

// Exposition de la story pour la factory avec les props de Vuetify
export default {
  title: 'Design System/Factories/SvnProIconButtonFactory',
  argTypes: {
    icon: {
      control: 'text',
      description: 'Icone à utiliser pour le bouton.',
      defaultValue: 'mdi-check',
    },
    variant: {
      control: 'select',
      options: [
        'filled',
        'outlined',
        'standard',
        'tonal',
        'elevated',
        'text',
        'flat',
      ],
      description: 'Variante du bouton (choix entre différents styles visuels)',
      defaultValue: 'filled',
    },
    value: {
      control: 'text',
      description: 'The value used when the component is selected in a group. If not provided, a unique ID will be used.',
    },
    color: {
      control: 'select',
      options: ['onSurfaceVariant', 'primary', 'secondary', 'success', 'info', 'warning', 'error'],
      description: 'Couleur du bouton',
      defaultValue: 'primary',
    },
    size: {
      control: 'select',
      options: Object.values(SizeEnum),
      description: 'Taille du bouton',
      defaultValue: 'default',
    },
    density: {
      control: 'select',
      options: ['default', 'comfortable', 'compact'],
      description: "Densité du bouton (ajuste l'espacement et la taille)",
      defaultValue: 'default',
    },
    state: {
      control: 'select',
      options: ['enabled', 'hovered', 'focused', 'pressed', 'disabled'],
      description: 'État actuel du bouton',
      defaultValue: 'enabled',
    },
    rounded: {
      control: 'select',
      options: ['none', 'sm', 'md', 'lg', 'xl', 'circle', 'pill'],
      description: 'Forme des bords du bouton',
      defaultValue: 'rounded',
    },
    elevation: {
      control: 'number',
      description: "Profondeur de l'ombre (élévation)",
      defaultValue: 2,
    },
    block: {
      control: 'boolean',
      description: 'Rend le bouton pleine largeur',
      defaultValue: false,
    },
    ripple: {
      control: 'boolean',
      description: 'Ajoute un effet ripple (ondulation) au clic',
      defaultValue: true,
    },
    loading: {
      control: 'boolean',
      description: 'Affiche un état de chargement sur le bouton',
      defaultValue: false,
    },
    disabled: {
      control: 'boolean',
      description: 'Désactive le bouton',
      defaultValue: false,
    },
    ariaLabel: {
      control: 'text',
      description: "Label ARIA pour l'accessibilité",
      defaultValue: 'Button',
    },
  },
};

// Template pour créer un bouton via la factory
const Template = (args) => {
  const ButtonComponent = SvnProIconButtonFactory(args); // Utilisation dynamique de la factory
  return {
    components: { ButtonComponent },
    setup() {
      return { args };
    },
    template: '<ButtonComponent v-bind="args" />',
  };
};

// Story principale
export const Playground = Template.bind({});
Playground.args = {
  icon: 'mdi-check',
  variant: 'filled',
  color: 'primary',
  size: SizeEnum.DEFAULT,
  density: 'default',
  state: 'enabled',
  rounded: 'rounded',
  elevation: 2,
  block: false,
  ripple: true,
  loading: false,
  disabled: false,
  ariaLabel: 'Button',
};
