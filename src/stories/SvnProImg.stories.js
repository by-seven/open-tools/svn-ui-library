import SvnProImg from '@/components/images/SvnProImg';

export default {
  title: 'Design System/Components/V2/SvnProImg',
  component: SvnProImg,
  tags: ['autodocs'],
  argTypes: {
    src: {
      control: 'text',
    },
    cover: {
      control: 'boolean',
    },
  },
  parameters: {
    docs: {
      description: {
        component: 'Component for image with vuetify',
      },
    },
  },
};

export const Playground = {
  render: (args) => ({
    components: { SvnProImg },
    setup() {
      return { args };
    },
    template: `<SvnProImg v-bind="args" />`,
  }),
  args: {
    src: 'https://aleph.byseven.co/rails/active_storage/representations/redirect/eyJfcmFpbHMiOnsibWVzc2FnZSI6IkJBaHBBajRsIiwiZXhwIjpudWxsLCJwdXIiOiJibG9iX2lkIn19--d8cdab422a915c2088be3bd197d08bc1838d77ee/eyJfcmFpbHMiOnsibWVzc2FnZSI6IkJBaDdCem9MWm05eWJXRjBTU0lJY0c1bkJqb0dSVlE2QzNKbGMybDZaVWtpQ1RFd01EQUdPd1pVIiwiZXhwIjpudWxsLCJwdXIiOiJ2YXJpYXRpb24ifX0=--e509fc07c09896c2c2e3f26b229df531f032ba39/Capture%20d%E2%80%99e%CC%81cran%202024-08-26%20a%CC%80%2014.56.36.png',
    cover: true,
  },
};
