import SvnProInfoTag from '../components/chip/SvnProInfoTag.vue';

export default {
  title: 'Design System/Components/V2/SvnProInfoTag',
  component: SvnProInfoTag,
  tags: ['autodocs'],
  argTypes: {
    text: {
      control: 'text',
    },
    tagType: {
      control: 'select',
      options: ['success', 'warning', 'error', 'primary', 'info'],
    },
    tagSize: {
      control: 'select',
      options: ['default', 'compact'],
    },
    tagStyle: {
      control: 'select',
      options: ['filled', 'outlined', 'text'],
    },
    leadingItem: {
      control: 'select',
      options: [undefined, 'point', 'icon'],
    },
    icon: {
      control: 'text',
    },
  },
  parameters: {
    docs: {
      description: {
        component: 'Component for the Info Tag.',
      },
    },
  },
};

export const Playground = {
  render: (args) => ({
    components: { SvnProInfoTag },
    setup() {
      return { args };
    },
    template: '<SvnProInfoTag v-bind="args" />',
  }),
  args: {
    text: 'Tag',
    tagType: 'primary',
    tagSize: 'compact',
    tagStyle: 'text',
    leadingItem: undefined,
    icon: undefined,
  },
};
