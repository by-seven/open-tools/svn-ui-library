import SvnProLearnBadge from '../components/chip/SvnProLearnBadge.vue';

export default {
  title: 'Design System/Components/V2/SvnProLearnBadge',
  component: SvnProLearnBadge,
  tags: ['autodocs'],
  argTypes: {
    count: {
      control: 'number',
    },
    levels: {
      control: 'object',
    },
    badgeType: {
      control: 'select',
      options: ['training', 'module', 'like', 'reaction'],
    },
  },
  parameters: {
    docs: {
      description: {
        component: 'Component for the Learn Badge.',
      },
    },
  },
};

export const Playground = {
  render: (args) => ({
    components: { SvnProLearnBadge },
    setup() {
      return { args };
    },
    template: '<SvnProLearnBadge v-bind="args" />',
  }),
  args: {
    count: 0,
    badgeType: 'training',
    levels: {
      levelOne: 1,
      levelTwo: 3,
      levelThree: 5,
      levelFour: 20,
      titleOne: 'Accomplished Apprentice',
      titleTwo: 'Trained and Competent',
      titleThree: 'Certified Expert',
      titleFour: 'Training Champion',
    },
  },
};
