import SvnProListItem from '../components/list/SvnProListItem.vue';
import SvnListTest from './Components/SvnListTest.vue';

export default {
  title: 'Design System/Components/V2/SvnProListItem',
  component: SvnProListItem,
  tags: ['autodocs'],
  argTypes: {
    title: {
      control: 'text',
    },
    subtitle: {
      control: 'text',
    },
    value: {
      control: 'text',
    },
    density: {
      control: 'select',
      options: ['default', 'comfortable', 'compact'],
    },
    minHeight: {
      control: 'number',
    },
    lines: {
      control: 'select',
      options: ['unlimited', 'one', 'two', 'three'],
    },
    leading: {
      control: 'select',
      options: [
        'none',
        'checkbox',
        'icon',
        'switch',
        'radio',
        'avatar',
        'image',
        'video',
      ],
    },
    trailing: {
      control: 'select',
      options: ['none', 'checkbox', 'icon', 'switch', 'radio'],
    },
    leadingIcon: {
      control: 'text',
    },
    trailingIcon: {
      control: 'text',
    },
    leadingColor: {
      control: 'text',
    },
    trailingColor: {
      control: 'text',
    },
    leadingMedia: {
      control: 'text',
    },
    leadingFirstname: {
      control: 'text',
    },
  },
  parameters: {
    docs: {
      description: {
        component: 'Component for the List Item, used inside the lists.',
      },
    },
  },
};

export const Playground = {
  render: (args) => ({
    components: { SvnProListItem },
    setup() {
      return { args };
    },
    template: '<SvnProListItem v-bind="args" />',
  }),
  args: {
    title: undefined,
    subtitle: undefined,
    value: undefined,
    lines: undefined,
    leadingMedia: undefined,
    leadingIcon: 'mingcute:left-line',
    leadingFirstname: undefined,
    leading: 'none',
    trailing: 'none',
    trailingIcon: 'mingcute:right-line',
    trailingColor: 'primary',
  },
};

export const ListItemWithList = {
  render: (args) => ({
    components: { SvnListTest },
    setup() {
      return { args };
    },
    template: '<SvnListTest v-bind="args" />',
  }),
  args: {
    items: [
      {
        title: 'Element One',
        subtitle: 'Subtitle One',
        value: 'value_one',
        lines: 'one',
        density: 'default',
        minHeight: 56,
        leading: 'none',
        leadingColor: 'primary',
        leadingIcon: '',
        leadingMedia: '',
        leadingFirstname: '',
        trailing: 'none',
        trailingColor: 'primary',
        trailingIcon: '',
        trailingMedia: '',
        trailingFirstname: '',
      },
      {
        title: 'Element Two',
        subtitle: 'Subtitle Two',
        value: 'value_two',
        lines: 'one',
        density: 'default',
        minHeight: 56,
        leading: 'none',
        leadingColor: 'primary',
        leadingIcon: '',
        leadingMedia: '',
        leadingFirstname: '',
        trailing: 'none',
        trailingColor: 'primary',
        trailingIcon: '',
        trailingMedia: '',
        trailingFirstname: '',
      },
      {
        title: 'Element Three',
        subtitle: 'Subtitle Three',
        value: 'value_three',
        lines: 'one',
        density: 'default',
        minHeight: 56,
        leading: 'none',
        leadingColor: 'primary',
        leadingIcon: '',
        leadingMedia: '',
        leadingFirstname: '',
        trailing: 'none',
        trailingColor: 'primary',
        trailingIcon: '',
        trailingMedia: '',
        trailingFirstname: '',
      },
      {
        type: 'divider',
      },
      {
        title: 'Element Four',
        subtitle: 'Subtitle Four',
        value: 'value_four',
        lines: 'one',
        density: 'default',
        minHeight: 56,
        leading: 'none',
        leadingColor: 'primary',
        leadingIcon: '',
        leadingMedia: '',
        leadingFirstname: '',
        trailing: 'none',
        trailingColor: 'primary',
        trailingIcon: '',
        trailingMedia: '',
        trailingFirstname: '',
      },
      {
        title: 'Element Five',
        subtitle: 'Subtitle Five',
        value: 'value_five',
        lines: 'one',
        density: 'default',
        minHeight: 56,
        leading: 'none',
        leadingColor: 'primary',
        leadingIcon: '',
        leadingMedia: '',
        leadingFirstname: '',
        trailing: 'none',
        trailingColor: 'primary',
        trailingIcon: '',
        trailingMedia: '',
        trailingFirstname: '',
      },
    ],
  },
};
