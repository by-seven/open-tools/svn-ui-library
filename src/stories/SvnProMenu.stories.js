import SvnProMenu from '../components/menu/SvnProMenu.vue';

const items = [
  {
    id: 0,
    title: 'Darell Steward',
  },
  {
    id: 1,
    title: 'Esther Howard',
  },
  {
    id: 2,
    title: 'Jenny Wilson',
  },
];

export default {
  title: 'Design System/Components/V2/SvnProMenu',
  component: SvnProMenu,
  tags: ['autodocs'],
  argTypes: {
    menuColor: {
      control: 'text',
    },
    listDensity: {
      control: 'select',
      options: ['default', 'compact', 'comfortable'],
    },
    closeOnContentClick: {
      control: 'boolean',
    },
    items: {
      contorl: 'array',
    },
    offset: {
      control: 'number',
    },
    menuClass: {
      control: 'text',
    },
  },
  parameters: {
    docs: {
      description: {
        component:
          'Component for Menu/Dropdown, shows an item or a list of items.',
      },
    },
  },
};

export const Playground = {
  render: (args) => ({
    components: { SvnProMenu },
    setup() {
      return { args };
    },
    template: '<SvnProMenu v-bind="args" />',
  }),
  args: {
    items: items,
    offset: undefined,
    menuColor: 'primary',
    menuClass: undefined,
    listDensity: 'default',
    closeOnContentClick: true,
  },
};
