import SvnProModal from '../components/modal/SvnProModal.vue';

export default {
  title: 'Design System/Components/V2/SvnProModal',
  component: SvnProModal,
  tags: ['autodocs'],
  argTypes: {
    title: {
      control: 'text',
    },
    contentHeight: {
      control: 'number',
    },
    actionOneTitle: {
      control: 'text',
    },
    actionTwoTitle: {
      control: 'text',
    },
    contentText: {
      control: 'text',
    },
    stickyBottom: {
      control: 'boolean',
    },
  },
  parameters: {
    docs: {
      description: {
        component: 'Component for the Modal.',
      },
    },
  },
};

export const Playground = {
  render: (args) => ({
    components: { SvnProModal },
    setup() {
      return { args };
    },
    template: '<SvnProModal v-bind="args" />',
  }),
  args: {
    title: 'Basic modal title',
    stickyBottom: true,
    contentText:
      'A modal is a type of dilaog that appears in front of app content to provide critical information, or prompt for a decision to be made.',
    contentHeight: undefined,
    actionOneTitle: undefined,
    actionTwoTitle: undefined,
  },
};
