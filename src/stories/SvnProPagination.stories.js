import SvnProPagination from '../components/pagination/SvnProPagination.vue';

export default {
  title: 'Design System/Components/V2/SvnProPagination',
  component: SvnProPagination,
  tags: ['autodocs'],
  argTypes: {
    previousText: {
      control: 'text',
    },
    nextText: {
      control: 'text',
    },
    variant: {
      control: 'select',
      options: ['default', 'compact'],
    },
    length: {
      control: 'number',
    },
    page: {
      control: 'number',
    },
  },
  parameters: {
    docs: {
      description: {
        component:
          'Component for the Pagination, usually at the bottom of Data tables.',
      },
    },
  },
};

export const Playground = {
  render: (args) => ({
    components: { SvnProPagination },
    setup() {
      return { args };
    },
    template: '<SvnProPagination v-bind="args" />',
  }),
  args: {
    variant: 'default',
    previousText: 'Previous',
    nextText: 'Next',
    length: 7,
    page: 1,
  },
};
