import SvnProProfile from "../components/profile/SvnProProfile.vue";

export default {
  title: "Design System/Components/V2/SvnProProfile",
  component: SvnProProfile,
  tags: ["autodocs"],
  argTypes: {
    size: {
      control: "select",
      options: ["default", "compact"],
    },
    avatar: {
      control: "text",
    },
    firstname: {
      control: "text",
    },
    lastname: {
      control: "text",
    },
    email: {
      control: "text",
    },
    tags: {
      control: "array",
    },
    showBadges: {
      control: "boolean",
    },
    pictureEditable: {
      control: "boolean",
    },
    badgeTypes: {
      control: "array",
    },
    showSettings: {
      control: "boolean",
    },
  },
  parameters: {
    docs: {
      description: {
        component: "Component for the Profile.",
      },
    },
  },
};

export const Playground = {
  render: (args) => ({
    components: { SvnProProfile },
    setup() {
      return { args };
    },
    template: '<SvnProProfile v-bind="args" />',
  }),
  args: {
    size: "compact",
    avatar: undefined,
    firstname: "John",
    lastname: "Doe",
    email: "johndoe@bigmammagroup.com",
    tags: [
      "Info tag 1",
      "Info tag 2",
      "Info g long 3",
      "Info tag moyen 4",
      "Info taewrwerg 5",
      "Info tag 6",
      "Info tag autre 7",
      "Info tag 8",
    ],
    showBadges: true,
    pictureEditable: true,
    showSettings: true,
    badgeTypes: [
      {
        name: "trainings",
        count: 1,
      },
      {
        name: "modules",
        count: 8,
      },
      {
        name: "likes",
        count: 12,
      },
      {
        name: "reactions",
        count: 0,
      },
    ],
  },
};
