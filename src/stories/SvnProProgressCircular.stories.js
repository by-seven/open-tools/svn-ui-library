import SvnProProgressCircular from "../components/progress/SvnProProgressCircular.vue";

export default {
  title: "Design System/Components/V2/SvnProgressCircular",
  component: SvnProProgressCircular,
  tags: ["autodocs"],
  argTypes: {
    bgColor: {
      control: "text",
    },
    color: {
      control: "text",
    },
    indeterminate: {
      control: "boolean",
    },
    modelValue: {
      control: "number",
    },
    size: {
      control: "number",
    },
    width: {
      control: "number",
    },
  },
  parameters: {
    docs: {
      description: {
        component:
          "Component for the Circular progress, used to indicate progress.",
      },
    },
  },
};

export const Playground = {
  render: (args) => ({
    components: { SvnProProgressCircular },
    setup() {
      return { args };
    },
    template: '<SvnProProgressCircular v-bind="args" />',
  }),
  args: {
    size: 48,
    width: 4,
    color: "primary",
    bgColor: undefined,
    indeterminate: false,
    modelValue: undefined,
  },
};
