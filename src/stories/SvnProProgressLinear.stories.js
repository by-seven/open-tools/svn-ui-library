import SvnProProgressLinear from "../components/progress/SvnProProgressLinear.vue";

const steps = [
  {
    id: 0,
    progress: 100,
  },
  {
    id: 1,
    progress: 100,
  },
  {
    id: 2,
    progress: 0,
  },
];

export default {
  title: "Design System/Components/V2/SvnProgressLinear",
  component: SvnProProgressLinear,
  tags: ["autodocs"],
  argTypes: {
    bgColor: {
      control: "text",
    },
    color: {
      control: "text",
    },
    indeterminate: {
      control: "boolean",
    },
    modelValue: {
      control: "number",
    },
    size: {
      control: "number",
    },
    height: {
      control: "number",
    },
    steps: {
      control: "array",
    },
  },
  parameters: {
    docs: {
      description: {
        component:
          "Component for the Linear progress, used to indicate progress.",
      },
    },
  },
};

export const Playground = {
  render: (args) => ({
    components: { SvnProProgressLinear },
    setup() {
      return { args };
    },
    template: '<SvnProProgressLinear v-bind="args" />',
  }),
  args: {
    size: 48,
    height: 4,
    steps: steps,
    color: "primary",
    bgColor: undefined,
    indeterminate: false,
    modelValue: undefined,
  },
};
