import SvnProQuestionCreateAdd from '../components/card/SvnProQuestionCreateAdd.vue';
import { InterviewAppQuestionTypes, ComponentSizes } from '../constants/types';

const menuItems = [
  {
    id: InterviewAppQuestionTypes.chapter,
    value: InterviewAppQuestionTypes.chapter,
    title: 'Chapter',
    icon: 'custom:mingcute:align-left-line',
  },
  {
    id: InterviewAppQuestionTypes.paragraph,
    value: InterviewAppQuestionTypes.paragraph,
    title: 'Paragraph',
    icon: 'custom:mingcute:align-left-line',
  },
  {
    id: InterviewAppQuestionTypes.open,
    value: InterviewAppQuestionTypes.open,
    title: 'Open question',
    icon: 'custom:mingcute:question-line',
  },
  {
    id: InterviewAppQuestionTypes.rating,
    value: InterviewAppQuestionTypes.rating,
    title: 'Rating',
    icon: 'custom:mingcute:star-line',
  },
  {
    id: InterviewAppQuestionTypes.mcq,
    value: InterviewAppQuestionTypes.mcq,
    title: 'Chapter',
    icon: 'custom:mingcute:checkbox-line',
  },
  {
    id: InterviewAppQuestionTypes.roadmap_create,
    value: InterviewAppQuestionTypes.roadmap_create,
    title: 'Roadmap - Create target',
    icon: 'custom:mingcute:target-line',
  },
  {
    id: InterviewAppQuestionTypes.roadmap_update,
    value: InterviewAppQuestionTypes.roadmap_update,
    title: 'Roadmap - Update target',
    icon: 'custom:mingcute:target-line',
  },
];

export default {
  title: 'Design System/Components/V2/SvnProQuestionCreateAdd',
  component: SvnProQuestionCreateAdd,
  tags: ['autodocs'],
  argTypes: {
    type: {
      control: 'select',
      options: [
        ...Object.values(InterviewAppQuestionTypes),
        'template',
        'add_block',
        'mcq_quiz',
      ],
    },
    size: {
      control: 'select',
      options: Object.values(ComponentSizes),
    },
    allowComments: {
      control: 'boolean',
    },
    addButtonText: {
      control: 'text',
    },
    ratingSelectLabel: {
      control: 'text',
    },
    questionLabel: {
      control: 'text',
    },
    descriptionLabel: {
      control: 'text',
    },
    addAnswerOptionText: {
      control: 'text',
    },
    addMenuItems: {
      control: 'array',
    },
    isFeedbackOrSurvey: {
      control: 'boolean',
    },
    visibleForInterviewerLabel: {
      control: 'text',
    },
    RequiredForInterviewerLabel: {
      control: 'text',
    },
    visibleForIntervieweeLabel: {
      control: 'text',
    },
    RequiredForIntervieweeLabel: {
      control: 'text',
    },
  },
};

export const Playground = {
  render: (args) => ({
    components: { SvnProQuestionCreateAdd },
    setup() {
      return { args };
    },
    template: '<SvnProQuestionCreateAdd v-bind="args" />',
  }),
  args: {
    size: ComponentSizes.compact,
    type: InterviewAppQuestionTypes.open,
    allowComments: true,
    addButtonText: 'Add',
    ratingSelectLabel: 'Max rating',
    questionLabel: 'Question',
    descriptionLabel: 'Description',
    addAnswerOptionText: 'Add answer option',
    addMenuItems: menuItems,
    isFeedbackOrSurvey: false,
    visibleForInterviewerLabel: 'Visible for interviewer',
    RequiredForInterviewerLabel: 'Required for interviewer',
    visibleForIntervieweeLabel: 'Visible for interviewee',
    RequiredForIntervieweeLabel: 'Required for interviewee',
  },
};

export const OpenQuestion = {
  render: (args) => ({
    components: { SvnProQuestionCreateAdd },
    setup() {
      return { args };
    },
    template: '<SvnProQuestionCreateAdd v-bind="args" />',
  }),
  args: {
    size: ComponentSizes.default,
    type: InterviewAppQuestionTypes.open,
  },
};

export const Rating = {
  render: (args) => ({
    components: { SvnProQuestionCreateAdd },
    setup() {
      return { args };
    },
    template: '<SvnProQuestionCreateAdd v-bind="args" />',
  }),
  args: {
    size: ComponentSizes.default,
    type: InterviewAppQuestionTypes.rating,
  },
};

export const Multi_Choice = {
  render: (args) => ({
    components: { SvnProQuestionCreateAdd },
    setup() {
      return { args };
    },
    template: '<SvnProQuestionCreateAdd v-bind="args" />',
  }),
  args: {
    size: ComponentSizes.default,
    type: InterviewAppQuestionTypes.mcq,
  },
};

export const Chapter = {
  render: (args) => ({
    components: { SvnProQuestionCreateAdd },
    setup() {
      return { args };
    },
    template: '<SvnProQuestionCreateAdd v-bind="args" />',
  }),
  args: {
    size: ComponentSizes.default,
    type: InterviewAppQuestionTypes.chapter,
    questionLabel: 'Chapter name',
    descriptionLabel: 'Description',
  },
};

export const Paragraph = {
  render: (args) => ({
    components: { SvnProQuestionCreateAdd },
    setup() {
      return { args };
    },
    template: '<SvnProQuestionCreateAdd v-bind="args" />',
  }),
  args: {
    size: ComponentSizes.default,
    type: InterviewAppQuestionTypes.paragraph,
    descriptionLabel: 'Type your text here',
  },
};

export const Template = {
  render: (args) => ({
    components: { SvnProQuestionCreateAdd },
    setup() {
      return { args };
    },
    template: '<SvnProQuestionCreateAdd v-bind="args" />',
  }),
  args: {
    size: ComponentSizes.default,
    type: 'template',
    questionLabel: 'Template name',
    descriptionLabel: 'Description',
  },
};

export const Add_Block = {
  render: (args) => ({
    components: { SvnProQuestionCreateAdd },
    setup() {
      return { args };
    },
    template: '<SvnProQuestionCreateAdd v-bind="args" />',
  }),
  args: {
    size: ComponentSizes.default,
    type: 'add_block',
    addMenuItems: menuItems,
  },
};
