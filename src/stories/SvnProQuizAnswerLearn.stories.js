
import SvnProQuizAnswerLearnItem from "@/components/quiz/SvnProQuizAnswerLearnItem.jsx";
import {SvnProQuizAnswerLearnItemStates} from "@/components/quiz/SvnProQuizAnswerLearnItem.jsx";

export default {
  title: "Design System/Components/V2/SvnProQuizAnswerLearnItem",
  component: SvnProQuizAnswerLearnItem,
  tags: ["autodocs"],
  argTypes: {
    leftLetter: {
      control: "text"
    },
    optionText: {
      control: "text",
    },
    state: {
      control: "select",
      options: Object.values(SvnProQuizAnswerLearnItemStates),
    },
    link: {
      control: 'boolean'
    }
  },
  parameters: {
    docs: {
      description: {
        component:
          "Button for quiz learn",
      },
    },
  },
};

export const Playground = {
  render: (args) => ({
    components: { SvnProQuizAnswerLearnItem },
    setup() {
      return { args };
    },
    template: '<SvnProQuizAnswerLearnItem v-bind="args" />',
  }),
  args: {
    leftLetter: 'a',
    optionText: 'hello',
    state: "partiallyCorrect",
    link: true,
  },
};
