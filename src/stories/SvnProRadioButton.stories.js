import SvnProRadioButton from '../components/input/SvnProRadioButton.vue';

export default {
  title: 'Design System/Components/V2/SvnProRadioButton',
  component: SvnProRadioButton,
  tags: ['autodocs'],
  argTypes: {
    disabled: {
      control: 'boolean',
    },
    readonly: {
      control: 'boolean',
    },
    value: {
      control: 'boolean',
    },
    modelValue: {
      control: 'boolean',
    },
    color: {
      control: 'text',
    },
    label: {
      control: 'text',
    },
    density: {
      control: 'select',
      options: ['default', 'comfortable', 'compact'],
    },
  },
  parameters: {
    docs: {
      description: {
        component:
          'Component for the Radio Button, used to display one radio buttons.',
      },
    },
  },
};

export const Playground = {
  render: (args) => ({
    components: { SvnProRadioButton },
    setup() {
      return { args };
    },
    template: '<SvnProRadioButton v-bind="args" />',
  }),
  args: {
    disabled: false,
    readonly: false,
    value: false,
    modelValue: false,
    density: 'compact',
    color: 'primary',
    label: undefined,
  },
};
