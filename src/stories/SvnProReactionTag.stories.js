import SvnProReactionTag from '../components/chip/SvnProReactionTag.vue';

export default {
  title: 'Design System/Components/V2/SvnProReactionTag',
  component: SvnProReactionTag,
  tags: ['autodocs'],
  argTypes: {
    reactionCount: {
      control: 'number',
    },
    reactionList: {
      control: 'array',
    },
  },
  parameters: {
    docs: {
      description: {
        component: 'Component for the Reaction Tag.',
      },
    },
  },
};

export const Playground = {
  render: (args) => ({
    components: { SvnProReactionTag },
    setup() {
      return { args };
    },
    template: '<SvnProReactionTag v-bind="args" />',
  }),
  args: {
    reactionCount: undefined,
    reactionList: ['like', 'recommend', 'question', 'remark'],
  },
};
