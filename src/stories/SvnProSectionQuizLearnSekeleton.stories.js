import SvnProSectionQuizLearnSkeleton from '@/components/quiz/skeleton/SvnProSectionQuizLearnSkeleton';

export default {
  // components: { Nop },
  title: 'Design System/Components/V2/SvnProSectionQuizLearnSkeleton',
  component: SvnProSectionQuizLearnSkeleton,
  tags: ['autodocs'],
  parameters: {
    docs: {
      description: {
        component: 'A section for questions in a quiz from learn',
      },
    },
  },
};

export const Playground = {
  render: (args) => ({
    components: {
      SvnProSectionQuizLearnSkeleton,
    },
    setup() {
      return { args };
    },
    template: `<SvnProSectionQuizLearnSkeleton v-bind="args"/>`,
  }),
};
