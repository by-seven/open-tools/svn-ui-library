import SvnProSectionQuizLearnBase, {
  SvnProSectionQuizLearnCorrectionType,
} from '@/components/quiz/SvnProSectionQuizLearnBase.jsx';
import SvnProQuizAnswerLearnItem from '@/components/quiz/SvnProQuizAnswerLearnItem.jsx';

export default {
  components: { SvnProQuizAnswerLearnItem },
  title: 'Design System/Components/V2/SvnProSectionQuizLearnSkt',
  component: SvnProSectionQuizLearnBase,
  tags: ['autodocs'],
  argTypes: {
    correctionType: {
      control: 'select',
      options: Object.values(SvnProSectionQuizLearnCorrectionType),
    },
    title: {
      control: 'text',
    },
    nbrOfItems: {
      control: 'number',
    },
  },
  parameters: {
    docs: {
      description: {
        component: 'A section for questions in a quiz from learn',
      },
    },
  },
};

export const Playground = {
  render: (args) => ({
    components: {
      SvnProSectionQuizLearnBase,
      SvnProQuizAnswerLearnItem,
    },
    setup() {
      if (args.nbrOfItems) {
        const list = [];

        for (let i = 0; i < args.nbrOfItems; i++) {
          list.push({
            leftLetter: 'a',
            optionText: 'here lorem ipsium bla bla',
          });
        }
        args = {
          ...args,
          list,
        };
      }

      return { args };
    },
    template: `<SvnProSectionQuizLearnBase v-bind="args" >
      <template #default="{item}">
        <SvnProQuizAnswerLearnItem
            :leftLetter="item.leftLetter"
            :optionText="item.optionText"
        ></SvnProQuizAnswerLearnItem>
      </template>
    </SvnProSectionQuizLearnBase>`,
  }),
  args: {
    nbrOfItems: 1,
  },
};
