import SvnProSelect from '../components/input/SvnProSelect.vue';

export default {
  title: 'Design System/Components/V2/SvnProSelect',
  component: SvnProSelect,
  tags: ['autodocs'],
  argTypes: {
    modelValue: {
      control: 'text',
    },
    label: {
      control: 'text',
    },
    itemValue: {
      control: 'text',
    },
    itemTitle: {
      control: 'text',
    },
    disabled: {
      control: 'boolean',
    },
    multiple: {
      control: 'boolean',
    },
    error: {
      control: 'boolean',
    },
    chips: {
      control: 'boolean',
    },
    withSelectAll: {
      control: 'boolean',
    },
    errorMessages: {
      control: 'array',
    },
    items: {
      control: 'array',
    },
  },
  parameters: {
    docs: {
      description: {
        component:
          'Component for the Select, used in many pages to display a list to the user where he can select one or many values.',
      },
    },
  },
};

const items = [
  {
    title: 'Element One',
    value: 'value_one',
  },
  {
    title: 'Element Two',
    value: 'value_two',
  },
  {
    title: 'Element Three',
    value: 'value_three',
  },
  {
    title: 'Element Four',
    value: 'value_four',
  },
  {
    title: 'Element Five',
    value: 'value_five',
  },
];

export const Playground = {
  render: (args) => ({
    components: { SvnProSelect },
    setup() {
      return { args };
    },
    template: '<SvnProSelect v-bind="args" />',
  }),
  args: {
    modelValue: undefined,
    label: 'Label',
    itemValue: 'value',
    itemTitle: 'title',
    withSelectAll: false,
    disabled: false,
    multiple: false,
    chips: false,
    error: false,
    errorMessages: [],
    items: items,
  },
};
