import SvnProSlider from '../components/input/SvnProSlider.vue';

export default {
  title: 'Design System/Components/V2/SvnProSlider',
  component: SvnProSlider,
  tags: ['autodocs'],
  argTypes: {
    color: {
      control: 'text',
    },
    modelValue: {
      control: 'number',
    },
    step: {
      control: 'number',
    },
    min: {
      control: 'number',
    },
    max: {
      control: 'number',
    },
    disabled: {
      control: 'boolean',
    },
    thumbLabel: {
      control: 'boolean',
    },
    showTicks: {
      control: 'boolean',
    },
  },
  parameters: {
    docs: {
      description: {
        component:
          'Component for the Slider, a bar with an input you can move.',
      },
    },
  },
};

export const Playground = {
  render: (args) => ({
    components: { SvnProSlider },
    setup() {
      return { args };
    },
    template: '<SvnProSlider v-bind="args" />',
  }),
  args: {
    step: 0,
    min: 0,
    max: 100,
    modelValue: 0,
    showTicks: false,
    disabled: false,
    color: 'primary',
    thumbLabel: false,
  },
};
