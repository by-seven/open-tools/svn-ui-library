import SvnProSnackbar from '../components/snackbar/SvnProSnackbar.vue';

export default {
  title: 'Design System/Components/V2/SvnProSnackbar',
  component: SvnProSnackbar,
  tags: ['autodocs'],
  argTypes: {
    modelValue: {
      control: 'boolean',
    },
    action: {
      control: 'boolean',
    },
    vertical: {
      control: 'boolean',
    },
    close: {
      control: 'boolean',
    },
    multiLine: {
      control: 'boolean',
    },
    actionText: {
      control: 'text',
    },
    supportingText: {
      control: 'text',
    },
    color: {
      control: 'text',
    },
  },
  parameters: {
    docs: {
      description: {
        component:
          'Component for the Snackbar, used to display a small indication at the bottom of the page.',
      },
    },
  },
};

export const Playground = {
  render: (args) => ({
    components: { SvnProSnackbar },
    setup() {
      return { args };
    },
    template: '<SvnProSnackbar v-bind="args" />',
  }),
  args: {
    modelValue: false,
    action: false,
    vertical: false,
    close: false,
    multiLine: false,
    color: 'onSurface',
    actionText: 'Action',
    supportingText: 'Snackbar supporting text',
  },
};
