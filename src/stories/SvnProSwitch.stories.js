import SvnProSwitch from '../components/input/SvnProSwitch.vue';

export default {
  title: 'Design System/Components/V2/SvnProSwitch',
  component: SvnProSwitch,
  tags: ['autodocs'],
  argTypes: {
    modelValue: {
      control: 'boolean',
    },
    disabled: {
      control: 'boolean',
    },
    readonly: {
      control: 'boolean',
    },
    icon: {
      control: 'boolean',
    },
    color: {
      control: 'text',
    },
    density: {
      control: 'select',
      options: ['default', 'comfortable', 'compact'],
    },
  },
  parameters: {
    docs: {
      description: {
        component: 'Component for the Switch.',
      },
    },
  },
};

export const Playground = {
  render: (args) => ({
    components: { SvnProSwitch },
    setup() {
      return { args };
    },
    template: '<SvnProSwitch v-bind="args" />',
  }),
  args: {
    modelValue: false,
    disabled: false,
    readonly: false,
    density: 'compact',
    color: 'primary',
    icon: false,
  },
};
