import SvnProTabs from "../components/tab/SvnProTabs.vue";
import { TabAlignTypes } from '../constants/types';

const tabItems = [
  {
    value: 'one',
    icon: 'custom:mingcute:settings-5-line',
    text: 'Tab 1',
    class: '',
  },
  {
    value: 'two',
    icon: 'custom:mingcute:settings-5-line',
    text: 'Tab 2',
    class: '',
  },
  {
    value: 'three',
    icon: 'custom:mingcute:settings-5-line',
    text: 'Tab 3',
    class: '',
  },
]

export default {
  title: "Design System/Components/V2/SvnProTabs",
  component: SvnProTabs,
  tags: ["autodocs"],
  parameters: {
    docs: {
      description: {
        component: '**SvnProTabs** allow you to create Tabs that you can use in your layouts.',
      },
    },
  },
  argTypes: {
    items: {
      control: 'array',
      description: 'Defines the list of v-tabs with their props.<br> Each element should have a **"value"** property to identify each button.<br>The list of the properties are: <br>-icon<br>-value<br>-text<br>-class',
    },
    showArrows: {
      control: 'boolean',
      description: 'Defines if pagination arrows are shown if the tab items overflow their container.<br><u>DISCLAIMER:</u> If the tab items overflow their container, pagination controls will appear on desktop. For mobile devices, arrows will only display with the **show-arrows** prop.',
    },
    grow: {
      control: 'boolean',
      description: 'Defines if the tabs should take up all available space.',
    },
    alignTabs: {
      control: 'select',
      description: 'Defines how to align the tabs relative to their container.<br>Possible values are: title, start, end, center.',
      options: Object.values(TabAlignTypes),
    },
    activeColor: {
      control: 'text',
      description: 'Defines the color of the active(s) v-tab.',
    },
  },
};

export const Playground = {
  render: (args) => ({
    components: { SvnProTabs },
    setup() {
      return { args };
    },
    template: '<SvnProTabs v-bind="args" />',
  }),
  args: {
    items: tabItems,
    showArrows: false,
    grow: false,
    alignTabs: TabAlignTypes.START,
    activeColor: 'primary',
  },
};
