import {
  createSvnProText,
  TextSize,
  TextWeight,
} from '../factories/text.factory';

// Création du composant Text à partir de la factory
const SvnProText = createSvnProText();

export default {
  title: 'Design System/Factories/SvnProTextFactory',
  component: SvnProText,
  argTypes: {
    // Texte à afficher
    text: {
      control: 'text',
      description: 'Le contenu du texte',
      defaultValue: 'Texte d’exemple avec différentes tailles et styles',
    },

    // Couleur du texte
    color: {
      control: 'select',
      options: [
        'primary',
        'secondary',
        'success',
        'info',
        'warning',
        'error',
        'onSurface',
      ],
      description: 'Couleur du texte basée sur Vuetify',
      defaultValue: 'onSurface',
    },

    // Nouvelle prop `size` (enum TextSize)
    size: {
      control: 'select',
      options: Object.values(TextSize),
      description: 'Taille du texte basée sur Vuetify',
      defaultValue: TextSize.BODY1,
    },

    // Nouvelle prop `weight` (enum TextWeight)
    weight: {
      control: 'select',
      options: Object.values(TextWeight),
      description: 'Poids du texte (font-weight)',
      defaultValue: TextWeight.MEDIUM,
    },

    // Anciennes props pour rétrocompatibilité
    bodyLarge: {
      control: 'boolean',
      description: 'Ancienne prop - taille Body Large',
      defaultValue: false,
    },
    bodyMedium: {
      control: 'boolean',
      description: 'Ancienne prop - taille Body Medium',
      defaultValue: false,
    },
    subtitleLarge: {
      control: 'boolean',
      description: 'Ancienne prop - taille Subtitle Large',
      defaultValue: false,
    },
    subtitleMedium: {
      control: 'boolean',
      description: 'Ancienne prop - taille Subtitle Medium',
      defaultValue: false,
    },

    // Texte en mode éditable
    textarea: {
      control: 'boolean',
      description: 'Utiliser textarea pour afficher le texte en mode édition',
      defaultValue: false,
    },
  },
};

// Template Storybook pour générer chaque instance
const Template = (args) => ({
  components: { SvnProText },
  setup() {
    return { args };
  },
  template: '<SvnProText v-bind="args" />',
});

// Playground pour tester toutes les combinaisons
export const Playground = Template.bind({});
Playground.args = {
  text: 'Texte d’exemple pour tester les différentes tailles et styles.',
  color: 'onSurface',
  size: TextSize.BODY1,
  weight: TextWeight.MEDIUM,
  bodyLarge: false,
  bodyMedium: false,
  subtitleLarge: false,
  subtitleMedium: false,
  textarea: false,
};

// Exemples spécifiques pour illustrer rétrocompatibilité et nouvelles props

export const Body1BoldText = Template.bind({});
Body1BoldText.args = {
  text: 'Texte en Body1, Bold',
  size: TextSize.BODY1,
  weight: TextWeight.BOLD,
  color: 'primary',
};

export const Subtitle2MediumText = Template.bind({});
Subtitle2MediumText.args = {
  text: 'Texte en Subtitle2, Medium',
  size: TextSize.SUBTITLE2,
  weight: TextWeight.MEDIUM,
  color: 'secondary',
};

export const CaptionRegularText = Template.bind({});
CaptionRegularText.args = {
  text: 'Texte en Caption, Regular',
  size: TextSize.CAPTION,
  weight: TextWeight.REGULAR,
  color: 'warning',
};

// Exemples rétrocompatibilité
export const RetroCompatibleBodyLarge = Template.bind({});
RetroCompatibleBodyLarge.args = {
  text: 'Texte en Body Large (ancienne prop)',
  bodyLarge: true,
  color: 'error',
  medium: true,
};
