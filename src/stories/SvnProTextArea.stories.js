import SvnProTextArea from '../components/input/SvnProTextArea.vue';

export default {
  title: 'Design System/Components/V2/SvnProTextArea',
  component: SvnProTextArea,
  tags: ['autodocs'],
  argTypes: {
    modelValue: {
      control: 'text',
    },
    label: {
      control: 'text',
    },
    color: {
      control: 'text',
    },
    variant: {
      control: 'select',
      options: ['outlined', 'filled'],
    },
    rows: {
      control: 'number',
    },
    maxRows: {
      control: 'number',
    },
    maxLength: {
      control: 'number',
    },
    error: {
      control: 'boolean',
    },
    disabled: {
      control: 'boolean',
    },
    counter: {
      control: 'boolean',
    },
    readonly: {
      control: 'boolean',
    },
    persistentCounter: {
      control: 'boolean',
    },
    messages: {
      control: 'text',
    },
    rules: {
      control: 'array',
    },
  },
  parameters: {
    docs: {
      description: {
        component:
          'Component for the TextArea, used for text areas containing Rich text.',
      },
    },
  },
};

export const Playground = {
  render: (args) => ({
    components: { SvnProTextArea },
    setup() {
      return { args };
    },
    template: '<SvnProTextArea v-bind="args" />',
  }),
  args: {
    modelValue: '',
    label: 'Label',
    color: 'primary',
    variant: 'outlined',
    rows: 1,
    maxRows: undefined,
    error: false,
    disabled: false,
    counter: false,
    readonly: false,
    persistentCounter: false,
    messages: undefined,
    rules: [],
    maxLength: undefined,
  },
};
