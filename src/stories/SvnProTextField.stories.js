import SvnProTextField from '../components/input/SvnProTextField.vue';

export default {
  title: 'Design System/Components/V2/SvnProTextField',
  component: SvnProTextField,
  tags: ['autodocs'],
  argTypes: {
    label: {
      control: 'text',
    },
    appendInnerIcon: {
      control: 'text',
    },
    prependInnerIcon: {
      control: 'text',
    },
    variant: {
      control: 'select',
      options: [
        undefined,
        'outlined',
        'underlined',
        'solo',
        'solo-filled',
        'solo-inverted',
        'plain',
      ],
    },
    autofocus: {
      control: 'boolean',
    },
    clearable: {
      control: 'boolean',
    },
    color: {
      control: 'text',
    },
    disabled: {
      control: 'boolean',
    },
    hint: {
      control: 'text',
    },
    placeholder: {
      control: 'text',
    },
    messages: {
      control: 'text',
    },
    error: {
      control: 'boolean',
    },
    counter: {
      control: 'boolean',
    },
    persistentCounter: {
      control: 'boolean',
    },
    maxLength: {
      control: 'number',
    },
    persistentHint: {
      control: 'boolean',
    },
    rules: {
      control: 'array',
    },
  },
  parameters: {
    docs: {
      description: {
        component: 'Component for the textfield.',
      },
    },
  },
};

export const Playground = {
  render: (args) => ({
    components: { SvnProTextField },
    setup() {
      return { args };
    },
    template: '<SvnProTextField v-bind="args" />',
  }),
  args: {
    label: 'Label',
    appendInnerIcon: undefined,
    prependInnerIcon: 'mdi-magnify',
    variant: undefined,
    placeholder: undefined,
    autofocus: false,
    persistentHint: false,
    clearable: true,
    color: 'primary',
    disabled: false,
    hint: undefined,
    error: false,
    messages: undefined,
    counter: false,
    persistentCounter: false,
    rules: [],
    maxLength: undefined,
  },
};
