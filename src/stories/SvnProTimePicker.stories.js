import SvnProTimePicker from '../components/picker/SvnProTimePicker.vue';

export default {
  title: 'Design System/Components/V2/SvnProTimePicker',
  component: SvnProTimePicker,
  tags: ['autodocs'],
  argTypes: {
    modelValue: {
      control: 'text',
    },
    color: {
      control: 'text',
    },
    title: {
      control: 'text',
    },
    disabled: {
      control: 'boolean',
    },
    actions: {
      control: 'boolean',
    },
    format: {
      control: 'select',
      options: ['ampm', '24hr'],
    },
  },
  parameters: {
    docs: {
      description: {
        component:
          'Component for the Time Picker, you can select hour, minute, am/pm, and 24hour format.',
      },
    },
  },
};

export const Playground = {
  render: (args) => ({
    components: { SvnProTimePicker },
    setup() {
      return { args };
    },
    template: '<SvnProTimePicker v-bind="args" />',
  }),
  args: {
    modelValue: undefined,
    color: 'primary',
    title: 'Select time',
    format: 'ampm',
    actions: false,
    disabled: false,
  },
};
