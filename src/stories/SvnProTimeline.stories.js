import SvnProTimeline from "../components/SvnProTimeline.vue";
import { SizeEnum } from '@/constants/types.js';

export default {
  title: "Design System/Components/V2/SvnProTimeline",
  component: SvnProTimeline,
  tags: ["autodocs"],
  parameters: {
    docs: {
      description: {
        component: `
          **SvnProTimeline** is a timeline component for tracking progress across steps.
          It features:
          - Customizable dot colors and icons
          - Clickable steps for navigation
          - Dynamic styling for the current and completed steps

          ### Props
          - **items**: Array of timeline steps. Each item should have:
            - **title**: The title of the step.
            - **icon**: Optional icon for the step.
          - **lineThickness**: Thickness of the timeline line (must be above zero, default: 1).
          - **lineInset**: Inset spacing of the line (non-negative number, default: 1).
          - **currentStepIndex**: Index of the currently active step (non-negative integer, default: 1).
          - **lastStepIndex**: Index of the last completed step (non-negative integer, default: 3).
          - **size**: Size of the timeline dots and icons (one of (\`${Object.values(SizeEnum).join(", ")}\`)).

          ### Events
          - **\`change-timeline(index)\`**:
            Triggered when a step is clicked. The event payload is the index of the clicked step.
        `,
      },
    },
  },
  argTypes: {
    items: {
      control: "array",
      description: "Array of timeline step objects with titles, icons, and colors.",
    },
    itemsDev: {
      description:
        'This prop is created only for testing purposes, allowing users to dynamically choose the number of items.',
      control: 'select',
      options: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9 , 10, 11],
    },
    lineThickness: {
      control: { type: "range", min: 1, max: 10 },
      description: "Thickness of the timeline line.",
    },
    lineInset: {
      control: { type: "range", min: 0, max: 20 },
      description: "Inset spacing for the timeline line.",
    },
    currentStepIndex: {
      control: { type: "range", min: 0, max: 10 },
      description: "Index of the currently active step.",
    },
    lastStepIndex: {
      control: { type: "range", min: 0, max: 10 },
      description: "Index of the last completed step.",
    },
    size: {
      control: "select",
      options: Object.values(SizeEnum),
      description: `Size of the timeline dots and icons (\`${Object.values(SizeEnum).join(", ")}\`).`,
    }
  },
};

const itemsWithIcons = [
  { title: "Set campaign", icon: "mdi mdi-format-title" },
  { title: "Settings", icon: "mdi mdi-cog" },
  { title: "Participant(s)", icon: "mdi mdi-account-group" },
  { title: "Template(s)", icon: "mdi mdi-book-multiple-outline" },
  { title: "Launch!", icon: "mdi mdi-rocket" },
];


const items = [
  { title: "Set campaign" },
  { title: "Settings" },
  { title: "Participant(s)" },
  { title: "Template(s)" },
  { title: "Launch!" },
];

const extraItems = [
  ...items, // Spread the existing items
  { title: "Analyze results" },
  { title: "View reports" },
  { title: "Edit campaign" },
  { title: "Copy settings" },
  { title: "Add participants" },
  { title: "Choose themes" },
];

export const Playground = {
  render: (args, { argTypes }) => ({
    components: { SvnProTimeline },
    props: Object.keys(argTypes),
    setup() {
      // Function to dynamically update items
      const updateItems = (count, items) => {
        // Validate input: ensure items is an array and count is valid
        if (!Array.isArray(items)) {
          console.warn("Invalid input: 'items' must be an array.");
          return [];
        }

        if (count <= 0) {
          console.warn("Invalid input: 'count' must be greater than 0.");
          return [];
        }

        // If the items array has fewer elements than the count, return all items
        if (items.length <= count) {
          console.log(
            'Items count is less than or equal to the requested count. Returning all items.'
          );
          return [...items]; // Return all items without modifying the original array
        }

        // Otherwise, slice the array to return exactly `count` items
        const list = items.slice(0, count); // Adjusted to return exactly `count` items
        console.log('Updated items list:', list);

        return list;
      };


      if (args.items?.length && args.itemsDev !== undefined) {
        const list = updateItems(args.itemsDev, extraItems);

        args = {
          ...args,
          items: list, // Overwrite `items` dynamically
        };
      }

      const handleTimelineChange = (index) => {
        console.log("Step clicked:", index);
        args.lastStepIndex = index
        args.currentStepIndex = index
      };

      return { args, handleTimelineChange };
    },
    template: `
      <svn-pro-timeline
        v-bind="args"
        @change-timeline="handleTimelineChange"
      />
    `,
  })
}

Playground.args = {
  items: items,
  lineThickness: 1,
  lineInset: 2,
  itemsDev: items.length,
  currentStepIndex: 2,
  lastStepIndex: 3,
  size: "small",
};

export const CustomSizeExample = {...Playground}
CustomSizeExample.args = {
  ...Playground.args,
  size: "large",
  lastStepIndex: 4,
  currentStepIndex: 4,
};

export const WithIcons = {...Playground}
WithIcons.args = {
  ...Playground.args,
  size: "large",
  currentStepIndex: 1,
  items: itemsWithIcons,
  itemsDev: itemsWithIcons.length,
};

export const NoCompletedSteps = {...Playground}
NoCompletedSteps.args = {
  ...Playground.args,
  currentStepIndex: 0,
  lastStepIndex: 0,
};

export const WithNoSteps = {...Playground}
WithNoSteps.args = {
  ...Playground.args,
  currentStepIndex: 0,
  items: [],
  itemsDev: 0,
  lastStepIndex: 0,
};
