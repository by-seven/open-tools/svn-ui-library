import SvnProTitle from '../components/styles/SvnProTitle.vue';

export default {
  title: 'Design System/Components/V2/SvnProTitle',
  component: SvnProTitle,
  tags: ['autodocs'],
  argTypes: {
    color: {
      control: 'text',
    },
    h1: {
      control: 'boolean',
    },
    h2: {
      control: 'boolean',
    },
    h3: {
      control: 'boolean',
    },
    h4: {
      control: 'boolean',
    },
    h5: {
      control: 'boolean',
    },
    h6: {
      control: 'boolean',
    },
    regular: {
      control: 'boolean',
    },
    medium: {
      control: 'boolean',
    },
    bold: {
      control: 'boolean',
    },
  },
  parameters: {
    docs: {
      description: {
        component:
          'Component for the Title, with 3 variants h1, h2 and h3. You can also apply many modifiers to style it.',
      },
    },
  },
};

export const Playground = {
  render: (args) => ({
    components: { SvnProTitle },
    setup() {
      return { args };
    },
    template: '<SvnProTitle v-bind="args" />',
  }),
  args: {
    color: 'onSurface',
    h1: true,
    h2: false,
    h3: false,
    h4: false,
    h5: false,
    h6: false,
    regular: true,
    medium: false,
    bold: false,
  },
};
