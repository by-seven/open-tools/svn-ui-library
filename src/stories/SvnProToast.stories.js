import SvnProToast from '../components/snackbar/SvnProToast.vue';

export default {
  title: 'Design System/Components/V2/SvnProToast',
  component: SvnProToast,
  tags: ['autodocs'],
  argTypes: {
    toastTitle: {
      control: 'text',
    },
    toastDescription: {
      control: 'text',
    },
    actionText: {
      control: 'text',
    },
    progressBar: {
      control: 'boolean',
    },
    action: {
      control: 'boolean',
    },
    badge: {
      control: 'select',
      options: ['trainings', 'modules', 'reactions', 'likes'],
    },
    toastType: {
      control: 'select',
      options: ['success', 'info', 'warning', 'error', 'badge'],
    },
  },
  parameters: {
    docs: {
      description: {
        component: 'Component for the Toast, used to display a notification.',
      },
    },
  },
};

export const Playground = {
  render: (args) => ({
    components: { SvnProToast },
    setup() {
      return { args };
    },
    template: '<SvnProToast v-bind="args" />',
  }),
  args: {
    toastTitle: 'Your document is ready',
    toastDescription:
      'Your <a href="https://google.com" target="_blank"><strong>Google</strong></a> PDF "document title" has been successfully  downloaded<br/>Congratulations',
    actionText: 'Action',
    progressBar: false,
    action: false,
    badge: 'trainings',
    toastType: 'success',
  },
};
