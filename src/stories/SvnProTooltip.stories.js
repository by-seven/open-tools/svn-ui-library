import SvnProTooltip from '../components/tooltip/SvnProTooltip.vue';

export default {
  title: 'Design System/Components/V2/SvnProTooltip',
  component: SvnProTooltip,
  tags: ['autodocs'],
  argTypes: {
    width: {
      control: 'number',
    },
    maxWidth: {
      control: 'number',
    },
    location: {
      control: 'text',
    },
    origin: {
      control: 'text',
    },
    text: {
      control: 'text',
    },
  },
  parameters: {
    docs: {
      description: {
        component:
          'Component for the Tooltip, used to display a text on hover.',
      },
    },
  },
};

export const Playground = {
  render: (args) => ({
    components: { SvnProTooltip },
    setup() {
      return { args };
    },
    template: '<SvnProTooltip v-bind="args" />',
  }),
  args: {
    width: undefined,
    maxWidth: 200,
    location: 'top center',
    origin: 'bottom center',
    text: 'This is just a tooltip, but lorem ipsum am sir dolot',
  },
};
