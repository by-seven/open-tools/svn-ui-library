import { userEvent, within } from '@storybook/test';
import { ComponentSizes } from "../constants/types.js";
import SvnProTopBar from '../components/menu/SvnProTopBar.vue';

export default {
  title: 'Design System/Components/V2/SvnProTopBar',
  component: SvnProTopBar,
  tags: ['autodocs'],
  parameters: {
    docs: {
      description: {
        component: '**SvnProTopBar** is a top bar used in many pages of the app.<br>It supports multiple props to customize the TopBar and have a unique look.',
      },
    },
  },
  argTypes: {
    hasRightContent: {
      control: 'boolean',
      description: 'Tells if the TopBar has a right content',
    },
    hasLeftContent: {
      control: 'boolean',
      description: 'Tells if the TopBar has a left content (the back button)',
    },
    hasCloseButton: {
      control: 'boolean',
      description: 'Tells if the TopBar has a close button inside the right content',
    },
    iconButtonsNumber: {
      control: 'select',
      options: [0, 1, 2, 3, 4],
      description: 'The number of icon buttons to display in the TopBar',
    },
    iconButtonsArray: {
      control: 'array',
      description: 'List of the icon buttons in the TopBar, each one is an object with a property "icon" and a property "onClick"',
    },
    hasPrimaryButton: {
      control: 'boolean',
      description: 'Tells if the TopBar has a primary button inside the right content',
    },
    primaryButtonIcon: {
      control: 'text',
      description: 'The value of the icon inside the primary button',
    },
    primaryButtonText: {
      control: 'text',
      description: 'The value of the text inside the primary button',
    },
    primaryButtonLoading: {
      control: 'boolean',
      description: 'Tells if the primary button is loading',
    },
    primaryButtonDisabled: {
      control: 'boolean',
      description: 'Tells if the primary button is disabled',
    },
    hasSecondaryButton: {
      control: 'boolean',
      description: 'Tells if the TopBar has a secondary button inside the right content',
    },
    secondaryButtonIcon: {
      control: 'text',
      description: 'The value of the icon inside the secondary button',
    },
    secondaryButtonText: {
      control: 'text',
      description: 'The value of the text inside the secondary button',
    },
    secondaryButtonLoading: {
      control: 'boolean',
      description: 'Tells if the secondary button is loading',
    },
    secondaryButtonDisabled: {
      control: 'boolean',
      description: 'Tells if the secondary button is disabled',
    },
    hasInfoTag: {
      control: 'boolean',
      description: 'Tells if the TopBar has an InfoTag inside the right content',
    },
    infoTagText: {
      control: 'text',
      description: 'The value of the text inside the infoTag',
    },
    infoTagSize: {
      control: 'select',
      options: [ComponentSizes.default, ComponentSizes.compact],
      description: 'The size of the infoTag, between "default" & "compact"',
    },
    onBack: {
      description: 'Emitted when the back button is clicked.',
    },
    onClose: {
      description: 'Emitted when the close button is clicked.',
    },
    onPrimaryClick: {
      description: 'Emitted when the primary button is clicked.',
    },
    onSecondaryClick: {
      description: 'Emitted when the secondary button is clicked.',
    },
  },
};

const iconButtonsItems = [
  { icon: 'custom:mingcute:settings-3-line', onClick: () => { alert('Clicked on button 1') } },
  { icon: 'custom:mingcute:settings-3-line', onClick: () => { alert('Clicked on button 2') } },
  { icon: 'custom:mingcute:settings-3-line', onClick: () => { alert('Clicked on button 3') } },
  { icon: 'custom:mingcute:settings-3-line', onClick: () => { alert('Clicked on button 4') } },
];

export const Playground = {
  render: (args, { argTypes }) => ({
    components: { SvnProTopBar },
    props: Object.keys(argTypes),
    setup() {
      return { args };
    },
    template: `
      <SvnProTopBar
        v-bind="args"
      />
    `,
  }),
  args: {
    hasRightContent: true,
    hasLeftContent: true,
    hasCloseButton: true,
    iconButtonsNumber: 0,
    iconButtonsArray: iconButtonsItems,
    hasPrimaryButton: true,
    primaryButtonIcon: 'custom:mingcute:arrow-right-circle-line',
    primaryButtonText: 'Primary button',
    primaryButtonLoading: false,
    primaryButtonDisabled: false,
    hasSecondaryButton: true,
    secondaryButtonIcon: 'custom:mingcute:arrow-right-circle-line',
    secondaryButtonText: 'Secondary button',
    secondaryButtonLoading: false,
    secondaryButtonDisabled: false,
    hasInfoTag: true,
    infoTagText: 'Lorem ipsum',
    infoTagSize: ComponentSizes.default,
  },
  play: async ({ canvasElement }) => {
    const canvas = within(canvasElement);

    const backButton = canvas.getByTestId('back-button')
    const primaryButton = canvas.getByTestId('primary-button')
    const secondaryButton = canvas.getByTestId('secondary-button')
    const closeButton = canvas.getByTestId('close-button')

    if (backButton) {
      await userEvent.click(backButton);
    }
    if (primaryButton) {
      await userEvent.click(primaryButton);
    }
    if (secondaryButton) {
      await userEvent.click(secondaryButton);
    }
    if (closeButton) {
      await userEvent.click(closeButton); 
    }
  }
};

export const ModuleEdit = {
  render: (args, { argTypes }) => ({
    components: { SvnProTopBar },
    props: Object.keys(argTypes),
    setup() {
      return { args };
    },
    template: `
      <SvnProTopBar
        v-bind="args"
      />
    `,
  }),
  args: {
    hasRightContent: true,
    hasLeftContent: false,
    hasCloseButton: true,
    iconButtonsNumber: 3,
    iconButtonsArray: [
      { icon: 'custom:mingcute:sparkles-2-line', onClick: () => { alert('Clicked on button 1') } },
      { icon: 'custom:mingcute:eye-2-line', onClick: () => { alert('Clicked on button 2') } },
      { icon: 'custom:mingcute:more-2-line', onClick: () => { alert('Clicked on button 3') } },
    ],
    hasPrimaryButton: false,
    hasSecondaryButton: false,
    hasInfoTag: false,
  },
};

export const ModuleShow = {
  render: (args, { argTypes }) => ({
    components: { SvnProTopBar },
    props: Object.keys(argTypes),
    setup() {
      return { args };
    },
    template: `
      <SvnProTopBar
        v-bind="args"
      />
    `,
  }),
  args: {
    hasRightContent: true,
    hasLeftContent: true,
    hasCloseButton: false,
    iconButtonsNumber: 4,
    iconButtonsArray: [
      { icon: 'custom:mingcute:edit-2-line', onClick: () => { alert('Clicked on button 1') } },
      { icon: 'custom:mingcute:chat-4-line', onClick: () => { alert('Clicked on button 2') } },
      { icon: 'custom:mingcute:heart-line', onClick: () => { alert('Clicked on button 3') } },
      { icon: 'custom:mingcute:link-line', onClick: () => { alert('Clicked on button 4') } },
    ],
    hasPrimaryButton: false,
    hasSecondaryButton: false,
    hasInfoTag: false,
  },
};

export const ModuleDraft = {
  render: (args, { argTypes }) => ({
    components: { SvnProTopBar },
    props: Object.keys(argTypes),
    setup() {
      return { args };
    },
    template: `
      <SvnProTopBar
        v-bind="args"
      />
    `,
  }),
  args: {
    hasRightContent: true,
    hasLeftContent: false,
    hasCloseButton: true,
    iconButtonsNumber: 3,
    iconButtonsArray: [
      { icon: 'custom:mingcute:sparkles-2-line', onClick: () => { alert('Clicked on button 1') } },
      { icon: 'custom:mingcute:eye-2-line', onClick: () => { alert('Clicked on button 2') } },
      { icon: 'custom:mingcute:more-2-line', onClick: () => { alert('Clicked on button 3') } },
    ],
    hasPrimaryButton: true,
    primaryButtonText: 'publish',
    hasSecondaryButton: false,
    hasInfoTag: false,
  },
};

export const QuizFill = {
  render: (args, { argTypes }) => ({
    components: { SvnProTopBar },
    props: Object.keys(argTypes),
    setup() {
      return { args };
    },
    template: `
      <SvnProTopBar
        v-bind="args"
      />
    `,
  }),
  args: {
    hasRightContent: true,
    hasLeftContent: false,
    hasCloseButton: true,
    iconButtonsNumber: 0,
    iconButtonsArray: [],
    hasPrimaryButton: false,
    hasSecondaryButton: false,
    hasInfoTag: false,
  },
};

export const QuizResult = {
  render: (args, { argTypes }) => ({
    components: { SvnProTopBar },
    props: Object.keys(argTypes),
    setup() {
      return { args };
    },
    template: `
      <SvnProTopBar
        v-bind="args"
      />
    `,
  }),
  args: {
    hasRightContent: true,
    hasLeftContent: false,
    hasCloseButton: true,
    iconButtonsNumber: 0,
    iconButtonsArray: [],
    hasPrimaryButton: false,
    hasSecondaryButton: false,
    hasInfoTag: false,
  },
};

export const FaceToFaceFill = {
  render: (args, { argTypes }) => ({
    components: { SvnProTopBar },
    props: Object.keys(argTypes),
    setup() {
      return { args };
    },
    template: `
      <SvnProTopBar
        v-bind="args"
      />
    `,
  }),
  args: {
    hasRightContent: true,
    hasLeftContent: false,
    hasCloseButton: true,
    iconButtonsNumber: 1,
    iconButtonsArray: [
      { icon: 'custom:mingcute:download-2-line', onClick: () => { alert('Clicked on button 1') } },
    ],
    hasPrimaryButton: false,
    hasSecondaryButton: false,
    hasInfoTag: false,
  },
};

export const TemplateEdit = {
  render: (args, { argTypes }) => ({
    components: { SvnProTopBar },
    props: Object.keys(argTypes),
    setup() {
      return { args };
    },
    template: `
      <SvnProTopBar
        v-bind="args"
      />
    `,
  }),
  args: {
    hasRightContent: true,
    hasLeftContent: false,
    hasCloseButton: true,
    iconButtonsNumber: 2,
    iconButtonsArray: [
      { icon: 'custom:mingcute:list-ordered-line', onClick: () => { alert('Clicked on button 1') } },
      { icon: 'custom:mingcute:eye-2-line', onClick: () => { alert('Clicked on button 2') } },
    ],
    hasPrimaryButton: false,
    hasSecondaryButton: false,
    hasInfoTag: true,
    infoTagText: 'Saved',
  },
};

export const TemplatePreview = {
  render: (args, { argTypes }) => ({
    components: { SvnProTopBar },
    props: Object.keys(argTypes),
    setup() {
      return { args };
    },
    template: `
      <SvnProTopBar
        v-bind="args"
      />
    `,
  }),
  args: {
    hasRightContent: true,
    hasLeftContent: false,
    hasCloseButton: true,
    iconButtonsNumber: 1,
    iconButtonsArray: [
      { icon: 'custom:mingcute:list-ordered-line', onClick: () => { alert('Clicked on button 1') } },
    ],
    hasPrimaryButton: false,
    hasSecondaryButton: false,
    hasInfoTag: false,
  },
};

export const InterviewEdit = {
  render: (args, { argTypes }) => ({
    components: { SvnProTopBar },
    props: Object.keys(argTypes),
    setup() {
      return { args };
    },
    template: `
      <SvnProTopBar
        v-bind="args"
      />
    `,
  }),
  args: {
    hasRightContent: true,
    hasLeftContent: false,
    hasCloseButton: false,
    iconButtonsNumber: 2,
    iconButtonsArray: [
      { icon: 'custom:mingcute:history-anticlockwise-line', onClick: () => { alert('Clicked on button 1') } },
      { icon: 'custom:mingcute:list-ordered-line', onClick: () => { alert('Clicked on button 2') } },
    ],
    hasPrimaryButton: true,
    primaryButtonText: 'Submit',
    primaryButtonIcon: 'custom:mingcute:send-line',
    hasSecondaryButton: true,
    secondaryButtonText: 'Download Pdf',
    secondaryButtonIcon: 'custom:mingcute:download-2-line',
    hasInfoTag: true,
    infoTagText: 'Saved',
  },
};

export const InterviewShow = {
  render: (args, { argTypes }) => ({
    components: { SvnProTopBar },
    props: Object.keys(argTypes),
    setup() {
      return { args };
    },
    template: `
      <SvnProTopBar
        v-bind="args"
      />
    `,
  }),
  args: {
    hasRightContent: true,
    hasLeftContent: false,
    hasCloseButton: false,
    iconButtonsNumber: 2,
    iconButtonsArray: [
      { icon: 'custom:mingcute:history-anticlockwise-line', onClick: () => { alert('Clicked on button 1') } },
      { icon: 'custom:mingcute:list-ordered-line', onClick: () => { alert('Clicked on button 2') } },
    ],
    hasPrimaryButton: false,
    hasSecondaryButton: true,
    secondaryButtonText: 'Download Pdf',
    secondaryButtonIcon: 'custom:mingcute:arrow-right-circle-line',
    hasInfoTag: false,
  },
};