import SvnProTopBarLearnModule from '../components/menu/SvnProTopBarLearnModule.vue';
import {
  TopBarLearnModuleTypes,
  TopBarLearnModuleSaveTypes,
} from '../constants/types.js';

export default {
  title: 'Design System/Components/V2/SvnProTopBarLearnModule',
  component: SvnProTopBarLearnModule,
  tags: ['autodocs'],
  argTypes: {
    type: {
      control: 'select',
      options: Object.values(TopBarLearnModuleTypes),
    },
    isFavorited: {
      control: 'boolean',
    },
    publishText: {
      control: 'text',
    },
    saveText: {
      control: 'text',
    },
    updateText: {
      control: 'text',
    },
    saveMode: {
      control: 'select',
      options: Object.values(TopBarLearnModuleSaveTypes),
    },
    creatorOrAdmin: {
      control: 'boolean',
    },
  },
};

export const Playground = {
  render: (args) => ({
    components: { SvnProTopBarLearnModule },
    setup() {
      return { args };
    },
    template: '<SvnProTopBarLearnModule v-bind="args" />',
  }),
  args: {
    type: 'show',
    saveMode: 'auto',
    saveText: 'Save',
    isFavorited: false,
    updateText: 'Update',
    creatorOrAdmin: false,
    publishText: 'Publish',
  },
};
