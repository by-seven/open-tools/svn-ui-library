import SvnProTrainingCard from "../components/card/SvnProTrainingCard.vue";

export default {
  title: "Design System/Components/V2/SvnProTrainingCard",
  component: SvnProTrainingCard,
  tags: ["autodocs"],
  argTypes: {
    cardSize: {
      control: "select",
      options: ["compact", "default"],
    },
    status: {
      control: "select",
      options: ["not_started", "in_progress", "completed"],
    },
    image: {
      control: "text",
    },
    title: {
      control: "text",
    },
    duration: {
      control: "text",
    },
    progress: {
      control: "number",
    },
    primaryButtonText: {
      control: "text",
    }
  },
  parameters: {
    docs: {
      description: {
        component: "Component for Learn Training Card.",
      },
    },
  },
}

export const Playground = {
  render: (args) => ({
    components: { SvnProTrainingCard },
    setup() {
      return { args };
    },
    template: '<SvnProTrainingCard v-bind="args" />',
  }),
  args: {
    cardSize: "compact",
    status: "completed",
    image: "https://sm.ign.com/ign_fr/cover/a/avatar-gen/avatar-generations_bssq.jpg",
    title: "Formation hygiène niveau 1 pour pizzaiolos niveau 1 pour pizzaiolosert",
    duration: "2h35",
    progress: 63,
    primaryButtonText: "Start training!",
  },
};