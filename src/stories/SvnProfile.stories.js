import SvnProfile from "../components/profile/SvnProfile.vue";

const tags = [
  "tag short 1",
  "tag loooooooong 1",
  "tag loooooooong 1",
  "tag loooooooong 1",
  "tag loooooooong 1",
  "tag loooooooong 1",
  "tag loooooooong 1",
  "tag loooooooong 1",
  "tag loooooooong 1",
  "tag loooooooong 1",
  "tag loooooooong 1",
  "tag loooooooong 1",
  "tag loooooooong 1",
];

export default {
  title: "Design System/Components/V1/SvnProfile",
  component: SvnProfile,
  tags: ["autodocs"],
  argTypes: {
    appVariant: {
      control: "select",
      options: ["aleph", "studio"],
    },
  },
  parameters: {
    docs: {
      description: {
        component: "Component for The Profile, with the picture and the tags.",
      },
    },
  },
};

export const Playground = {
  render: (args) => ({
    components: { SvnProfile },
    setup() {
      return { args };
    },
    template: '<SvnProfile v-bind="args" />',
  }),
  args: {
    tags: tags,
    firstname: "Johnnnn",
    lastname: "Doe",
    email: "johndoe@gmail.com",
    role: "Commercial Director",
    appVariant: "aleph",
  },
};
