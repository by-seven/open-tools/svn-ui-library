import SvnStudioTrainerList from "../components/list/SvnStudioTrainerList.vue";

const trainerList = [
  {
    avatar:
      "https://sm.ign.com/ign_fr/cover/a/avatar-gen/avatar-generations_bssq.jpg",
    firstname: "John",
    lastname: "Doe",
  },
  {
    avatar:
      "https://sm.ign.com/ign_fr/cover/a/avatar-gen/avatar-generations_bssq.jpg",
    firstname: "Sane",
    lastname: "Doe",
  },
  {
    avatar:
      "https://sm.ign.com/ign_fr/cover/a/avatar-gen/avatar-generations_bssq.jpg",
    firstname: "Paul",
    lastname: "Doe",
  },
  {
    avatar:
      "https://sm.ign.com/ign_fr/cover/a/avatar-gen/avatar-generations_bssq.jpg",
    firstname: "Rick",
    lastname: "Doe",
  },
  {
    avatar:
      "https://sm.ign.com/ign_fr/cover/a/avatar-gen/avatar-generations_bssq.jpg",
    firstname: "Rick",
    lastname: "Doe",
  },
];

export default {
  title: "Design System/Components/V1/SvnStudioTrainerList",
  component: SvnStudioTrainerList,
  tags: ["autodocs"],
  parameters: {
    docs: {
      description: {
        component:
          "Component for the List of Trainers with the '+' to add new ones.",
      },
    },
  },
};

export const Playground = {
  render: (args) => ({
    components: { SvnStudioTrainerList },
    setup() {
      return { args };
    },
    template: '<SvnStudioTrainerList v-bind="args" />',
  }),
  args: {
    trainersList: trainerList,
    edit: true,
  },
};
