import SvnStudioTrainingCard from "../components/card/SvnStudioTrainingCard.vue";

const card = {
  startline: "27/03/2023",
  endline: "14/06/2023",
  title: "Content title of the training",
  description: "Get Your Guide - France Company",
  logo: "https://upload.wikimedia.org/wikipedia/fr/8/86/Paris_Saint-Germain_Logo.svg",
  creator:
    "https://sm.ign.com/ign_fr/cover/a/avatar-gen/avatar-generations_bssq.jpg",
  session_number: 2,
};

export default {
  title: "Design System/Components/V1/SvnStudioTrainingCard",
  component: SvnStudioTrainingCard,
  tags: ["autodocs"],
  parameters: {
    docs: {
      description: {
        component: "Component for the Training card in Studio.",
      },
    },
  },
};

export const Playground = {
  render: (args) => ({
    components: { SvnStudioTrainingCard },
    setup() {
      return { args };
    },
    template: '<SvnStudioTrainingCard v-bind="args" />',
  }),
  args: {
    card: card,
    dateNotSetText: "Date not set",
  },
};
