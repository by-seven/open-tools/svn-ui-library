import SvnSwitch from "../components/input/SvnSwitch.vue";

export default {
  title: "Design System/Components/V1/SvnSwitch",
  component: SvnSwitch,
  tags: ["autodocs"],
  parameters: {
    docs: {
      description: {
        component: "Component for the switch.",
      },
    },
  },
};

export const Playground = {
  render: (args) => ({
    components: { SvnSwitch },
    setup() {
      return { args };
    },
    template: '<SvnSwitch v-bind="args" />',
  }),
  args: {
    readOnly: false,
    density: "default",
    color: "primary",
    label: "A switch",
    inset: false,
    ripple: false,
  },
};
