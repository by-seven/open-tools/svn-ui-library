import SvnText from "../components/styles/SvnText.vue";

export default {
  title: "Design System/Components/V1/SvnText",
  component: SvnText,
  tags: ["autodocs"],
  argTypes: {
    color: {
      control: "text",
    },
    base: {
      control: "boolean",
    },
  },
  parameters: {
    docs: {
      description: {
        component:
          "Component for the Title, with 3 variants h1, h2 and h3. You can also apply many modifiers to style it.",
      },
    },
  },
};

export const Playground = {
  render: (args) => ({
    components: { SvnText },
    setup() {
      return { args };
    },
    template: '<SvnText v-bind="args" />',
  }),
  args: {
    color: "primary",
    base: true,
    sm: false,
    xs: false,
    xxs: false,
    bold: false,
    semiBold: false,
    medium: false,
    regular: false,
    italic: false,
    text: "One</br>Two</br>Three</br>Four</br>Five",
    textarea: false,
  },
};
