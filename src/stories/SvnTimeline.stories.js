import SvnTimeline from "../components/SvnTimeline.vue";

export default {
  title: "Design System/Components/V1/SvnTimeline",
  component: SvnTimeline,
  tags: ["autodocs"],
  argTypes: {
    truncateLine: {
      control: "text",
    },
    lineThickness: {
      control: {
        type: "number",
      },
    },
    size: {
      control: "select",
      options: ["x-small", "small", "large"],
    },
  },
  parameters: {
    docs: {
      description: {
        component: "Component for the Timeline.",
      },
    },
  },
};

const items = [
  { title: "Set up campaign", state: null, dotColor: "primary" },
  { title: "Choose your setting", state: "in_progress", dotColor: "primary" },
  {
    title: "Select the participant",
    state: "not_started",
    dotColor: "primary",
  },
];

export const Playground = {
  render: (args) => ({
    components: { SvnTimeline },
    setup() {
      return { args };
    },
    template: '<SvnTimeline v-bind="args" />',
  }),
  args: {
    items: items,
    truncateLine: "both",
    lineThickness: 1,
    size: "small",
  },
};
