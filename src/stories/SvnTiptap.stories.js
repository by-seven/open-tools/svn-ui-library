import SvnTiptapTest from './Components/SvnTiptapTest.vue';

export default {
  title: 'Design System/Components/V2/SvnProTiptap',
  component: SvnTiptapTest,
  tags: ['autodocs'],
  argTypes: {
    isEditable: { control: 'boolean' },
    withBorder: { control: 'boolean' },
    withPadding: { control: 'boolean' },
    extensionLeftMenu: { control: 'boolean' },
    size: { control: 'select', options: ['default', 'compact'] },
  },
  parameters: {
    docs: {
      description: {
        component: 'Component for the NEW Text Editor (THE TIPTAP GOAT).',
      },
    },
  },
};

export const Playground = {
  render: (args) => ({
    components: { SvnTiptapTest },
    setup() {
      return { args };
    },
    template: '<SvnTiptapTest v-bind="args" />',
  }),
  args: {
    size: 'default',
    isEditable: true,
    withBorder: true,
    withPadding: true,
    extensionLeftMenu: true,
    htmlData: '<p>🔥Vue.js + Tiptap Block Editor Template</p>',
  },
};
