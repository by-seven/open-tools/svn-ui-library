import SvnTitle from "../components/styles/SvnTitle.vue";

export default {
  title: "Design System/Components/V1/SvnTitle",
  component: SvnTitle,
  tags: ["autodocs"],
  argTypes: {
    color: {
      control: "text",
    },
    h1: {
      control: "boolean",
    },
  },
  parameters: {
    docs: {
      description: {
        component:
          "Component for the Title, with 3 variants h1, h2 and h3. You can also apply many modifiers to style it.",
      },
    },
  },
};

export const Playground = {
  render: (args) => ({
    components: { SvnTitle },
    setup() {
      return { args };
    },
    template: '<SvnTitle v-bind="args" />',
  }),
  args: {
    color: "primary",
    h1: true,
    h2: false,
    h3: false,
    bold: false,
    semiBold: false,
    medium: false,
    regular: false,
    italic: false,
  },
};
