import SvnTooltip from "../components/SvnTooltip.vue";

export default {
  title: "Design System/Components/V1/SvnTooltip",
  component: SvnTooltip,
  tags: ["autodocs"],
  parameters: {
    docs: {
      description: {
        component: "Component for the Tooltip, with the colors of the App.",
      },
    },
  },
};

export const Playground = {
  render: (args) => ({
    components: { SvnTooltip },
    setup() {
      return { args };
    },
    template: '<SvnTooltip v-bind="args" />',
  }),
  args: {
    tooltipText: "This is just a tooltip, but lorem ipsum am sir dolot",
  },
};
