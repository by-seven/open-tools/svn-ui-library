// Utilities
import { getCurrentInstance } from './getCurrentInstance'

export function useRender (render) {
  const vm = getCurrentInstance('useRender');

  vm.render = render;
}