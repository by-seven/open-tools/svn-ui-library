declare const _default: __VLS_WithTemplateSlots<typeof __VLS_component, __VLS_TemplateResult["slots"]>;
export default _default;
type __VLS_WithTemplateSlots<T, S> = T & (new () => {
    $slots: S;
});
declare const __VLS_component: import("vue").DefineComponent<{}, {
    $emit: (event: "click" | "input", ...args: any[]) => void;
    persistent: boolean;
    value: string;
    users: unknown[];
    searchText: string;
    $props: {
        readonly persistent?: boolean | undefined;
        readonly value?: string | undefined;
        readonly users?: unknown[] | undefined;
        readonly searchText?: string | undefined;
    };
}, {}, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").PublicProps, Readonly<{}> & Readonly<{}>, {}, {}, {}, {}, string, import("vue").ComponentProvideOptions, true, {}, any>;
type __VLS_TemplateResult = {
    slots: {
        dropdown?(_: {}): any;
        "dropdown-additional"?(_: {
            search: string;
        }): any;
    };
    refs: {};
    attrs: Partial<typeof __VLS_inheritedAttrs>;
};
declare var __VLS_inheritedAttrs: {};
