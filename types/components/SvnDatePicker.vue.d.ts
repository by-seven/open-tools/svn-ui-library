declare const _default: import("vue").DefineComponent<{}, {
    $emit: (event: "update:modelValue", ...args: any[]) => void;
    error: boolean;
    label: string;
    title: string;
    variant: string;
    disable: boolean;
    modelValue: string;
    density: string;
    baseColor: string;
    placeholder: string;
    inputClass: string;
    headerText: string;
    $props: {
        readonly error?: boolean | undefined;
        readonly label?: string | undefined;
        readonly title?: string | undefined;
        readonly variant?: string | undefined;
        readonly disable?: boolean | undefined;
        readonly modelValue?: string | undefined;
        readonly density?: string | undefined;
        readonly baseColor?: string | undefined;
        readonly placeholder?: string | undefined;
        readonly inputClass?: string | undefined;
        readonly headerText?: string | undefined;
    };
}, {}, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").PublicProps, Readonly<{}> & Readonly<{}>, {}, {}, {}, {}, string, import("vue").ComponentProvideOptions, true, {}, any>;
export default _default;
