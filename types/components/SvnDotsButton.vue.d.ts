declare const _default: __VLS_WithTemplateSlots<typeof __VLS_component, __VLS_TemplateResult["slots"]>;
export default _default;
type __VLS_WithTemplateSlots<T, S> = T & (new () => {
    $slots: S;
});
declare const __VLS_component: import("vue").DefineComponent<{}, {
    table: boolean;
    circle: boolean;
    variant: string;
    icon: string;
    buttonSize: string;
    disable: boolean;
    to: string;
    danger: boolean;
    colorClass: string;
    show: boolean;
    $props: {
        readonly table?: boolean | undefined;
        readonly circle?: boolean | undefined;
        readonly variant?: string | undefined;
        readonly icon?: string | undefined;
        readonly buttonSize?: string | undefined;
        readonly disable?: boolean | undefined;
        readonly to?: string | undefined;
        readonly danger?: boolean | undefined;
        readonly colorClass?: string | undefined;
        readonly show?: boolean | undefined;
    };
}, {}, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").PublicProps, Readonly<{}> & Readonly<{}>, {}, {}, {}, {}, string, import("vue").ComponentProvideOptions, true, {}, any>;
type __VLS_TemplateResult = {
    slots: {
        before?(_: {}): any;
        items?(_: {}): any;
        after?(_: {}): any;
    };
    refs: {};
    attrs: Partial<typeof __VLS_inheritedAttrs>;
};
declare var __VLS_inheritedAttrs: {};
