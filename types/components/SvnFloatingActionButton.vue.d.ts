declare const _default: __VLS_WithTemplateSlots<typeof __VLS_component, __VLS_TemplateResult["slots"]>;
export default _default;
type __VLS_WithTemplateSlots<T, S> = T & (new () => {
    $slots: S;
});
declare const __VLS_component: import("vue").DefineComponent<{}, {
    dialog: import("vue").Ref<boolean, boolean>;
    default: boolean;
    menu: boolean;
    text: string;
    icon: string;
    square: boolean;
    round: boolean;
    mini: boolean;
    iconCustomClass: string;
    scrimColor: string;
    loading: boolean;
    doubleIcon: boolean;
    pageRatioDisplay: number;
    toTop: boolean;
    $props: {
        readonly default?: boolean | undefined;
        readonly menu?: boolean | undefined;
        readonly text?: string | undefined;
        readonly icon?: string | undefined;
        readonly square?: boolean | undefined;
        readonly round?: boolean | undefined;
        readonly mini?: boolean | undefined;
        readonly iconCustomClass?: string | undefined;
        readonly scrimColor?: string | undefined;
        readonly loading?: boolean | undefined;
        readonly doubleIcon?: boolean | undefined;
        readonly pageRatioDisplay?: number | undefined;
        readonly toTop?: boolean | undefined;
    };
}, {}, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").PublicProps, Readonly<{}> & Readonly<{}>, {}, {}, {}, {}, string, import("vue").ComponentProvideOptions, true, {}, any>;
type __VLS_TemplateResult = {
    slots: {
        default?(_: {}): any;
        loading?(_: {}): any;
    };
    refs: {};
    attrs: Partial<typeof __VLS_inheritedAttrs>;
};
declare var __VLS_inheritedAttrs: {};
