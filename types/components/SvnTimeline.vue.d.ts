declare const _default: __VLS_WithTemplateSlots<typeof __VLS_component, __VLS_TemplateResult["slots"]>;
export default _default;
type __VLS_WithTemplateSlots<T, S> = T & (new () => {
    $slots: S;
});
declare const __VLS_component: import("vue").DefineComponent<{}, {
    size: string;
    items: Record<string, any>;
    truncateLine: string;
    lineThickness: number;
    lineInset: number;
    dotColor: string;
    $props: {
        readonly size?: string | undefined;
        readonly items?: Record<string, any> | undefined;
        readonly truncateLine?: string | undefined;
        readonly lineThickness?: number | undefined;
        readonly lineInset?: number | undefined;
        readonly dotColor?: string | undefined;
    };
}, {}, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").PublicProps, Readonly<{}> & Readonly<{}>, {}, {}, {}, {}, string, import("vue").ComponentProvideOptions, true, {}, any>;
type __VLS_TemplateResult = {
    slots: {
        title?(_: {
            item: any;
        }): any;
        centerItem?(_: {
            item: any;
        }): any;
        block?(_: {
            item: any;
        }): any;
    };
    refs: {};
    attrs: Partial<typeof __VLS_inheritedAttrs>;
};
declare var __VLS_inheritedAttrs: {};
