declare const _default: __VLS_WithTemplateSlots<typeof __VLS_component, __VLS_TemplateResult["slots"]>;
export default _default;
type __VLS_WithTemplateSlots<T, S> = T & (new () => {
    $slots: S;
});
declare const __VLS_component: import("vue").DefineComponent<{}, {
    $emit: (event: "update:modelValue", ...args: any[]) => void;
    color: string;
    width: string;
    modelValue: string;
    size: string;
    bgColor: string;
    indeterminate: boolean;
    loadingSize: string;
    $props: {
        readonly color?: string | undefined;
        readonly width?: string | undefined;
        readonly modelValue?: string | undefined;
        readonly size?: string | undefined;
        readonly bgColor?: string | undefined;
        readonly indeterminate?: boolean | undefined;
        readonly loadingSize?: string | undefined;
    };
}, {}, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").PublicProps, Readonly<{}> & Readonly<{}>, {}, {}, {}, {}, string, import("vue").ComponentProvideOptions, true, {}, any>;
type __VLS_TemplateResult = {
    slots: {
        default?(_: {}): any;
    };
    refs: {};
    attrs: Partial<typeof __VLS_inheritedAttrs>;
};
declare var __VLS_inheritedAttrs: {};
