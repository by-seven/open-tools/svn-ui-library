declare const _default: import("vue").DefineComponent<{}, {
    size: string;
    alt: string;
    avatar: string;
    firstname: string;
    lastname: string;
    $props: {
        readonly size?: string | undefined;
        readonly alt?: string | undefined;
        readonly avatar?: string | undefined;
        readonly firstname?: string | undefined;
        readonly lastname?: string | undefined;
    };
}, {}, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").PublicProps, Readonly<{}> & Readonly<{}>, {}, {}, {}, {}, string, import("vue").ComponentProvideOptions, true, {}, any>;
export default _default;
