declare const _default: import("vue").DefineComponent<{}, {
    type: string;
    start: boolean;
    size: number;
    image?: string | undefined;
    firstname?: string | undefined;
    $props: {
        readonly type?: string | undefined;
        readonly start?: boolean | undefined;
        readonly size?: number | undefined;
        readonly image?: string | undefined;
        readonly firstname?: string | undefined;
    };
}, {}, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").PublicProps, Readonly<{}> & Readonly<{}>, {}, {}, {}, {}, string, import("vue").ComponentProvideOptions, true, {}, any>;
export default _default;
