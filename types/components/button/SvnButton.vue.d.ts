declare const _default: __VLS_WithTemplateSlots<typeof __VLS_component, __VLS_TemplateResult["slots"]>;
export default _default;
type __VLS_WithTemplateSlots<T, S> = T & (new () => {
    $slots: S;
});
declare const __VLS_component: import("vue").DefineComponent<{}, {
    $emit: (event: "click", ...args: any[]) => void;
    text: string;
    variant: string;
    disable: boolean;
    to: string;
    danger: boolean;
    colorClass: string;
    loading: boolean;
    block: boolean;
    loaderColor: string;
    prependIcon: string;
    appendIcon: string;
    prependClass: string;
    loadingColorClass: string;
    $props: {
        readonly text?: string | undefined;
        readonly variant?: string | undefined;
        readonly disable?: boolean | undefined;
        readonly to?: string | undefined;
        readonly danger?: boolean | undefined;
        readonly colorClass?: string | undefined;
        readonly loading?: boolean | undefined;
        readonly block?: boolean | undefined;
        readonly loaderColor?: string | undefined;
        readonly prependIcon?: string | undefined;
        readonly appendIcon?: string | undefined;
        readonly prependClass?: string | undefined;
        readonly loadingColorClass?: string | undefined;
    };
}, {}, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").PublicProps, Readonly<{}> & Readonly<{}>, {}, {}, {}, {}, string, import("vue").ComponentProvideOptions, true, {}, any>;
type __VLS_TemplateResult = {
    slots: {
        prepend?(_: {}): any;
        default?(_: {}): any;
        "button-text"?(_: {}): any;
        append?(_: {}): any;
        loader?(_: {}): any;
    };
    refs: {};
    attrs: Partial<typeof __VLS_inheritedAttrs>;
};
declare var __VLS_inheritedAttrs: {};
