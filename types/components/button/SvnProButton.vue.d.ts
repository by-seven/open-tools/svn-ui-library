declare const _default: __VLS_WithTemplateSlots<typeof __VLS_component, __VLS_TemplateResult["slots"]>;
export default _default;
type __VLS_WithTemplateSlots<T, S> = T & (new () => {
    $slots: S;
});
declare const __VLS_component: import("vue").DefineComponent<{}, {
    text: string;
    variant: string;
    color: string;
    loading: boolean;
    disabled: boolean;
    block: boolean;
    append: boolean;
    prependIcon?: string | undefined;
    $props: {
        readonly text?: string | undefined;
        readonly variant?: string | undefined;
        readonly color?: string | undefined;
        readonly loading?: boolean | undefined;
        readonly disabled?: boolean | undefined;
        readonly block?: boolean | undefined;
        readonly append?: boolean | undefined;
        readonly prependIcon?: string | undefined;
    };
}, {}, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").PublicProps, Readonly<{}> & Readonly<{}>, {}, {}, {}, {}, string, import("vue").ComponentProvideOptions, true, {}, any>;
type __VLS_TemplateResult = {
    slots: {
        append?(_: {}): any;
    };
    refs: {};
    attrs: Partial<typeof __VLS_inheritedAttrs>;
};
declare var __VLS_inheritedAttrs: {};
