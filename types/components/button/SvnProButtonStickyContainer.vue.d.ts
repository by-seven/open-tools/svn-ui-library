declare const _default: import("vue").DefineComponent<{}, {
    $emit: (event: "clickPrimaryAction" | "clickSecondaryAction", ...args: any[]) => void;
    configuration: string;
    hasPrimaryAction: boolean;
    hasSecondaryAction: boolean;
    primaryActionText: string;
    secondaryActionText: string;
    $props: {
        readonly configuration?: string | undefined;
        readonly hasPrimaryAction?: boolean | undefined;
        readonly hasSecondaryAction?: boolean | undefined;
        readonly primaryActionText?: string | undefined;
        readonly secondaryActionText?: string | undefined;
    };
}, {}, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").PublicProps, Readonly<{}> & Readonly<{}>, {}, {}, {}, {}, string, import("vue").ComponentProvideOptions, true, {}, any>;
export default _default;
