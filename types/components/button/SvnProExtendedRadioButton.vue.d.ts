declare const _default: import("vue").DefineComponent<{}, {
    $emit: (event: "item-clicked", ...args: any[]) => void;
    icon: string;
    disabled: boolean;
    title?: string | undefined;
    subtitle?: string | undefined;
    value?: string | undefined;
    $props: {
        readonly icon?: string | undefined;
        readonly disabled?: boolean | undefined;
        readonly title?: string | undefined;
        readonly subtitle?: string | undefined;
        readonly value?: string | undefined;
    };
}, {}, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").PublicProps, Readonly<{}> & Readonly<{}>, {}, {}, {}, {}, string, import("vue").ComponentProvideOptions, true, {}, any>;
export default _default;
