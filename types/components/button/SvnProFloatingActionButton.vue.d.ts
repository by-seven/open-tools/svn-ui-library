declare const _default: import("vue").DefineComponent<{}, {
    $emit: (event: "click", ...args: any[]) => void;
    text: string;
    variant: string;
    icon: string;
    pageRatioDisplay: number;
    toTop: boolean;
    size: string;
    extended: boolean;
    $props: {
        readonly text?: string | undefined;
        readonly variant?: string | undefined;
        readonly icon?: string | undefined;
        readonly pageRatioDisplay?: number | undefined;
        readonly toTop?: boolean | undefined;
        readonly size?: string | undefined;
        readonly extended?: boolean | undefined;
    };
}, {}, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").PublicProps, Readonly<{}> & Readonly<{}>, {}, {}, {}, {}, string, import("vue").ComponentProvideOptions, true, {}, any>;
export default _default;
