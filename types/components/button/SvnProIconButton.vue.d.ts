declare const _default: import("vue").DefineComponent<{}, {
    toggle: boolean;
    variant: string;
    icon: string;
    color: string;
    disabled: boolean;
    rounded: string;
    $props: {
        readonly toggle?: boolean | undefined;
        readonly variant?: string | undefined;
        readonly icon?: string | undefined;
        readonly color?: string | undefined;
        readonly disabled?: boolean | undefined;
        readonly rounded?: string | undefined;
    };
}, {}, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").PublicProps, Readonly<{}> & Readonly<{}>, {}, {}, {}, {}, string, import("vue").ComponentProvideOptions, true, {}, any>;
export default _default;
