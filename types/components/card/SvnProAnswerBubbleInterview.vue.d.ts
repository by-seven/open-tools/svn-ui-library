declare const _default: import("vue").DefineComponent<{}, {
    side: boolean;
    user: Record<string, any>;
    comment: string;
    crossed: boolean;
    selectedValue: string;
    ratingLength: string;
    bubbleTitle: string;
    isLastInterviewCompletedAndLocked: boolean;
    questionType: string;
    $props: {
        readonly side?: boolean | undefined;
        readonly user?: Record<string, any> | undefined;
        readonly comment?: string | undefined;
        readonly crossed?: boolean | undefined;
        readonly selectedValue?: string | undefined;
        readonly ratingLength?: string | undefined;
        readonly bubbleTitle?: string | undefined;
        readonly isLastInterviewCompletedAndLocked?: boolean | undefined;
        readonly questionType?: string | undefined;
    };
}, {}, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").PublicProps, Readonly<{}> & Readonly<{}>, {}, {}, {}, {}, string, import("vue").ComponentProvideOptions, true, {}, any>;
export default _default;
