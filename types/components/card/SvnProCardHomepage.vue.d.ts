declare const _default: import("vue").DefineComponent<{}, {
    $emit: (event: "action-clicked", ...args: any[]) => void;
    appName: string;
    appIcon: string;
    appColor: string;
    appDescription: string;
    appLabel?: string | undefined;
    $props: {
        readonly appName?: string | undefined;
        readonly appIcon?: string | undefined;
        readonly appColor?: string | undefined;
        readonly appDescription?: string | undefined;
        readonly appLabel?: string | undefined;
    };
}, {}, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").PublicProps, Readonly<{}> & Readonly<{}>, {}, {}, {}, {}, string, import("vue").ComponentProvideOptions, true, {}, any>;
export default _default;
