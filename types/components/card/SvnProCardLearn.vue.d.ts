declare const _default: __VLS_WithTemplateSlots<typeof __VLS_component, __VLS_TemplateResult["slots"]>;
export default _default;
type __VLS_WithTemplateSlots<T, S> = T & (new () => {
    $slots: S;
});
declare const __VLS_component: import("vue").DefineComponent<{}, {
    $emit: (event: "on-favorite" | "on-share" | "on-edit" | "on-delete" | "go-to-item-show", ...args: any[]) => void;
    state: string;
    editable: boolean;
    cardType: string;
    cardSize: string;
    reactionList: unknown[];
    isFavorited: boolean;
    editText: string;
    deleteText: string;
    favoriteIcon: string;
    isAcquired: boolean;
    editableIcon: string;
    shareable: boolean;
    shareableIcon: string;
    playlistImages: unknown[];
    themes: unknown[];
    acquiredText: string;
    acquiredIcon: string;
    playlistModules: unknown[];
    progress?: number | undefined;
    duration?: string | undefined;
    cardTitle?: string | undefined;
    reactionCount?: number | undefined;
    cardImage?: string | undefined;
    remaining?: string | undefined;
    count?: number | undefined;
    $props: {
        readonly state?: string | undefined;
        readonly editable?: boolean | undefined;
        readonly cardType?: string | undefined;
        readonly cardSize?: string | undefined;
        readonly reactionList?: unknown[] | undefined;
        readonly isFavorited?: boolean | undefined;
        readonly editText?: string | undefined;
        readonly deleteText?: string | undefined;
        readonly favoriteIcon?: string | undefined;
        readonly isAcquired?: boolean | undefined;
        readonly editableIcon?: string | undefined;
        readonly shareable?: boolean | undefined;
        readonly shareableIcon?: string | undefined;
        readonly playlistImages?: unknown[] | undefined;
        readonly themes?: unknown[] | undefined;
        readonly acquiredText?: string | undefined;
        readonly acquiredIcon?: string | undefined;
        readonly playlistModules?: unknown[] | undefined;
        readonly progress?: number | undefined;
        readonly duration?: string | undefined;
        readonly cardTitle?: string | undefined;
        readonly reactionCount?: number | undefined;
        readonly cardImage?: string | undefined;
        readonly remaining?: string | undefined;
        readonly count?: number | undefined;
    };
}, {}, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").PublicProps, Readonly<{}> & Readonly<{}>, {}, {}, {}, {}, string, import("vue").ComponentProvideOptions, true, {}, any>;
type __VLS_TemplateResult = {
    slots: {
        menu?(_: {}): any;
        menu?(_: {}): any;
        menu?(_: {}): any;
        menu?(_: {}): any;
    };
    refs: {};
    attrs: Partial<typeof __VLS_inheritedAttrs>;
};
declare var __VLS_inheritedAttrs: {};
