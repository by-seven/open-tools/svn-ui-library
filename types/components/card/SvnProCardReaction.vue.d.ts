declare const _default: import("vue").DefineComponent<{}, {
    $emit: (event: "toggle-reaction", ...args: any[]) => void;
    cardSize: string;
    reactions: unknown[];
    feedback: boolean;
    feedbackTitle: string;
    feedbackDescription: string;
    $props: {
        readonly cardSize?: string | undefined;
        readonly reactions?: unknown[] | undefined;
        readonly feedback?: boolean | undefined;
        readonly feedbackTitle?: string | undefined;
        readonly feedbackDescription?: string | undefined;
    };
}, {}, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").PublicProps, Readonly<{}> & Readonly<{}>, {}, {}, {}, {}, string, import("vue").ComponentProvideOptions, true, {}, any>;
export default _default;
