declare const _default: import("vue").DefineComponent<{}, {
    $emit: (event: "click", ...args: any[]) => void;
    indicatorIcon?: string | undefined;
    indicatorName?: string | undefined;
    targetTitle?: string | undefined;
    targetDeadline?: string | undefined;
    $props: {
        readonly indicatorIcon?: string | undefined;
        readonly indicatorName?: string | undefined;
        readonly targetTitle?: string | undefined;
        readonly targetDeadline?: string | undefined;
    };
}, {}, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").PublicProps, Readonly<{}> & Readonly<{}>, {}, {}, {}, {}, string, import("vue").ComponentProvideOptions, true, {}, any>;
export default _default;
