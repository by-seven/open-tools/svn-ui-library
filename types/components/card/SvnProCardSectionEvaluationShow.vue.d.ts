declare const _default: import("vue").DefineComponent<{}, {
    $emit: (event: "action-clicked", ...args: any[]) => void;
    cardType: string;
    cardTitle: string;
    cardDescription: string;
    cardButtonText: string;
    $props: {
        readonly cardType?: string | undefined;
        readonly cardTitle?: string | undefined;
        readonly cardDescription?: string | undefined;
        readonly cardButtonText?: string | undefined;
    };
}, {}, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").PublicProps, Readonly<{}> & Readonly<{}>, {}, {}, {}, {}, string, import("vue").ComponentProvideOptions, true, {}, any>;
export default _default;
