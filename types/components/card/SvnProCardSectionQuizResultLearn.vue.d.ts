declare const _default: import("vue").DefineComponent<{}, {
    $emit: (event: "back-to-training" | "retry-module" | "next-module", ...args: any[]) => void;
    cardType: string;
    cardSize: string;
    cardTitle: string;
    cardDescription: string;
    backToTrainingText: string;
    retryQuizText: string;
    nextModuleText: string;
    $props: {
        readonly cardType?: string | undefined;
        readonly cardSize?: string | undefined;
        readonly cardTitle?: string | undefined;
        readonly cardDescription?: string | undefined;
        readonly backToTrainingText?: string | undefined;
        readonly retryQuizText?: string | undefined;
        readonly nextModuleText?: string | undefined;
    };
}, {}, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").PublicProps, Readonly<{}> & Readonly<{}>, {}, {}, {}, {}, string, import("vue").ComponentProvideOptions, true, {}, any>;
export default _default;
