declare const _default: import("vue").DefineComponent<{}, {
    interview: boolean;
    size: string;
    roadmap: boolean;
    training: boolean;
    title?: string | undefined;
    subtitle?: string | undefined;
    interviewsSubmitted?: string | undefined;
    modulesAcquired?: string | undefined;
    currentTargets?: string | undefined;
    emoji?: string | undefined;
    $props: {
        readonly interview?: boolean | undefined;
        readonly size?: string | undefined;
        readonly roadmap?: boolean | undefined;
        readonly training?: boolean | undefined;
        readonly title?: string | undefined;
        readonly subtitle?: string | undefined;
        readonly interviewsSubmitted?: string | undefined;
        readonly modulesAcquired?: string | undefined;
        readonly currentTargets?: string | undefined;
        readonly emoji?: string | undefined;
    };
}, {}, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").PublicProps, Readonly<{}> & Readonly<{}>, {}, {}, {}, {}, string, import("vue").ComponentProvideOptions, true, {}, any>;
export default _default;
