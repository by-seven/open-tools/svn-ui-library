declare const _default: import("vue").DefineComponent<{}, {
    $emit: (event: "click-primary-button" | "go-to-item-show", ...args: any[]) => void;
    primaryButtonText: string;
    cardSize: string;
    status: string;
    progress?: number | undefined;
    title?: string | undefined;
    image?: string | undefined;
    duration?: string | undefined;
    $props: {
        readonly primaryButtonText?: string | undefined;
        readonly cardSize?: string | undefined;
        readonly status?: string | undefined;
        readonly progress?: number | undefined;
        readonly title?: string | undefined;
        readonly image?: string | undefined;
        readonly duration?: string | undefined;
    };
}, {}, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").PublicProps, Readonly<{}> & Readonly<{}>, {}, {}, {}, {}, string, import("vue").ComponentProvideOptions, true, {}, any>;
export default _default;
