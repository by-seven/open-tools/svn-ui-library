declare const _default: import("vue").DefineComponent<{}, {
    card: Record<string, any>;
    dateNotSetText: string;
    $props: {
        readonly card?: Record<string, any> | undefined;
        readonly dateNotSetText?: string | undefined;
    };
}, {}, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").PublicProps, Readonly<{}> & Readonly<{}>, {}, {}, {}, {}, string, import("vue").ComponentProvideOptions, true, {}, any>;
export default _default;
