declare const _default: import("vue").DefineComponent<{}, {
    text: number;
    icon: string;
    appendText: string;
    $props: {
        readonly text?: number | undefined;
        readonly icon?: string | undefined;
        readonly appendText?: string | undefined;
    };
}, {}, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").PublicProps, Readonly<{}> & Readonly<{}>, {}, {}, {}, {}, string, import("vue").ComponentProvideOptions, true, {}, any>;
export default _default;
