declare const _default: import("vue").DefineComponent<{}, {
    max: number;
    dot: boolean;
    content: number;
    $props: {
        readonly max?: number | undefined;
        readonly dot?: boolean | undefined;
        readonly content?: number | undefined;
    };
}, {}, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").PublicProps, Readonly<{}> & Readonly<{}>, {}, {}, {}, {}, string, import("vue").ComponentProvideOptions, true, {}, any>;
export default _default;
