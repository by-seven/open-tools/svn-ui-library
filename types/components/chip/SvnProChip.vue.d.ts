declare const _default: __VLS_WithTemplateSlots<typeof __VLS_component, __VLS_TemplateResult["slots"]>;
export default _default;
type __VLS_WithTemplateSlots<T, S> = T & (new () => {
    $slots: S;
});
declare const __VLS_component: import("vue").DefineComponent<{}, {
    text: string;
    variant: string;
    disabled: boolean;
    avatar: boolean;
    closable: boolean;
    avatarType: string;
    isSlotAppend: boolean;
    prependIcon?: string | undefined;
    appendIcon?: string | undefined;
    elevation?: number | undefined;
    filterIcon?: string | undefined;
    avatarImage?: string | undefined;
    $props: {
        readonly text?: string | undefined;
        readonly variant?: string | undefined;
        readonly disabled?: boolean | undefined;
        readonly avatar?: boolean | undefined;
        readonly closable?: boolean | undefined;
        readonly avatarType?: string | undefined;
        readonly isSlotAppend?: boolean | undefined;
        readonly prependIcon?: string | undefined;
        readonly appendIcon?: string | undefined;
        readonly elevation?: number | undefined;
        readonly filterIcon?: string | undefined;
        readonly avatarImage?: string | undefined;
    };
}, {}, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").PublicProps, Readonly<{}> & Readonly<{}>, {}, {}, {}, {}, string, import("vue").ComponentProvideOptions, true, {}, any>;
type __VLS_TemplateResult = {
    slots: {
        prepend?(_: {}): any;
        append?(_: {}): any;
    };
    refs: {};
    attrs: Partial<typeof __VLS_inheritedAttrs>;
};
declare var __VLS_inheritedAttrs: {};
