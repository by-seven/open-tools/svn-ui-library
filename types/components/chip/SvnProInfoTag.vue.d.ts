declare const _default: import("vue").DefineComponent<{}, {
    tagType: string;
    tagSize: string;
    tagStyle: string;
    breakAll: boolean;
    text?: string | undefined;
    icon?: string | undefined;
    leadingItem?: string | undefined;
    $props: {
        readonly tagType?: string | undefined;
        readonly tagSize?: string | undefined;
        readonly tagStyle?: string | undefined;
        readonly breakAll?: boolean | undefined;
        readonly text?: string | undefined;
        readonly icon?: string | undefined;
        readonly leadingItem?: string | undefined;
    };
}, {}, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").PublicProps, Readonly<{}> & Readonly<{}>, {}, {}, {}, {}, string, import("vue").ComponentProvideOptions, true, {}, any>;
export default _default;
