declare const _default: import("vue").DefineComponent<{}, {
    levels: Record<string, any>;
    badgeType: string;
    count?: number | undefined;
    $props: {
        readonly levels?: Record<string, any> | undefined;
        readonly badgeType?: string | undefined;
        readonly count?: number | undefined;
    };
}, {}, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").PublicProps, Readonly<{}> & Readonly<{}>, {}, {}, {}, {}, string, import("vue").ComponentProvideOptions, true, {}, any>;
export default _default;
