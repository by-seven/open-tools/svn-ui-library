declare const _default: import("vue").DefineComponent<{}, {
    reactionList: unknown[];
    reactionCount?: number | undefined;
    $props: {
        readonly reactionList?: unknown[] | undefined;
        readonly reactionCount?: number | undefined;
    };
}, {}, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").PublicProps, Readonly<{}> & Readonly<{}>, {}, {}, {}, {}, string, import("vue").ComponentProvideOptions, true, {}, any>;
export default _default;
