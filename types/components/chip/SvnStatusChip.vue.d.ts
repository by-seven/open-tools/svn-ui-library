declare const _default: import("vue").DefineComponent<{}, {
    error: boolean;
    flat: boolean;
    text: string;
    disabled: boolean;
    compact: boolean;
    state: string;
    success: boolean;
    warning: boolean;
    dot: boolean;
    $props: {
        readonly error?: boolean | undefined;
        readonly flat?: boolean | undefined;
        readonly text?: string | undefined;
        readonly disabled?: boolean | undefined;
        readonly compact?: boolean | undefined;
        readonly state?: string | undefined;
        readonly success?: boolean | undefined;
        readonly warning?: boolean | undefined;
        readonly dot?: boolean | undefined;
    };
}, {}, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").PublicProps, Readonly<{}> & Readonly<{}>, {}, {}, {}, {}, string, import("vue").ComponentProvideOptions, true, {}, any>;
export default _default;
