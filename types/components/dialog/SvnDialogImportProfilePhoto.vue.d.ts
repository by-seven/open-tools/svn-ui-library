declare const _default: __VLS_WithTemplateSlots<typeof __VLS_component, __VLS_TemplateResult["slots"]>;
export default _default;
type __VLS_WithTemplateSlots<T, S> = T & (new () => {
    $slots: S;
});
declare const __VLS_component: import("vue").DefineComponent<{}, {
    dialog: import("vue").Ref<null, null>;
    uploadedAvatar: import("vue").Ref<null, null>;
    $emit: (event: "uploadFile" | "deletePicture", ...args: any[]) => void;
    dialogCustomClass: string;
    $props: {
        readonly dialogCustomClass?: string | undefined;
    };
}, {}, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").PublicProps, Readonly<{}> & Readonly<{}>, {}, {}, {}, {}, string, import("vue").ComponentProvideOptions, true, {}, any>;
type __VLS_TemplateResult = {
    slots: {
        activator?(_: {}): any;
    };
    refs: {
        dialog: ({
            $: ComponentInternalInstance;
            $data: {};
            $props: Partial<{}> & Omit<{} & VNodeProps & AllowedComponentProps & ComponentCustomProps & Readonly<{}> & Readonly<{}>, never>;
            $attrs: Data;
            $refs: Data;
            $slots: Readonly<InternalSlots>;
            $root: ComponentPublicInstance<{}, {}, {}, {}, {}, {}, {}, {}, false, ComponentOptionsBase<any, any, any, any, any, any, any, any, any, {}, {}, string, {}, {}, {}, string, ComponentProvideOptions>, {}, {}, "", {}, any> | null;
            $parent: ComponentPublicInstance<{}, {}, {}, {}, {}, {}, {}, {}, false, ComponentOptionsBase<any, any, any, any, any, any, any, any, any, {}, {}, string, {}, {}, {}, string, ComponentProvideOptions>, {}, {}, "", {}, any> | null;
            $host: Element | null;
            $emit: (event: string, ...args: any[]) => void;
            $el: any;
            $options: ComponentOptionsBase<ToResolvedProps<{}, {}>, {
                dialog: Ref<boolean, boolean>;
                $emit: (event: "clickPrimaryButton" | "clickSecondaryButton", ...args: any[]) => void;
                title: string;
                icon: string;
                iconSize: number;
                persistent: boolean;
                bind: boolean;
                activatorText: string;
                activatorVariant: string;
                displayClose: boolean;
                dialogHasTextArea: boolean;
                withInputs: boolean;
                closeIcon: string;
                primaryButtonLoadingClass: string;
                primaryButtonClass: string;
                secondaryButtonClass: string;
                subtitle: string;
                description: string;
                centerDescription: boolean;
                primaryButtonDisabled: boolean;
                primaryButtonLoading: boolean;
                primaryButtonText: string;
                secondaryButtonText: string;
                loaderColor: string;
                primaryButtonDanger: boolean;
                secondaryButtonDanger: boolean;
                secondaryButtonLoading: boolean;
                primaryButtonCloseOnClick: boolean;
                secondaryButtonCloseOnClick: boolean;
                dialogCustomClass: string;
                $props: {
                    readonly title?: string | undefined;
                    readonly icon?: string | undefined;
                    readonly iconSize?: number | undefined;
                    readonly persistent?: boolean | undefined;
                    readonly bind?: boolean | undefined;
                    readonly activatorText?: string | undefined;
                    readonly activatorVariant?: string | undefined;
                    readonly displayClose?: boolean | undefined;
                    readonly dialogHasTextArea?: boolean | undefined;
                    readonly withInputs?: boolean | undefined;
                    readonly closeIcon?: string | undefined;
                    readonly primaryButtonLoadingClass?: string | undefined;
                    readonly primaryButtonClass?: string | undefined;
                    readonly secondaryButtonClass?: string | undefined;
                    readonly subtitle?: string | undefined;
                    readonly description?: string | undefined;
                    readonly centerDescription?: boolean | undefined;
                    readonly primaryButtonDisabled?: boolean | undefined;
                    readonly primaryButtonLoading?: boolean | undefined;
                    readonly primaryButtonText?: string | undefined;
                    readonly secondaryButtonText?: string | undefined;
                    readonly loaderColor?: string | undefined;
                    readonly primaryButtonDanger?: boolean | undefined;
                    readonly secondaryButtonDanger?: boolean | undefined;
                    readonly secondaryButtonLoading?: boolean | undefined;
                    readonly primaryButtonCloseOnClick?: boolean | undefined;
                    readonly secondaryButtonCloseOnClick?: boolean | undefined;
                    readonly dialogCustomClass?: string | undefined;
                };
            }, {}, {}, {}, ComponentOptionsMixin, ComponentOptionsMixin, {}, string, {}, {}, string, {}, GlobalComponents, GlobalDirectives, string, ComponentProvideOptions> & MergedComponentOptionsOverride;
            $forceUpdate: () => void;
            $nextTick: typeof nextTick;
            $watch<T extends string | ((...args: any) => any)>(source: T, cb: T extends (...args: any) => infer R ? (args_0: R, args_1: R, args_2: OnCleanup) => any : (args_0: any, args_1: any, args_2: OnCleanup) => any, options?: WatchOptions<boolean> | undefined): WatchStopHandle;
        } & Readonly<{}> & Omit<Readonly<{}> & Readonly<{}>, "$props" | "$emit" | "dialog" | "title" | "icon" | "iconSize" | "persistent" | "bind" | "activatorText" | "activatorVariant" | "displayClose" | "dialogHasTextArea" | "withInputs" | "closeIcon" | "primaryButtonLoadingClass" | "primaryButtonClass" | "secondaryButtonClass" | "subtitle" | "description" | "centerDescription" | "primaryButtonDisabled" | "primaryButtonLoading" | "primaryButtonText" | "secondaryButtonText" | "loaderColor" | "primaryButtonDanger" | "secondaryButtonDanger" | "secondaryButtonLoading" | "primaryButtonCloseOnClick" | "secondaryButtonCloseOnClick" | "dialogCustomClass"> & ShallowUnwrapRef<{
            dialog: Ref<boolean, boolean>;
            $emit: (event: "clickPrimaryButton" | "clickSecondaryButton", ...args: any[]) => void;
            title: string;
            icon: string;
            iconSize: number;
            persistent: boolean;
            bind: boolean;
            activatorText: string;
            activatorVariant: string;
            displayClose: boolean;
            dialogHasTextArea: boolean;
            withInputs: boolean;
            closeIcon: string;
            primaryButtonLoadingClass: string;
            primaryButtonClass: string;
            secondaryButtonClass: string;
            subtitle: string;
            description: string;
            centerDescription: boolean;
            primaryButtonDisabled: boolean;
            primaryButtonLoading: boolean;
            primaryButtonText: string;
            secondaryButtonText: string;
            loaderColor: string;
            primaryButtonDanger: boolean;
            secondaryButtonDanger: boolean;
            secondaryButtonLoading: boolean;
            primaryButtonCloseOnClick: boolean;
            secondaryButtonCloseOnClick: boolean;
            dialogCustomClass: string;
            $props: {
                readonly title?: string | undefined;
                readonly icon?: string | undefined;
                readonly iconSize?: number | undefined;
                readonly persistent?: boolean | undefined;
                readonly bind?: boolean | undefined;
                readonly activatorText?: string | undefined;
                readonly activatorVariant?: string | undefined;
                readonly displayClose?: boolean | undefined;
                readonly dialogHasTextArea?: boolean | undefined;
                readonly withInputs?: boolean | undefined;
                readonly closeIcon?: string | undefined;
                readonly primaryButtonLoadingClass?: string | undefined;
                readonly primaryButtonClass?: string | undefined;
                readonly secondaryButtonClass?: string | undefined;
                readonly subtitle?: string | undefined;
                readonly description?: string | undefined;
                readonly centerDescription?: boolean | undefined;
                readonly primaryButtonDisabled?: boolean | undefined;
                readonly primaryButtonLoading?: boolean | undefined;
                readonly primaryButtonText?: string | undefined;
                readonly secondaryButtonText?: string | undefined;
                readonly loaderColor?: string | undefined;
                readonly primaryButtonDanger?: boolean | undefined;
                readonly secondaryButtonDanger?: boolean | undefined;
                readonly secondaryButtonLoading?: boolean | undefined;
                readonly primaryButtonCloseOnClick?: boolean | undefined;
                readonly secondaryButtonCloseOnClick?: boolean | undefined;
                readonly dialogCustomClass?: string | undefined;
            };
        }> & ExtractComputedReturns<{}> & ComponentCustomProperties & {} & {
            $slots: {
                "activator-div"?(_: {}): any;
                activator?(_: {}): any;
                "activator-text"?(_: {}): any;
                dialog?(_: {}): any;
                close?(_: {
                    close: () => void;
                }): any;
                icon?(_: {}): any;
                title?(_: {}): any;
                "title-text"?(_: {}): any;
                subtitle?(_: {}): any;
                "subtitle-text"?(_: {}): any;
                description?(_: {}): any;
                "description-text"?(_: {}): any;
                input?(_: {}): any;
                body?(_: {}): any;
                actionButtons?(_: {}): any;
                "primary-button"?(_: {}): any;
                "primary-button-text"?(_: {}): any;
                "secondary-button"?(_: {}): any;
                "secondary-button-text"?(_: {}): any;
            };
        }) | null;
        avatar: any;
    };
    attrs: Partial<typeof __VLS_inheritedAttrs>;
};
declare var __VLS_inheritedAttrs: {};
