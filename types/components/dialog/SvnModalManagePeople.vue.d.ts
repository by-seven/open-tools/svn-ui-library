declare const _default: __VLS_WithTemplateSlots<typeof __VLS_component, __VLS_TemplateResult["slots"]>;
export default _default;
type __VLS_WithTemplateSlots<T, S> = T & (new () => {
    $slots: S;
});
declare const __VLS_component: import("vue").DefineComponent<{}, {
    $emit: (event: "add-user" | "remove-user" | "custom-search", ...args: any[]) => void;
    title: string;
    persistent: boolean;
    activatorText: string;
    activatorVariant: string;
    vertical: boolean;
    modalTitleCenter: boolean;
    verticalHeader: boolean;
    prependHeaderIcon: string;
    appendHeaderIcon: string;
    appendIconCloseModal: boolean;
    headerCustomClass: string;
    modalCustomClass: string;
    searchUsers: unknown[];
    listUsers: unknown[];
    $props: {
        readonly title?: string | undefined;
        readonly persistent?: boolean | undefined;
        readonly activatorText?: string | undefined;
        readonly activatorVariant?: string | undefined;
        readonly vertical?: boolean | undefined;
        readonly modalTitleCenter?: boolean | undefined;
        readonly verticalHeader?: boolean | undefined;
        readonly prependHeaderIcon?: string | undefined;
        readonly appendHeaderIcon?: string | undefined;
        readonly appendIconCloseModal?: boolean | undefined;
        readonly headerCustomClass?: string | undefined;
        readonly modalCustomClass?: string | undefined;
        readonly searchUsers?: unknown[] | undefined;
        readonly listUsers?: unknown[] | undefined;
    };
}, {}, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").PublicProps, Readonly<{}> & Readonly<{}>, {}, {}, {}, {}, string, import("vue").ComponentProvideOptions, true, {}, any>;
type __VLS_TemplateResult = {
    slots: {
        activator?(_: {}): any;
    };
    refs: {};
    attrs: Partial<typeof __VLS_inheritedAttrs>;
};
declare var __VLS_inheritedAttrs: {};
