declare const _default: __VLS_WithTemplateSlots<typeof __VLS_component, __VLS_TemplateResult["slots"]>;
export default _default;
type __VLS_WithTemplateSlots<T, S> = T & (new () => {
    $slots: S;
});
declare const __VLS_component: import("vue").DefineComponent<{}, {
    dialog: import("vue").Ref<boolean, boolean>;
    $emit: (event: "click-primary-button" | "click-secondary-button" | "click-outside", ...args: any[]) => void;
    title: string;
    icon?: string | undefined;
    subtitle?: string | undefined;
    contentText?: string | undefined;
    contentHeight?: number | undefined;
    actionOneTitle?: string | undefined;
    actionTwoTitle?: string | undefined;
    $props: {
        readonly title?: string | undefined;
        readonly icon?: string | undefined;
        readonly subtitle?: string | undefined;
        readonly contentText?: string | undefined;
        readonly contentHeight?: number | undefined;
        readonly actionOneTitle?: string | undefined;
        readonly actionTwoTitle?: string | undefined;
    };
}, {}, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").PublicProps, Readonly<{}> & Readonly<{}>, {}, {}, {}, {}, string, import("vue").ComponentProvideOptions, true, {}, any>;
type __VLS_TemplateResult = {
    slots: {
        activator?(_: {
            props: any;
        }): any;
        title?(_: {}): any;
        text?(_: {}): any;
    };
    refs: {};
    attrs: Partial<typeof __VLS_inheritedAttrs>;
};
declare var __VLS_inheritedAttrs: {};
