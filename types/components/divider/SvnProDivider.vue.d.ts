declare const _default: import("vue").DefineComponent<{}, {
    color: string;
    vertical: boolean;
    inset: boolean;
    thickness: number;
    $props: {
        readonly color?: string | undefined;
        readonly vertical?: boolean | undefined;
        readonly inset?: boolean | undefined;
        readonly thickness?: number | undefined;
    };
}, {}, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").PublicProps, Readonly<{}> & Readonly<{}>, {}, {}, {}, {}, string, import("vue").ComponentProvideOptions, true, {}, any>;
export default _default;
