declare const _default: import("vue").DefineComponent<{}, {
    $emit: (event: "on-save", ...args: any[]) => void;
    readOnly: boolean;
    aleph: boolean;
    htmlData: string;
    editorId: string;
    createImageUrl: string;
    fullWidth: boolean;
    data?: Record<string, any> | undefined;
    $props: {
        readonly readOnly?: boolean | undefined;
        readonly aleph?: boolean | undefined;
        readonly htmlData?: string | undefined;
        readonly editorId?: string | undefined;
        readonly createImageUrl?: string | undefined;
        readonly fullWidth?: boolean | undefined;
        readonly data?: Record<string, any> | undefined;
    };
}, {}, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").PublicProps, Readonly<{}> & Readonly<{}>, {}, {}, {}, {}, string, import("vue").ComponentProvideOptions, true, {}, any>;
export default _default;
