declare const _default: import("vue").DefineComponent<{}, {
    $emit: (event: "on-save", ...args: any[]) => void;
    size: string;
    htmlData: string;
    createImageUrl: string;
    withBorder: boolean;
    withPadding: boolean;
    isEditable: boolean;
    extensionLeftMenu: boolean;
    extensionSelection: unknown[];
    extensionSlashCommand: unknown[];
    extensionGlobal: unknown[];
    $props: {
        readonly size?: string | undefined;
        readonly htmlData?: string | undefined;
        readonly createImageUrl?: string | undefined;
        readonly withBorder?: boolean | undefined;
        readonly withPadding?: boolean | undefined;
        readonly isEditable?: boolean | undefined;
        readonly extensionLeftMenu?: boolean | undefined;
        readonly extensionSelection?: unknown[] | undefined;
        readonly extensionSlashCommand?: unknown[] | undefined;
        readonly extensionGlobal?: unknown[] | undefined;
    };
}, {}, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").PublicProps, Readonly<{}> & Readonly<{}>, {}, {}, {}, {}, string, import("vue").ComponentProvideOptions, true, {}, any>;
export default _default;
