/**
 * @class Alert
 * @classdesc Alert Tool for Editor.js
 * @property {AlertData} data - Alert Tool`s input and output data
 * @property {object} api - Editor.js API instance
 *
 * @typedef {object} AlertData
 * @description Alert Tool`s input and output data
 * @property {string} type - Alert type
 * @property {string} alignType - Alert align type
 * @property {string} message - Alert message
 *
 * @typedef {object} AlertConfig
 * @description Alert Tool`s initial configuration
 * @property {string} defaultType - default Alert type
 * @property {string} defaultAlignType - default align Alert type
 * @property {string} messagePlaceholder - placeholder to show in Alert`s message input
 */
export default class Alert {
    /**
     * Get Toolbox settings
     *
     * @public
     * @returns {string}
     */
    public static get toolbox(): string;
    /**
     * Allow to press Enter inside the Alert block
     * @public
     * @returns {boolean}
     */
    public static get enableLineBreaks(): boolean;
    /**
     * Default Alert type
     *
     * @public
     * @returns {string}
     */
    public static get DEFAULT_TYPE(): string;
    /**
     * Default Alert align type
     *
     * @public
     * @returns {string}
     */
    public static get DEFAULT_ALIGN_TYPE(): string;
    /**
     * Default placeholder for Alert message
     *
     * @public
     * @returns {string}
     */
    public static get DEFAULT_MESSAGE_PLACEHOLDER(): string;
    /**
     * Supported Alert types
     *
     * @public
     * @returns {array}
     */
    public static get ALERT_TYPES(): array;
    /**
     * Supported Align types
     *
     * @public
     * @returns {array}
     */
    public static get ALIGN_TYPES(): array;
    /**
     * Returns true to notify the core that read-only mode is supported
     *
     * @return {boolean}
     */
    static get isReadOnlySupported(): boolean;
    /**
     * Allow Alert to be converted to/from other blocks
     */
    static get conversionConfig(): {
        export: (data: any) => any;
        import: (string: any) => {
            message: any;
            type: string;
            alignType: string;
        };
    };
    /**
     * Sanitizer config for Alert Tool saved data
     * @returns {Object}
     */
    static get sanitize(): Object;
    /**
     * Render plugin`s main Element and fill it with saved data
     *
     * @param {AlertData} data — previously saved data
     * @param {AlertConfig} config — user config for Tool
     * @param {Object} api - Editor.js API
     * @param {boolean} readOnly - read only mode flag
     */
    constructor({ data, config, api, readOnly }: AlertData);
    /**
     * Alert Tool`s styles
     *
     * @returns {Object}
     */
    get CSS(): Object;
    api: any;
    defaultType: any;
    defaultAlign: any;
    messagePlaceholder: any;
    data: {
        type: any;
        align: any;
        message: any;
    };
    container: Element | undefined;
    readOnly: any;
    /**
     * Create Alert Tool container
     *
     * @returns {Element}
     */
    render(): Element;
    /**
     * Create Block's settings block
     *
     * @returns {array}
     */
    renderSettings(): array;
    /**
     * Helper for formatting Alert Type / Align Type
     *
     * @param {string} type - Alert type or Align type
     * @returns {string}
     */
    _getFormattedName(name: any): string;
    /**
     * Helper for changing style of Alert block with the selected Alert type
     *
     * @param {string} newType - new Alert type to be applied to the block
     * @private
     */
    private _changeAlertType;
    /**
     * Helper for changing align of Alert block with the selected Align type
     *
     * @param {string} newAlign - new align type to be applied to the block
     * @private
     */
    private _changeAlignType;
    /**
     * Extract Alert data from Alert Tool element
     *
     * @param {HTMLDivElement} alertElement - element to save
     * @returns {AlertData}
     */
    save(alertElement: HTMLDivElement): AlertData;
    /**
     * Helper for making Elements with attributes
     *
     * @param  {string} tagName           - new Element tag name
     * @param  {array|string} classNames  - list or name of CSS classname(s)
     * @param  {Object} attributes        - any attributes
     * @returns {Element}
     * @private
     */
    private _make;
    /**
     * Fill Alert's message with the pasted content
     *
     * @param {PasteEvent} event - event with pasted content
     */
    onPaste(event: PasteEvent): void;
}
export type AlertData = object;
export type AlertConfig = object;
