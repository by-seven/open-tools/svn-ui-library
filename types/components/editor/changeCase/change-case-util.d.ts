export function lowerCase(text: any): any;
export function upperCase(text: any): any;
export function localeLowerCase(text: any, locale: any): any;
export function localeUpperCase(text: any, locale: any): any;
export function sentenceCase(text: any): any;
export function titleCase(text: any): any;
export function toggleCase(text: any): any;
