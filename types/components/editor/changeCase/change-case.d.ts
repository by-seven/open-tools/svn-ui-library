export default class ChangeCase {
    static get isInline(): boolean;
    constructor({ config, api }: {
        config: any;
        api: any;
    });
    set state(state: boolean);
    get state(): boolean;
    api: any;
    button: HTMLButtonElement | null;
    optionButtons: any[];
    _state: boolean;
    selectedText: any;
    range: any;
    _settings: any;
    CSS: {
        actions: string;
        toolbarLabel: string;
        tool: string;
        toolbarBtnActive: any;
        inlineButton: any;
    };
    caseOptions: {
        titleCase: string;
        lowerCase: string;
        upperCase: string;
        localeLowerCase: string;
        localeUpperCase: string;
        sentenceCase: string;
        toggleCase: string;
    };
    get title(): string;
    render(): HTMLButtonElement;
    checkState(selection: any): void;
    convertCase(range: any, option: any): void;
    surround(range: any): void;
    renderActions(): HTMLDivElement;
    actions: HTMLDivElement | undefined;
    destroy(): void;
}
