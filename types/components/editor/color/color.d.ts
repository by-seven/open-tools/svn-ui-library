export default Color;
/**
 * Text Color Tool for Editor.js
 */
declare class Color {
    /**
     * Specifies Tool as Inline Toolbar Tool
     *
     * @return {boolean}
     */
    static get isInline(): boolean;
    /**
     * Sanitizer rule
     * @return {{color: {class: string}}}
     */
    static get sanitize(): {
        color: {
            class: string;
        };
    };
    /**
     * @param {{api: object}}  - Editor.js API
     */
    constructor({ config, api }: {
        api: object;
    });
    api: object;
    config: any;
    clickedOnLeft: boolean;
    pluginType: any;
    parentTag: string;
    hasCustomPicker: any;
    color: any;
    picker: ColorPlugin | null;
    icon: HTMLDivElement | null;
    /**
     * Toolbar Button
     *
     * @type {HTMLElement|null}
     */
    button: HTMLElement | null;
    /**
     * CSS classes
     */
    iconClasses: {
        base: any;
        active: any;
    };
    /**
     * Create button element for Toolbar
     *
     * @return {HTMLElement}
     */
    render(): HTMLElement;
    /**
     * Create left part button
     *
     * @return {HTMLElement}
     */
    createLeftButton(): HTMLElement;
    /**
     * Create button icon
     *
     * @return {HTMLElement}
     */
    createButtonIcon(): HTMLElement;
    /**
     * Create right part button
     *
     * @return {HTMLElement}
     */
    createRightButton(sharedScope: any): HTMLElement;
    /**
     * handle selected fragment
     *
     * @param {Range} range - selected fragment
     */
    surround(range: Range): void;
    /**
     * Wrap selected fragment
     *
     * @param {Range} range - selected fragment
     */
    wrap(range: Range): void;
    /**
     * Wrap selected marker fragment
     *
     * @param newWrapper - wrapper for selected fragment
     */
    wrapMarker(newWrapper: any): void;
    /**
     * Wrap selected text color fragment
     *
     * @param {Range} newWrapper - wrapper for selected fragment
     */
    wrapTextColor(newWrapper: Range): void;
    /**
     * Unwrap selected fragment
     *
     * @param {Range} termWrapper - parent of selected fragment
     */
    unwrap(termWrapper: Range): void;
    /**
     * update color without create a new tag
     *
     * @param {Range} termWrapper - parent of selected fragment
     */
    updateWrapper(termWrapper: Range): void;
    /**
     * remove wrapper
     *
     * @param {Range} termWrapper - parent of selected fragment
     */
    removeWrapper(termWrapper: Range): void;
    /**
     * Check and change Term's state for current selection
     */
    checkState(): boolean;
    /**
     * handle icon active state for legacy wrappers
     */
    handleLegacyWrapper(legacyWrapper: any, termTag: any): any;
    clear(): void;
}
import { ColorPlugin } from './picker';
