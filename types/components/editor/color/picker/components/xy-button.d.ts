export default class XyButton extends HTMLElement {
    static get observedAttributes(): string[];
    focus(): void;
    set disabled(value: boolean);
    get disabled(): boolean;
    get toggle(): boolean;
    set htmltype(value: string | null);
    get htmltype(): string | null;
    get name(): string | null;
    set checked(value: boolean);
    get checked(): boolean;
    set href(value: string | null);
    get href(): string | null;
    get target(): string;
    get rel(): string | null;
    get download(): string | null;
    set icon(value: string | null);
    get icon(): string | null;
    set loading(value: boolean);
    get loading(): boolean;
    connectedCallback(): void;
    btn: HTMLElement | null | undefined;
    ico: HTMLElement | null | undefined;
    load: HTMLElement | undefined;
    attributeChangedCallback(name: any, oldValue: any, newValue: any): void;
}
