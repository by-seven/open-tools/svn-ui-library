/**
 * Convert CSS variables to color string.
 * @param colorValue original value provided by users
 * @returns string color string
 */
export function handleCSSVariables(colorValue: any): any;
export function throttle(fn: any, delay: any): (...args: any[]) => void;
/**
 * Cache the latest text/marker color
 * @param defaultColor
 * @param pluginType
 * @returns defaultColor
 */
export function setDefaultColorCache(defaultColor: any, pluginType: any): any;
/**
 * Get cached text/marker color
 * @param defaultColor
 * @param pluginType
 * @returns string cachedDefaultColor/defaultColor
 */
export function getDefaultColorCache(defaultColor: any, pluginType: any): any;
/**
 * Cache custom color
 * @param customColor,
 * @param pluginType
 */
export function setCustomColorCache(customColor: any, pluginType: any): void;
/**
 * Get cached custom color
 * @param pluginType
 * @returns string cachedCustomColor
 */
export function getCustomColorCache(pluginType: any): any;
export const CONVERTER_BTN: "ce-inline-toolbar__dropdown";
export const CONVERTER_PANEL: "ce-conversion-toolbar--showed";
