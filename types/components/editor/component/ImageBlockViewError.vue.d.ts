declare const _default: import("vue").DefineComponent<{}, {
    selected: boolean;
    editor: import("@tiptap/core").Editor;
    deleteNode: () => void;
    updateAttributes: (attributes: Record<string, any>) => void;
    node: import("prosemirror-model").Node;
    decorations: import("@tiptap/vue-3").DecorationWithType[];
    extension: import("@tiptap/vue-3").Node<any, any>;
    getPos: () => number;
    $props: {
        readonly selected?: boolean | undefined;
        readonly editor?: import("@tiptap/core").Editor | undefined;
        readonly deleteNode?: (() => void) | undefined;
        readonly updateAttributes?: ((attributes: Record<string, any>) => void) | undefined;
        readonly node?: import("prosemirror-model").Node | undefined;
        readonly decorations?: import("@tiptap/vue-3").DecorationWithType[] | undefined;
        readonly extension?: import("@tiptap/vue-3").Node<any, any> | undefined;
        readonly getPos?: (() => number) | undefined;
    };
}, {}, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").PublicProps, Readonly<{}> & Readonly<{}>, {}, {}, {}, {}, string, import("vue").ComponentProvideOptions, true, {}, any>;
export default _default;
