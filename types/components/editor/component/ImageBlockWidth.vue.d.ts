declare const _default: import("vue").DefineComponent<{}, {
    $emit: (event: "handle-change", ...args: any[]) => void;
    width: string;
    $props: {
        readonly width?: string | undefined;
    };
}, {}, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").PublicProps, Readonly<{}> & Readonly<{}>, {}, {}, {}, {}, string, import("vue").ComponentProvideOptions, true, {}, any>;
export default _default;
