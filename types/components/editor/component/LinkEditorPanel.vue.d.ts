declare const _default: import("vue").DefineComponent<{}, {
    $emit: (event: "on-set-link" | "on-clear", ...args: any[]) => void;
    size: string;
    initialUrl: string;
    initialOpenInNewTab: boolean;
    $props: {
        readonly size?: string | undefined;
        readonly initialUrl?: string | undefined;
        readonly initialOpenInNewTab?: boolean | undefined;
    };
}, {}, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").PublicProps, Readonly<{}> & Readonly<{}>, {}, {}, {}, {}, string, import("vue").ComponentProvideOptions, true, {}, any>;
export default _default;
