declare const _default: import("vue").DefineComponent<{}, {
    $emit: (event: "on-clear" | "on-edit", ...args: any[]) => void;
    url: string;
    $props: {
        readonly url?: string | undefined;
    };
}, {}, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").PublicProps, Readonly<{}> & Readonly<{}>, {}, {}, {}, {}, string, import("vue").ComponentProvideOptions, true, {}, any>;
export default _default;
