export default Divider;
declare class Divider {
    static get toolbox(): {
        title: string;
        icon: string;
    };
    static get isReadOnlySupported(): boolean;
    constructor({ api, data, config, readOnly, block }: {
        api: any;
        data: any;
        config: any;
        readOnly: any;
        block: any;
    });
    api: any;
    data: {
        alignment: any;
        fullWidth: any;
    };
    config: any;
    readOnly: any;
    block: any;
    wrapper: HTMLHRElement | undefined;
    divider: any;
    settings: {
        name: string;
        icon: string;
        classes: string[];
    }[];
    render(): HTMLHRElement;
    renderSettings(): HTMLDivElement;
    save(blockContent: any): {
        alignment: any;
        fullWidth: any;
    };
}
