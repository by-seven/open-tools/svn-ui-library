export const Emoji: import("@tiptap/core").Node<import("@tiptap-pro/extension-emoji").EmojiOptions, import("@tiptap-pro/extension-emoji").EmojiStorage>;
export default Emoji;
