export namespace emojiSuggestion {
    function items({ editor, query }: {
        editor: any;
        query: any;
    }): any;
    let allowSpaces: boolean;
    function render(): {
        onStart: (props: any) => void;
        onUpdate: (props: any) => void;
        onKeyDown: (props: any) => any;
        onExit: () => void;
    };
}
export default emojiSuggestion;
