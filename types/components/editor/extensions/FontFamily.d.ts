export const FontFamily: import("@tiptap/core").Extension<import("@tiptap/extension-font-family").FontFamilyOptions, any>;
export default FontFamily;
