export const HardBreak: import("@tiptap/core").Node<import("@tiptap/extension-hard-break").HardBreakOptions, any>;
export default HardBreak;
