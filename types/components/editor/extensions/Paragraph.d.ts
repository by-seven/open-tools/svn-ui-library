export const Paragraph: import("@tiptap/core").Node<import("@tiptap/extension-paragraph").ParagraphOptions, any>;
export default Paragraph;
