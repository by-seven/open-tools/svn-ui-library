export const Placeholder: import("@tiptap/core").Extension<import("@tiptap/extension-placeholder").PlaceholderOptions, any>;
export default Placeholder;
