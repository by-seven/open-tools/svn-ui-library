export function isRectSelected(rect: any): (selection: any) => boolean;
export function findTable(selection: any): {
    pos: number;
    start: number;
    depth: number;
    node: import("prosemirror-model").Node;
} | undefined;
export function isCellSelection(selection: any): selection is CellSelection;
export function isColumnSelected(columnIndex: any): (selection: any) => boolean;
export function isRowSelected(rowIndex: any): (selection: any) => boolean;
export function isTableSelected(selection: any): boolean;
export function getCellsInColumn(columnIndex: any): (selection: any) => any;
export function getCellsInRow(rowIndex: any): (selection: any) => any;
export function getCellsInTable(selection: any): {
    pos: number;
    start: number;
    node: Node | null;
}[] | null;
export function findParentNodeClosestToPos($pos: any, predicate: any): {
    pos: any;
    start: any;
    depth: any;
    node: any;
} | null;
export function findCellClosestToPos($pos: any): {
    pos: any;
    start: any;
    depth: any;
    node: any;
} | null;
export function selectColumn(index: any): (tr: any) => any;
export function selectRow(index: any): (tr: any) => any;
export function selectTable(tr: any): any;
import { Node } from '@tiptap/pm/model';
import { CellSelection } from '@tiptap/pm/tables';
