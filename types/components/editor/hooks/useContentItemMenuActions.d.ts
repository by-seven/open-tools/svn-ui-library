export default useContentItemMenuActions;
declare function useContentItemMenuActions(editor: any, currentNode: any, currentNodePos: any): {
    resetTextFormatting: () => void;
    duplicateNode: () => void;
    copyNodeToClipboard: () => void;
    deleteNode: () => void;
    handleAdd: () => void;
};
