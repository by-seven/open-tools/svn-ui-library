export function useData(): {
    currentNode: import("vue").ComputedRef<null>;
    currentNodePos: import("vue").ComputedRef<number>;
    setCurrentNode: (node: any) => void;
    setCurrentNodePos: (pos: any) => void;
    handleNodeChange: (data: any) => void;
};
