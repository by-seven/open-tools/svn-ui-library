export function useTextMenuCommands(editor: any): {
    onBold: () => void;
    onItalic: () => void;
    onUnderline: () => void;
    onStrike: () => void;
    onSetFont: (font: any) => any;
    onSetFontSize: (fontSize: any) => any;
    onAlignLeft: () => void;
    onAlignCenter: () => void;
    onAlignRight: () => void;
    onAlignJustify: () => void;
    onLink: (url: any, inNewTab: any) => void;
    onChangeHighlight: (color: any) => void;
    onClearHighlight: () => void;
    onChangeColor: (color: any) => void;
    onClearColor: () => void;
};
export default useTextMenuCommands;
