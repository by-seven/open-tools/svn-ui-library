export function useTextMenuContentTypes(editor: any): ({
    type: string;
    title: string;
    id: string;
    icon?: undefined;
    disabled?: undefined;
    isActive?: undefined;
    onClick?: undefined;
} | {
    icon: string;
    title: string;
    id: string;
    disabled: () => boolean;
    isActive: () => any;
    onClick: () => any;
    type: string;
})[];
