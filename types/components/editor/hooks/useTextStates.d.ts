export function useTextStates(editor: any): {
    isBold: any;
    isItalic: any;
    isStrike: any;
    isUnderline: any;
    isCode: any;
    isSubscript: any;
    isSuperscript: any;
    isAlignLeft: any;
    isAlignCenter: any;
    isAlignRight: any;
    isAlignJustify: any;
    currentColor: any;
    currentHighlight: any;
    currentFont: any;
    currentSize: any;
    shouldShow: ({ view, from }: {
        view: any;
        from: any;
    }) => boolean;
};
export default useTextStates;
