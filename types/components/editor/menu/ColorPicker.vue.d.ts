declare const _default: import("vue").DefineComponent<{}, {
    icon: string;
    size: string;
    editor: Editor;
    pickerType: string;
    $props: {
        readonly icon?: string | undefined;
        readonly size?: string | undefined;
        readonly editor?: Editor | undefined;
        readonly pickerType?: string | undefined;
    };
}, {}, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").PublicProps, Readonly<{}> & Readonly<{}>, {}, {}, {}, {}, string, import("vue").ComponentProvideOptions, true, {}, any>;
export default _default;
import { Editor } from '@tiptap/vue-3';
