declare const _default: import("vue").DefineComponent<{}, {
    size: string;
    editor: Editor;
    activeItem: Record<string, any>;
    selectionItems: unknown[];
    $props: {
        readonly size?: string | undefined;
        readonly editor?: Editor | undefined;
        readonly activeItem?: Record<string, any> | undefined;
        readonly selectionItems?: unknown[] | undefined;
    };
}, {}, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").PublicProps, Readonly<{}> & Readonly<{}>, {}, {}, {}, {}, string, import("vue").ComponentProvideOptions, true, {}, any>;
export default _default;
import { Editor } from '@tiptap/vue-3';
