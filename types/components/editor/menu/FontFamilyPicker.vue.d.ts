declare const _default: import("vue").DefineComponent<{}, {
    $emit: (event: "select-font", ...args: any[]) => void;
    size: string;
    value: string;
    $props: {
        readonly size?: string | undefined;
        readonly value?: string | undefined;
    };
}, {}, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").PublicProps, Readonly<{}> & Readonly<{}>, {}, {}, {}, {}, string, import("vue").ComponentProvideOptions, true, {}, any>;
export default _default;
