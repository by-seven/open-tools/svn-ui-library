declare const _default: import("vue").DefineComponent<{}, {
    $emit: (event: "edit-link", ...args: any[]) => void;
    size: string;
    editor: Editor;
    elementTarget: null;
    $props: {
        readonly size?: string | undefined;
        readonly editor?: Editor | undefined;
        readonly elementTarget?: null | undefined;
    };
}, {}, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").PublicProps, Readonly<{}> & Readonly<{}>, {}, {}, {}, {}, string, import("vue").ComponentProvideOptions, true, {}, any>;
export default _default;
import { Editor } from '@tiptap/vue-3';
