declare const _default: import("vue").DefineComponent<{}, {
    items?: any;
    editor?: any;
    command?: any;
    $props: {
        readonly items?: any;
        readonly editor?: any;
        readonly command?: any;
    };
}, {}, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").PublicProps, Readonly<{}> & Readonly<{}>, {}, {}, {}, {}, string, import("vue").ComponentProvideOptions, true, {}, any>;
export default _default;
