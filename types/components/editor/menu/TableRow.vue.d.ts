declare const _default: import("vue").DefineComponent<{}, {
    editor: Editor;
    elementTarget: null;
    $props: {
        readonly editor?: Editor | undefined;
        readonly elementTarget?: null | undefined;
    };
}, {}, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").PublicProps, Readonly<{}> & Readonly<{}>, {}, {}, {}, {}, string, import("vue").ComponentProvideOptions, true, {}, any>;
export default _default;
import { Editor } from '@tiptap/vue-3';
