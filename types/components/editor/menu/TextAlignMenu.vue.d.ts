declare const _default: import("vue").DefineComponent<{}, {
    size: string;
    editor: Editor;
    $props: {
        readonly size?: string | undefined;
        readonly editor?: Editor | undefined;
    };
}, {}, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").PublicProps, Readonly<{}> & Readonly<{}>, {}, {}, {}, {}, string, import("vue").ComponentProvideOptions, true, {}, any>;
export default _default;
import { Editor } from '@tiptap/vue-3';
