export default class Quote {
    static get toolbox(): {
        title: string;
        icon: string;
    };
    static get sanitize(): {
        text: {
            br: boolean;
        };
    };
    static get isReadOnlySupported(): boolean;
    static get conversionConfig(): {
        export: string;
        import: string;
    };
    constructor({ data, config, api, readOnly }: {
        data: any;
        config: any;
        api: any;
        readOnly: any;
    });
    api: any;
    readOnly: any;
    onKeyUp(e: any): void;
    _data: {};
    _element: HTMLElement | null;
    _preserveBlank: any;
    set data(data: {});
    get data(): {};
    drawView(): HTMLElement;
    render(): HTMLElement;
    merge(data: any): void;
    validate(savedData: any): boolean;
    save(toolsContent: any): {
        text: any;
    };
    onPaste(event: any): void;
    hydrate(): void;
}
