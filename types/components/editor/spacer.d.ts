export default Spacer;
declare class Spacer {
    static get toolbox(): {
        name: string;
        icon: string;
    };
    static get isReadOnlySupported(): boolean;
    constructor({ api, readOnly, data }: {
        api: any;
        readOnly: any;
        data: any;
    });
    api: any;
    readOnly: any;
    data: {
        lines: any;
    };
    text: HTMLParagraphElement | undefined;
    current: any;
    render(): HTMLParagraphElement;
    renderSettings(): HTMLDivElement;
    save(blockContent: any): {
        lines: any;
    };
}
