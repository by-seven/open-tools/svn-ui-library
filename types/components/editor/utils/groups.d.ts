export const GROUPS: {
    name: string;
    title: string;
    commands: ({
        name: string;
        label: string;
        iconName: string;
        description: string;
        aliases: string[];
        action: (editor: any) => void;
        shouldBeHidden?: undefined;
    } | {
        name: string;
        label: string;
        iconName: string;
        description: string;
        action: (editor: any) => void;
        aliases?: undefined;
        shouldBeHidden?: undefined;
    } | {
        name: string;
        label: string;
        iconName: string;
        description: string;
        shouldBeHidden: (editor: any) => any;
        action: (editor: any) => void;
        aliases?: undefined;
    })[];
}[];
export default GROUPS;
