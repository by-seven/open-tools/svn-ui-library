export function isColumnGripSelected({ editor, view, state, from, }: {
    editor: any;
    view: any;
    state: any;
    from: any;
}): boolean;
export default isColumnGripSelected;
