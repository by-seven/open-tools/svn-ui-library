export function isTextSelected({ editor }: {
    editor: any;
}): boolean;
export default isTextSelected;
