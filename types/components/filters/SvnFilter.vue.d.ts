declare const _default: import("vue").DefineComponent<{}, {
    $emit: (event: "add-filter" | "activator-clicked", ...args: any[]) => void;
    activatorText: string;
    activatorVariant: string;
    activatorIcon: string;
    activatorColor: string;
    activatorHeight: number;
    filterItems: unknown[];
    selectedFilters: number;
    showResults: boolean;
    $props: {
        readonly activatorText?: string | undefined;
        readonly activatorVariant?: string | undefined;
        readonly activatorIcon?: string | undefined;
        readonly activatorColor?: string | undefined;
        readonly activatorHeight?: number | undefined;
        readonly filterItems?: unknown[] | undefined;
        readonly selectedFilters?: number | undefined;
        readonly showResults?: boolean | undefined;
    };
}, {}, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").PublicProps, Readonly<{}> & Readonly<{}>, {}, {}, {}, {}, string, import("vue").ComponentProvideOptions, true, {}, any>;
export default _default;
