declare const _default: import("vue").DefineComponent<{}, {
    $emit: (event: "update-selected-data-filters" | "delete-filter", ...args: any[]) => void;
    data: Record<string, any>;
    item: Record<string, any>;
    $props: {
        readonly data?: Record<string, any> | undefined;
        readonly item?: Record<string, any> | undefined;
    };
}, {}, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").PublicProps, Readonly<{}> & Readonly<{}>, {}, {}, {}, {}, string, import("vue").ComponentProvideOptions, true, {}, any>;
export default _default;
