declare const _default: __VLS_WithTemplateSlots<typeof __VLS_component, __VLS_TemplateResult["slots"]>;
export default _default;
type __VLS_WithTemplateSlots<T, S> = T & (new () => {
    $slots: S;
});
declare const __VLS_component: import("vue").DefineComponent<{}, {
    $emit: (event: "input" | "clear" | "update:modelValue" | "on-intersect" | "intersect", ...args: any[]) => void;
    label: string;
    color: string;
    loading: boolean;
    disabled: boolean;
    modelValue: null;
    closeIcon: string;
    density: string;
    baseColor: string;
    prependInnerIcon: string;
    multiple: boolean;
    items: unknown[];
    returnObject: boolean;
    itemTitle: string;
    itemValue: string;
    closableChips: boolean;
    menuProps: Record<string, any>;
    avatar: boolean;
    overflowY: boolean;
    checkbox: boolean;
    avatarChip: boolean;
    disablingFunction: Function;
    clearOnSelect: boolean;
    withAddAndRemoveButtons: boolean;
    chipClass: string;
    $props: {
        readonly label?: string | undefined;
        readonly color?: string | undefined;
        readonly loading?: boolean | undefined;
        readonly disabled?: boolean | undefined;
        readonly modelValue?: null | undefined;
        readonly closeIcon?: string | undefined;
        readonly density?: string | undefined;
        readonly baseColor?: string | undefined;
        readonly prependInnerIcon?: string | undefined;
        readonly multiple?: boolean | undefined;
        readonly items?: unknown[] | undefined;
        readonly returnObject?: boolean | undefined;
        readonly itemTitle?: string | undefined;
        readonly itemValue?: string | undefined;
        readonly closableChips?: boolean | undefined;
        readonly menuProps?: Record<string, any> | undefined;
        readonly avatar?: boolean | undefined;
        readonly overflowY?: boolean | undefined;
        readonly checkbox?: boolean | undefined;
        readonly avatarChip?: boolean | undefined;
        readonly disablingFunction?: Function | undefined;
        readonly clearOnSelect?: boolean | undefined;
        readonly withAddAndRemoveButtons?: boolean | undefined;
        readonly chipClass?: string | undefined;
    };
}, {}, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").PublicProps, Readonly<{}> & Readonly<{}>, {}, {}, {}, {}, string, import("vue").ComponentProvideOptions, true, {}, any>;
type __VLS_TemplateResult = {
    slots: {
        "prepend-details"?(_: {
            item: any;
        }): any;
        "main-content"?(_: {
            item: any;
        }): any;
        "append-content"?(_: {
            item: any;
        }): any;
        "chip-content"?(_: {
            item: any;
            props: {
                readonly label: string;
                readonly color: string;
                readonly loading: boolean;
                readonly disabled: boolean;
                readonly modelValue: null;
                readonly closeIcon: string;
                readonly density: string;
                readonly baseColor: string;
                readonly prependInnerIcon: string;
                readonly multiple: boolean;
                readonly items: unknown[];
                readonly returnObject: boolean;
                readonly itemTitle: string;
                readonly itemValue: string;
                readonly closableChips: boolean;
                readonly menuProps: Record<string, any>;
                readonly avatar: boolean;
                readonly overflowY: boolean;
                readonly checkbox: boolean;
                readonly avatarChip: boolean;
                readonly disablingFunction: Function;
                readonly clearOnSelect: boolean;
                readonly withAddAndRemoveButtons: boolean;
                readonly chipClass: string;
            };
        }): any;
        "no-data"?(_: {}): any;
    };
    refs: {
        autocomplete: any;
    };
    attrs: Partial<typeof __VLS_inheritedAttrs>;
};
declare var __VLS_inheritedAttrs: {};
