declare const _default: import("vue").DefineComponent<{}, {
    $emit: (event: "update:modelValue", ...args: any[]) => void;
    error: boolean;
    label: string;
    color: string;
    disabled: boolean;
    modelValue: boolean;
    rules: unknown[];
    indeterminate: boolean;
    $props: {
        readonly error?: boolean | undefined;
        readonly label?: string | undefined;
        readonly color?: string | undefined;
        readonly disabled?: boolean | undefined;
        readonly modelValue?: boolean | undefined;
        readonly rules?: unknown[] | undefined;
        readonly indeterminate?: boolean | undefined;
    };
}, {}, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").PublicProps, Readonly<{}> & Readonly<{}>, {}, {}, {}, {}, string, import("vue").ComponentProvideOptions, true, {}, any>;
export default _default;
