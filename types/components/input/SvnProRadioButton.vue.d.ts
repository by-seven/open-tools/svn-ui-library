declare const _default: import("vue").DefineComponent<{}, {
    color: string;
    disabled: boolean;
    readonly: boolean;
    density: string;
    value: boolean;
    label?: string | undefined;
    modelValue?: undefined;
    $props: {
        readonly color?: string | undefined;
        readonly disabled?: boolean | undefined;
        readonly readonly?: boolean | undefined;
        readonly density?: string | undefined;
        readonly value?: boolean | undefined;
        readonly label?: string | undefined;
        readonly modelValue?: undefined;
    };
}, {}, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").PublicProps, Readonly<{}> & Readonly<{}>, {}, {}, {}, {}, string, import("vue").ComponentProvideOptions, true, {}, any>;
export default _default;
