declare const _default: import("vue").DefineComponent<{}, {
    $emit: (event: "clear" | "update:modelValue", ...args: any[]) => void;
    error: boolean;
    label: string;
    disabled: boolean;
    modelValue: null;
    errorMessages: unknown[];
    multiple: boolean;
    items: unknown[];
    itemTitle: string;
    itemValue: string;
    withSelectAll: boolean;
    $props: {
        readonly error?: boolean | undefined;
        readonly label?: string | undefined;
        readonly disabled?: boolean | undefined;
        readonly modelValue?: null | undefined;
        readonly errorMessages?: unknown[] | undefined;
        readonly multiple?: boolean | undefined;
        readonly items?: unknown[] | undefined;
        readonly itemTitle?: string | undefined;
        readonly itemValue?: string | undefined;
        readonly withSelectAll?: boolean | undefined;
    };
}, {}, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").PublicProps, Readonly<{}> & Readonly<{}>, {}, {}, {}, {}, string, import("vue").ComponentProvideOptions, true, {}, any>;
export default _default;
