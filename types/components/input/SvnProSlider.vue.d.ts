declare const _default: import("vue").DefineComponent<{}, {
    $emit: (event: "update:modelValue", ...args: any[]) => void;
    color: string;
    disabled: boolean;
    modelValue: number;
    max: number;
    min: number;
    step: number;
    thumbLabel: boolean;
    showTicks: boolean;
    $props: {
        readonly color?: string | undefined;
        readonly disabled?: boolean | undefined;
        readonly modelValue?: number | undefined;
        readonly max?: number | undefined;
        readonly min?: number | undefined;
        readonly step?: number | undefined;
        readonly thumbLabel?: boolean | undefined;
        readonly showTicks?: boolean | undefined;
    };
}, {}, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").PublicProps, Readonly<{}> & Readonly<{}>, {}, {}, {}, {}, string, import("vue").ComponentProvideOptions, true, {}, any>;
export default _default;
