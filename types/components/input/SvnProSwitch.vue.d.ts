declare const _default: import("vue").DefineComponent<{}, {
    $emit: (event: "update:modelValue", ...args: any[]) => void;
    icon: boolean;
    color: string;
    disabled: boolean;
    modelValue: boolean;
    readonly: boolean;
    density: string;
    switchId: string;
    $props: {
        readonly icon?: boolean | undefined;
        readonly color?: string | undefined;
        readonly disabled?: boolean | undefined;
        readonly modelValue?: boolean | undefined;
        readonly readonly?: boolean | undefined;
        readonly density?: string | undefined;
        readonly switchId?: string | undefined;
    };
}, {}, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").PublicProps, Readonly<{}> & Readonly<{}>, {}, {}, {}, {}, string, import("vue").ComponentProvideOptions, true, {}, any>;
export default _default;
