declare const _default: __VLS_WithTemplateSlots<typeof __VLS_component, __VLS_TemplateResult["slots"]>;
export default _default;
type __VLS_WithTemplateSlots<T, S> = T & (new () => {
    $slots: S;
});
declare const __VLS_component: import("vue").DefineComponent<{}, {
    $emit: (event: "clear" | "update:modelValue", ...args: any[]) => void;
    error: boolean;
    label: string;
    variant: string;
    color: string;
    disabled: boolean;
    modelValue: null;
    rules: unknown[];
    persistentHint: boolean;
    clearable: boolean;
    prependInnerIcon: string;
    autofocus: boolean;
    counter: boolean;
    persistentCounter: boolean;
    append: boolean;
    errorMessages?: string | undefined;
    messages?: string | undefined;
    hint?: string | undefined;
    appendInnerIcon?: string | undefined;
    placeholder?: string | undefined;
    maxLength?: number | undefined;
    $props: {
        readonly error?: boolean | undefined;
        readonly label?: string | undefined;
        readonly variant?: string | undefined;
        readonly color?: string | undefined;
        readonly disabled?: boolean | undefined;
        readonly modelValue?: null | undefined;
        readonly rules?: unknown[] | undefined;
        readonly persistentHint?: boolean | undefined;
        readonly clearable?: boolean | undefined;
        readonly prependInnerIcon?: string | undefined;
        readonly autofocus?: boolean | undefined;
        readonly counter?: boolean | undefined;
        readonly persistentCounter?: boolean | undefined;
        readonly append?: boolean | undefined;
        readonly errorMessages?: string | undefined;
        readonly messages?: string | undefined;
        readonly hint?: string | undefined;
        readonly appendInnerIcon?: string | undefined;
        readonly placeholder?: string | undefined;
        readonly maxLength?: number | undefined;
    };
}, {}, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").PublicProps, Readonly<{}> & Readonly<{}>, {}, {}, {}, {}, string, import("vue").ComponentProvideOptions, true, {}, any>;
type __VLS_TemplateResult = {
    slots: {
        append?(_: {}): any;
    };
    refs: {};
    attrs: Partial<typeof __VLS_inheritedAttrs>;
};
declare var __VLS_inheritedAttrs: {};
