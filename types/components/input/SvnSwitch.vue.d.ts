declare const _default: import("vue").DefineComponent<{}, {
    $emit: (event: "update:modelValue", ...args: any[]) => void;
    label: string;
    color: string;
    modelValue: string;
    density: string;
    ripple: boolean;
    inset: boolean;
    $props: {
        readonly label?: string | undefined;
        readonly color?: string | undefined;
        readonly modelValue?: string | undefined;
        readonly density?: string | undefined;
        readonly ripple?: boolean | undefined;
        readonly inset?: boolean | undefined;
    };
}, {}, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").PublicProps, Readonly<{}> & Readonly<{}>, {}, {}, {}, {}, string, import("vue").ComponentProvideOptions, true, {}, any>;
export default _default;
