declare const _default: import("vue").DefineComponent<{}, {
    edit: boolean;
    trainersList: unknown[];
    $props: {
        readonly edit?: boolean | undefined;
        readonly trainersList?: unknown[] | undefined;
    };
}, {}, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").PublicProps, Readonly<{}> & Readonly<{}>, {}, {}, {}, {}, string, import("vue").ComponentProvideOptions, true, {}, any>;
export default _default;
