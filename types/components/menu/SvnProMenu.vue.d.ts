declare const _default: __VLS_WithTemplateSlots<typeof __VLS_component, __VLS_TemplateResult["slots"]>;
export default _default;
type __VLS_WithTemplateSlots<T, S> = T & (new () => {
    $slots: S;
});
declare const __VLS_component: import("vue").DefineComponent<{}, {
    origin: string;
    location: string;
    closeOnContentClick: boolean;
    offset: number;
    items: unknown[];
    menuColor: string;
    listDensity: string;
    menuClass?: string | undefined;
    $props: {
        readonly origin?: string | undefined;
        readonly location?: string | undefined;
        readonly closeOnContentClick?: boolean | undefined;
        readonly offset?: number | undefined;
        readonly items?: unknown[] | undefined;
        readonly menuColor?: string | undefined;
        readonly listDensity?: string | undefined;
        readonly menuClass?: string | undefined;
    };
}, {}, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").PublicProps, Readonly<{}> & Readonly<{}>, {}, {}, {}, {}, string, import("vue").ComponentProvideOptions, true, {}, any>;
type __VLS_TemplateResult = {
    slots: {
        activator?(_: {
            isActive: any;
        }): any;
        dropdown?(_: {}): any;
    };
    refs: {};
    attrs: Partial<typeof __VLS_inheritedAttrs>;
};
declare var __VLS_inheritedAttrs: {};
