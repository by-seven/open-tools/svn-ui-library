declare const _default: import("vue").DefineComponent<{}, {
    $emit: (event: "toggle-comments" | "toggle-favorite" | "copy-link" | "edit-module" | "save-module" | "preview-module" | "close-and-go-back" | "closeAndGoBack" | "publish-module" | "download-pdf", ...args: any[]) => void;
    type: string;
    isFavorited: boolean;
    saveMode: string;
    saveText: string;
    menuItems: unknown[];
    updateText: string;
    publishText: string;
    creatorOrAdmin: boolean;
    $props: {
        readonly type?: string | undefined;
        readonly isFavorited?: boolean | undefined;
        readonly saveMode?: string | undefined;
        readonly saveText?: string | undefined;
        readonly menuItems?: unknown[] | undefined;
        readonly updateText?: string | undefined;
        readonly publishText?: string | undefined;
        readonly creatorOrAdmin?: boolean | undefined;
    };
}, {}, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").PublicProps, Readonly<{}> & Readonly<{}>, {}, {}, {}, {}, string, import("vue").ComponentProvideOptions, true, {}, any>;
export default _default;
