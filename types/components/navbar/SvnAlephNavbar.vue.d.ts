declare const _default: import("vue").DefineComponent<{}, {
    $emit: (event: "logout" | "homeButtonClick", ...args: any[]) => void;
    items: unknown[];
    user: Record<string, any>;
    email: string;
    userAvatar: string;
    userCompanyLogo: string;
    modalItems: unknown[];
    appName: string;
    appMenu: unknown[];
    soonText: string;
    $props: {
        readonly items?: unknown[] | undefined;
        readonly user?: Record<string, any> | undefined;
        readonly email?: string | undefined;
        readonly userAvatar?: string | undefined;
        readonly userCompanyLogo?: string | undefined;
        readonly modalItems?: unknown[] | undefined;
        readonly appName?: string | undefined;
        readonly appMenu?: unknown[] | undefined;
        readonly soonText?: string | undefined;
    };
}, {}, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").PublicProps, Readonly<{}> & Readonly<{}>, {}, {}, {}, {}, string, import("vue").ComponentProvideOptions, true, {}, any>;
export default _default;
