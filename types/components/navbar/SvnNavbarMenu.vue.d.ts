declare const _default: __VLS_WithTemplateSlots<typeof __VLS_component, __VLS_TemplateResult["slots"]>;
export default _default;
type __VLS_WithTemplateSlots<T, S> = T & (new () => {
    $slots: S;
});
declare const __VLS_component: import("vue").DefineComponent<{}, {
    modal: import("vue").Ref<null, null>;
    $emit: (event: "navigate", ...args: any[]) => void;
    variant: string;
    items: unknown[];
    $props: {
        readonly variant?: string | undefined;
        readonly items?: unknown[] | undefined;
    };
}, {}, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").PublicProps, Readonly<{}> & Readonly<{}>, {}, {}, {}, {}, string, import("vue").ComponentProvideOptions, true, {}, any>;
type __VLS_TemplateResult = {
    slots: {
        modal?(_: {}): any;
        "modal-activator"?(_: {}): any;
        "modal-content-body"?(_: {}): any;
    };
    refs: {
        modal: any;
    };
    attrs: Partial<typeof __VLS_inheritedAttrs>;
};
declare var __VLS_inheritedAttrs: {};
