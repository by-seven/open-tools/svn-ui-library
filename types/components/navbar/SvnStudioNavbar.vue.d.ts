declare const _default: import("vue").DefineComponent<{}, {
    items: unknown[];
    userAvatar: string;
    companyLogo: string;
    $props: {
        readonly items?: unknown[] | undefined;
        readonly userAvatar?: string | undefined;
        readonly companyLogo?: string | undefined;
    };
}, {}, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").PublicProps, Readonly<{}> & Readonly<{}>, {}, {}, {}, {}, string, import("vue").ComponentProvideOptions, true, {}, any>;
export default _default;
