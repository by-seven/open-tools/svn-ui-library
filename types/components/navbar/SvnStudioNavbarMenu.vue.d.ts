declare const _default: import("vue").DefineComponent<{}, {
    $emit: (event: "navigate", ...args: any[]) => void;
    variant: string;
    items: unknown[];
    $props: {
        readonly variant?: string | undefined;
        readonly items?: unknown[] | undefined;
    };
}, {}, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").PublicProps, Readonly<{}> & Readonly<{}>, {}, {}, {}, {}, string, import("vue").ComponentProvideOptions, true, {}, any>;
export default _default;
