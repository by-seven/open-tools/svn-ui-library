declare const _default: __VLS_WithTemplateSlots<typeof __VLS_component, __VLS_TemplateResult["slots"]>;
export default _default;
type __VLS_WithTemplateSlots<T, S> = T & (new () => {
    $slots: S;
});
declare const __VLS_component: import("vue").DefineComponent<{}, {
    $emit: (event: "edit-picture" | "image-selected" | "open-badges-modal", ...args: any[]) => void;
    size: string;
    firstname: string;
    lastname: string;
    email: string;
    tags: unknown[];
    showBadges: boolean;
    pictureEditable: boolean;
    badgeTypes: unknown[];
    avatar?: string | undefined;
    $props: {
        readonly size?: string | undefined;
        readonly firstname?: string | undefined;
        readonly lastname?: string | undefined;
        readonly email?: string | undefined;
        readonly tags?: unknown[] | undefined;
        readonly showBadges?: boolean | undefined;
        readonly pictureEditable?: boolean | undefined;
        readonly badgeTypes?: unknown[] | undefined;
        readonly avatar?: string | undefined;
    };
}, {}, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").PublicProps, Readonly<{}> & Readonly<{}>, {}, {}, {}, {}, string, import("vue").ComponentProvideOptions, true, {}, any>;
type __VLS_TemplateResult = {
    slots: {
        settings?(_: {}): any;
        settings?(_: {}): any;
    };
    refs: {
        picker: any;
    };
    attrs: Partial<typeof __VLS_inheritedAttrs>;
};
declare var __VLS_inheritedAttrs: {};
