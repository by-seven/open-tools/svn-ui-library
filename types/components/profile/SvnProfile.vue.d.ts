declare const _default: import("vue").DefineComponent<{}, {
    $emit: (event: "uploadFile" | "deletePicture", ...args: any[]) => void;
    dialogCustomClass: string;
    role: string;
    avatar: string;
    firstname: string;
    lastname: string;
    email: string;
    tags: unknown[];
    appVariant: string;
    avatarSize: string;
    edit: boolean;
    $props: {
        readonly dialogCustomClass?: string | undefined;
        readonly role?: string | undefined;
        readonly avatar?: string | undefined;
        readonly firstname?: string | undefined;
        readonly lastname?: string | undefined;
        readonly email?: string | undefined;
        readonly tags?: unknown[] | undefined;
        readonly appVariant?: string | undefined;
        readonly avatarSize?: string | undefined;
        readonly edit?: boolean | undefined;
    };
}, {}, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").PublicProps, Readonly<{}> & Readonly<{}>, {}, {}, {}, {}, string, import("vue").ComponentProvideOptions, true, {}, any>;
export default _default;
