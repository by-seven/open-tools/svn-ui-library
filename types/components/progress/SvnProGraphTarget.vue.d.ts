declare const _default: import("vue").DefineComponent<{}, {
    progress: string;
    indicatorType: string;
    notSetText: string;
    currentValue?: string | undefined;
    targetValue?: string | undefined;
    $props: {
        readonly progress?: string | undefined;
        readonly indicatorType?: string | undefined;
        readonly notSetText?: string | undefined;
        readonly currentValue?: string | undefined;
        readonly targetValue?: string | undefined;
    };
}, {}, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").PublicProps, Readonly<{}> & Readonly<{}>, {}, {}, {}, {}, string, import("vue").ComponentProvideOptions, true, {}, any>;
export default _default;
