declare const _default: __VLS_WithTemplateSlots<typeof __VLS_component, __VLS_TemplateResult["slots"]>;
export default _default;
type __VLS_WithTemplateSlots<T, S> = T & (new () => {
    $slots: S;
});
declare const __VLS_component: import("vue").DefineComponent<{}, {
    color: string;
    width: number;
    size: number;
    indeterminate: boolean;
    modelValue?: number | undefined;
    bgColor?: string | undefined;
    $props: {
        readonly color?: string | undefined;
        readonly width?: number | undefined;
        readonly size?: number | undefined;
        readonly indeterminate?: boolean | undefined;
        readonly modelValue?: number | undefined;
        readonly bgColor?: string | undefined;
    };
}, {}, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").PublicProps, Readonly<{}> & Readonly<{}>, {}, {}, {}, {}, string, import("vue").ComponentProvideOptions, true, {}, any>;
type __VLS_TemplateResult = {
    slots: {
        default?(_: {}): any;
    };
    refs: {};
    attrs: Partial<typeof __VLS_inheritedAttrs>;
};
declare var __VLS_inheritedAttrs: {};
