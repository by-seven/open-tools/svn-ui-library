declare const _default: import("vue").DefineComponent<{}, {
    color: string;
    height: number;
    size: number;
    indeterminate: boolean;
    steps: unknown[];
    modelValue?: number | undefined;
    bgColor?: string | undefined;
    $props: {
        readonly color?: string | undefined;
        readonly height?: number | undefined;
        readonly size?: number | undefined;
        readonly indeterminate?: boolean | undefined;
        readonly steps?: unknown[] | undefined;
        readonly modelValue?: number | undefined;
        readonly bgColor?: string | undefined;
    };
}, {}, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").PublicProps, Readonly<{}> & Readonly<{}>, {}, {}, {}, {}, string, import("vue").ComponentProvideOptions, true, {}, any>;
export default _default;
