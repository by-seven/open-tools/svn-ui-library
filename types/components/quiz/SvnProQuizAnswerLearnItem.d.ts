export const SvnProQuizAnswerLearnItemStates: Readonly<{
    partiallyCorrect: "partiallyCorrect";
    wrong: "wrong";
    correct: "correct";
    enable: "enable";
    selected: "selected";
}>;
declare const _default: import("vue").DefineComponent<import("vue").ExtractPropTypes<{
    leftLetter: StringConstructor;
    optionText: StringConstructor;
    state: {
        type: StringConstructor;
        default: () => "partiallyCorrect";
    };
    link: {
        type: BooleanConstructor;
        default: () => boolean;
    };
}>, {}, {}, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").PublicProps, Readonly<import("vue").ExtractPropTypes<{
    leftLetter: StringConstructor;
    optionText: StringConstructor;
    state: {
        type: StringConstructor;
        default: () => "partiallyCorrect";
    };
    link: {
        type: BooleanConstructor;
        default: () => boolean;
    };
}>> & Readonly<{}>, {
    link: boolean;
    state: string;
}, {}, {}, {}, string, import("vue").ComponentProvideOptions, true, {}, any>;
export default _default;
