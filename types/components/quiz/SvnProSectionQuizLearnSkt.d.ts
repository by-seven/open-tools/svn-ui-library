export const SvnProSectionQuizLearnCorrectionType: Readonly<{
    wrongAnswer: "wrongAnswer";
    partiallyRight: "partiallyRight";
    rightAnswer: "rightAnswer";
    none: "none";
}>;
declare const _default: import("vue").DefineComponent<import("vue").ExtractPropTypes<{
    correctionType: {
        type: StringConstructor;
        default(): "none";
    };
    title: {
        type: StringConstructor;
    };
    list: {
        type: ArrayConstructor;
        default(): never[];
    };
}>, void, {}, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").PublicProps, Readonly<import("vue").ExtractPropTypes<{
    correctionType: {
        type: StringConstructor;
        default(): "none";
    };
    title: {
        type: StringConstructor;
    };
    list: {
        type: ArrayConstructor;
        default(): never[];
    };
}>> & Readonly<{}>, {
    list: unknown[];
    correctionType: string;
}, {}, {}, {}, string, import("vue").ComponentProvideOptions, true, {}, any>;
export default _default;
