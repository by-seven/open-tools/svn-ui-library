declare const _default: __VLS_WithTemplateSlots<typeof __VLS_component, __VLS_TemplateResult["slots"]>;
export default _default;
type __VLS_WithTemplateSlots<T, S> = T & (new () => {
    $slots: S;
});
declare const __VLS_component: import("vue").DefineComponent<{}, {
    shadow: boolean;
    entity: string;
    indexTitle: string;
    indexSubtitle: string;
    indexIcon: string;
    withOptions: boolean;
    titleWrapperClass: string;
    $props: {
        readonly shadow?: boolean | undefined;
        readonly entity?: string | undefined;
        readonly indexTitle?: string | undefined;
        readonly indexSubtitle?: string | undefined;
        readonly indexIcon?: string | undefined;
        readonly withOptions?: boolean | undefined;
        readonly titleWrapperClass?: string | undefined;
    };
}, {}, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").PublicProps, Readonly<{}> & Readonly<{}>, {}, {}, {}, {}, string, import("vue").ComponentProvideOptions, true, {}, any>;
type __VLS_TemplateResult = {
    slots: {
        "title-content"?(_: {}): any;
        "title-content"?(_: {}): any;
        "title-content"?(_: {}): any;
        "title-content"?(_: {}): any;
        "title-content"?(_: {}): any;
        "create-index"?(_: {}): any;
        "create-index"?(_: {}): any;
        "create-index"?(_: {}): any;
        "create-index"?(_: {}): any;
        "create-index"?(_: {}): any;
        inputs?(_: {}): any;
        inputs?(_: {}): any;
        inputs?(_: {}): any;
        inputs?(_: {}): any;
        inputs?(_: {}): any;
        inputs?(_: {}): any;
        body?(_: {}): any;
        body?(_: {}): any;
        body?(_: {}): any;
        body?(_: {}): any;
        body?(_: {}): any;
        tabs?(_: {}): any;
        tabs?(_: {}): any;
        tabs?(_: {}): any;
    };
    refs: {};
    attrs: Partial<typeof __VLS_inheritedAttrs>;
};
declare var __VLS_inheritedAttrs: {};
