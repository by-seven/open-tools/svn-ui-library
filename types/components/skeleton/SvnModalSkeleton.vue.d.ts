declare const _default: __VLS_WithTemplateSlots<typeof __VLS_component, __VLS_TemplateResult["slots"]>;
export default _default;
type __VLS_WithTemplateSlots<T, S> = T & (new () => {
    $slots: S;
});
declare const __VLS_component: import("vue").DefineComponent<{}, {
    dialog: import("vue").Ref<boolean, boolean>;
    $emit: (event: "prependIconClick" | "appendIconClick" | "onClickOutside", ...args: any[]) => void;
    transition: string;
    persistent: boolean;
    fullscreen: boolean;
    activatorText: string;
    activatorVariant: string;
    fullheight: boolean;
    modalTitle: string;
    modalHeight: string;
    modalTitleCenter: boolean;
    verticalHeader: boolean;
    prependHeaderIcon: string;
    appendHeaderIcon: string;
    appendIconCloseModal: boolean;
    headerCustomClass: string;
    modalCustomClass: string;
    $props: {
        readonly transition?: string | undefined;
        readonly persistent?: boolean | undefined;
        readonly fullscreen?: boolean | undefined;
        readonly activatorText?: string | undefined;
        readonly activatorVariant?: string | undefined;
        readonly fullheight?: boolean | undefined;
        readonly modalTitle?: string | undefined;
        readonly modalHeight?: string | undefined;
        readonly modalTitleCenter?: boolean | undefined;
        readonly verticalHeader?: boolean | undefined;
        readonly prependHeaderIcon?: string | undefined;
        readonly appendHeaderIcon?: string | undefined;
        readonly appendIconCloseModal?: boolean | undefined;
        readonly headerCustomClass?: string | undefined;
        readonly modalCustomClass?: string | undefined;
    };
}, {}, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").PublicProps, Readonly<{}> & Readonly<{}>, {}, {}, {}, {}, string, import("vue").ComponentProvideOptions, true, {}, any>;
type __VLS_TemplateResult = {
    slots: {
        "activator-div"?(_: any): any;
        activator?(_: {}): any;
        "activator-text"?(_: {}): any;
        modal?(_: {}): any;
        header?(_: {
            close: () => void;
        }): any;
        "header-text"?(_: {}): any;
        content?(_: {}): any;
        "content-body"?(_: {}): any;
        "bottom-content"?(_: {}): any;
    };
    refs: {};
    attrs: Partial<typeof __VLS_inheritedAttrs>;
};
declare var __VLS_inheritedAttrs: {};
