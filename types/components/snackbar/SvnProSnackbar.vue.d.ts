declare const _default: import("vue").DefineComponent<{}, {
    snackbar: import("vue").Ref<null, null>;
    $emit: (event: "update:modelValue" | "action-clicked" | "close-clicked", ...args: any[]) => void;
    close: boolean;
    color: string;
    modelValue: boolean;
    vertical: boolean;
    supportingText: string;
    action: boolean;
    twoLines: boolean;
    actionText: string;
    $props: {
        readonly close?: boolean | undefined;
        readonly color?: string | undefined;
        readonly modelValue?: boolean | undefined;
        readonly vertical?: boolean | undefined;
        readonly supportingText?: string | undefined;
        readonly action?: boolean | undefined;
        readonly twoLines?: boolean | undefined;
        readonly actionText?: string | undefined;
    };
}, {}, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").PublicProps, Readonly<{}> & Readonly<{}>, {}, {}, {}, {}, string, import("vue").ComponentProvideOptions, true, {}, any>;
export default _default;
