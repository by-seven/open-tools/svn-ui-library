declare const _default: import("vue").DefineComponent<{}, {
    badge: string;
    action: boolean;
    actionText: string;
    toastType: string;
    toastTitle: string;
    toastDescription: string;
    progressBar: boolean;
    $props: {
        readonly badge?: string | undefined;
        readonly action?: boolean | undefined;
        readonly actionText?: string | undefined;
        readonly toastType?: string | undefined;
        readonly toastTitle?: string | undefined;
        readonly toastDescription?: string | undefined;
        readonly progressBar?: boolean | undefined;
    };
}, {}, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").PublicProps, Readonly<{}> & Readonly<{}>, {}, {}, {}, {}, string, import("vue").ComponentProvideOptions, true, {}, any>;
export default _default;
