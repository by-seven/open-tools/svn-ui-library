declare const _default: import("vue").DefineComponent<{}, {
    $emit: (event: "update:modelValue" | "click-primary" | "click-secondary", ...args: any[]) => void;
    variant: string;
    size: string;
    actions: boolean;
    supportingText: string;
    title?: string | undefined;
    icon?: string | undefined;
    prependSecondary?: string | undefined;
    prependPrimary?: string | undefined;
    actionPrimaryTitle?: string | undefined;
    actionSecondaryTitle?: string | undefined;
    $props: {
        readonly variant?: string | undefined;
        readonly size?: string | undefined;
        readonly actions?: boolean | undefined;
        readonly supportingText?: string | undefined;
        readonly title?: string | undefined;
        readonly icon?: string | undefined;
        readonly prependSecondary?: string | undefined;
        readonly prependPrimary?: string | undefined;
        readonly actionPrimaryTitle?: string | undefined;
        readonly actionSecondaryTitle?: string | undefined;
    };
}, {}, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").PublicProps, Readonly<{}> & Readonly<{}>, {}, {}, {}, {}, string, import("vue").ComponentProvideOptions, true, {}, any>;
export default _default;
