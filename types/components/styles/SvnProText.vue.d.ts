declare const _default: __VLS_WithTemplateSlots<typeof __VLS_component, __VLS_TemplateResult["slots"]>;
export default _default;
type __VLS_WithTemplateSlots<T, S> = T & (new () => {
    $slots: S;
});
declare const __VLS_component: import("vue").DefineComponent<{}, {
    button: boolean;
    caption: boolean;
    textarea: boolean;
    text: string;
    color: string;
    bold: boolean;
    medium: boolean;
    regular: boolean;
    bodyLarge: boolean;
    bodyMedium: boolean;
    subtitleLarge: boolean;
    subtitleMedium: boolean;
    overline: boolean;
    $props: {
        readonly button?: boolean | undefined;
        readonly caption?: boolean | undefined;
        readonly textarea?: boolean | undefined;
        readonly text?: string | undefined;
        readonly color?: string | undefined;
        readonly bold?: boolean | undefined;
        readonly medium?: boolean | undefined;
        readonly regular?: boolean | undefined;
        readonly bodyLarge?: boolean | undefined;
        readonly bodyMedium?: boolean | undefined;
        readonly subtitleLarge?: boolean | undefined;
        readonly subtitleMedium?: boolean | undefined;
        readonly overline?: boolean | undefined;
    };
}, {}, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").PublicProps, Readonly<{}> & Readonly<{}>, {}, {}, {}, {}, string, import("vue").ComponentProvideOptions, true, {}, any>;
type __VLS_TemplateResult = {
    slots: {
        default?(_: {}): any;
    };
    refs: {};
    attrs: Partial<typeof __VLS_inheritedAttrs>;
};
declare var __VLS_inheritedAttrs: {};
