declare const _default: __VLS_WithTemplateSlots<typeof __VLS_component, __VLS_TemplateResult["slots"]>;
export default _default;
type __VLS_WithTemplateSlots<T, S> = T & (new () => {
    $slots: S;
});
declare const __VLS_component: import("vue").DefineComponent<{}, {
    base: boolean;
    textarea: boolean;
    text: string;
    sm: boolean;
    color: string;
    bold: boolean;
    semiBold: boolean;
    medium: boolean;
    regular: boolean;
    italic: boolean;
    xs: boolean;
    xxs: boolean;
    $props: {
        readonly base?: boolean | undefined;
        readonly textarea?: boolean | undefined;
        readonly text?: string | undefined;
        readonly sm?: boolean | undefined;
        readonly color?: string | undefined;
        readonly bold?: boolean | undefined;
        readonly semiBold?: boolean | undefined;
        readonly medium?: boolean | undefined;
        readonly regular?: boolean | undefined;
        readonly italic?: boolean | undefined;
        readonly xs?: boolean | undefined;
        readonly xxs?: boolean | undefined;
    };
}, {}, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").PublicProps, Readonly<{}> & Readonly<{}>, {}, {}, {}, {}, string, import("vue").ComponentProvideOptions, true, {}, any>;
type __VLS_TemplateResult = {
    slots: {
        default?(_: {}): any;
        default?(_: {}): any;
        default?(_: {}): any;
        default?(_: {}): any;
    };
    refs: {};
    attrs: Partial<typeof __VLS_inheritedAttrs>;
};
declare var __VLS_inheritedAttrs: {};
