declare const _default: __VLS_WithTemplateSlots<typeof __VLS_component, __VLS_TemplateResult["slots"]>;
export default _default;
type __VLS_WithTemplateSlots<T, S> = T & (new () => {
    $slots: S;
});
declare const __VLS_component: import("vue").DefineComponent<{}, {
    h1: boolean;
    h2: boolean;
    h3: boolean;
    color: string;
    bold: boolean;
    semiBold: boolean;
    medium: boolean;
    regular: boolean;
    italic: boolean;
    customClass: string;
    $props: {
        readonly h1?: boolean | undefined;
        readonly h2?: boolean | undefined;
        readonly h3?: boolean | undefined;
        readonly color?: string | undefined;
        readonly bold?: boolean | undefined;
        readonly semiBold?: boolean | undefined;
        readonly medium?: boolean | undefined;
        readonly regular?: boolean | undefined;
        readonly italic?: boolean | undefined;
        readonly customClass?: string | undefined;
    };
}, {}, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").PublicProps, Readonly<{}> & Readonly<{}>, {}, {}, {}, {}, string, import("vue").ComponentProvideOptions, true, {}, any>;
type __VLS_TemplateResult = {
    slots: {
        default?(_: {}): any;
        default?(_: {}): any;
        default?(_: {}): any;
    };
    refs: {};
    attrs: Partial<typeof __VLS_inheritedAttrs>;
};
declare var __VLS_inheritedAttrs: {};
