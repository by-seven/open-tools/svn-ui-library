declare const _default: __VLS_WithTemplateSlots<typeof __VLS_component, __VLS_TemplateResult["slots"]>;
export default _default;
type __VLS_WithTemplateSlots<T, S> = T & (new () => {
    $slots: S;
});
declare const __VLS_component: import("vue").DefineComponent<{}, {
    $emit: (event: "update-pagination" | "load-more", ...args: any[]) => void;
    loading: boolean;
    items: unknown[];
    search: string;
    mobile: boolean;
    page: number;
    dataTablePage: number;
    paginationTotalPages: number;
    headerItems: unknown[];
    showSelect: boolean;
    itemsPerPage: number;
    $props: {
        readonly loading?: boolean | undefined;
        readonly items?: unknown[] | undefined;
        readonly search?: string | undefined;
        readonly mobile?: boolean | undefined;
        readonly page?: number | undefined;
        readonly dataTablePage?: number | undefined;
        readonly paginationTotalPages?: number | undefined;
        readonly headerItems?: unknown[] | undefined;
        readonly showSelect?: boolean | undefined;
        readonly itemsPerPage?: number | undefined;
    };
}, {}, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").PublicProps, Readonly<{}> & Readonly<{}>, {}, {}, {}, {}, string, import("vue").ComponentProvideOptions, true, {}, any>;
type __VLS_TemplateResult = {
    slots: {
        loading?(_: {}): any;
        loader?(_: {}): any;
        item?(_: {
            item: any;
        }): any;
        "no-data"?(_: {}): any;
        bottom?(_: {}): any;
        mobile?(_: {}): any;
    };
    refs: {};
    attrs: Partial<typeof __VLS_inheritedAttrs>;
};
declare var __VLS_inheritedAttrs: {};
