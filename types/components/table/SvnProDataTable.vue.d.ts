declare const _default: __VLS_WithTemplateSlots<typeof __VLS_component, __VLS_TemplateResult["slots"]>;
export default _default;
type __VLS_WithTemplateSlots<T, S> = T & (new () => {
    $slots: S;
});
declare const __VLS_component: import("vue").DefineComponent<{}, {
    $emit: (event: "update:modelValue" | "update-pagination", ...args: any[]) => void;
    loading: boolean;
    modelValue: null;
    density: string;
    items: unknown[];
    itemValue: string;
    page: number;
    paginationTotalPages: number;
    showSelect: boolean;
    itemsPerPage: number;
    headers: unknown[];
    $props: {
        readonly loading?: boolean | undefined;
        readonly modelValue?: null | undefined;
        readonly density?: string | undefined;
        readonly items?: unknown[] | undefined;
        readonly itemValue?: string | undefined;
        readonly page?: number | undefined;
        readonly paginationTotalPages?: number | undefined;
        readonly showSelect?: boolean | undefined;
        readonly itemsPerPage?: number | undefined;
        readonly headers?: unknown[] | undefined;
    };
}, {}, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").PublicProps, Readonly<{}> & Readonly<{}>, {}, {}, {}, {}, string, import("vue").ComponentProvideOptions, true, {}, any>;
type __VLS_TemplateResult = {
    slots: {
        item?(_: {
            item: any;
            index: any;
        }): any;
        loading?(_: {}): any;
        "no-data"?(_: {}): any;
    };
    refs: {};
    attrs: Partial<typeof __VLS_inheritedAttrs>;
};
declare var __VLS_inheritedAttrs: {};
