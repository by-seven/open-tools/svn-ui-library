declare const _default: import("vue").DefineComponent<{}, {
    $emit: (event: "update:modelValue", ...args: any[]) => void;
    text: string;
    prependIcon: string;
    sortable: boolean;
    info: boolean;
    $props: {
        readonly text?: string | undefined;
        readonly prependIcon?: string | undefined;
        readonly sortable?: boolean | undefined;
        readonly info?: boolean | undefined;
    };
}, {}, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").PublicProps, Readonly<{}> & Readonly<{}>, {}, {}, {}, {}, string, import("vue").ComponentProvideOptions, true, {}, any>;
export default _default;
