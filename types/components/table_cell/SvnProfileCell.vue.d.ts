declare const _default: import("vue").DefineComponent<{}, {
    avatar: string;
    firstname: string;
    lastname: string;
    email: string;
    $props: {
        readonly avatar?: string | undefined;
        readonly firstname?: string | undefined;
        readonly lastname?: string | undefined;
        readonly email?: string | undefined;
    };
}, {}, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").PublicProps, Readonly<{}> & Readonly<{}>, {}, {}, {}, {}, string, import("vue").ComponentProvideOptions, true, {}, any>;
export default _default;
