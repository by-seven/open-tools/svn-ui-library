declare const _default: import("vue").DefineComponent<{}, {
    prependText: string;
    appendText: string;
    $props: {
        readonly prependText?: string | undefined;
        readonly appendText?: string | undefined;
    };
}, {}, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").PublicProps, Readonly<{}> & Readonly<{}>, {}, {}, {}, {}, string, import("vue").ComponentProvideOptions, true, {}, any>;
export default _default;
