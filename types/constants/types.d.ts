export const TopBarLearnModuleTypes: Readonly<{
    draft: "draft";
    show: "show";
    edit: "edit";
    quiz: "quiz";
    view_mode: "view_mode";
    face_to_face: "face_to_face";
}>;
export const TopBarLearnModuleSaveTypes: Readonly<{
    manual: "manual";
    auto: "auto";
}>;
export const ComponentSizes: Readonly<{
    compact: "compact";
    default: "default";
}>;
export const CardInterviewTargetTypes: Readonly<{
    create: "create";
    create_proposal: "create_proposal";
    update_show: "update_show";
}>;
export const ButtonVariants: Readonly<{
    elevated: "elevated";
    flat: "flat";
    text: "text";
    tonal: "tonal";
    outlined: "outlined";
    plain: "plain";
}>;
export const InfotagStyles: Readonly<{
    text: "text";
    filled: "filled";
    outlined: "outlined";
}>;
export const TargetIndicatorStatuses: Readonly<{
    completed: "completed";
    in_progress: "in_progress";
    uncompleted: "uncompleted";
}>;
export const TargetObjectiveIndicatorTypes: Readonly<{
    rating: "rating";
    boolean: "boolean";
    numeric: "numeric";
    percentage: "percentage";
    multi_choice: "multi_choice";
}>;
export const InterviewCompletionStatuses: Readonly<{
    submitted: "submitted";
    in_progress: "in_progress";
    not_started: "not_started";
    not_available: "not_available";
}>;
export const InterviewAppQuestionTypes: Readonly<{
    chapter: "InterviewApp::Questions::Chapter";
    mcq: "InterviewApp::Questions::Mcq";
    open: "InterviewApp::Questions::Open";
    paragraph: "InterviewApp::Questions::Paragraph";
    rating: "InterviewApp::Questions::Rating";
    roadmap_update: "InterviewApp::Questions::UpdateRoadmap";
    roadmap_create: "InterviewApp::Questions::CreateRoadmap";
}>;
