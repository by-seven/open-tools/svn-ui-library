declare namespace _default {
    function install(app: any): void;
}
export default _default;
export { TopBarLearnModuleTypes, TopBarLearnModuleSaveTypes, ComponentSizes, CardInterviewTargetTypes, ButtonVariants, InfotagStyles, TargetIndicatorStatuses, TargetObjectiveIndicatorTypes, InterviewCompletionStatuses, InterviewAppQuestionTypes };
