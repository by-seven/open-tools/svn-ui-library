declare const _default: import("vue").DefineComponent<{}, {
    modelValue: null;
    density: string;
    items: unknown[];
    itemValue: string;
    page: number;
    paginationTotalPages: number;
    showSelect: boolean;
    itemsPerPage: number;
    headers: unknown[];
    $props: {
        readonly modelValue?: null | undefined;
        readonly density?: string | undefined;
        readonly items?: unknown[] | undefined;
        readonly itemValue?: string | undefined;
        readonly page?: number | undefined;
        readonly paginationTotalPages?: number | undefined;
        readonly showSelect?: boolean | undefined;
        readonly itemsPerPage?: number | undefined;
        readonly headers?: unknown[] | undefined;
    };
}, {}, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").PublicProps, Readonly<{}> & Readonly<{}>, {}, {}, {}, {}, string, import("vue").ComponentProvideOptions, true, {}, any>;
export default _default;
