declare const _default: import("vue").DefineComponent<{}, {
    $emit: (event: "item-clicked", ...args: any[]) => void;
    items: unknown[];
    $props: {
        readonly items?: unknown[] | undefined;
    };
}, {}, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").PublicProps, Readonly<{}> & Readonly<{}>, {}, {}, {}, {}, string, import("vue").ComponentProvideOptions, true, {}, any>;
export default _default;
