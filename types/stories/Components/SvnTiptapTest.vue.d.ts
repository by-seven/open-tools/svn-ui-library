declare const _default: import("vue").DefineComponent<{}, {
    size: string;
    htmlData: string;
    withBorder: boolean;
    withPadding: boolean;
    isEditable: boolean;
    extensionLeftMenu: boolean;
    $props: {
        readonly size?: string | undefined;
        readonly htmlData?: string | undefined;
        readonly withBorder?: boolean | undefined;
        readonly withPadding?: boolean | undefined;
        readonly isEditable?: boolean | undefined;
        readonly extensionLeftMenu?: boolean | undefined;
    };
}, {}, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").PublicProps, Readonly<{}> & Readonly<{}>, {}, {}, {}, {}, string, import("vue").ComponentProvideOptions, true, {}, any>;
export default _default;
