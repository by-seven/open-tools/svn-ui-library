declare namespace _default {
    export let title: string;
    export { SvnAutocomplete as component };
    export let tags: string[];
    export namespace parameters {
        namespace docs {
            namespace description {
                let component: string;
            }
        }
    }
}
export default _default;
export namespace Playground {
    function render(args: any): {
        components: {
            SvnAutocomplete: {
                new (...args: any[]): import("vue").CreateComponentPublicInstanceWithMixins<Readonly<{}> & Readonly<{}>, {
                    $emit: (event: "input" | "clear" | "update:modelValue" | "on-intersect" | "intersect", ...args: any[]) => void;
                    label: string;
                    color: string;
                    loading: boolean;
                    disabled: boolean;
                    modelValue: null;
                    closeIcon: string;
                    density: string;
                    baseColor: string;
                    prependInnerIcon: string;
                    multiple: boolean;
                    items: unknown[];
                    returnObject: boolean;
                    itemTitle: string;
                    itemValue: string;
                    closableChips: boolean;
                    menuProps: Record<string, any>;
                    avatar: boolean;
                    overflowY: boolean;
                    checkbox: boolean;
                    avatarChip: boolean;
                    disablingFunction: Function;
                    clearOnSelect: boolean;
                    withAddAndRemoveButtons: boolean;
                    chipClass: string;
                    $props: {
                        readonly label?: string | undefined;
                        readonly color?: string | undefined;
                        readonly loading?: boolean | undefined;
                        readonly disabled?: boolean | undefined;
                        readonly modelValue?: null | undefined;
                        readonly closeIcon?: string | undefined;
                        readonly density?: string | undefined;
                        readonly baseColor?: string | undefined;
                        readonly prependInnerIcon?: string | undefined;
                        readonly multiple?: boolean | undefined;
                        readonly items?: unknown[] | undefined;
                        readonly returnObject?: boolean | undefined;
                        readonly itemTitle?: string | undefined;
                        readonly itemValue?: string | undefined;
                        readonly closableChips?: boolean | undefined;
                        readonly menuProps?: Record<string, any> | undefined;
                        readonly avatar?: boolean | undefined;
                        readonly overflowY?: boolean | undefined;
                        readonly checkbox?: boolean | undefined;
                        readonly avatarChip?: boolean | undefined;
                        readonly disablingFunction?: Function | undefined;
                        readonly clearOnSelect?: boolean | undefined;
                        readonly withAddAndRemoveButtons?: boolean | undefined;
                        readonly chipClass?: string | undefined;
                    };
                }, {}, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps & Readonly<{}> & Readonly<{}>, {}, true, {}, {}, import("vue").GlobalComponents, import("vue").GlobalDirectives, string, {}, any, import("vue").ComponentProvideOptions, {
                    P: {};
                    B: {};
                    D: {};
                    C: {};
                    M: {};
                    Defaults: {};
                }, Readonly<{}> & Readonly<{}>, {
                    $emit: (event: "input" | "clear" | "update:modelValue" | "on-intersect" | "intersect", ...args: any[]) => void;
                    label: string;
                    color: string;
                    loading: boolean;
                    disabled: boolean;
                    modelValue: null;
                    closeIcon: string;
                    density: string;
                    baseColor: string;
                    prependInnerIcon: string;
                    multiple: boolean;
                    items: unknown[];
                    returnObject: boolean;
                    itemTitle: string;
                    itemValue: string;
                    closableChips: boolean;
                    menuProps: Record<string, any>;
                    avatar: boolean;
                    overflowY: boolean;
                    checkbox: boolean;
                    avatarChip: boolean;
                    disablingFunction: Function;
                    clearOnSelect: boolean;
                    withAddAndRemoveButtons: boolean;
                    chipClass: string;
                    $props: {
                        readonly label?: string | undefined;
                        readonly color?: string | undefined;
                        readonly loading?: boolean | undefined;
                        readonly disabled?: boolean | undefined;
                        readonly modelValue?: null | undefined;
                        readonly closeIcon?: string | undefined;
                        readonly density?: string | undefined;
                        readonly baseColor?: string | undefined;
                        readonly prependInnerIcon?: string | undefined;
                        readonly multiple?: boolean | undefined;
                        readonly items?: unknown[] | undefined;
                        readonly returnObject?: boolean | undefined;
                        readonly itemTitle?: string | undefined;
                        readonly itemValue?: string | undefined;
                        readonly closableChips?: boolean | undefined;
                        readonly menuProps?: Record<string, any> | undefined;
                        readonly avatar?: boolean | undefined;
                        readonly overflowY?: boolean | undefined;
                        readonly checkbox?: boolean | undefined;
                        readonly avatarChip?: boolean | undefined;
                        readonly disablingFunction?: Function | undefined;
                        readonly clearOnSelect?: boolean | undefined;
                        readonly withAddAndRemoveButtons?: boolean | undefined;
                        readonly chipClass?: string | undefined;
                    };
                }, {}, {}, {}, {}>;
                __isFragment?: never;
                __isTeleport?: never;
                __isSuspense?: never;
            } & import("vue").ComponentOptionsBase<Readonly<{}> & Readonly<{}>, {
                $emit: (event: "input" | "clear" | "update:modelValue" | "on-intersect" | "intersect", ...args: any[]) => void;
                label: string;
                color: string;
                loading: boolean;
                disabled: boolean;
                modelValue: null;
                closeIcon: string;
                density: string;
                baseColor: string;
                prependInnerIcon: string;
                multiple: boolean;
                items: unknown[];
                returnObject: boolean;
                itemTitle: string;
                itemValue: string;
                closableChips: boolean;
                menuProps: Record<string, any>;
                avatar: boolean;
                overflowY: boolean;
                checkbox: boolean;
                avatarChip: boolean;
                disablingFunction: Function;
                clearOnSelect: boolean;
                withAddAndRemoveButtons: boolean;
                chipClass: string;
                $props: {
                    readonly label?: string | undefined;
                    readonly color?: string | undefined;
                    readonly loading?: boolean | undefined;
                    readonly disabled?: boolean | undefined;
                    readonly modelValue?: null | undefined;
                    readonly closeIcon?: string | undefined;
                    readonly density?: string | undefined;
                    readonly baseColor?: string | undefined;
                    readonly prependInnerIcon?: string | undefined;
                    readonly multiple?: boolean | undefined;
                    readonly items?: unknown[] | undefined;
                    readonly returnObject?: boolean | undefined;
                    readonly itemTitle?: string | undefined;
                    readonly itemValue?: string | undefined;
                    readonly closableChips?: boolean | undefined;
                    readonly menuProps?: Record<string, any> | undefined;
                    readonly avatar?: boolean | undefined;
                    readonly overflowY?: boolean | undefined;
                    readonly checkbox?: boolean | undefined;
                    readonly avatarChip?: boolean | undefined;
                    readonly disablingFunction?: Function | undefined;
                    readonly clearOnSelect?: boolean | undefined;
                    readonly withAddAndRemoveButtons?: boolean | undefined;
                    readonly chipClass?: string | undefined;
                };
            }, {}, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, {}, {}, string, {}, import("vue").GlobalComponents, import("vue").GlobalDirectives, string, import("vue").ComponentProvideOptions> & import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps & (new () => {
                $slots: {
                    "prepend-details"?(_: {
                        item: any;
                    }): any;
                    "main-content"?(_: {
                        item: any;
                    }): any;
                    "append-content"?(_: {
                        item: any;
                    }): any;
                    "chip-content"?(_: {
                        item: any;
                        props: {
                            readonly label: string;
                            readonly color: string;
                            readonly loading: boolean;
                            readonly disabled: boolean;
                            readonly modelValue: null;
                            readonly closeIcon: string;
                            readonly density: string;
                            readonly baseColor: string;
                            readonly prependInnerIcon: string;
                            readonly multiple: boolean;
                            readonly items: unknown[];
                            readonly returnObject: boolean;
                            readonly itemTitle: string;
                            readonly itemValue: string;
                            readonly closableChips: boolean;
                            readonly menuProps: Record<string, any>;
                            readonly avatar: boolean;
                            readonly overflowY: boolean;
                            readonly checkbox: boolean;
                            readonly avatarChip: boolean;
                            readonly disablingFunction: Function;
                            readonly clearOnSelect: boolean;
                            readonly withAddAndRemoveButtons: boolean;
                            readonly chipClass: string;
                        };
                    }): any;
                    "no-data"?(_: {}): any;
                };
            });
        };
        setup(): {
            args: any;
        };
        template: string;
    };
    namespace args {
        export { items };
        export let label: string;
        export let multiple: boolean;
        export let checkbox: boolean;
        export let closableChips: boolean;
        export let disabled: boolean;
        export let avatarChip: boolean;
        export let clearOnSelect: boolean;
        export let resetSearchOnUnfocus: boolean;
        export let returnObject: boolean;
    }
}
import SvnAutocomplete from "../components/input/SvnAutocomplete.vue";
declare const items: {
    id: number;
    avatar: null;
    firstname: string;
    lastname: string;
    fullname: string;
}[];
