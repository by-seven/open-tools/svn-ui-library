declare namespace _default {
    export let title: string;
    export { SvnAvatar as component };
    export let tags: string[];
    export namespace argTypes {
        namespace size {
            let control: string;
            let options: string[];
        }
        namespace avatar {
            let control_1: string;
            export { control_1 as control };
        }
    }
    export namespace parameters {
        namespace docs {
            namespace description {
                let component: string;
            }
        }
    }
}
export default _default;
export namespace Playground {
    function render(args: any): {
        components: {
            SvnAvatar: import("vue").DefineComponent<{}, {
                size: string;
                alt: string;
                avatar: string;
                firstname: string;
                lastname: string;
                $props: {
                    readonly size?: string | undefined;
                    readonly alt?: string | undefined;
                    readonly avatar?: string | undefined;
                    readonly firstname?: string | undefined;
                    readonly lastname?: string | undefined;
                };
            }, {}, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").PublicProps, Readonly<{}> & Readonly<{}>, {}, {}, {}, {}, string, import("vue").ComponentProvideOptions, true, {}, any>;
        };
        setup(): {
            args: any;
        };
        template: string;
    };
    namespace args {
        export let firstname: string;
        export let lastname: string;
        let size_1: string;
        export { size_1 as size };
        export let alt: string;
        let avatar_1: string;
        export { avatar_1 as avatar };
        export let rounded: boolean;
    }
}
import SvnAvatar from "../components/avatar/SvnAvatar.vue";
