declare namespace _default {
    export let title: string;
    export { SvnButton as component };
    export let tags: string[];
    export namespace argTypes {
        namespace variant {
            let control: string;
            let options: string[];
        }
        namespace text {
            let control_1: string;
            export { control_1 as control };
        }
    }
    export namespace parameters {
        namespace docs {
            namespace description {
                let component: string;
            }
        }
    }
}
export default _default;
export namespace Playground {
    function render(args: any): {
        components: {
            SvnButton: {
                new (...args: any[]): import("vue").CreateComponentPublicInstanceWithMixins<Readonly<{}> & Readonly<{}>, {
                    $emit: (event: "click", ...args: any[]) => void;
                    text: string;
                    variant: string;
                    disable: boolean;
                    to: string;
                    danger: boolean;
                    colorClass: string;
                    loading: boolean;
                    block: boolean;
                    loaderColor: string;
                    prependIcon: string;
                    appendIcon: string;
                    prependClass: string;
                    loadingColorClass: string;
                    $props: {
                        readonly text?: string | undefined;
                        readonly variant?: string | undefined;
                        readonly disable?: boolean | undefined;
                        readonly to?: string | undefined;
                        readonly danger?: boolean | undefined;
                        readonly colorClass?: string | undefined;
                        readonly loading?: boolean | undefined;
                        readonly block?: boolean | undefined;
                        readonly loaderColor?: string | undefined;
                        readonly prependIcon?: string | undefined;
                        readonly appendIcon?: string | undefined;
                        readonly prependClass?: string | undefined;
                        readonly loadingColorClass?: string | undefined;
                    };
                }, {}, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps & Readonly<{}> & Readonly<{}>, {}, true, {}, {}, import("vue").GlobalComponents, import("vue").GlobalDirectives, string, {}, any, import("vue").ComponentProvideOptions, {
                    P: {};
                    B: {};
                    D: {};
                    C: {};
                    M: {};
                    Defaults: {};
                }, Readonly<{}> & Readonly<{}>, {
                    $emit: (event: "click", ...args: any[]) => void;
                    text: string;
                    variant: string;
                    disable: boolean;
                    to: string;
                    danger: boolean;
                    colorClass: string;
                    loading: boolean;
                    block: boolean;
                    loaderColor: string;
                    prependIcon: string;
                    appendIcon: string;
                    prependClass: string;
                    loadingColorClass: string;
                    $props: {
                        readonly text?: string | undefined;
                        readonly variant?: string | undefined;
                        readonly disable?: boolean | undefined;
                        readonly to?: string | undefined;
                        readonly danger?: boolean | undefined;
                        readonly colorClass?: string | undefined;
                        readonly loading?: boolean | undefined;
                        readonly block?: boolean | undefined;
                        readonly loaderColor?: string | undefined;
                        readonly prependIcon?: string | undefined;
                        readonly appendIcon?: string | undefined;
                        readonly prependClass?: string | undefined;
                        readonly loadingColorClass?: string | undefined;
                    };
                }, {}, {}, {}, {}>;
                __isFragment?: never;
                __isTeleport?: never;
                __isSuspense?: never;
            } & import("vue").ComponentOptionsBase<Readonly<{}> & Readonly<{}>, {
                $emit: (event: "click", ...args: any[]) => void;
                text: string;
                variant: string;
                disable: boolean;
                to: string;
                danger: boolean;
                colorClass: string;
                loading: boolean;
                block: boolean;
                loaderColor: string;
                prependIcon: string;
                appendIcon: string;
                prependClass: string;
                loadingColorClass: string;
                $props: {
                    readonly text?: string | undefined;
                    readonly variant?: string | undefined;
                    readonly disable?: boolean | undefined;
                    readonly to?: string | undefined;
                    readonly danger?: boolean | undefined;
                    readonly colorClass?: string | undefined;
                    readonly loading?: boolean | undefined;
                    readonly block?: boolean | undefined;
                    readonly loaderColor?: string | undefined;
                    readonly prependIcon?: string | undefined;
                    readonly appendIcon?: string | undefined;
                    readonly prependClass?: string | undefined;
                    readonly loadingColorClass?: string | undefined;
                };
            }, {}, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, {}, {}, string, {}, import("vue").GlobalComponents, import("vue").GlobalDirectives, string, import("vue").ComponentProvideOptions> & import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps & (new () => {
                $slots: {
                    prepend?(_: {}): any;
                    default?(_: {}): any;
                    "button-text"?(_: {}): any;
                    append?(_: {}): any;
                    loader?(_: {}): any;
                };
            });
        };
        setup(): {
            args: any;
        };
        template: string;
    };
    namespace args {
        export let disable: boolean;
        let variant_1: string;
        export { variant_1 as variant };
        export let danger: boolean;
        export let loading: boolean;
        export let block: boolean;
        let text_1: string;
        export { text_1 as text };
    }
}
import SvnButton from "../components/button/SvnButton.vue";
