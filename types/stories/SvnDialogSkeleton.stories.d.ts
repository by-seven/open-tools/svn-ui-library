declare namespace _default {
    export let title: string;
    export { SvnDialogSkeleton as component };
    export let tags: string[];
    export namespace argTypes {
        namespace activatorVariant {
            let control: string;
            let options: string[];
        }
    }
    export namespace parameters {
        namespace docs {
            namespace description {
                let component: string;
            }
        }
    }
}
export default _default;
export namespace Playground {
    function render(args: any): {
        components: {
            SvnDialogSkeleton: {
                new (...args: any[]): import("vue").CreateComponentPublicInstanceWithMixins<Readonly<{}> & Readonly<{}>, {
                    dialog: import("vue").Ref<boolean, boolean>;
                    $emit: (event: "clickPrimaryButton" | "clickSecondaryButton", ...args: any[]) => void;
                    title: string;
                    icon: string;
                    iconSize: number;
                    persistent: boolean;
                    bind: boolean;
                    activatorText: string;
                    activatorVariant: string;
                    displayClose: boolean;
                    dialogHasTextArea: boolean;
                    withInputs: boolean;
                    closeIcon: string;
                    primaryButtonLoadingClass: string;
                    primaryButtonClass: string;
                    secondaryButtonClass: string;
                    subtitle: string;
                    description: string;
                    centerDescription: boolean;
                    primaryButtonDisabled: boolean;
                    primaryButtonLoading: boolean;
                    primaryButtonText: string;
                    secondaryButtonText: string;
                    loaderColor: string;
                    primaryButtonDanger: boolean;
                    secondaryButtonDanger: boolean;
                    secondaryButtonLoading: boolean;
                    primaryButtonCloseOnClick: boolean;
                    secondaryButtonCloseOnClick: boolean;
                    dialogCustomClass: string;
                    $props: {
                        readonly title?: string | undefined;
                        readonly icon?: string | undefined;
                        readonly iconSize?: number | undefined;
                        readonly persistent?: boolean | undefined;
                        readonly bind?: boolean | undefined;
                        readonly activatorText?: string | undefined;
                        readonly activatorVariant?: string | undefined;
                        readonly displayClose?: boolean | undefined;
                        readonly dialogHasTextArea?: boolean | undefined;
                        readonly withInputs?: boolean | undefined;
                        readonly closeIcon?: string | undefined;
                        readonly primaryButtonLoadingClass?: string | undefined;
                        readonly primaryButtonClass?: string | undefined;
                        readonly secondaryButtonClass?: string | undefined;
                        readonly subtitle?: string | undefined;
                        readonly description?: string | undefined;
                        readonly centerDescription?: boolean | undefined;
                        readonly primaryButtonDisabled?: boolean | undefined;
                        readonly primaryButtonLoading?: boolean | undefined;
                        readonly primaryButtonText?: string | undefined;
                        readonly secondaryButtonText?: string | undefined;
                        readonly loaderColor?: string | undefined;
                        readonly primaryButtonDanger?: boolean | undefined;
                        readonly secondaryButtonDanger?: boolean | undefined;
                        readonly secondaryButtonLoading?: boolean | undefined;
                        readonly primaryButtonCloseOnClick?: boolean | undefined;
                        readonly secondaryButtonCloseOnClick?: boolean | undefined;
                        readonly dialogCustomClass?: string | undefined;
                    };
                }, {}, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps & Readonly<{}> & Readonly<{}>, {}, true, {}, {}, import("vue").GlobalComponents, import("vue").GlobalDirectives, string, {}, any, import("vue").ComponentProvideOptions, {
                    P: {};
                    B: {};
                    D: {};
                    C: {};
                    M: {};
                    Defaults: {};
                }, Readonly<{}> & Readonly<{}>, {
                    dialog: import("vue").Ref<boolean, boolean>;
                    $emit: (event: "clickPrimaryButton" | "clickSecondaryButton", ...args: any[]) => void;
                    title: string;
                    icon: string;
                    iconSize: number;
                    persistent: boolean;
                    bind: boolean;
                    activatorText: string;
                    activatorVariant: string;
                    displayClose: boolean;
                    dialogHasTextArea: boolean;
                    withInputs: boolean;
                    closeIcon: string;
                    primaryButtonLoadingClass: string;
                    primaryButtonClass: string;
                    secondaryButtonClass: string;
                    subtitle: string;
                    description: string;
                    centerDescription: boolean;
                    primaryButtonDisabled: boolean;
                    primaryButtonLoading: boolean;
                    primaryButtonText: string;
                    secondaryButtonText: string;
                    loaderColor: string;
                    primaryButtonDanger: boolean;
                    secondaryButtonDanger: boolean;
                    secondaryButtonLoading: boolean;
                    primaryButtonCloseOnClick: boolean;
                    secondaryButtonCloseOnClick: boolean;
                    dialogCustomClass: string;
                    $props: {
                        readonly title?: string | undefined;
                        readonly icon?: string | undefined;
                        readonly iconSize?: number | undefined;
                        readonly persistent?: boolean | undefined;
                        readonly bind?: boolean | undefined;
                        readonly activatorText?: string | undefined;
                        readonly activatorVariant?: string | undefined;
                        readonly displayClose?: boolean | undefined;
                        readonly dialogHasTextArea?: boolean | undefined;
                        readonly withInputs?: boolean | undefined;
                        readonly closeIcon?: string | undefined;
                        readonly primaryButtonLoadingClass?: string | undefined;
                        readonly primaryButtonClass?: string | undefined;
                        readonly secondaryButtonClass?: string | undefined;
                        readonly subtitle?: string | undefined;
                        readonly description?: string | undefined;
                        readonly centerDescription?: boolean | undefined;
                        readonly primaryButtonDisabled?: boolean | undefined;
                        readonly primaryButtonLoading?: boolean | undefined;
                        readonly primaryButtonText?: string | undefined;
                        readonly secondaryButtonText?: string | undefined;
                        readonly loaderColor?: string | undefined;
                        readonly primaryButtonDanger?: boolean | undefined;
                        readonly secondaryButtonDanger?: boolean | undefined;
                        readonly secondaryButtonLoading?: boolean | undefined;
                        readonly primaryButtonCloseOnClick?: boolean | undefined;
                        readonly secondaryButtonCloseOnClick?: boolean | undefined;
                        readonly dialogCustomClass?: string | undefined;
                    };
                }, {}, {}, {}, {}>;
                __isFragment?: never;
                __isTeleport?: never;
                __isSuspense?: never;
            } & import("vue").ComponentOptionsBase<Readonly<{}> & Readonly<{}>, {
                dialog: import("vue").Ref<boolean, boolean>;
                $emit: (event: "clickPrimaryButton" | "clickSecondaryButton", ...args: any[]) => void;
                title: string;
                icon: string;
                iconSize: number;
                persistent: boolean;
                bind: boolean;
                activatorText: string;
                activatorVariant: string;
                displayClose: boolean;
                dialogHasTextArea: boolean;
                withInputs: boolean;
                closeIcon: string;
                primaryButtonLoadingClass: string;
                primaryButtonClass: string;
                secondaryButtonClass: string;
                subtitle: string;
                description: string;
                centerDescription: boolean;
                primaryButtonDisabled: boolean;
                primaryButtonLoading: boolean;
                primaryButtonText: string;
                secondaryButtonText: string;
                loaderColor: string;
                primaryButtonDanger: boolean;
                secondaryButtonDanger: boolean;
                secondaryButtonLoading: boolean;
                primaryButtonCloseOnClick: boolean;
                secondaryButtonCloseOnClick: boolean;
                dialogCustomClass: string;
                $props: {
                    readonly title?: string | undefined;
                    readonly icon?: string | undefined;
                    readonly iconSize?: number | undefined;
                    readonly persistent?: boolean | undefined;
                    readonly bind?: boolean | undefined;
                    readonly activatorText?: string | undefined;
                    readonly activatorVariant?: string | undefined;
                    readonly displayClose?: boolean | undefined;
                    readonly dialogHasTextArea?: boolean | undefined;
                    readonly withInputs?: boolean | undefined;
                    readonly closeIcon?: string | undefined;
                    readonly primaryButtonLoadingClass?: string | undefined;
                    readonly primaryButtonClass?: string | undefined;
                    readonly secondaryButtonClass?: string | undefined;
                    readonly subtitle?: string | undefined;
                    readonly description?: string | undefined;
                    readonly centerDescription?: boolean | undefined;
                    readonly primaryButtonDisabled?: boolean | undefined;
                    readonly primaryButtonLoading?: boolean | undefined;
                    readonly primaryButtonText?: string | undefined;
                    readonly secondaryButtonText?: string | undefined;
                    readonly loaderColor?: string | undefined;
                    readonly primaryButtonDanger?: boolean | undefined;
                    readonly secondaryButtonDanger?: boolean | undefined;
                    readonly secondaryButtonLoading?: boolean | undefined;
                    readonly primaryButtonCloseOnClick?: boolean | undefined;
                    readonly secondaryButtonCloseOnClick?: boolean | undefined;
                    readonly dialogCustomClass?: string | undefined;
                };
            }, {}, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, {}, {}, string, {}, import("vue").GlobalComponents, import("vue").GlobalDirectives, string, import("vue").ComponentProvideOptions> & import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps & (new () => {
                $slots: {
                    "activator-div"?(_: {}): any;
                    activator?(_: {}): any;
                    "activator-text"?(_: {}): any;
                    dialog?(_: {}): any;
                    close?(_: {
                        close: () => void;
                    }): any;
                    icon?(_: {}): any;
                    title?(_: {}): any;
                    "title-text"?(_: {}): any;
                    subtitle?(_: {}): any;
                    "subtitle-text"?(_: {}): any;
                    description?(_: {}): any;
                    "description-text"?(_: {}): any;
                    input?(_: {}): any;
                    body?(_: {}): any;
                    actionButtons?(_: {}): any;
                    "primary-button"?(_: {}): any;
                    "primary-button-text"?(_: {}): any;
                    "secondary-button"?(_: {}): any;
                    "secondary-button-text"?(_: {}): any;
                };
            });
        };
        setup(): {
            args: any;
        };
        template: string;
    };
    namespace args {
        export let primaryButtonText: string;
        export let secondaryButtonText: string;
        let title_1: string;
        export { title_1 as title };
        export let icon: string;
        export let subtitle: string;
        let description_1: string;
        export { description_1 as description };
        export let dialogCustomClass: string;
        export let closeIcon: string;
        export let activatorText: string;
        export let primaryButtonClass: string;
        export let secondaryButtonClass: string;
        export let primaryButtonDisabled: boolean;
        export let iconSize: number;
        export let primaryButtonDanger: boolean;
        export let secondaryButtonDanger: boolean;
        export let primaryButtonCloseOnClick: boolean;
        export let secondaryButtonCloseOnClick: boolean;
        export let withInputs: boolean;
    }
}
import SvnDialogSkeleton from "../components/skeleton/SvnDialogSkeleton.vue";
