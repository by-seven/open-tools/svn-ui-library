declare namespace _default {
    export let title: string;
    export { SvnDotsButton as component };
    export let tags: string[];
    export namespace argTypes {
        namespace disable {
            let control: string;
        }
        namespace table {
            let control_1: string;
            export { control_1 as control };
        }
        namespace variant {
            let control_2: string;
            export { control_2 as control };
            export let options: string[];
        }
        namespace buttonSize {
            let control_3: string;
            export { control_3 as control };
            let options_1: string[];
            export { options_1 as options };
        }
    }
    export namespace parameters {
        namespace docs {
            namespace description {
                let component: string;
            }
        }
    }
}
export default _default;
export namespace Playground {
    function render(args: any): {
        components: {
            SvnDotsButton: {
                new (...args: any[]): import("vue").CreateComponentPublicInstanceWithMixins<Readonly<{}> & Readonly<{}>, {
                    table: boolean;
                    circle: boolean;
                    variant: string;
                    icon: string;
                    buttonSize: string;
                    disable: boolean;
                    to: string;
                    danger: boolean;
                    colorClass: string;
                    show: boolean;
                    $props: {
                        readonly table?: boolean | undefined;
                        readonly circle?: boolean | undefined;
                        readonly variant?: string | undefined;
                        readonly icon?: string | undefined;
                        readonly buttonSize?: string | undefined;
                        readonly disable?: boolean | undefined;
                        readonly to?: string | undefined;
                        readonly danger?: boolean | undefined;
                        readonly colorClass?: string | undefined;
                        readonly show?: boolean | undefined;
                    };
                }, {}, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps & Readonly<{}> & Readonly<{}>, {}, true, {}, {}, import("vue").GlobalComponents, import("vue").GlobalDirectives, string, {}, any, import("vue").ComponentProvideOptions, {
                    P: {};
                    B: {};
                    D: {};
                    C: {};
                    M: {};
                    Defaults: {};
                }, Readonly<{}> & Readonly<{}>, {
                    table: boolean;
                    circle: boolean;
                    variant: string;
                    icon: string;
                    buttonSize: string;
                    disable: boolean;
                    to: string;
                    danger: boolean;
                    colorClass: string;
                    show: boolean;
                    $props: {
                        readonly table?: boolean | undefined;
                        readonly circle?: boolean | undefined;
                        readonly variant?: string | undefined;
                        readonly icon?: string | undefined;
                        readonly buttonSize?: string | undefined;
                        readonly disable?: boolean | undefined;
                        readonly to?: string | undefined;
                        readonly danger?: boolean | undefined;
                        readonly colorClass?: string | undefined;
                        readonly show?: boolean | undefined;
                    };
                }, {}, {}, {}, {}>;
                __isFragment?: never;
                __isTeleport?: never;
                __isSuspense?: never;
            } & import("vue").ComponentOptionsBase<Readonly<{}> & Readonly<{}>, {
                table: boolean;
                circle: boolean;
                variant: string;
                icon: string;
                buttonSize: string;
                disable: boolean;
                to: string;
                danger: boolean;
                colorClass: string;
                show: boolean;
                $props: {
                    readonly table?: boolean | undefined;
                    readonly circle?: boolean | undefined;
                    readonly variant?: string | undefined;
                    readonly icon?: string | undefined;
                    readonly buttonSize?: string | undefined;
                    readonly disable?: boolean | undefined;
                    readonly to?: string | undefined;
                    readonly danger?: boolean | undefined;
                    readonly colorClass?: string | undefined;
                    readonly show?: boolean | undefined;
                };
            }, {}, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, {}, {}, string, {}, import("vue").GlobalComponents, import("vue").GlobalDirectives, string, import("vue").ComponentProvideOptions> & import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps & (new () => {
                $slots: {
                    before?(_: {}): any;
                    items?(_: {}): any;
                    after?(_: {}): any;
                };
            });
        };
        setup(): {
            args: any;
        };
        template: string;
    };
    namespace args {
        let disable_1: boolean;
        export { disable_1 as disable };
        let variant_1: string;
        export { variant_1 as variant };
        export let icon: string;
        let buttonSize_1: string;
        export { buttonSize_1 as buttonSize };
        export let to: string;
        export let danger: boolean;
        export let circle: boolean;
        let table_1: boolean;
        export { table_1 as table };
        export let colorClass: string;
    }
}
import SvnDotsButton from "../components/SvnDotsButton.vue";
