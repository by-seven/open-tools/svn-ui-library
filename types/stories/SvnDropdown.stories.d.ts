declare namespace _default {
    export let title: string;
    export { SvnDropdown as component };
    export let tags: string[];
    export namespace parameters {
        namespace docs {
            namespace description {
                let component: string;
            }
        }
    }
}
export default _default;
export namespace Playground {
    function render(args: any): {
        components: {
            SvnDropdown: {
                new (...args: any[]): import("vue").CreateComponentPublicInstanceWithMixins<Readonly<{}> & Readonly<{}>, {
                    offset: number;
                    items: unknown[];
                    closeOnContentCLick: boolean;
                    $props: {
                        readonly offset?: number | undefined;
                        readonly items?: unknown[] | undefined;
                        readonly closeOnContentCLick?: boolean | undefined;
                    };
                }, {}, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps & Readonly<{}> & Readonly<{}>, {}, true, {}, {}, import("vue").GlobalComponents, import("vue").GlobalDirectives, string, {}, any, import("vue").ComponentProvideOptions, {
                    P: {};
                    B: {};
                    D: {};
                    C: {};
                    M: {};
                    Defaults: {};
                }, Readonly<{}> & Readonly<{}>, {
                    offset: number;
                    items: unknown[];
                    closeOnContentCLick: boolean;
                    $props: {
                        readonly offset?: number | undefined;
                        readonly items?: unknown[] | undefined;
                        readonly closeOnContentCLick?: boolean | undefined;
                    };
                }, {}, {}, {}, {}>;
                __isFragment?: never;
                __isTeleport?: never;
                __isSuspense?: never;
            } & import("vue").ComponentOptionsBase<Readonly<{}> & Readonly<{}>, {
                offset: number;
                items: unknown[];
                closeOnContentCLick: boolean;
                $props: {
                    readonly offset?: number | undefined;
                    readonly items?: unknown[] | undefined;
                    readonly closeOnContentCLick?: boolean | undefined;
                };
            }, {}, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, {}, {}, string, {}, import("vue").GlobalComponents, import("vue").GlobalDirectives, string, import("vue").ComponentProvideOptions> & import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps & (new () => {
                $slots: {
                    activator?(_: {}): any;
                    dropdown?(_: {}): any;
                };
            });
        };
        setup(): {
            args: any;
        };
        template: string;
    };
    namespace args {
        export { items };
        export let closeOnContentCLick: boolean;
    }
}
import SvnDropdown from "../components/button/SvnDropdown.vue";
declare const items: {
    id: number;
    title: string;
}[];
