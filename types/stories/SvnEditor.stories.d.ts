declare namespace _default {
    export let title: string;
    export { SvnEditor as component };
    export let tags: string[];
    export namespace parameters {
        namespace docs {
            namespace description {
                let component: string;
            }
        }
    }
}
export default _default;
export namespace Playground {
    function render(args: any): {
        components: {
            SvnEditor: import("vue").DefineComponent<{}, {
                $emit: (event: "on-save", ...args: any[]) => void;
                readOnly: boolean;
                aleph: boolean;
                htmlData: string;
                editorId: string;
                createImageUrl: string;
                fullWidth: boolean;
                data?: Record<string, any> | undefined;
                $props: {
                    readonly readOnly?: boolean | undefined;
                    readonly aleph?: boolean | undefined;
                    readonly htmlData?: string | undefined;
                    readonly editorId?: string | undefined;
                    readonly createImageUrl?: string | undefined;
                    readonly fullWidth?: boolean | undefined;
                    readonly data?: Record<string, any> | undefined;
                };
            }, {}, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").PublicProps, Readonly<{}> & Readonly<{}>, {}, {}, {}, {}, string, import("vue").ComponentProvideOptions, true, {}, any>;
        };
        setup(): {
            args: any;
        };
        template: string;
    };
    namespace args {
        let readOnly: boolean;
        let editorId: string;
        let createImageUrl: string;
        let aleph: boolean;
    }
}
import SvnEditor from "../components/editor/SvnEditor.vue";
