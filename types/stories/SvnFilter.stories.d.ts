declare namespace _default {
    export let title: string;
    export { SvnFilter as component };
    export let tags: string[];
    export namespace argTypes {
        namespace activatorVariant {
            let control: string;
            let options: string[];
        }
    }
    export namespace parameters {
        namespace docs {
            namespace description {
                let component: string;
            }
        }
    }
}
export default _default;
export namespace Playground {
    function render(args: any): {
        components: {
            SvnFilter: import("vue").DefineComponent<{}, {
                activatorText: string;
                activatorVariant: string;
                activatorIcon: string;
                activatorColor: string;
                activatorHeight: number;
                mockData: unknown[];
                $props: {
                    readonly activatorText?: string | undefined;
                    readonly activatorVariant?: string | undefined;
                    readonly activatorIcon?: string | undefined;
                    readonly activatorColor?: string | undefined;
                    readonly activatorHeight?: number | undefined;
                    readonly mockData?: unknown[] | undefined;
                };
            }, {}, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").PublicProps, Readonly<{}> & Readonly<{}>, {}, {}, {}, {}, string, import("vue").ComponentProvideOptions, true, {}, any>;
        };
        setup(): {
            args: any;
        };
        template: string;
    };
    namespace args {
        export { data as mockData };
        export let activatorText: string;
        export let activatorIcon: string;
        let activatorVariant_1: string;
        export { activatorVariant_1 as activatorVariant };
        export let activatorColor: string;
        export let activatorHeight: number;
    }
}
import SvnFilter from "../views/Components/Filter.vue";
declare const data: {
    id: number;
    campaign_draft_id: number;
    campaign_type: string;
    can_set_copilot: boolean;
    categories: {
        id: number;
        title: string;
    }[];
    completion: number;
    deadline: string;
    deleted_at: null;
    employees_count: number;
    startline: string;
    status: string;
    title: string;
}[];
