declare namespace _default {
    export let title: string;
    export { SvnFloatingActionButton as component };
    export let tags: string[];
    export namespace parameters {
        namespace docs {
            namespace description {
                let component: string;
            }
        }
    }
}
export default _default;
export namespace Playground {
    function render(args: any): {
        components: {
            SvnFloatingActionButton: {
                new (...args: any[]): import("vue").CreateComponentPublicInstanceWithMixins<Readonly<{}> & Readonly<{}>, {
                    dialog: import("vue").Ref<boolean, boolean>;
                    default: boolean;
                    menu: boolean;
                    text: string;
                    icon: string;
                    square: boolean;
                    round: boolean;
                    mini: boolean;
                    iconCustomClass: string;
                    scrimColor: string;
                    loading: boolean;
                    doubleIcon: boolean;
                    pageRatioDisplay: number;
                    toTop: boolean;
                    $props: {
                        readonly default?: boolean | undefined;
                        readonly menu?: boolean | undefined;
                        readonly text?: string | undefined;
                        readonly icon?: string | undefined;
                        readonly square?: boolean | undefined;
                        readonly round?: boolean | undefined;
                        readonly mini?: boolean | undefined;
                        readonly iconCustomClass?: string | undefined;
                        readonly scrimColor?: string | undefined;
                        readonly loading?: boolean | undefined;
                        readonly doubleIcon?: boolean | undefined;
                        readonly pageRatioDisplay?: number | undefined;
                        readonly toTop?: boolean | undefined;
                    };
                }, {}, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps & Readonly<{}> & Readonly<{}>, {}, true, {}, {}, import("vue").GlobalComponents, import("vue").GlobalDirectives, string, {}, any, import("vue").ComponentProvideOptions, {
                    P: {};
                    B: {};
                    D: {};
                    C: {};
                    M: {};
                    Defaults: {};
                }, Readonly<{}> & Readonly<{}>, {
                    dialog: import("vue").Ref<boolean, boolean>;
                    default: boolean;
                    menu: boolean;
                    text: string;
                    icon: string;
                    square: boolean;
                    round: boolean;
                    mini: boolean;
                    iconCustomClass: string;
                    scrimColor: string;
                    loading: boolean;
                    doubleIcon: boolean;
                    pageRatioDisplay: number;
                    toTop: boolean;
                    $props: {
                        readonly default?: boolean | undefined;
                        readonly menu?: boolean | undefined;
                        readonly text?: string | undefined;
                        readonly icon?: string | undefined;
                        readonly square?: boolean | undefined;
                        readonly round?: boolean | undefined;
                        readonly mini?: boolean | undefined;
                        readonly iconCustomClass?: string | undefined;
                        readonly scrimColor?: string | undefined;
                        readonly loading?: boolean | undefined;
                        readonly doubleIcon?: boolean | undefined;
                        readonly pageRatioDisplay?: number | undefined;
                        readonly toTop?: boolean | undefined;
                    };
                }, {}, {}, {}, {}>;
                __isFragment?: never;
                __isTeleport?: never;
                __isSuspense?: never;
            } & import("vue").ComponentOptionsBase<Readonly<{}> & Readonly<{}>, {
                dialog: import("vue").Ref<boolean, boolean>;
                default: boolean;
                menu: boolean;
                text: string;
                icon: string;
                square: boolean;
                round: boolean;
                mini: boolean;
                iconCustomClass: string;
                scrimColor: string;
                loading: boolean;
                doubleIcon: boolean;
                pageRatioDisplay: number;
                toTop: boolean;
                $props: {
                    readonly default?: boolean | undefined;
                    readonly menu?: boolean | undefined;
                    readonly text?: string | undefined;
                    readonly icon?: string | undefined;
                    readonly square?: boolean | undefined;
                    readonly round?: boolean | undefined;
                    readonly mini?: boolean | undefined;
                    readonly iconCustomClass?: string | undefined;
                    readonly scrimColor?: string | undefined;
                    readonly loading?: boolean | undefined;
                    readonly doubleIcon?: boolean | undefined;
                    readonly pageRatioDisplay?: number | undefined;
                    readonly toTop?: boolean | undefined;
                };
            }, {}, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, {}, {}, string, {}, import("vue").GlobalComponents, import("vue").GlobalDirectives, string, import("vue").ComponentProvideOptions> & import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps & (new () => {
                $slots: {
                    default?(_: {}): any;
                    loading?(_: {}): any;
                };
            });
        };
        setup(): {
            args: any;
        };
        template: string;
    };
    namespace args {
        let _default: boolean;
        export { _default as default };
        export let square: boolean;
        export let round: boolean;
        export let mini: boolean;
        export let menu: boolean;
        export let text: string;
        export let icon: string;
    }
}
import SvnFloatingActionButton from "../components/SvnFloatingActionButton.vue";
