declare namespace _default {
    export let title: string;
    export { SvnProHeaderSection as component };
    export let tags: string[];
    export namespace argTypes {
        export namespace title_1 {
            let control: string;
        }
        export { title_1 as title };
        export namespace subtitle {
            let control_1: string;
            export { control_1 as control };
        }
        export namespace size {
            let control_2: string;
            export { control_2 as control };
            export let options: string[];
        }
        export namespace emoji {
            let control_3: string;
            export { control_3 as control };
        }
        export namespace interviewsSubmitted {
            let control_4: string;
            export { control_4 as control };
        }
        export namespace modulesAcquired {
            let control_5: string;
            export { control_5 as control };
        }
        export namespace currentTargets {
            let control_6: string;
            export { control_6 as control };
        }
        export namespace interview {
            let control_7: string;
            export { control_7 as control };
        }
        export namespace training {
            let control_8: string;
            export { control_8 as control };
        }
        export namespace roadmap {
            let control_9: string;
            export { control_9 as control };
        }
    }
    export namespace parameters {
        namespace docs {
            namespace description {
                let component: string;
            }
        }
    }
}
export default _default;
export namespace Playground {
    function render(args: any): {
        components: {
            SvnProHeaderSection: import("vue").DefineComponent<{}, {
                interview: boolean;
                size: string;
                roadmap: boolean;
                training: boolean;
                title?: string | undefined;
                subtitle?: string | undefined;
                interviewsSubmitted?: string | undefined;
                modulesAcquired?: string | undefined;
                currentTargets?: string | undefined;
                emoji?: string | undefined;
                $props: {
                    readonly interview?: boolean | undefined;
                    readonly size?: string | undefined;
                    readonly roadmap?: boolean | undefined;
                    readonly training?: boolean | undefined;
                    readonly title?: string | undefined;
                    readonly subtitle?: string | undefined;
                    readonly interviewsSubmitted?: string | undefined;
                    readonly modulesAcquired?: string | undefined;
                    readonly currentTargets?: string | undefined;
                    readonly emoji?: string | undefined;
                };
            }, {}, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").PublicProps, Readonly<{}> & Readonly<{}>, {}, {}, {}, {}, string, import("vue").ComponentProvideOptions, true, {}, any>;
        };
        setup(): {
            args: any;
        };
        template: string;
    };
    namespace args {
        let title_2: string;
        export { title_2 as title };
        let subtitle_1: string;
        export { subtitle_1 as subtitle };
        let size_1: string;
        export { size_1 as size };
        let emoji_1: string;
        export { emoji_1 as emoji };
        let interview_1: boolean;
        export { interview_1 as interview };
        let training_1: boolean;
        export { training_1 as training };
        let roadmap_1: boolean;
        export { roadmap_1 as roadmap };
        let interviewsSubmitted_1: string;
        export { interviewsSubmitted_1 as interviewsSubmitted };
        let modulesAcquired_1: string;
        export { modulesAcquired_1 as modulesAcquired };
        let currentTargets_1: string;
        export { currentTargets_1 as currentTargets };
    }
}
import SvnProHeaderSection from "../components/card/SvnProHeaderSection.vue";
