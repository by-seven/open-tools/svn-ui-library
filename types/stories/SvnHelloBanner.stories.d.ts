declare namespace _default {
    export let title: string;
    export { SvnHelloBanner as component };
    export let tags: string[];
    export namespace argTypes {
        namespace app {
            let control: string;
            let options: string[];
        }
    }
    export namespace parameters {
        namespace docs {
            namespace description {
                let component: string;
            }
        }
    }
}
export default _default;
export namespace Playground {
    function render(args: any): {
        components: {
            SvnHelloBanner: import("vue").DefineComponent<{}, {
                subtitle: string;
                firstname: string;
                app: string;
                url: string;
                applications: unknown[];
                interviewsSubmitted: number;
                completedTrainings: number;
                modulesAcquired: number;
                completedTargets: number;
                currentTargets: number;
                textChipFirst: string;
                textChipSecond: string;
                textChipThird: string;
                $props: {
                    readonly subtitle?: string | undefined;
                    readonly firstname?: string | undefined;
                    readonly app?: string | undefined;
                    readonly url?: string | undefined;
                    readonly applications?: unknown[] | undefined;
                    readonly interviewsSubmitted?: number | undefined;
                    readonly completedTrainings?: number | undefined;
                    readonly modulesAcquired?: number | undefined;
                    readonly completedTargets?: number | undefined;
                    readonly currentTargets?: number | undefined;
                    readonly textChipFirst?: string | undefined;
                    readonly textChipSecond?: string | undefined;
                    readonly textChipThird?: string | undefined;
                };
            }, {}, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").PublicProps, Readonly<{}> & Readonly<{}>, {}, {}, {}, {}, string, import("vue").ComponentProvideOptions, true, {}, any>;
        };
        setup(): {
            args: any;
        };
        template: string;
    };
    namespace args {
        let app_1: string;
        export { app_1 as app };
        export let url: string;
        export let firstname: string;
        export let subtitle: string;
        export let completedTrainings: number;
        export let modulesAcquired: number;
        export let completedTargets: number;
        export let currentTargets: number;
        export let textChipFirst: string;
        export let textChipSecond: string;
    }
}
import SvnHelloBanner from "../components/banner/SvnHelloBanner.vue";
