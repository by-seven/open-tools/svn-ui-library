declare namespace _default {
    export let title: string;
    export { SvnIconButton as component };
    export let tags: string[];
    export namespace argTypes {
        namespace variant {
            let control: string;
            let options: string[];
        }
        namespace buttonSize {
            let control_1: string;
            export { control_1 as control };
            let options_1: string[];
            export { options_1 as options };
        }
    }
    export namespace parameters {
        namespace docs {
            namespace description {
                let component: string;
            }
        }
    }
}
export default _default;
export namespace Playground {
    function render(args: any): {
        components: {
            SvnIconButton: {
                new (...args: any[]): import("vue").CreateComponentPublicInstanceWithMixins<Readonly<{}> & Readonly<{}>, {
                    circle: boolean;
                    variant: string;
                    icon: string;
                    buttonSize: string;
                    disable: boolean;
                    to: string;
                    danger: boolean;
                    colorClass: string;
                    $props: {
                        readonly circle?: boolean | undefined;
                        readonly variant?: string | undefined;
                        readonly icon?: string | undefined;
                        readonly buttonSize?: string | undefined;
                        readonly disable?: boolean | undefined;
                        readonly to?: string | undefined;
                        readonly danger?: boolean | undefined;
                        readonly colorClass?: string | undefined;
                    };
                }, {}, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps & Readonly<{}> & Readonly<{}>, {}, true, {}, {}, import("vue").GlobalComponents, import("vue").GlobalDirectives, string, {}, any, import("vue").ComponentProvideOptions, {
                    P: {};
                    B: {};
                    D: {};
                    C: {};
                    M: {};
                    Defaults: {};
                }, Readonly<{}> & Readonly<{}>, {
                    circle: boolean;
                    variant: string;
                    icon: string;
                    buttonSize: string;
                    disable: boolean;
                    to: string;
                    danger: boolean;
                    colorClass: string;
                    $props: {
                        readonly circle?: boolean | undefined;
                        readonly variant?: string | undefined;
                        readonly icon?: string | undefined;
                        readonly buttonSize?: string | undefined;
                        readonly disable?: boolean | undefined;
                        readonly to?: string | undefined;
                        readonly danger?: boolean | undefined;
                        readonly colorClass?: string | undefined;
                    };
                }, {}, {}, {}, {}>;
                __isFragment?: never;
                __isTeleport?: never;
                __isSuspense?: never;
            } & import("vue").ComponentOptionsBase<Readonly<{}> & Readonly<{}>, {
                circle: boolean;
                variant: string;
                icon: string;
                buttonSize: string;
                disable: boolean;
                to: string;
                danger: boolean;
                colorClass: string;
                $props: {
                    readonly circle?: boolean | undefined;
                    readonly variant?: string | undefined;
                    readonly icon?: string | undefined;
                    readonly buttonSize?: string | undefined;
                    readonly disable?: boolean | undefined;
                    readonly to?: string | undefined;
                    readonly danger?: boolean | undefined;
                    readonly colorClass?: string | undefined;
                };
            }, {}, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, {}, {}, string, {}, import("vue").GlobalComponents, import("vue").GlobalDirectives, string, import("vue").ComponentProvideOptions> & import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps & (new () => {
                $slots: {
                    default?(_: {}): any;
                };
            });
        };
        setup(): {
            args: any;
        };
        template: string;
    };
    namespace args {
        export let disable: boolean;
        let variant_1: string;
        export { variant_1 as variant };
        export let icon: string;
        let buttonSize_1: string;
        export { buttonSize_1 as buttonSize };
        export let danger: boolean;
        export let circle: boolean;
    }
}
import SvnIconButton from "../components/button/SvnIconButton.vue";
