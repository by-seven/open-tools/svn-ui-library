declare namespace _default {
    export let title: string;
    export { SvnIndexSkeleton as component };
    export let tags: string[];
    export namespace argTypes {
        namespace entity {
            let control: string;
            let options: string[];
        }
    }
    export namespace parameters {
        namespace docs {
            namespace description {
                let component: string;
            }
        }
    }
}
export default _default;
export namespace Playground {
    function render(args: any): {
        components: {
            SvnIndexSkeleton: {
                new (...args: any[]): import("vue").CreateComponentPublicInstanceWithMixins<Readonly<{}> & Readonly<{}>, {
                    shadow: boolean;
                    entity: string;
                    indexTitle: string;
                    indexSubtitle: string;
                    indexIcon: string;
                    withOptions: boolean;
                    titleWrapperClass: string;
                    $props: {
                        readonly shadow?: boolean | undefined;
                        readonly entity?: string | undefined;
                        readonly indexTitle?: string | undefined;
                        readonly indexSubtitle?: string | undefined;
                        readonly indexIcon?: string | undefined;
                        readonly withOptions?: boolean | undefined;
                        readonly titleWrapperClass?: string | undefined;
                    };
                }, {}, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps & Readonly<{}> & Readonly<{}>, {}, true, {}, {}, import("vue").GlobalComponents, import("vue").GlobalDirectives, string, {}, any, import("vue").ComponentProvideOptions, {
                    P: {};
                    B: {};
                    D: {};
                    C: {};
                    M: {};
                    Defaults: {};
                }, Readonly<{}> & Readonly<{}>, {
                    shadow: boolean;
                    entity: string;
                    indexTitle: string;
                    indexSubtitle: string;
                    indexIcon: string;
                    withOptions: boolean;
                    titleWrapperClass: string;
                    $props: {
                        readonly shadow?: boolean | undefined;
                        readonly entity?: string | undefined;
                        readonly indexTitle?: string | undefined;
                        readonly indexSubtitle?: string | undefined;
                        readonly indexIcon?: string | undefined;
                        readonly withOptions?: boolean | undefined;
                        readonly titleWrapperClass?: string | undefined;
                    };
                }, {}, {}, {}, {}>;
                __isFragment?: never;
                __isTeleport?: never;
                __isSuspense?: never;
            } & import("vue").ComponentOptionsBase<Readonly<{}> & Readonly<{}>, {
                shadow: boolean;
                entity: string;
                indexTitle: string;
                indexSubtitle: string;
                indexIcon: string;
                withOptions: boolean;
                titleWrapperClass: string;
                $props: {
                    readonly shadow?: boolean | undefined;
                    readonly entity?: string | undefined;
                    readonly indexTitle?: string | undefined;
                    readonly indexSubtitle?: string | undefined;
                    readonly indexIcon?: string | undefined;
                    readonly withOptions?: boolean | undefined;
                    readonly titleWrapperClass?: string | undefined;
                };
            }, {}, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, {}, {}, string, {}, import("vue").GlobalComponents, import("vue").GlobalDirectives, string, import("vue").ComponentProvideOptions> & import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps & (new () => {
                $slots: {
                    "title-content"?(_: {}): any;
                    "title-content"?(_: {}): any;
                    "title-content"?(_: {}): any;
                    "title-content"?(_: {}): any;
                    "title-content"?(_: {}): any;
                    "create-index"?(_: {}): any;
                    "create-index"?(_: {}): any;
                    "create-index"?(_: {}): any;
                    "create-index"?(_: {}): any;
                    "create-index"?(_: {}): any;
                    inputs?(_: {}): any;
                    inputs?(_: {}): any;
                    inputs?(_: {}): any;
                    inputs?(_: {}): any;
                    inputs?(_: {}): any;
                    inputs?(_: {}): any;
                    body?(_: {}): any;
                    body?(_: {}): any;
                    body?(_: {}): any;
                    body?(_: {}): any;
                    body?(_: {}): any;
                    tabs?(_: {}): any;
                    tabs?(_: {}): any;
                    tabs?(_: {}): any;
                };
            });
        };
        setup(): {
            args: any;
        };
        template: string;
    };
    namespace args {
        export let withOptions: boolean;
        export let tableShowSelect: boolean;
        let entity_1: string;
        export { entity_1 as entity };
        export let indexTitle: string;
        export let indexSubtitle: string;
        export let indexIcon: string;
        export { headers as tableHeaders };
        export { items as tableItems };
    }
}
import SvnIndexSkeleton from "../components/skeleton/SvnIndexSkeleton.vue";
declare const headers: {
    align: string;
    key: string;
    sortable: boolean;
    title: string;
}[];
declare const items: {
    id: number;
    access_level: string;
    avatar: string;
    email: string;
    firstname: string;
    fullname: string;
    hire_date: string;
    job_title: null;
    lastname: string;
}[];
