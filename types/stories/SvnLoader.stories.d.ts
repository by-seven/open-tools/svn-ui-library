declare namespace _default {
    export let title: string;
    export { SvnLoader as component };
    export let tags: string[];
    export namespace argTypes {
        namespace loadingSize {
            let control: string;
            let options: string[];
        }
    }
    export namespace parameters {
        namespace docs {
            namespace description {
                let component: string;
            }
        }
    }
}
export default _default;
export namespace Playground {
    function render(args: any): {
        components: {
            SvnLoader: {
                new (...args: any[]): import("vue").CreateComponentPublicInstanceWithMixins<Readonly<{}> & Readonly<{}>, {
                    $emit: (event: "update:modelValue", ...args: any[]) => void;
                    color: string;
                    width: string;
                    modelValue: string;
                    size: string;
                    bgColor: string;
                    indeterminate: boolean;
                    loadingSize: string;
                    $props: {
                        readonly color?: string | undefined;
                        readonly width?: string | undefined;
                        readonly modelValue?: string | undefined;
                        readonly size?: string | undefined;
                        readonly bgColor?: string | undefined;
                        readonly indeterminate?: boolean | undefined;
                        readonly loadingSize?: string | undefined;
                    };
                }, {}, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps & Readonly<{}> & Readonly<{}>, {}, true, {}, {}, import("vue").GlobalComponents, import("vue").GlobalDirectives, string, {}, any, import("vue").ComponentProvideOptions, {
                    P: {};
                    B: {};
                    D: {};
                    C: {};
                    M: {};
                    Defaults: {};
                }, Readonly<{}> & Readonly<{}>, {
                    $emit: (event: "update:modelValue", ...args: any[]) => void;
                    color: string;
                    width: string;
                    modelValue: string;
                    size: string;
                    bgColor: string;
                    indeterminate: boolean;
                    loadingSize: string;
                    $props: {
                        readonly color?: string | undefined;
                        readonly width?: string | undefined;
                        readonly modelValue?: string | undefined;
                        readonly size?: string | undefined;
                        readonly bgColor?: string | undefined;
                        readonly indeterminate?: boolean | undefined;
                        readonly loadingSize?: string | undefined;
                    };
                }, {}, {}, {}, {}>;
                __isFragment?: never;
                __isTeleport?: never;
                __isSuspense?: never;
            } & import("vue").ComponentOptionsBase<Readonly<{}> & Readonly<{}>, {
                $emit: (event: "update:modelValue", ...args: any[]) => void;
                color: string;
                width: string;
                modelValue: string;
                size: string;
                bgColor: string;
                indeterminate: boolean;
                loadingSize: string;
                $props: {
                    readonly color?: string | undefined;
                    readonly width?: string | undefined;
                    readonly modelValue?: string | undefined;
                    readonly size?: string | undefined;
                    readonly bgColor?: string | undefined;
                    readonly indeterminate?: boolean | undefined;
                    readonly loadingSize?: string | undefined;
                };
            }, {}, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, {}, {}, string, {}, import("vue").GlobalComponents, import("vue").GlobalDirectives, string, import("vue").ComponentProvideOptions> & import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps & (new () => {
                $slots: {
                    default?(_: {}): any;
                };
            });
        };
        setup(): {
            args: any;
        };
        template: string;
    };
}
import SvnLoader from "../components/animation/SvnLoader.vue";
