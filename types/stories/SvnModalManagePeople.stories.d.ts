declare namespace _default {
    export let title: string;
    export { SvnModalManagePeople as component };
    export let tags: string[];
    export namespace parameters {
        namespace docs {
            namespace description {
                let component: string;
            }
        }
    }
}
export default _default;
export namespace Playground {
    function render(args: any): {
        components: {
            SvnModalManagePeople: {
                new (...args: any[]): import("vue").CreateComponentPublicInstanceWithMixins<Readonly<{}> & Readonly<{}>, {
                    $emit: (event: "add-user" | "remove-user" | "custom-search", ...args: any[]) => void;
                    title: string;
                    persistent: boolean;
                    activatorText: string;
                    activatorVariant: string;
                    vertical: boolean;
                    modalTitleCenter: boolean;
                    verticalHeader: boolean;
                    prependHeaderIcon: string;
                    appendHeaderIcon: string;
                    appendIconCloseModal: boolean;
                    headerCustomClass: string;
                    modalCustomClass: string;
                    searchUsers: unknown[];
                    listUsers: unknown[];
                    $props: {
                        readonly title?: string | undefined;
                        readonly persistent?: boolean | undefined;
                        readonly activatorText?: string | undefined;
                        readonly activatorVariant?: string | undefined;
                        readonly vertical?: boolean | undefined;
                        readonly modalTitleCenter?: boolean | undefined;
                        readonly verticalHeader?: boolean | undefined;
                        readonly prependHeaderIcon?: string | undefined;
                        readonly appendHeaderIcon?: string | undefined;
                        readonly appendIconCloseModal?: boolean | undefined;
                        readonly headerCustomClass?: string | undefined;
                        readonly modalCustomClass?: string | undefined;
                        readonly searchUsers?: unknown[] | undefined;
                        readonly listUsers?: unknown[] | undefined;
                    };
                }, {}, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps & Readonly<{}> & Readonly<{}>, {}, true, {}, {}, import("vue").GlobalComponents, import("vue").GlobalDirectives, string, {}, any, import("vue").ComponentProvideOptions, {
                    P: {};
                    B: {};
                    D: {};
                    C: {};
                    M: {};
                    Defaults: {};
                }, Readonly<{}> & Readonly<{}>, {
                    $emit: (event: "add-user" | "remove-user" | "custom-search", ...args: any[]) => void;
                    title: string;
                    persistent: boolean;
                    activatorText: string;
                    activatorVariant: string;
                    vertical: boolean;
                    modalTitleCenter: boolean;
                    verticalHeader: boolean;
                    prependHeaderIcon: string;
                    appendHeaderIcon: string;
                    appendIconCloseModal: boolean;
                    headerCustomClass: string;
                    modalCustomClass: string;
                    searchUsers: unknown[];
                    listUsers: unknown[];
                    $props: {
                        readonly title?: string | undefined;
                        readonly persistent?: boolean | undefined;
                        readonly activatorText?: string | undefined;
                        readonly activatorVariant?: string | undefined;
                        readonly vertical?: boolean | undefined;
                        readonly modalTitleCenter?: boolean | undefined;
                        readonly verticalHeader?: boolean | undefined;
                        readonly prependHeaderIcon?: string | undefined;
                        readonly appendHeaderIcon?: string | undefined;
                        readonly appendIconCloseModal?: boolean | undefined;
                        readonly headerCustomClass?: string | undefined;
                        readonly modalCustomClass?: string | undefined;
                        readonly searchUsers?: unknown[] | undefined;
                        readonly listUsers?: unknown[] | undefined;
                    };
                }, {}, {}, {}, {}>;
                __isFragment?: never;
                __isTeleport?: never;
                __isSuspense?: never;
            } & import("vue").ComponentOptionsBase<Readonly<{}> & Readonly<{}>, {
                $emit: (event: "add-user" | "remove-user" | "custom-search", ...args: any[]) => void;
                title: string;
                persistent: boolean;
                activatorText: string;
                activatorVariant: string;
                vertical: boolean;
                modalTitleCenter: boolean;
                verticalHeader: boolean;
                prependHeaderIcon: string;
                appendHeaderIcon: string;
                appendIconCloseModal: boolean;
                headerCustomClass: string;
                modalCustomClass: string;
                searchUsers: unknown[];
                listUsers: unknown[];
                $props: {
                    readonly title?: string | undefined;
                    readonly persistent?: boolean | undefined;
                    readonly activatorText?: string | undefined;
                    readonly activatorVariant?: string | undefined;
                    readonly vertical?: boolean | undefined;
                    readonly modalTitleCenter?: boolean | undefined;
                    readonly verticalHeader?: boolean | undefined;
                    readonly prependHeaderIcon?: string | undefined;
                    readonly appendHeaderIcon?: string | undefined;
                    readonly appendIconCloseModal?: boolean | undefined;
                    readonly headerCustomClass?: string | undefined;
                    readonly modalCustomClass?: string | undefined;
                    readonly searchUsers?: unknown[] | undefined;
                    readonly listUsers?: unknown[] | undefined;
                };
            }, {}, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, {}, {}, string, {}, import("vue").GlobalComponents, import("vue").GlobalDirectives, string, import("vue").ComponentProvideOptions> & import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps & (new () => {
                $slots: {
                    activator?(_: {}): any;
                };
            });
        };
        setup(): {
            args: any;
        };
        template: string;
    };
    namespace args {
        let title_1: string;
        export { title_1 as title };
        export { mockUsers as searchUsers };
        export let persistent: boolean;
        export let modalTitleCenter: boolean;
        export let vertical: boolean;
        export let vertialHeader: boolean;
        export let appendIconCloseModal: boolean;
        export let activatorText: string;
        export let prependHeaderIcon: string;
        export let appendHeaderIcon: string;
        export let headerCustomClass: string;
        export let modalCustomClass: string;
        export let activatorVariant: string;
    }
}
import SvnModalManagePeople from "../components/dialog/SvnModalManagePeople.vue";
declare const mockUsers: {
    id: number;
    avatar: null;
    firstname: string;
    lastname: string;
    fullname: string;
}[];
