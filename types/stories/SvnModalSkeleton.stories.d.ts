declare namespace _default {
    export let title: string;
    export { SvnModalSkeleton as component };
    export let tags: string[];
    export namespace argTypes {
        namespace activatorVariant {
            let control: string;
            let options: string[];
        }
    }
    export namespace parameters {
        namespace docs {
            namespace description {
                let component: string;
            }
        }
    }
}
export default _default;
export namespace Playground {
    function render(args: any): {
        components: {
            SvnModalSkeleton: {
                new (...args: any[]): import("vue").CreateComponentPublicInstanceWithMixins<Readonly<{}> & Readonly<{}>, {
                    dialog: import("vue").Ref<boolean, boolean>;
                    $emit: (event: "prependIconClick" | "appendIconClick" | "onClickOutside", ...args: any[]) => void;
                    transition: string;
                    persistent: boolean;
                    fullscreen: boolean;
                    activatorText: string;
                    activatorVariant: string;
                    fullheight: boolean;
                    modalTitle: string;
                    modalHeight: string;
                    modalTitleCenter: boolean;
                    verticalHeader: boolean;
                    prependHeaderIcon: string;
                    appendHeaderIcon: string;
                    appendIconCloseModal: boolean;
                    headerCustomClass: string;
                    modalCustomClass: string;
                    $props: {
                        readonly transition?: string | undefined;
                        readonly persistent?: boolean | undefined;
                        readonly fullscreen?: boolean | undefined;
                        readonly activatorText?: string | undefined;
                        readonly activatorVariant?: string | undefined;
                        readonly fullheight?: boolean | undefined;
                        readonly modalTitle?: string | undefined;
                        readonly modalHeight?: string | undefined;
                        readonly modalTitleCenter?: boolean | undefined;
                        readonly verticalHeader?: boolean | undefined;
                        readonly prependHeaderIcon?: string | undefined;
                        readonly appendHeaderIcon?: string | undefined;
                        readonly appendIconCloseModal?: boolean | undefined;
                        readonly headerCustomClass?: string | undefined;
                        readonly modalCustomClass?: string | undefined;
                    };
                }, {}, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps & Readonly<{}> & Readonly<{}>, {}, true, {}, {}, import("vue").GlobalComponents, import("vue").GlobalDirectives, string, {}, any, import("vue").ComponentProvideOptions, {
                    P: {};
                    B: {};
                    D: {};
                    C: {};
                    M: {};
                    Defaults: {};
                }, Readonly<{}> & Readonly<{}>, {
                    dialog: import("vue").Ref<boolean, boolean>;
                    $emit: (event: "prependIconClick" | "appendIconClick" | "onClickOutside", ...args: any[]) => void;
                    transition: string;
                    persistent: boolean;
                    fullscreen: boolean;
                    activatorText: string;
                    activatorVariant: string;
                    fullheight: boolean;
                    modalTitle: string;
                    modalHeight: string;
                    modalTitleCenter: boolean;
                    verticalHeader: boolean;
                    prependHeaderIcon: string;
                    appendHeaderIcon: string;
                    appendIconCloseModal: boolean;
                    headerCustomClass: string;
                    modalCustomClass: string;
                    $props: {
                        readonly transition?: string | undefined;
                        readonly persistent?: boolean | undefined;
                        readonly fullscreen?: boolean | undefined;
                        readonly activatorText?: string | undefined;
                        readonly activatorVariant?: string | undefined;
                        readonly fullheight?: boolean | undefined;
                        readonly modalTitle?: string | undefined;
                        readonly modalHeight?: string | undefined;
                        readonly modalTitleCenter?: boolean | undefined;
                        readonly verticalHeader?: boolean | undefined;
                        readonly prependHeaderIcon?: string | undefined;
                        readonly appendHeaderIcon?: string | undefined;
                        readonly appendIconCloseModal?: boolean | undefined;
                        readonly headerCustomClass?: string | undefined;
                        readonly modalCustomClass?: string | undefined;
                    };
                }, {}, {}, {}, {}>;
                __isFragment?: never;
                __isTeleport?: never;
                __isSuspense?: never;
            } & import("vue").ComponentOptionsBase<Readonly<{}> & Readonly<{}>, {
                dialog: import("vue").Ref<boolean, boolean>;
                $emit: (event: "prependIconClick" | "appendIconClick" | "onClickOutside", ...args: any[]) => void;
                transition: string;
                persistent: boolean;
                fullscreen: boolean;
                activatorText: string;
                activatorVariant: string;
                fullheight: boolean;
                modalTitle: string;
                modalHeight: string;
                modalTitleCenter: boolean;
                verticalHeader: boolean;
                prependHeaderIcon: string;
                appendHeaderIcon: string;
                appendIconCloseModal: boolean;
                headerCustomClass: string;
                modalCustomClass: string;
                $props: {
                    readonly transition?: string | undefined;
                    readonly persistent?: boolean | undefined;
                    readonly fullscreen?: boolean | undefined;
                    readonly activatorText?: string | undefined;
                    readonly activatorVariant?: string | undefined;
                    readonly fullheight?: boolean | undefined;
                    readonly modalTitle?: string | undefined;
                    readonly modalHeight?: string | undefined;
                    readonly modalTitleCenter?: boolean | undefined;
                    readonly verticalHeader?: boolean | undefined;
                    readonly prependHeaderIcon?: string | undefined;
                    readonly appendHeaderIcon?: string | undefined;
                    readonly appendIconCloseModal?: boolean | undefined;
                    readonly headerCustomClass?: string | undefined;
                    readonly modalCustomClass?: string | undefined;
                };
            }, {}, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, {}, {}, string, {}, import("vue").GlobalComponents, import("vue").GlobalDirectives, string, import("vue").ComponentProvideOptions> & import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps & (new () => {
                $slots: {
                    "activator-div"?(_: any): any;
                    activator?(_: {}): any;
                    "activator-text"?(_: {}): any;
                    modal?(_: {}): any;
                    header?(_: {
                        close: () => void;
                    }): any;
                    "header-text"?(_: {}): any;
                    content?(_: {}): any;
                    "content-body"?(_: {}): any;
                    "bottom-content"?(_: {}): any;
                };
            });
        };
        setup(): {
            args: any;
        };
        template: string;
    };
    namespace args {
        let activatorVariant_1: string;
        export { activatorVariant_1 as activatorVariant };
        export let activatorText: string;
        export let fullscreen: boolean;
        export let persistent: boolean;
        export let modalTitle: string;
        export let modalTitleCenter: boolean;
        export let verticalHeader: boolean;
        export let appendIconCloseModal: boolean;
        export let transition: string;
    }
}
import SvnModalSkeleton from "../components/skeleton/SvnModalSkeleton.vue";
