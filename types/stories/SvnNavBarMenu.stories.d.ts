declare namespace _default {
    export let title: string;
    export { SvnNavbarMenu as component };
    export let tags: string[];
    export namespace argTypes {
        namespace variant {
            let control: string;
            let options: string[];
        }
    }
    export namespace parameters {
        namespace docs {
            namespace description {
                let component: string;
            }
        }
    }
}
export default _default;
export namespace Playground {
    function render(args: any): {
        components: {
            SvnNavbarMenu: {
                new (...args: any[]): import("vue").CreateComponentPublicInstanceWithMixins<Readonly<{}> & Readonly<{}>, {
                    modal: import("vue").Ref<null, null>;
                    $emit: (event: "navigate", ...args: any[]) => void;
                    variant: string;
                    items: unknown[];
                    $props: {
                        readonly variant?: string | undefined;
                        readonly items?: unknown[] | undefined;
                    };
                }, {}, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps & Readonly<{}> & Readonly<{}>, {}, true, {}, {}, import("vue").GlobalComponents, import("vue").GlobalDirectives, string, {}, any, import("vue").ComponentProvideOptions, {
                    P: {};
                    B: {};
                    D: {};
                    C: {};
                    M: {};
                    Defaults: {};
                }, Readonly<{}> & Readonly<{}>, {
                    modal: import("vue").Ref<null, null>;
                    $emit: (event: "navigate", ...args: any[]) => void;
                    variant: string;
                    items: unknown[];
                    $props: {
                        readonly variant?: string | undefined;
                        readonly items?: unknown[] | undefined;
                    };
                }, {}, {}, {}, {}>;
                __isFragment?: never;
                __isTeleport?: never;
                __isSuspense?: never;
            } & import("vue").ComponentOptionsBase<Readonly<{}> & Readonly<{}>, {
                modal: import("vue").Ref<null, null>;
                $emit: (event: "navigate", ...args: any[]) => void;
                variant: string;
                items: unknown[];
                $props: {
                    readonly variant?: string | undefined;
                    readonly items?: unknown[] | undefined;
                };
            }, {}, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, {}, {}, string, {}, import("vue").GlobalComponents, import("vue").GlobalDirectives, string, import("vue").ComponentProvideOptions> & import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps & (new () => {
                $slots: {
                    modal?(_: {}): any;
                    "modal-activator"?(_: {}): any;
                    "modal-content-body"?(_: {}): any;
                };
            });
        };
        setup(): {
            args: any;
        };
        template: string;
    };
    namespace args {
        export { menuItems as items };
        let variant_1: string;
        export { variant_1 as variant };
    }
}
import SvnNavbarMenu from "../components/navbar/SvnNavbarMenu.vue";
declare const menuItems: {
    title: string;
    to: string;
}[];
