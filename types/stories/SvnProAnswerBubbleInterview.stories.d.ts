declare namespace _default {
    export let title: string;
    export { SvnProAnswerBubbleInterview as component };
    export let tags: string[];
    export namespace argTypes {
        namespace side {
            let control: string;
        }
        namespace comment {
            let control_1: string;
            export { control_1 as control };
        }
        namespace user {
            let control_2: string;
            export { control_2 as control };
        }
        namespace crossed {
            let control_3: string;
            export { control_3 as control };
        }
        namespace selectedValue {
            let control_4: string;
            export { control_4 as control };
        }
        namespace questionType {
            let control_5: string;
            export { control_5 as control };
            export let options: ("InterviewApp::Questions::Chapter" | "InterviewApp::Questions::Mcq" | "InterviewApp::Questions::Open" | "InterviewApp::Questions::Paragraph" | "InterviewApp::Questions::Rating" | "InterviewApp::Questions::UpdateRoadmap" | "InterviewApp::Questions::CreateRoadmap")[];
        }
        namespace ratingLength {
            let control_6: string;
            export { control_6 as control };
        }
        namespace bubbleTitle {
            let control_7: string;
            export { control_7 as control };
        }
        namespace isLastInterviewCompletedAndLocked {
            let control_8: string;
            export { control_8 as control };
        }
    }
}
export default _default;
export namespace Playground {
    function render(args: any): {
        components: {
            SvnProAnswerBubbleInterview: import("vue").DefineComponent<{}, {
                side: boolean;
                user: Record<string, any>;
                comment: string;
                crossed: boolean;
                selectedValue: string;
                ratingLength: string;
                bubbleTitle: string;
                isLastInterviewCompletedAndLocked: boolean;
                questionType: string;
                $props: {
                    readonly side?: boolean | undefined;
                    readonly user?: Record<string, any> | undefined;
                    readonly comment?: string | undefined;
                    readonly crossed?: boolean | undefined;
                    readonly selectedValue?: string | undefined;
                    readonly ratingLength?: string | undefined;
                    readonly bubbleTitle?: string | undefined;
                    readonly isLastInterviewCompletedAndLocked?: boolean | undefined;
                    readonly questionType?: string | undefined;
                };
            }, {}, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").PublicProps, Readonly<{}> & Readonly<{}>, {}, {}, {}, {}, string, import("vue").ComponentProvideOptions, true, {}, any>;
        };
        setup(): {
            args: any;
        };
        template: string;
    };
    namespace args {
        let side_1: boolean;
        export { side_1 as side };
        let comment_1: string;
        export { comment_1 as comment };
        let bubbleTitle_1: string;
        export { bubbleTitle_1 as bubbleTitle };
        export namespace user_1 {
            let firstname: string;
            let lastname: string;
            let avatar: {
                50: string;
            };
        }
        export { user_1 as user };
        let crossed_1: boolean;
        export { crossed_1 as crossed };
        let selectedValue_1: string;
        export { selectedValue_1 as selectedValue };
        let questionType_1: "InterviewApp::Questions::Open";
        export { questionType_1 as questionType };
        let ratingLength_1: string;
        export { ratingLength_1 as ratingLength };
        let isLastInterviewCompletedAndLocked_1: boolean;
        export { isLastInterviewCompletedAndLocked_1 as isLastInterviewCompletedAndLocked };
    }
}
import SvnProAnswerBubbleInterview from "../components/card/SvnProAnswerBubbleInterview.vue";
