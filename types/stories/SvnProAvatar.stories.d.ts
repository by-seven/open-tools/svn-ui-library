declare namespace _default {
    export let title: string;
    export { SvnProAvatar as component };
    export let tags: string[];
    export namespace argTypes {
        namespace type {
            let control: string;
            let options: string[];
        }
        namespace image {
            let control_1: string;
            export { control_1 as control };
        }
        namespace firstname {
            let control_2: string;
            export { control_2 as control };
        }
        namespace start {
            let control_3: string;
            export { control_3 as control };
        }
        namespace size {
            let control_4: string;
            export { control_4 as control };
            let options_1: number[];
            export { options_1 as options };
        }
    }
    export namespace parameters {
        namespace docs {
            namespace description {
                let component: string;
            }
        }
    }
}
export default _default;
export namespace Playground {
    function render(args: any): {
        components: {
            SvnProAvatar: import("vue").DefineComponent<{}, {
                type: string;
                start: boolean;
                size: number;
                image?: string | undefined;
                firstname?: string | undefined;
                $props: {
                    readonly type?: string | undefined;
                    readonly start?: boolean | undefined;
                    readonly size?: number | undefined;
                    readonly image?: string | undefined;
                    readonly firstname?: string | undefined;
                };
            }, {}, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").PublicProps, Readonly<{}> & Readonly<{}>, {}, {}, {}, {}, string, import("vue").ComponentProvideOptions, true, {}, any>;
        };
        setup(): {
            args: any;
        };
        template: string;
    };
    namespace args {
        let size_1: number;
        export { size_1 as size };
        let type_1: string;
        export { type_1 as type };
        let image_1: undefined;
        export { image_1 as image };
        let firstname_1: string;
        export { firstname_1 as firstname };
        let start_1: boolean;
        export { start_1 as start };
    }
}
import SvnProAvatar from "../components/avatar/SvnProAvatar.vue";
