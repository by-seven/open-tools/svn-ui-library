declare namespace _default {
    export let title: string;
    export { SvnProBadge as component };
    export let tags: string[];
    export namespace argTypes {
        namespace dot {
            let control: string;
        }
        namespace content {
            let control_1: string;
            export { control_1 as control };
        }
        namespace max {
            let control_2: string;
            export { control_2 as control };
        }
    }
    export namespace parameters {
        namespace docs {
            namespace description {
                let component: string;
            }
        }
    }
}
export default _default;
export namespace Playground {
    function render(args: any): {
        components: {
            SvnProBadge: import("vue").DefineComponent<{}, {
                max: number;
                dot: boolean;
                content: number;
                $props: {
                    readonly max?: number | undefined;
                    readonly dot?: boolean | undefined;
                    readonly content?: number | undefined;
                };
            }, {}, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").PublicProps, Readonly<{}> & Readonly<{}>, {}, {}, {}, {}, string, import("vue").ComponentProvideOptions, true, {}, any>;
        };
        setup(): {
            args: any;
        };
        template: string;
    };
    namespace args {
        let dot_1: boolean;
        export { dot_1 as dot };
        let max_1: undefined;
        export { max_1 as max };
        let content_1: number;
        export { content_1 as content };
    }
}
import SvnProBadge from "../components/chip/SvnProBadge.vue";
