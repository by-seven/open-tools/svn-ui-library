declare namespace _default {
    export let title: string;
    export { SvnProBreadcrumbs as component };
    export let tags: string[];
    export namespace argTypes {
        namespace items {
            let control: string;
        }
    }
    export namespace parameters {
        namespace docs {
            namespace description {
                let component: string;
            }
        }
    }
}
export default _default;
export namespace Playground {
    function render(args: any): {
        components: {
            SvnProBreadcrumbs: import("vue").DefineComponent<{}, {
                items: unknown[];
                $props: {
                    readonly items?: unknown[] | undefined;
                };
            }, {}, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").PublicProps, Readonly<{}> & Readonly<{}>, {}, {}, {}, {}, string, import("vue").ComponentProvideOptions, true, {}, any>;
        };
        setup(): {
            args: any;
        };
        template: string;
    };
    namespace args {
        let items_1: {
            title: string;
            disabled: boolean;
            href: string;
        }[];
        export { items_1 as items };
    }
}
import SvnProBreadcrumbs from "../components/button/SvnProBreadcrumbs.vue";
