declare namespace _default {
    export let title: string;
    export { SvnProButton as component };
    export let tags: string[];
    export namespace argTypes {
        namespace text {
            let control: string;
        }
        namespace color {
            let control_1: string;
            export { control_1 as control };
        }
        namespace variant {
            let control_2: string;
            export { control_2 as control };
            export let options: string[];
        }
        namespace prependIcon {
            let control_3: string;
            export { control_3 as control };
        }
        namespace disabled {
            let control_4: string;
            export { control_4 as control };
        }
        namespace loading {
            let control_5: string;
            export { control_5 as control };
        }
        namespace block {
            let control_6: string;
            export { control_6 as control };
        }
    }
    export namespace parameters {
        namespace docs {
            namespace description {
                let component: string;
            }
        }
    }
}
export default _default;
export namespace Playground {
    function render(args: any): {
        components: {
            SvnProButton: {
                new (...args: any[]): import("vue").CreateComponentPublicInstanceWithMixins<Readonly<{}> & Readonly<{}>, {
                    text: string;
                    variant: string;
                    color: string;
                    loading: boolean;
                    disabled: boolean;
                    block: boolean;
                    append: boolean;
                    prependIcon?: string | undefined;
                    $props: {
                        readonly text?: string | undefined;
                        readonly variant?: string | undefined;
                        readonly color?: string | undefined;
                        readonly loading?: boolean | undefined;
                        readonly disabled?: boolean | undefined;
                        readonly block?: boolean | undefined;
                        readonly append?: boolean | undefined;
                        readonly prependIcon?: string | undefined;
                    };
                }, {}, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps & Readonly<{}> & Readonly<{}>, {}, true, {}, {}, import("vue").GlobalComponents, import("vue").GlobalDirectives, string, {}, any, import("vue").ComponentProvideOptions, {
                    P: {};
                    B: {};
                    D: {};
                    C: {};
                    M: {};
                    Defaults: {};
                }, Readonly<{}> & Readonly<{}>, {
                    text: string;
                    variant: string;
                    color: string;
                    loading: boolean;
                    disabled: boolean;
                    block: boolean;
                    append: boolean;
                    prependIcon?: string | undefined;
                    $props: {
                        readonly text?: string | undefined;
                        readonly variant?: string | undefined;
                        readonly color?: string | undefined;
                        readonly loading?: boolean | undefined;
                        readonly disabled?: boolean | undefined;
                        readonly block?: boolean | undefined;
                        readonly append?: boolean | undefined;
                        readonly prependIcon?: string | undefined;
                    };
                }, {}, {}, {}, {}>;
                __isFragment?: never;
                __isTeleport?: never;
                __isSuspense?: never;
            } & import("vue").ComponentOptionsBase<Readonly<{}> & Readonly<{}>, {
                text: string;
                variant: string;
                color: string;
                loading: boolean;
                disabled: boolean;
                block: boolean;
                append: boolean;
                prependIcon?: string | undefined;
                $props: {
                    readonly text?: string | undefined;
                    readonly variant?: string | undefined;
                    readonly color?: string | undefined;
                    readonly loading?: boolean | undefined;
                    readonly disabled?: boolean | undefined;
                    readonly block?: boolean | undefined;
                    readonly append?: boolean | undefined;
                    readonly prependIcon?: string | undefined;
                };
            }, {}, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, {}, {}, string, {}, import("vue").GlobalComponents, import("vue").GlobalDirectives, string, import("vue").ComponentProvideOptions> & import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps & (new () => {
                $slots: {
                    append?(_: {}): any;
                };
            });
        };
        setup(): {
            args: any;
        };
        template: string;
    };
    namespace args {
        let text_1: string;
        export { text_1 as text };
        let color_1: string;
        export { color_1 as color };
        let variant_1: string;
        export { variant_1 as variant };
        let prependIcon_1: undefined;
        export { prependIcon_1 as prependIcon };
        let disabled_1: boolean;
        export { disabled_1 as disabled };
        let loading_1: boolean;
        export { loading_1 as loading };
        let block_1: boolean;
        export { block_1 as block };
    }
}
import SvnProButton from "../components/button/SvnProButton.vue";
