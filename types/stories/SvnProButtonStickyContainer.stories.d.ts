declare namespace _default {
    export let title: string;
    export { SvnProButtonStickyContainer as component };
    export let tags: string[];
    export namespace argTypes {
        namespace configuration {
            let control: string;
            let options: string[];
        }
        namespace primaryActionText {
            let control_1: string;
            export { control_1 as control };
        }
        namespace secondaryActionText {
            let control_2: string;
            export { control_2 as control };
        }
        namespace hasPrimaryAction {
            let control_3: string;
            export { control_3 as control };
        }
        namespace hasSecondaryAction {
            let control_4: string;
            export { control_4 as control };
        }
    }
    export namespace parameters {
        namespace docs {
            namespace description {
                let component: string;
            }
        }
    }
}
export default _default;
export namespace Playground {
    function render(args: any): {
        components: {
            SvnProButtonStickyContainer: import("vue").DefineComponent<{}, {
                $emit: (event: "clickPrimaryAction" | "clickSecondaryAction", ...args: any[]) => void;
                configuration: string;
                hasPrimaryAction: boolean;
                hasSecondaryAction: boolean;
                primaryActionText: string;
                secondaryActionText: string;
                $props: {
                    readonly configuration?: string | undefined;
                    readonly hasPrimaryAction?: boolean | undefined;
                    readonly hasSecondaryAction?: boolean | undefined;
                    readonly primaryActionText?: string | undefined;
                    readonly secondaryActionText?: string | undefined;
                };
            }, {}, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").PublicProps, Readonly<{}> & Readonly<{}>, {}, {}, {}, {}, string, import("vue").ComponentProvideOptions, true, {}, any>;
        };
        setup(): {
            args: any;
        };
        template: string;
    };
    namespace args {
        let configuration_1: string;
        export { configuration_1 as configuration };
        let primaryActionText_1: string;
        export { primaryActionText_1 as primaryActionText };
        let secondaryActionText_1: string;
        export { secondaryActionText_1 as secondaryActionText };
        let hasPrimaryAction_1: boolean;
        export { hasPrimaryAction_1 as hasPrimaryAction };
        let hasSecondaryAction_1: boolean;
        export { hasSecondaryAction_1 as hasSecondaryAction };
    }
}
import SvnProButtonStickyContainer from "../components/button/SvnProButtonStickyContainer.vue";
