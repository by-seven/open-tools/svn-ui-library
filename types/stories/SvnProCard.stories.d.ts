declare namespace _default {
    export let title: string;
    export { SvnCardTest as component };
    export let tags: string[];
    export namespace argTypes {
        namespace variant {
            let control: string;
            let options: string[];
        }
    }
    export namespace parameters {
        namespace docs {
            namespace description {
                let component: string;
            }
        }
    }
}
export default _default;
export namespace Playground {
    function render(args: any): {
        components: {
            SvnCardTest: import("vue").DefineComponent<{}, {
                variant: string;
                $props: {
                    readonly variant?: string | undefined;
                };
            }, {}, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").PublicProps, Readonly<{}> & Readonly<{}>, {}, {}, {}, {}, string, import("vue").ComponentProvideOptions, true, {}, any>;
        };
        setup(): {
            args: any;
        };
        template: string;
    };
    namespace args {
        let variant_1: string;
        export { variant_1 as variant };
    }
}
import SvnCardTest from "./Components/SvnCardTest.vue";
