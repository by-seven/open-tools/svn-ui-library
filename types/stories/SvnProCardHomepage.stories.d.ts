declare namespace _default {
    export let title: string;
    export { SvnProCardHomepage as component };
    export let tags: string[];
    export namespace argTypes {
        namespace appIcon {
            let control: string;
        }
        namespace appColor {
            let control_1: string;
            export { control_1 as control };
        }
        namespace appName {
            let control_2: string;
            export { control_2 as control };
        }
        namespace appDescription {
            let control_3: string;
            export { control_3 as control };
        }
        namespace appLabel {
            let control_4: string;
            export { control_4 as control };
        }
    }
    export namespace parameters {
        namespace docs {
            namespace description {
                let component: string;
            }
        }
    }
}
export default _default;
export namespace Playground {
    function render(args: any): {
        components: {
            SvnProCardHomepage: import("vue").DefineComponent<{}, {
                $emit: (event: "action-clicked", ...args: any[]) => void;
                appName: string;
                appIcon: string;
                appColor: string;
                appDescription: string;
                appLabel?: string | undefined;
                $props: {
                    readonly appName?: string | undefined;
                    readonly appIcon?: string | undefined;
                    readonly appColor?: string | undefined;
                    readonly appDescription?: string | undefined;
                    readonly appLabel?: string | undefined;
                };
            }, {}, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").PublicProps, Readonly<{}> & Readonly<{}>, {}, {}, {}, {}, string, import("vue").ComponentProvideOptions, true, {}, any>;
        };
        setup(): {
            args: any;
        };
        template: string;
    };
    namespace args {
        let appIcon_1: string;
        export { appIcon_1 as appIcon };
        let appColor_1: string;
        export { appColor_1 as appColor };
        let appName_1: string;
        export { appName_1 as appName };
        let appDescription_1: string;
        export { appDescription_1 as appDescription };
        let appLabel_1: string;
        export { appLabel_1 as appLabel };
    }
}
import SvnProCardHomepage from "../components/card/SvnProCardHomepage.vue";
