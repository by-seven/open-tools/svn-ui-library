declare namespace _default {
    export let title: string;
    export { SvnProCardInterviewTarget as component };
    export let tags: string[];
    export namespace argTypes {
        namespace size {
            let control: string;
            let options: ("default" | "compact")[];
        }
        namespace type {
            let control_1: string;
            export { control_1 as control };
            let options_1: ("create" | "create_proposal" | "update_show")[];
            export { options_1 as options };
        }
        namespace comments {
            let contorl: string;
        }
        namespace answerLabel {
            let control_2: string;
            export { control_2 as control };
        }
        namespace createPrependicon {
            let control_3: string;
            export { control_3 as control };
        }
        namespace deleteIcon {
            let control_4: string;
            export { control_4 as control };
        }
        namespace buttonText {
            let control_5: string;
            export { control_5 as control };
        }
        namespace infoText {
            let control_6: string;
            export { control_6 as control };
        }
        namespace noDeadlineText {
            let control_7: string;
            export { control_7 as control };
        }
        namespace interviewLocked {
            let control_8: string;
            export { control_8 as control };
        }
        namespace isFinalUpdate {
            let control_9: string;
            export { control_9 as control };
        }
        namespace hasEmployeeSuggestion {
            let control_10: string;
            export { control_10 as control };
        }
        namespace hasManagerOrCrossedSuggestion {
            let control_11: string;
            export { control_11 as control };
        }
        namespace interviewStatus {
            let control_12: string;
            export { control_12 as control };
            let options_2: ("in_progress" | "not_started" | "submitted" | "not_available")[];
            export { options_2 as options };
        }
        namespace tagStyle {
            let control_13: string;
            export { control_13 as control };
            let options_3: ("text" | "outlined" | "filled")[];
            export { options_3 as options };
        }
        namespace buttonVariant {
            let control_14: string;
            export { control_14 as control };
            let options_4: ("flat" | "text" | "outlined" | "plain" | "elevated" | "tonal")[];
            export { options_4 as options };
        }
        namespace deleteButtonVariant {
            let control_15: string;
            export { control_15 as control };
            let options_5: ("flat" | "text" | "outlined" | "plain" | "elevated" | "tonal")[];
            export { options_5 as options };
        }
        namespace targetTitle {
            let control_16: string;
            export { control_16 as control };
        }
        namespace targetDeadline {
            let control_17: string;
            export { control_17 as control };
        }
        namespace targetDescription {
            let control_18: string;
            export { control_18 as control };
        }
        namespace variableLabelOne {
            let control_19: string;
            export { control_19 as control };
        }
        namespace variableLabelTwo {
            let control_20: string;
            export { control_20 as control };
        }
        namespace tooltipOneContent {
            let control_21: string;
            export { control_21 as control };
        }
        namespace tooltipTwoContent {
            let control_22: string;
            export { control_22 as control };
        }
        namespace hasManagerOrCrossedSuggestionAndSubmitted {
            let control_23: string;
            export { control_23 as control };
        }
        namespace targetObjectiveIndicator {
            let control_24: string;
            export { control_24 as control };
        }
    }
}
export default _default;
export namespace Playground {
    function render(args: any): {
        components: {
            SvnProCardInterviewTarget: {
                new (...args: any[]): import("vue").CreateComponentPublicInstanceWithMixins<Readonly<{}> & Readonly<{}>, {
                    $emit: (event: "click-action" | "click-delete" | "update-snapshot", ...args: any[]) => void;
                    type: string;
                    size: string;
                    tagStyle: string;
                    targetTitle: string;
                    targetDeadline: string;
                    comments: string;
                    interviewLocked: boolean;
                    answerLabel: string;
                    commentLabel: string;
                    isFinalUpdate: boolean;
                    interviewStatus: string;
                    noDeadlineText: string;
                    createPrependicon: string;
                    deleteIcon: string;
                    infoText: string;
                    buttonText: string;
                    buttonVariant: string;
                    deleteButtonVariant: string;
                    targetDescription: string;
                    targetObjectiveIndicator: Record<string, any>;
                    tooltipOneContent: string;
                    tooltipTwoContent: string;
                    variableLabelOne: string;
                    variableLabelTwo: string;
                    hasEmployeeSuggestion: boolean;
                    hasManagerOrCrossedSuggestion: boolean;
                    hasManagerOrCrossedSuggestionAndSubmitted: boolean;
                    $props: {
                        readonly type?: string | undefined;
                        readonly size?: string | undefined;
                        readonly tagStyle?: string | undefined;
                        readonly targetTitle?: string | undefined;
                        readonly targetDeadline?: string | undefined;
                        readonly comments?: string | undefined;
                        readonly interviewLocked?: boolean | undefined;
                        readonly answerLabel?: string | undefined;
                        readonly commentLabel?: string | undefined;
                        readonly isFinalUpdate?: boolean | undefined;
                        readonly interviewStatus?: string | undefined;
                        readonly noDeadlineText?: string | undefined;
                        readonly createPrependicon?: string | undefined;
                        readonly deleteIcon?: string | undefined;
                        readonly infoText?: string | undefined;
                        readonly buttonText?: string | undefined;
                        readonly buttonVariant?: string | undefined;
                        readonly deleteButtonVariant?: string | undefined;
                        readonly targetDescription?: string | undefined;
                        readonly targetObjectiveIndicator?: Record<string, any> | undefined;
                        readonly tooltipOneContent?: string | undefined;
                        readonly tooltipTwoContent?: string | undefined;
                        readonly variableLabelOne?: string | undefined;
                        readonly variableLabelTwo?: string | undefined;
                        readonly hasEmployeeSuggestion?: boolean | undefined;
                        readonly hasManagerOrCrossedSuggestion?: boolean | undefined;
                        readonly hasManagerOrCrossedSuggestionAndSubmitted?: boolean | undefined;
                    };
                }, {}, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps & Readonly<{}> & Readonly<{}>, {}, true, {}, {}, import("vue").GlobalComponents, import("vue").GlobalDirectives, string, {}, any, import("vue").ComponentProvideOptions, {
                    P: {};
                    B: {};
                    D: {};
                    C: {};
                    M: {};
                    Defaults: {};
                }, Readonly<{}> & Readonly<{}>, {
                    $emit: (event: "click-action" | "click-delete" | "update-snapshot", ...args: any[]) => void;
                    type: string;
                    size: string;
                    tagStyle: string;
                    targetTitle: string;
                    targetDeadline: string;
                    comments: string;
                    interviewLocked: boolean;
                    answerLabel: string;
                    commentLabel: string;
                    isFinalUpdate: boolean;
                    interviewStatus: string;
                    noDeadlineText: string;
                    createPrependicon: string;
                    deleteIcon: string;
                    infoText: string;
                    buttonText: string;
                    buttonVariant: string;
                    deleteButtonVariant: string;
                    targetDescription: string;
                    targetObjectiveIndicator: Record<string, any>;
                    tooltipOneContent: string;
                    tooltipTwoContent: string;
                    variableLabelOne: string;
                    variableLabelTwo: string;
                    hasEmployeeSuggestion: boolean;
                    hasManagerOrCrossedSuggestion: boolean;
                    hasManagerOrCrossedSuggestionAndSubmitted: boolean;
                    $props: {
                        readonly type?: string | undefined;
                        readonly size?: string | undefined;
                        readonly tagStyle?: string | undefined;
                        readonly targetTitle?: string | undefined;
                        readonly targetDeadline?: string | undefined;
                        readonly comments?: string | undefined;
                        readonly interviewLocked?: boolean | undefined;
                        readonly answerLabel?: string | undefined;
                        readonly commentLabel?: string | undefined;
                        readonly isFinalUpdate?: boolean | undefined;
                        readonly interviewStatus?: string | undefined;
                        readonly noDeadlineText?: string | undefined;
                        readonly createPrependicon?: string | undefined;
                        readonly deleteIcon?: string | undefined;
                        readonly infoText?: string | undefined;
                        readonly buttonText?: string | undefined;
                        readonly buttonVariant?: string | undefined;
                        readonly deleteButtonVariant?: string | undefined;
                        readonly targetDescription?: string | undefined;
                        readonly targetObjectiveIndicator?: Record<string, any> | undefined;
                        readonly tooltipOneContent?: string | undefined;
                        readonly tooltipTwoContent?: string | undefined;
                        readonly variableLabelOne?: string | undefined;
                        readonly variableLabelTwo?: string | undefined;
                        readonly hasEmployeeSuggestion?: boolean | undefined;
                        readonly hasManagerOrCrossedSuggestion?: boolean | undefined;
                        readonly hasManagerOrCrossedSuggestionAndSubmitted?: boolean | undefined;
                    };
                }, {}, {}, {}, {}>;
                __isFragment?: never;
                __isTeleport?: never;
                __isSuspense?: never;
            } & import("vue").ComponentOptionsBase<Readonly<{}> & Readonly<{}>, {
                $emit: (event: "click-action" | "click-delete" | "update-snapshot", ...args: any[]) => void;
                type: string;
                size: string;
                tagStyle: string;
                targetTitle: string;
                targetDeadline: string;
                comments: string;
                interviewLocked: boolean;
                answerLabel: string;
                commentLabel: string;
                isFinalUpdate: boolean;
                interviewStatus: string;
                noDeadlineText: string;
                createPrependicon: string;
                deleteIcon: string;
                infoText: string;
                buttonText: string;
                buttonVariant: string;
                deleteButtonVariant: string;
                targetDescription: string;
                targetObjectiveIndicator: Record<string, any>;
                tooltipOneContent: string;
                tooltipTwoContent: string;
                variableLabelOne: string;
                variableLabelTwo: string;
                hasEmployeeSuggestion: boolean;
                hasManagerOrCrossedSuggestion: boolean;
                hasManagerOrCrossedSuggestionAndSubmitted: boolean;
                $props: {
                    readonly type?: string | undefined;
                    readonly size?: string | undefined;
                    readonly tagStyle?: string | undefined;
                    readonly targetTitle?: string | undefined;
                    readonly targetDeadline?: string | undefined;
                    readonly comments?: string | undefined;
                    readonly interviewLocked?: boolean | undefined;
                    readonly answerLabel?: string | undefined;
                    readonly commentLabel?: string | undefined;
                    readonly isFinalUpdate?: boolean | undefined;
                    readonly interviewStatus?: string | undefined;
                    readonly noDeadlineText?: string | undefined;
                    readonly createPrependicon?: string | undefined;
                    readonly deleteIcon?: string | undefined;
                    readonly infoText?: string | undefined;
                    readonly buttonText?: string | undefined;
                    readonly buttonVariant?: string | undefined;
                    readonly deleteButtonVariant?: string | undefined;
                    readonly targetDescription?: string | undefined;
                    readonly targetObjectiveIndicator?: Record<string, any> | undefined;
                    readonly tooltipOneContent?: string | undefined;
                    readonly tooltipTwoContent?: string | undefined;
                    readonly variableLabelOne?: string | undefined;
                    readonly variableLabelTwo?: string | undefined;
                    readonly hasEmployeeSuggestion?: boolean | undefined;
                    readonly hasManagerOrCrossedSuggestion?: boolean | undefined;
                    readonly hasManagerOrCrossedSuggestionAndSubmitted?: boolean | undefined;
                };
            }, {}, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, {}, {}, string, {}, import("vue").GlobalComponents, import("vue").GlobalDirectives, string, import("vue").ComponentProvideOptions> & import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps & (new () => {
                $slots: {
                    "employee-suggestion"?(_: {}): any;
                    "manager-cross-suggestion"?(_: {}): any;
                    "manager-cross-suggestion-submitted"?(_: {}): any;
                };
            });
        };
        setup(): {
            args: any;
        };
        template: string;
    };
    namespace args {
        let type_1: string;
        export { type_1 as type };
        let size_1: string;
        export { size_1 as size };
        let deleteIcon_1: string;
        export { deleteIcon_1 as deleteIcon };
        let createPrependicon_1: string;
        export { createPrependicon_1 as createPrependicon };
        let buttonText_1: string;
        export { buttonText_1 as buttonText };
        let buttonVariant_1: string;
        export { buttonVariant_1 as buttonVariant };
        let tagStyle_1: string;
        export { tagStyle_1 as tagStyle };
        let answerLabel_1: string;
        export { answerLabel_1 as answerLabel };
        export let commentLabel: string;
        let comments_1: string;
        export { comments_1 as comments };
        let isFinalUpdate_1: boolean;
        export { isFinalUpdate_1 as isFinalUpdate };
        let interviewLocked_1: boolean;
        export { interviewLocked_1 as interviewLocked };
        let hasEmployeeSuggestion_1: boolean;
        export { hasEmployeeSuggestion_1 as hasEmployeeSuggestion };
        let hasManagerOrCrossedSuggestion_1: boolean;
        export { hasManagerOrCrossedSuggestion_1 as hasManagerOrCrossedSuggestion };
        let hasManagerOrCrossedSuggestionAndSubmitted_1: boolean;
        export { hasManagerOrCrossedSuggestionAndSubmitted_1 as hasManagerOrCrossedSuggestionAndSubmitted };
        let interviewStatus_1: string;
        export { interviewStatus_1 as interviewStatus };
        let deleteButtonVariant_1: string;
        export { deleteButtonVariant_1 as deleteButtonVariant };
        let noDeadlineText_1: string;
        export { noDeadlineText_1 as noDeadlineText };
        let targetTitle_1: string;
        export { targetTitle_1 as targetTitle };
        let targetDeadline_1: string;
        export { targetDeadline_1 as targetDeadline };
        let targetDescription_1: string;
        export { targetDescription_1 as targetDescription };
        let infoText_1: string;
        export { infoText_1 as infoText };
        let variableLabelOne_1: string;
        export { variableLabelOne_1 as variableLabelOne };
        let variableLabelTwo_1: string;
        export { variableLabelTwo_1 as variableLabelTwo };
        let tooltipOneContent_1: string;
        export { tooltipOneContent_1 as tooltipOneContent };
        let tooltipTwoContent_1: string;
        export { tooltipTwoContent_1 as tooltipTwoContent };
        export namespace targetObjectiveIndicator_1 {
            export let status: string;
            export let indicator_type: string;
            export namespace options_6 {
                let starting_value: string;
                let current_value: string;
                let target_value: string;
                let multi_choice_list: {
                    option: string;
                    checkbox: boolean;
                }[];
            }
            export { options_6 as options };
        }
        export { targetObjectiveIndicator_1 as targetObjectiveIndicator };
    }
}
import SvnProCardInterviewTarget from "../components/card/SvnProCardInterviewTarget.vue";
