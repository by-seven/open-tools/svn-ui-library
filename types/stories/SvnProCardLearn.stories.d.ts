declare namespace _default {
    export let title: string;
    export { SvnProCardLearn as component };
    export let tags: string[];
    export namespace argTypes {
        namespace state {
            let control: string;
            let options: string[];
        }
        namespace cardType {
            let control_1: string;
            export { control_1 as control };
            let options_1: string[];
            export { options_1 as options };
        }
        namespace cardImage {
            let control_2: string;
            export { control_2 as control };
        }
        namespace editText {
            let control_3: string;
            export { control_3 as control };
        }
        namespace deleteText {
            let control_4: string;
            export { control_4 as control };
        }
        namespace cardTitle {
            let control_5: string;
            export { control_5 as control };
        }
        namespace duration {
            let control_6: string;
            export { control_6 as control };
        }
        namespace remaining {
            let control_7: string;
            export { control_7 as control };
        }
        namespace count {
            let control_8: string;
            export { control_8 as control };
        }
        namespace progress {
            let control_9: string;
            export { control_9 as control };
        }
        namespace isFavorited {
            let control_10: string;
            export { control_10 as control };
        }
        namespace editable {
            let control_11: string;
            export { control_11 as control };
        }
        namespace shareable {
            let control_12: string;
            export { control_12 as control };
        }
        namespace playlistImages {
            let control_13: string;
            export { control_13 as control };
        }
        namespace themes {
            let control_14: string;
            export { control_14 as control };
        }
        namespace acquiredText {
            let control_15: string;
            export { control_15 as control };
        }
        namespace acquiredIcon {
            let control_16: string;
            export { control_16 as control };
        }
        namespace reactionCount {
            let control_17: string;
            export { control_17 as control };
        }
        namespace isAcquired {
            let control_18: string;
            export { control_18 as control };
        }
        namespace cardSize {
            let control_19: string;
            export { control_19 as control };
            let options_2: string[];
            export { options_2 as options };
        }
        namespace reactionList {
            let control_20: string;
            export { control_20 as control };
        }
    }
    export namespace parameters {
        namespace docs {
            namespace description {
                let component: string;
            }
        }
    }
}
export default _default;
export namespace Playground {
    function render(args: any): {
        components: {
            SvnProCardLearn: {
                new (...args: any[]): import("vue").CreateComponentPublicInstanceWithMixins<Readonly<{}> & Readonly<{}>, {
                    $emit: (event: "on-favorite" | "on-share" | "on-edit" | "on-delete" | "go-to-item-show", ...args: any[]) => void;
                    state: string;
                    editable: boolean;
                    cardType: string;
                    cardSize: string;
                    reactionList: unknown[];
                    isFavorited: boolean;
                    editText: string;
                    deleteText: string;
                    favoriteIcon: string;
                    isAcquired: boolean;
                    editableIcon: string;
                    shareable: boolean;
                    shareableIcon: string;
                    playlistImages: unknown[];
                    themes: unknown[];
                    acquiredText: string;
                    acquiredIcon: string;
                    playlistModules: unknown[];
                    progress?: number | undefined;
                    duration?: string | undefined;
                    cardTitle?: string | undefined;
                    reactionCount?: number | undefined;
                    cardImage?: string | undefined;
                    remaining?: string | undefined;
                    count?: number | undefined;
                    $props: {
                        readonly state?: string | undefined;
                        readonly editable?: boolean | undefined;
                        readonly cardType?: string | undefined;
                        readonly cardSize?: string | undefined;
                        readonly reactionList?: unknown[] | undefined;
                        readonly isFavorited?: boolean | undefined;
                        readonly editText?: string | undefined;
                        readonly deleteText?: string | undefined;
                        readonly favoriteIcon?: string | undefined;
                        readonly isAcquired?: boolean | undefined;
                        readonly editableIcon?: string | undefined;
                        readonly shareable?: boolean | undefined;
                        readonly shareableIcon?: string | undefined;
                        readonly playlistImages?: unknown[] | undefined;
                        readonly themes?: unknown[] | undefined;
                        readonly acquiredText?: string | undefined;
                        readonly acquiredIcon?: string | undefined;
                        readonly playlistModules?: unknown[] | undefined;
                        readonly progress?: number | undefined;
                        readonly duration?: string | undefined;
                        readonly cardTitle?: string | undefined;
                        readonly reactionCount?: number | undefined;
                        readonly cardImage?: string | undefined;
                        readonly remaining?: string | undefined;
                        readonly count?: number | undefined;
                    };
                }, {}, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps & Readonly<{}> & Readonly<{}>, {}, true, {}, {}, import("vue").GlobalComponents, import("vue").GlobalDirectives, string, {}, any, import("vue").ComponentProvideOptions, {
                    P: {};
                    B: {};
                    D: {};
                    C: {};
                    M: {};
                    Defaults: {};
                }, Readonly<{}> & Readonly<{}>, {
                    $emit: (event: "on-favorite" | "on-share" | "on-edit" | "on-delete" | "go-to-item-show", ...args: any[]) => void;
                    state: string;
                    editable: boolean;
                    cardType: string;
                    cardSize: string;
                    reactionList: unknown[];
                    isFavorited: boolean;
                    editText: string;
                    deleteText: string;
                    favoriteIcon: string;
                    isAcquired: boolean;
                    editableIcon: string;
                    shareable: boolean;
                    shareableIcon: string;
                    playlistImages: unknown[];
                    themes: unknown[];
                    acquiredText: string;
                    acquiredIcon: string;
                    playlistModules: unknown[];
                    progress?: number | undefined;
                    duration?: string | undefined;
                    cardTitle?: string | undefined;
                    reactionCount?: number | undefined;
                    cardImage?: string | undefined;
                    remaining?: string | undefined;
                    count?: number | undefined;
                    $props: {
                        readonly state?: string | undefined;
                        readonly editable?: boolean | undefined;
                        readonly cardType?: string | undefined;
                        readonly cardSize?: string | undefined;
                        readonly reactionList?: unknown[] | undefined;
                        readonly isFavorited?: boolean | undefined;
                        readonly editText?: string | undefined;
                        readonly deleteText?: string | undefined;
                        readonly favoriteIcon?: string | undefined;
                        readonly isAcquired?: boolean | undefined;
                        readonly editableIcon?: string | undefined;
                        readonly shareable?: boolean | undefined;
                        readonly shareableIcon?: string | undefined;
                        readonly playlistImages?: unknown[] | undefined;
                        readonly themes?: unknown[] | undefined;
                        readonly acquiredText?: string | undefined;
                        readonly acquiredIcon?: string | undefined;
                        readonly playlistModules?: unknown[] | undefined;
                        readonly progress?: number | undefined;
                        readonly duration?: string | undefined;
                        readonly cardTitle?: string | undefined;
                        readonly reactionCount?: number | undefined;
                        readonly cardImage?: string | undefined;
                        readonly remaining?: string | undefined;
                        readonly count?: number | undefined;
                    };
                }, {}, {}, {}, {}>;
                __isFragment?: never;
                __isTeleport?: never;
                __isSuspense?: never;
            } & import("vue").ComponentOptionsBase<Readonly<{}> & Readonly<{}>, {
                $emit: (event: "on-favorite" | "on-share" | "on-edit" | "on-delete" | "go-to-item-show", ...args: any[]) => void;
                state: string;
                editable: boolean;
                cardType: string;
                cardSize: string;
                reactionList: unknown[];
                isFavorited: boolean;
                editText: string;
                deleteText: string;
                favoriteIcon: string;
                isAcquired: boolean;
                editableIcon: string;
                shareable: boolean;
                shareableIcon: string;
                playlistImages: unknown[];
                themes: unknown[];
                acquiredText: string;
                acquiredIcon: string;
                playlistModules: unknown[];
                progress?: number | undefined;
                duration?: string | undefined;
                cardTitle?: string | undefined;
                reactionCount?: number | undefined;
                cardImage?: string | undefined;
                remaining?: string | undefined;
                count?: number | undefined;
                $props: {
                    readonly state?: string | undefined;
                    readonly editable?: boolean | undefined;
                    readonly cardType?: string | undefined;
                    readonly cardSize?: string | undefined;
                    readonly reactionList?: unknown[] | undefined;
                    readonly isFavorited?: boolean | undefined;
                    readonly editText?: string | undefined;
                    readonly deleteText?: string | undefined;
                    readonly favoriteIcon?: string | undefined;
                    readonly isAcquired?: boolean | undefined;
                    readonly editableIcon?: string | undefined;
                    readonly shareable?: boolean | undefined;
                    readonly shareableIcon?: string | undefined;
                    readonly playlistImages?: unknown[] | undefined;
                    readonly themes?: unknown[] | undefined;
                    readonly acquiredText?: string | undefined;
                    readonly acquiredIcon?: string | undefined;
                    readonly playlistModules?: unknown[] | undefined;
                    readonly progress?: number | undefined;
                    readonly duration?: string | undefined;
                    readonly cardTitle?: string | undefined;
                    readonly reactionCount?: number | undefined;
                    readonly cardImage?: string | undefined;
                    readonly remaining?: string | undefined;
                    readonly count?: number | undefined;
                };
            }, {}, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, {}, {}, string, {}, import("vue").GlobalComponents, import("vue").GlobalDirectives, string, import("vue").ComponentProvideOptions> & import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps & (new () => {
                $slots: {
                    menu?(_: {}): any;
                    menu?(_: {}): any;
                    menu?(_: {}): any;
                    menu?(_: {}): any;
                };
            });
        };
        setup(): {
            args: any;
        };
        template: string;
    };
    namespace args {
        let state_1: string;
        export { state_1 as state };
        let cardType_1: string;
        export { cardType_1 as cardType };
        let cardImage_1: string;
        export { cardImage_1 as cardImage };
        let cardTitle_1: string;
        export { cardTitle_1 as cardTitle };
        let duration_1: string;
        export { duration_1 as duration };
        let remaining_1: string;
        export { remaining_1 as remaining };
        let count_1: number;
        export { count_1 as count };
        let progress_1: number;
        export { progress_1 as progress };
        let isFavorited_1: boolean;
        export { isFavorited_1 as isFavorited };
        let editable_1: boolean;
        export { editable_1 as editable };
        let editText_1: string;
        export { editText_1 as editText };
        let deleteText_1: string;
        export { deleteText_1 as deleteText };
        let playlistImages_1: string[];
        export { playlistImages_1 as playlistImages };
        let themes_1: string[];
        export { themes_1 as themes };
        let acquiredText_1: string;
        export { acquiredText_1 as acquiredText };
        let acquiredIcon_1: string;
        export { acquiredIcon_1 as acquiredIcon };
        let reactionCount_1: number;
        export { reactionCount_1 as reactionCount };
        let reactionList_1: string[];
        export { reactionList_1 as reactionList };
        let shareable_1: boolean;
        export { shareable_1 as shareable };
        let isAcquired_1: boolean;
        export { isAcquired_1 as isAcquired };
        let cardSize_1: string;
        export { cardSize_1 as cardSize };
        export { playlistModules };
    }
}
import SvnProCardLearn from "../components/card/SvnProCardLearn.vue";
declare const playlistModules: {
    title: string;
    cover_url: {
        "150": string;
    };
    duration: string;
    submission_status: string;
    themes: {
        name: string;
    }[];
}[];
