declare namespace _default {
    export let title: string;
    export { SvnProCardReaction as component };
    export let tags: string[];
    export namespace argTypes {
        namespace feedback {
            let control: string;
        }
        namespace feedbackTitle {
            let control_1: string;
            export { control_1 as control };
        }
        namespace cardSize {
            let control_2: string;
            export { control_2 as control };
            export let options: string[];
        }
        namespace feedbackDescription {
            let control_3: string;
            export { control_3 as control };
        }
    }
    export namespace parameters {
        namespace docs {
            namespace description {
                let component: string;
            }
        }
    }
}
export default _default;
export namespace Playground {
    function render(args: any): {
        components: {
            SvnProCardReaction: import("vue").DefineComponent<{}, {
                $emit: (event: "toggle-reaction", ...args: any[]) => void;
                cardSize: string;
                reactions: unknown[];
                feedback: boolean;
                feedbackTitle: string;
                feedbackDescription: string;
                $props: {
                    readonly cardSize?: string | undefined;
                    readonly reactions?: unknown[] | undefined;
                    readonly feedback?: boolean | undefined;
                    readonly feedbackTitle?: string | undefined;
                    readonly feedbackDescription?: string | undefined;
                };
            }, {}, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").PublicProps, Readonly<{}> & Readonly<{}>, {}, {}, {}, {}, string, import("vue").ComponentProvideOptions, true, {}, any>;
        };
        setup(): {
            args: any;
        };
        template: string;
    };
    namespace args {
        let feedback_1: boolean;
        export { feedback_1 as feedback };
        let cardSize_1: string;
        export { cardSize_1 as cardSize };
        let feedbackTitle_1: string;
        export { feedbackTitle_1 as feedbackTitle };
        let feedbackDescription_1: string;
        export { feedbackDescription_1 as feedbackDescription };
        export let reactions: {
            value: string;
            icon: string;
            text: string;
            active: boolean;
        }[];
    }
}
import SvnProCardReaction from "../components/card/SvnProCardReaction.vue";
