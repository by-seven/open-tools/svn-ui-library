declare namespace _default {
    export let title: string;
    export { SvnProCardRoadmapTemplate as component };
    export let tags: string[];
    export namespace argTypes {
        export namespace icon {
            let control: string;
        }
        export namespace title_1 {
            let control_1: string;
            export { control_1 as control };
        }
        export { title_1 as title };
        export namespace subtitle {
            let control_2: string;
            export { control_2 as control };
        }
        export namespace disabled {
            let control_3: string;
            export { control_3 as control };
        }
        export namespace value {
            let control_4: string;
            export { control_4 as control };
        }
        export namespace items {
            let control_5: string;
            export { control_5 as control };
        }
    }
    export namespace parameters {
        namespace docs {
            namespace description {
                let component: string;
            }
        }
    }
}
export default _default;
export namespace Playground {
    function render(args: any): {
        components: {
            SvnProCardRoadmapTemplate: import("vue").DefineComponent<{}, {
                $emit: (event: "click", ...args: any[]) => void;
                indicatorIcon?: string | undefined;
                indicatorName?: string | undefined;
                targetTitle?: string | undefined;
                targetDeadline?: string | undefined;
                $props: {
                    readonly indicatorIcon?: string | undefined;
                    readonly indicatorName?: string | undefined;
                    readonly targetTitle?: string | undefined;
                    readonly targetDeadline?: string | undefined;
                };
            }, {}, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").PublicProps, Readonly<{}> & Readonly<{}>, {}, {}, {}, {}, string, import("vue").ComponentProvideOptions, true, {}, any>;
        };
        setup(): {
            args: any;
        };
        template: string;
    };
    namespace args {
        let indicatorIcon: string;
        let indicatorName: string;
        let targetTitle: string;
        let targetDeadline: string;
    }
}
import SvnProCardRoadmapTemplate from "../components/card/SvnProCardRoadmapTemplate.vue";
