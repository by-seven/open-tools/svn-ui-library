declare namespace _default {
    export let title: string;
    export { SvnProCardSectionEvaluationShow as component };
    export let tags: string[];
    export namespace argTypes {
        namespace cardType {
            let control: string;
            let options: string[];
        }
        namespace cardTitle {
            let control_1: string;
            export { control_1 as control };
        }
        namespace cardDescription {
            let control_2: string;
            export { control_2 as control };
        }
        namespace cardButtonText {
            let control_3: string;
            export { control_3 as control };
        }
    }
    export namespace parameters {
        namespace docs {
            namespace description {
                let component: string;
            }
        }
    }
}
export default _default;
export namespace Playground {
    function render(args: any): {
        components: {
            SvnProCardSectionEvaluationShow: import("vue").DefineComponent<{}, {
                $emit: (event: "action-clicked", ...args: any[]) => void;
                cardType: string;
                cardTitle: string;
                cardDescription: string;
                cardButtonText: string;
                $props: {
                    readonly cardType?: string | undefined;
                    readonly cardTitle?: string | undefined;
                    readonly cardDescription?: string | undefined;
                    readonly cardButtonText?: string | undefined;
                };
            }, {}, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").PublicProps, Readonly<{}> & Readonly<{}>, {}, {}, {}, {}, string, import("vue").ComponentProvideOptions, true, {}, any>;
        };
        setup(): {
            args: any;
        };
        template: string;
    };
    namespace args {
        let cardType_1: string;
        export { cardType_1 as cardType };
        let cardTitle_1: string;
        export { cardTitle_1 as cardTitle };
        let cardDescription_1: string;
        export { cardDescription_1 as cardDescription };
        let cardButtonText_1: string;
        export { cardButtonText_1 as cardButtonText };
    }
}
import SvnProCardSectionEvaluationShow from "../components/card/SvnProCardSectionEvaluationShow.vue";
