declare namespace _default {
    export let title: string;
    export { SvnProCardSectionQuizResultLearn as component };
    export let tags: string[];
    export namespace argTypes {
        namespace cardType {
            let control: string;
            let options: string[];
        }
        namespace cardSize {
            let control_1: string;
            export { control_1 as control };
            let options_1: string[];
            export { options_1 as options };
        }
        namespace cardTitle {
            let control_2: string;
            export { control_2 as control };
        }
        namespace cardDescription {
            let control_3: string;
            export { control_3 as control };
        }
        namespace backToTrainingText {
            let control_4: string;
            export { control_4 as control };
        }
        namespace retryQuizText {
            let control_5: string;
            export { control_5 as control };
        }
        namespace nextModuleText {
            let control_6: string;
            export { control_6 as control };
        }
    }
    export namespace parameters {
        namespace docs {
            namespace description {
                let component: string;
            }
        }
    }
}
export default _default;
export namespace Playground {
    function render(args: any): {
        components: {
            SvnProCardSectionQuizResultLearn: import("vue").DefineComponent<{}, {
                $emit: (event: "back-to-training" | "retry-module" | "next-module", ...args: any[]) => void;
                cardType: string;
                cardSize: string;
                cardTitle: string;
                cardDescription: string;
                backToTrainingText: string;
                retryQuizText: string;
                nextModuleText: string;
                $props: {
                    readonly cardType?: string | undefined;
                    readonly cardSize?: string | undefined;
                    readonly cardTitle?: string | undefined;
                    readonly cardDescription?: string | undefined;
                    readonly backToTrainingText?: string | undefined;
                    readonly retryQuizText?: string | undefined;
                    readonly nextModuleText?: string | undefined;
                };
            }, {}, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").PublicProps, Readonly<{}> & Readonly<{}>, {}, {}, {}, {}, string, import("vue").ComponentProvideOptions, true, {}, any>;
        };
        setup(): {
            args: any;
        };
        template: string;
    };
    namespace args {
        let cardType_1: string;
        export { cardType_1 as cardType };
        let cardSize_1: string;
        export { cardSize_1 as cardSize };
        let cardTitle_1: string;
        export { cardTitle_1 as cardTitle };
        let cardDescription_1: string;
        export { cardDescription_1 as cardDescription };
        let backToTrainingText_1: string;
        export { backToTrainingText_1 as backToTrainingText };
        let retryQuizText_1: string;
        export { retryQuizText_1 as retryQuizText };
        let nextModuleText_1: string;
        export { nextModuleText_1 as nextModuleText };
    }
}
import SvnProCardSectionQuizResultLearn from "../components/card/SvnProCardSectionQuizResultLearn.vue";
