declare namespace _default {
    export let title: string;
    export { SvnProCheckbox as component };
    export let tags: string[];
    export namespace argTypes {
        namespace color {
            let control: string;
        }
        namespace label {
            let control_1: string;
            export { control_1 as control };
        }
        namespace error {
            let control_2: string;
            export { control_2 as control };
        }
        namespace disabled {
            let control_3: string;
            export { control_3 as control };
        }
        namespace indeterminate {
            let control_4: string;
            export { control_4 as control };
        }
        namespace rules {
            let control_5: string;
            export { control_5 as control };
        }
    }
    export namespace parameters {
        namespace docs {
            namespace description {
                let component: string;
            }
        }
    }
}
export default _default;
export namespace Playground {
    function render(args: any): {
        components: {
            SvnProCheckbox: import("vue").DefineComponent<{}, {
                $emit: (event: "update:modelValue", ...args: any[]) => void;
                error: boolean;
                label: string;
                color: string;
                disabled: boolean;
                modelValue: boolean;
                rules: unknown[];
                indeterminate: boolean;
                $props: {
                    readonly error?: boolean | undefined;
                    readonly label?: string | undefined;
                    readonly color?: string | undefined;
                    readonly disabled?: boolean | undefined;
                    readonly modelValue?: boolean | undefined;
                    readonly rules?: unknown[] | undefined;
                    readonly indeterminate?: boolean | undefined;
                };
            }, {}, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").PublicProps, Readonly<{}> & Readonly<{}>, {}, {}, {}, {}, string, import("vue").ComponentProvideOptions, true, {}, any>;
        };
        setup(): {
            args: any;
        };
        template: string;
    };
    namespace args {
        let color_1: string;
        export { color_1 as color };
        let label_1: undefined;
        export { label_1 as label };
        let error_1: boolean;
        export { error_1 as error };
        let disabled_1: boolean;
        export { disabled_1 as disabled };
        let indeterminate_1: boolean;
        export { indeterminate_1 as indeterminate };
        let rules_1: never[];
        export { rules_1 as rules };
    }
}
import SvnProCheckbox from "../components/input/SvnProCheckbox.vue";
