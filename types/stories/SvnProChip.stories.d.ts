declare namespace _default {
    export let title: string;
    export { SvnProChip as component };
    export let tags: string[];
    export namespace argTypes {
        namespace variant {
            let control: string;
            let options: string[];
        }
        namespace text {
            let control_1: string;
            export { control_1 as control };
        }
        namespace prependIcon {
            let control_2: string;
            export { control_2 as control };
        }
        namespace appendIcon {
            let control_3: string;
            export { control_3 as control };
        }
        namespace closable {
            let control_4: string;
            export { control_4 as control };
        }
        namespace avatar {
            let control_5: string;
            export { control_5 as control };
        }
        namespace filterIcon {
            let control_6: string;
            export { control_6 as control };
        }
        namespace avatarType {
            let control_7: string;
            export { control_7 as control };
            let options_1: string[];
            export { options_1 as options };
        }
        namespace avatarImage {
            let control_8: string;
            export { control_8 as control };
        }
        namespace disabled {
            let control_9: string;
            export { control_9 as control };
        }
        namespace elevation {
            let control_10: string;
            export { control_10 as control };
        }
    }
    export namespace parameters {
        namespace docs {
            namespace description {
                let component: string;
            }
        }
    }
}
export default _default;
export namespace Playground {
    function render(args: any): {
        components: {
            SvnProChip: {
                new (...args: any[]): import("vue").CreateComponentPublicInstanceWithMixins<Readonly<{}> & Readonly<{}>, {
                    text: string;
                    variant: string;
                    disabled: boolean;
                    avatar: boolean;
                    closable: boolean;
                    avatarType: string;
                    isSlotAppend: boolean;
                    prependIcon?: string | undefined;
                    appendIcon?: string | undefined;
                    elevation?: number | undefined;
                    filterIcon?: string | undefined;
                    avatarImage?: string | undefined;
                    $props: {
                        readonly text?: string | undefined;
                        readonly variant?: string | undefined;
                        readonly disabled?: boolean | undefined;
                        readonly avatar?: boolean | undefined;
                        readonly closable?: boolean | undefined;
                        readonly avatarType?: string | undefined;
                        readonly isSlotAppend?: boolean | undefined;
                        readonly prependIcon?: string | undefined;
                        readonly appendIcon?: string | undefined;
                        readonly elevation?: number | undefined;
                        readonly filterIcon?: string | undefined;
                        readonly avatarImage?: string | undefined;
                    };
                }, {}, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps & Readonly<{}> & Readonly<{}>, {}, true, {}, {}, import("vue").GlobalComponents, import("vue").GlobalDirectives, string, {}, any, import("vue").ComponentProvideOptions, {
                    P: {};
                    B: {};
                    D: {};
                    C: {};
                    M: {};
                    Defaults: {};
                }, Readonly<{}> & Readonly<{}>, {
                    text: string;
                    variant: string;
                    disabled: boolean;
                    avatar: boolean;
                    closable: boolean;
                    avatarType: string;
                    isSlotAppend: boolean;
                    prependIcon?: string | undefined;
                    appendIcon?: string | undefined;
                    elevation?: number | undefined;
                    filterIcon?: string | undefined;
                    avatarImage?: string | undefined;
                    $props: {
                        readonly text?: string | undefined;
                        readonly variant?: string | undefined;
                        readonly disabled?: boolean | undefined;
                        readonly avatar?: boolean | undefined;
                        readonly closable?: boolean | undefined;
                        readonly avatarType?: string | undefined;
                        readonly isSlotAppend?: boolean | undefined;
                        readonly prependIcon?: string | undefined;
                        readonly appendIcon?: string | undefined;
                        readonly elevation?: number | undefined;
                        readonly filterIcon?: string | undefined;
                        readonly avatarImage?: string | undefined;
                    };
                }, {}, {}, {}, {}>;
                __isFragment?: never;
                __isTeleport?: never;
                __isSuspense?: never;
            } & import("vue").ComponentOptionsBase<Readonly<{}> & Readonly<{}>, {
                text: string;
                variant: string;
                disabled: boolean;
                avatar: boolean;
                closable: boolean;
                avatarType: string;
                isSlotAppend: boolean;
                prependIcon?: string | undefined;
                appendIcon?: string | undefined;
                elevation?: number | undefined;
                filterIcon?: string | undefined;
                avatarImage?: string | undefined;
                $props: {
                    readonly text?: string | undefined;
                    readonly variant?: string | undefined;
                    readonly disabled?: boolean | undefined;
                    readonly avatar?: boolean | undefined;
                    readonly closable?: boolean | undefined;
                    readonly avatarType?: string | undefined;
                    readonly isSlotAppend?: boolean | undefined;
                    readonly prependIcon?: string | undefined;
                    readonly appendIcon?: string | undefined;
                    readonly elevation?: number | undefined;
                    readonly filterIcon?: string | undefined;
                    readonly avatarImage?: string | undefined;
                };
            }, {}, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, {}, {}, string, {}, import("vue").GlobalComponents, import("vue").GlobalDirectives, string, import("vue").ComponentProvideOptions> & import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps & (new () => {
                $slots: {
                    prepend?(_: {}): any;
                    append?(_: {}): any;
                };
            });
        };
        setup(): {
            args: any;
        };
        template: string;
    };
    namespace args {
        let variant_1: string;
        export { variant_1 as variant };
        let text_1: string;
        export { text_1 as text };
        let prependIcon_1: undefined;
        export { prependIcon_1 as prependIcon };
        let appendIcon_1: undefined;
        export { appendIcon_1 as appendIcon };
        let closable_1: boolean;
        export { closable_1 as closable };
        let avatar_1: boolean;
        export { avatar_1 as avatar };
        let filterIcon_1: undefined;
        export { filterIcon_1 as filterIcon };
        let disabled_1: boolean;
        export { disabled_1 as disabled };
        let elevation_1: undefined;
        export { elevation_1 as elevation };
        let avatarType_1: string;
        export { avatarType_1 as avatarType };
        let avatarImage_1: undefined;
        export { avatarImage_1 as avatarImage };
    }
}
export namespace ChipGroup {
    export function render_1(args: any): {
        components: {
            SvnChipTest: import("vue").DefineComponent<{}, {
                $emit: (event: "update:modelValue", ...args: any[]) => void;
                text: string;
                variant: string;
                disabled: boolean;
                chips: unknown[];
                avatar: boolean;
                closable: boolean;
                avatarType: string;
                prependIcon?: string | undefined;
                appendIcon?: string | undefined;
                elevation?: number | undefined;
                filterIcon?: string | undefined;
                avatarImage?: string | undefined;
                $props: {
                    readonly text?: string | undefined;
                    readonly variant?: string | undefined;
                    readonly disabled?: boolean | undefined;
                    readonly chips?: unknown[] | undefined;
                    readonly avatar?: boolean | undefined;
                    readonly closable?: boolean | undefined;
                    readonly avatarType?: string | undefined;
                    readonly prependIcon?: string | undefined;
                    readonly appendIcon?: string | undefined;
                    readonly elevation?: number | undefined;
                    readonly filterIcon?: string | undefined;
                    readonly avatarImage?: string | undefined;
                };
            }, {}, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").PublicProps, Readonly<{}> & Readonly<{}>, {}, {}, {}, {}, string, import("vue").ComponentProvideOptions, true, {}, any>;
        };
        setup(): {
            args: any;
        };
        template: string;
    };
    export { render_1 as render };
    export namespace args_1 {
        let variant_2: string;
        export { variant_2 as variant };
        export let chips: string[];
        let prependIcon_2: undefined;
        export { prependIcon_2 as prependIcon };
        let appendIcon_2: undefined;
        export { appendIcon_2 as appendIcon };
        let closable_2: boolean;
        export { closable_2 as closable };
        let avatar_2: boolean;
        export { avatar_2 as avatar };
        let filterIcon_2: undefined;
        export { filterIcon_2 as filterIcon };
        let disabled_2: boolean;
        export { disabled_2 as disabled };
        let elevation_2: undefined;
        export { elevation_2 as elevation };
        let avatarType_2: string;
        export { avatarType_2 as avatarType };
        let avatarImage_2: undefined;
        export { avatarImage_2 as avatarImage };
    }
    export { args_1 as args };
}
import SvnProChip from "../components/chip/SvnProChip.vue";
