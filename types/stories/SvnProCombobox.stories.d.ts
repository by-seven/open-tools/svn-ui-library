declare namespace _default {
    export let title: string;
    export { SvnProCombobox as component };
    export let tags: string[];
    export namespace argTypes {
        namespace modelValue {
            let control: string;
        }
        namespace menuProps {
            let control_1: string;
            export { control_1 as control };
        }
        namespace items {
            let control_2: string;
            export { control_2 as control };
        }
        namespace itemTitle {
            let control_3: string;
            export { control_3 as control };
        }
        namespace itemValue {
            let control_4: string;
            export { control_4 as control };
        }
        namespace prependInnerIcon {
            let control_5: string;
            export { control_5 as control };
        }
        namespace label {
            let control_6: string;
            export { control_6 as control };
        }
        namespace multiple {
            let control_7: string;
            export { control_7 as control };
        }
        namespace disabled {
            let control_8: string;
            export { control_8 as control };
        }
        namespace messages {
            let control_9: string;
            export { control_9 as control };
        }
        namespace withSelectAll {
            let control_10: string;
            export { control_10 as control };
        }
        namespace error {
            let control_11: string;
            export { control_11 as control };
        }
    }
    export namespace parameters {
        namespace docs {
            namespace description {
                let component: string;
            }
        }
    }
}
export default _default;
export namespace Playground {
    function render(args: any): {
        components: {
            SvnProCombobox: import("vue").DefineComponent<{}, {
                $emit: (event: "input" | "clear" | "update:modelValue" | "on-intersect" | "intersect", ...args: any[]) => void;
                error: boolean;
                label: string;
                disabled: boolean;
                modelValue: null;
                prependInnerIcon: string;
                multiple: boolean;
                items: unknown[];
                itemTitle: string;
                itemValue: string;
                menuProps: Record<string, any>;
                withSelectAll: boolean;
                messages?: string | undefined;
                $props: {
                    readonly error?: boolean | undefined;
                    readonly label?: string | undefined;
                    readonly disabled?: boolean | undefined;
                    readonly modelValue?: null | undefined;
                    readonly prependInnerIcon?: string | undefined;
                    readonly multiple?: boolean | undefined;
                    readonly items?: unknown[] | undefined;
                    readonly itemTitle?: string | undefined;
                    readonly itemValue?: string | undefined;
                    readonly menuProps?: Record<string, any> | undefined;
                    readonly withSelectAll?: boolean | undefined;
                    readonly messages?: string | undefined;
                };
            }, {}, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").PublicProps, Readonly<{}> & Readonly<{}>, {}, {}, {}, {}, string, import("vue").ComponentProvideOptions, true, {}, any>;
        };
        setup(): {
            args: any;
        };
        template: string;
    };
    namespace args {
        let modelValue_1: undefined;
        export { modelValue_1 as modelValue };
        let menuProps_1: undefined;
        export { menuProps_1 as menuProps };
        export { items };
        let itemTitle_1: string;
        export { itemTitle_1 as itemTitle };
        let itemValue_1: string;
        export { itemValue_1 as itemValue };
        let prependInnerIcon_1: undefined;
        export { prependInnerIcon_1 as prependInnerIcon };
        let label_1: string;
        export { label_1 as label };
        let multiple_1: boolean;
        export { multiple_1 as multiple };
        let disabled_1: boolean;
        export { disabled_1 as disabled };
        let messages_1: undefined;
        export { messages_1 as messages };
        let withSelectAll_1: boolean;
        export { withSelectAll_1 as withSelectAll };
        let error_1: boolean;
        export { error_1 as error };
    }
}
import SvnProCombobox from "../components/input/SvnProCombobox.vue";
declare const items_1: {
    title: string;
    value: string;
}[];
