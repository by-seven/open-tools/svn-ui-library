declare namespace _default {
    export let title: string;
    export { SvnDataTableTest as component };
    export let tags: string[];
    export namespace argTypes {
        namespace headers {
            let control: string;
        }
        namespace items {
            let control_1: string;
            export { control_1 as control };
        }
        namespace density {
            let control_2: string;
            export { control_2 as control };
            export let options: string[];
        }
        namespace itemValue {
            let control_3: string;
            export { control_3 as control };
        }
        namespace showSelect {
            let control_4: string;
            export { control_4 as control };
        }
        namespace page {
            let control_5: string;
            export { control_5 as control };
        }
        namespace itemsPerPage {
            let control_6: string;
            export { control_6 as control };
        }
        namespace paginationTotalPages {
            let control_7: string;
            export { control_7 as control };
        }
    }
    export namespace parameters {
        namespace docs {
            namespace description {
                let component: string;
            }
        }
    }
}
export default _default;
export namespace Playground {
    function render(args: any): {
        components: {
            SvnDataTableTest: import("vue").DefineComponent<{}, {
                modelValue: null;
                density: string;
                items: unknown[];
                itemValue: string;
                page: number;
                paginationTotalPages: number;
                showSelect: boolean;
                itemsPerPage: number;
                headers: unknown[];
                $props: {
                    readonly modelValue?: null | undefined;
                    readonly density?: string | undefined;
                    readonly items?: unknown[] | undefined;
                    readonly itemValue?: string | undefined;
                    readonly page?: number | undefined;
                    readonly paginationTotalPages?: number | undefined;
                    readonly showSelect?: boolean | undefined;
                    readonly itemsPerPage?: number | undefined;
                    readonly headers?: unknown[] | undefined;
                };
            }, {}, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").PublicProps, Readonly<{}> & Readonly<{}>, {}, {}, {}, {}, string, import("vue").ComponentProvideOptions, true, {}, any>;
        };
        setup(): {
            args: any;
        };
        template: string;
    };
    namespace args {
        let itemValue_1: string;
        export { itemValue_1 as itemValue };
        let density_1: string;
        export { density_1 as density };
        let showSelect_1: boolean;
        export { showSelect_1 as showSelect };
        let page_1: number;
        export { page_1 as page };
        let itemsPerPage_1: number;
        export { itemsPerPage_1 as itemsPerPage };
        let paginationTotalPages_1: number;
        export { paginationTotalPages_1 as paginationTotalPages };
        let headers_1: {
            align: string;
            key: string;
            sortable: boolean;
            title: string;
        }[];
        export { headers_1 as headers };
        let items_1: {
            title: string;
            avatar: string;
            firstname: string;
            lastname: string;
            jobTitle: string;
            salary: string;
            skills: string[];
            email: string;
            emoji: string;
            icon: string;
        }[];
        export { items_1 as items };
    }
}
import SvnDataTableTest from "./Components/SvnDataTableTest.vue";
