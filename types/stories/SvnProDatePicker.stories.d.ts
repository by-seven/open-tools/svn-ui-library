declare namespace _default {
    export let title: string;
    export { SvnProDatePicker as component };
    export let tags: string[];
    export namespace argTypes {
        export namespace modelValue {
            let control: string;
        }
        export namespace color {
            let control_1: string;
            export { control_1 as control };
        }
        export namespace title_1 {
            let control_2: string;
            export { control_2 as control };
        }
        export { title_1 as title };
        export namespace disabled {
            let control_3: string;
            export { control_3 as control };
        }
        export namespace actions {
            let control_4: string;
            export { control_4 as control };
        }
        export namespace range {
            let control_5: string;
            export { control_5 as control };
        }
    }
    export namespace parameters {
        namespace docs {
            namespace description {
                let component: string;
            }
        }
    }
}
export default _default;
export namespace Playground {
    function render(args: any): {
        components: {
            SvnProDatePicker: {
                new (...args: any[]): import("vue").CreateComponentPublicInstanceWithMixins<Readonly<{}> & Readonly<{}>, {
                    $emit: (event: "update:modelValue", ...args: any[]) => void;
                    title: string;
                    color: string;
                    disabled: boolean;
                    modelValue: null;
                    range: boolean;
                    actions: boolean;
                    $props: {
                        readonly title?: string | undefined;
                        readonly color?: string | undefined;
                        readonly disabled?: boolean | undefined;
                        readonly modelValue?: null | undefined;
                        readonly range?: boolean | undefined;
                        readonly actions?: boolean | undefined;
                    };
                }, {}, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps & Readonly<{}> & Readonly<{}>, {}, true, {}, {}, import("vue").GlobalComponents, import("vue").GlobalDirectives, string, {}, any, import("vue").ComponentProvideOptions, {
                    P: {};
                    B: {};
                    D: {};
                    C: {};
                    M: {};
                    Defaults: {};
                }, Readonly<{}> & Readonly<{}>, {
                    $emit: (event: "update:modelValue", ...args: any[]) => void;
                    title: string;
                    color: string;
                    disabled: boolean;
                    modelValue: null;
                    range: boolean;
                    actions: boolean;
                    $props: {
                        readonly title?: string | undefined;
                        readonly color?: string | undefined;
                        readonly disabled?: boolean | undefined;
                        readonly modelValue?: null | undefined;
                        readonly range?: boolean | undefined;
                        readonly actions?: boolean | undefined;
                    };
                }, {}, {}, {}, {}>;
                __isFragment?: never;
                __isTeleport?: never;
                __isSuspense?: never;
            } & import("vue").ComponentOptionsBase<Readonly<{}> & Readonly<{}>, {
                $emit: (event: "update:modelValue", ...args: any[]) => void;
                title: string;
                color: string;
                disabled: boolean;
                modelValue: null;
                range: boolean;
                actions: boolean;
                $props: {
                    readonly title?: string | undefined;
                    readonly color?: string | undefined;
                    readonly disabled?: boolean | undefined;
                    readonly modelValue?: null | undefined;
                    readonly range?: boolean | undefined;
                    readonly actions?: boolean | undefined;
                };
            }, {}, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, {}, {}, string, {}, import("vue").GlobalComponents, import("vue").GlobalDirectives, string, import("vue").ComponentProvideOptions> & import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps & (new () => {
                $slots: {
                    actions?(_: {}): any;
                };
            });
        };
        setup(): {
            args: any;
        };
        template: string;
    };
    namespace args {
        let modelValue_1: undefined;
        export { modelValue_1 as modelValue };
        let color_1: string;
        export { color_1 as color };
        let title_2: string;
        export { title_2 as title };
        let range_1: boolean;
        export { range_1 as range };
        let actions_1: boolean;
        export { actions_1 as actions };
        let disabled_1: boolean;
        export { disabled_1 as disabled };
    }
}
import SvnProDatePicker from "../components/picker/SvnProDatePicker.vue";
