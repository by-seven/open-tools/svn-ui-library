declare namespace _default {
    export let title: string;
    export { SvnProDialogUploadImage as component };
    export let tags: string[];
    export let argTypes: {};
    export namespace parameters {
        namespace docs {
            namespace description {
                let component: string;
            }
        }
    }
}
export default _default;
export namespace Playground {
    function render(args: any): {
        components: {
            SvnProDialogUploadImage: {
                new (...args: any[]): import("vue").CreateComponentPublicInstanceWithMixins<Readonly<{}> & Readonly<{}>, {
                    dialogUpload: import("vue").Ref<boolean, boolean>;
                    $emit: (event: "error" | "file-uploaded", ...args: any[]) => void;
                }, {}, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps & Readonly<{}> & Readonly<{}>, {}, true, {}, {}, import("vue").GlobalComponents, import("vue").GlobalDirectives, string, {}, any, import("vue").ComponentProvideOptions, {
                    P: {};
                    B: {};
                    D: {};
                    C: {};
                    M: {};
                    Defaults: {};
                }, Readonly<{}> & Readonly<{}>, {
                    dialogUpload: import("vue").Ref<boolean, boolean>;
                    $emit: (event: "error" | "file-uploaded", ...args: any[]) => void;
                }, {}, {}, {}, {}>;
                __isFragment?: never;
                __isTeleport?: never;
                __isSuspense?: never;
            } & import("vue").ComponentOptionsBase<Readonly<{}> & Readonly<{}>, {
                dialogUpload: import("vue").Ref<boolean, boolean>;
                $emit: (event: "error" | "file-uploaded", ...args: any[]) => void;
            }, {}, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, {}, {}, string, {}, import("vue").GlobalComponents, import("vue").GlobalDirectives, string, import("vue").ComponentProvideOptions> & import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps & (new () => {
                $slots: {
                    activator?(_: {
                        isActive: any;
                    }): any;
                };
            });
        };
        setup(): {
            args: any;
        };
        template: string;
    };
    let args: {};
}
import SvnProDialogUploadImage from "../components/dialog/SvnProDialogUploadImage.vue";
