declare namespace _default {
    export let title: string;
    export { SvnProDivider as component };
    export let tags: string[];
    export namespace argTypes {
        namespace color {
            let control: string;
        }
        namespace vertical {
            let control_1: string;
            export { control_1 as control };
        }
        namespace inset {
            let control_2: string;
            export { control_2 as control };
        }
        namespace thickness {
            let control_3: string;
            export { control_3 as control };
        }
    }
    export namespace parameters {
        namespace docs {
            namespace description {
                let component: string;
            }
        }
    }
}
export default _default;
export namespace Playground {
    function render(args: any): {
        components: {
            SvnProDivider: import("vue").DefineComponent<{}, {
                color: string;
                vertical: boolean;
                inset: boolean;
                thickness: number;
                $props: {
                    readonly color?: string | undefined;
                    readonly vertical?: boolean | undefined;
                    readonly inset?: boolean | undefined;
                    readonly thickness?: number | undefined;
                };
            }, {}, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").PublicProps, Readonly<{}> & Readonly<{}>, {}, {}, {}, {}, string, import("vue").ComponentProvideOptions, true, {}, any>;
        };
        setup(): {
            args: any;
        };
        template: string;
    };
    namespace args {
        let color_1: string;
        export { color_1 as color };
        let vertical_1: boolean;
        export { vertical_1 as vertical };
        let inset_1: boolean;
        export { inset_1 as inset };
        let thickness_1: number;
        export { thickness_1 as thickness };
    }
}
import SvnProDivider from "../components/divider/SvnProDivider.vue";
