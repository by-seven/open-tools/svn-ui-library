declare namespace _default {
    export let title: string;
    export { SvnProEmptyStates as component };
    export let tags: string[];
    export namespace argTypes {
        export namespace variant {
            let control: string;
            let options: string[];
        }
        export namespace size {
            let control_1: string;
            export { control_1 as control };
            let options_1: string[];
            export { options_1 as options };
        }
        export namespace title_1 {
            let control_2: string;
            export { control_2 as control };
        }
        export { title_1 as title };
        export namespace supportingText {
            let control_3: string;
            export { control_3 as control };
        }
        export namespace actionPrimaryTitle {
            let control_4: string;
            export { control_4 as control };
        }
        export namespace actionSecondaryTitle {
            let control_5: string;
            export { control_5 as control };
        }
        export namespace actions {
            let control_6: string;
            export { control_6 as control };
        }
    }
    export namespace parameters {
        namespace docs {
            namespace description {
                let component: string;
            }
        }
    }
}
export default _default;
export namespace Playground {
    function render(args: any): {
        components: {
            SvnProEmptyStates: import("vue").DefineComponent<{}, {
                $emit: (event: "update:modelValue" | "click-primary" | "click-secondary", ...args: any[]) => void;
                variant: string;
                size: string;
                actions: boolean;
                supportingText: string;
                title?: string | undefined;
                icon?: string | undefined;
                prependSecondary?: string | undefined;
                prependPrimary?: string | undefined;
                actionPrimaryTitle?: string | undefined;
                actionSecondaryTitle?: string | undefined;
                $props: {
                    readonly variant?: string | undefined;
                    readonly size?: string | undefined;
                    readonly actions?: boolean | undefined;
                    readonly supportingText?: string | undefined;
                    readonly title?: string | undefined;
                    readonly icon?: string | undefined;
                    readonly prependSecondary?: string | undefined;
                    readonly prependPrimary?: string | undefined;
                    readonly actionPrimaryTitle?: string | undefined;
                    readonly actionSecondaryTitle?: string | undefined;
                };
            }, {}, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").PublicProps, Readonly<{}> & Readonly<{}>, {}, {}, {}, {}, string, import("vue").ComponentProvideOptions, true, {}, any>;
        };
        setup(): {
            args: any;
        };
        template: string;
    };
    namespace args {
        let variant_1: string;
        export { variant_1 as variant };
        let size_1: string;
        export { size_1 as size };
        let title_2: undefined;
        export { title_2 as title };
        let supportingText_1: undefined;
        export { supportingText_1 as supportingText };
        let actionPrimaryTitle_1: undefined;
        export { actionPrimaryTitle_1 as actionPrimaryTitle };
        let actionSecondaryTitle_1: undefined;
        export { actionSecondaryTitle_1 as actionSecondaryTitle };
        let actions_1: boolean;
        export { actions_1 as actions };
    }
}
import SvnProEmptyStates from "../components/states/SvnProEmptyStates.vue";
