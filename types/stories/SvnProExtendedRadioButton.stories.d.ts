declare namespace _default {
    export let title: string;
    export { SvnProExtendedRadioButton as component };
    export let tags: string[];
    export namespace argTypes {
        export namespace icon {
            let control: string;
        }
        export namespace title_1 {
            let control_1: string;
            export { control_1 as control };
        }
        export { title_1 as title };
        export namespace subtitle {
            let control_2: string;
            export { control_2 as control };
        }
        export namespace disabled {
            let control_3: string;
            export { control_3 as control };
        }
        export namespace value {
            let control_4: string;
            export { control_4 as control };
        }
        export namespace items {
            let control_5: string;
            export { control_5 as control };
        }
    }
    export namespace parameters {
        namespace docs {
            namespace description {
                let component: string;
            }
        }
    }
}
export default _default;
export namespace Playground {
    function render(args: any): {
        components: {
            SvnProExtendedRadioButton: import("vue").DefineComponent<{}, {
                $emit: (event: "item-clicked", ...args: any[]) => void;
                icon: string;
                disabled: boolean;
                title?: string | undefined;
                subtitle?: string | undefined;
                value?: string | undefined;
                $props: {
                    readonly icon?: string | undefined;
                    readonly disabled?: boolean | undefined;
                    readonly title?: string | undefined;
                    readonly subtitle?: string | undefined;
                    readonly value?: string | undefined;
                };
            }, {}, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").PublicProps, Readonly<{}> & Readonly<{}>, {}, {}, {}, {}, string, import("vue").ComponentProvideOptions, true, {}, any>;
        };
        setup(): {
            args: any;
        };
        template: string;
    };
    namespace args {
        let icon_1: string;
        export { icon_1 as icon };
        let title_2: string;
        export { title_2 as title };
        let subtitle_1: string;
        export { subtitle_1 as subtitle };
        let disabled_1: boolean;
        export { disabled_1 as disabled };
        let value_1: undefined;
        export { value_1 as value };
    }
}
export namespace MultipleExtendedRadioButton {
    export function render_1(args: any): {
        components: {
            SvnProExtendedRadioButtonTest: import("vue").DefineComponent<{}, {
                $emit: (event: "item-clicked", ...args: any[]) => void;
                items: unknown[];
                $props: {
                    readonly items?: unknown[] | undefined;
                };
            }, {}, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").PublicProps, Readonly<{}> & Readonly<{}>, {}, {}, {}, {}, string, import("vue").ComponentProvideOptions, true, {}, any>;
        };
        setup(): {
            args: any;
        };
        template: string;
    };
    export { render_1 as render };
    export namespace args_1 {
        export { items };
    }
    export { args_1 as args };
}
import SvnProExtendedRadioButton from "../components/button/SvnProExtendedRadioButton.vue";
declare const items_1: {
    value: string;
    icon: string;
    title: string;
    subtitle: string;
    disabled: boolean;
}[];
