declare namespace _default {
    export let title: string;
    export { SvnProFloatingActionButton as component };
    export let tags: string[];
    export namespace argTypes {
        namespace extended {
            let control: string;
        }
        namespace toTop {
            let control_1: string;
            export { control_1 as control };
        }
        namespace text {
            let control_2: string;
            export { control_2 as control };
        }
        namespace variant {
            let control_3: string;
            export { control_3 as control };
            export let options: string[];
        }
        namespace icon {
            let control_4: string;
            export { control_4 as control };
        }
        namespace size {
            let control_5: string;
            export { control_5 as control };
            let options_1: string[];
            export { options_1 as options };
        }
    }
    export namespace parameters {
        namespace docs {
            namespace description {
                let component: string;
            }
        }
    }
}
export default _default;
export namespace Playground {
    function render(args: any): {
        components: {
            SvnProFloatingActionButton: import("vue").DefineComponent<{}, {
                $emit: (event: "click", ...args: any[]) => void;
                text: string;
                variant: string;
                icon: string;
                pageRatioDisplay: number;
                toTop: boolean;
                size: string;
                extended: boolean;
                $props: {
                    readonly text?: string | undefined;
                    readonly variant?: string | undefined;
                    readonly icon?: string | undefined;
                    readonly pageRatioDisplay?: number | undefined;
                    readonly toTop?: boolean | undefined;
                    readonly size?: string | undefined;
                    readonly extended?: boolean | undefined;
                };
            }, {}, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").PublicProps, Readonly<{}> & Readonly<{}>, {}, {}, {}, {}, string, import("vue").ComponentProvideOptions, true, {}, any>;
        };
        setup(): {
            args: any;
        };
        template: string;
    };
    namespace args {
        let extended_1: boolean;
        export { extended_1 as extended };
        let toTop_1: boolean;
        export { toTop_1 as toTop };
        let text_1: string;
        export { text_1 as text };
        let variant_1: string;
        export { variant_1 as variant };
        let icon_1: string;
        export { icon_1 as icon };
        let size_1: string;
        export { size_1 as size };
    }
}
import SvnProFloatingActionButton from "../components/button/SvnProFloatingActionButton.vue";
