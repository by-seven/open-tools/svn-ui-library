declare namespace _default {
    export let title: string;
    export { SvnProGraphTarget as component };
    export let tags: string[];
    export namespace argTypes {
        namespace progress {
            let control: string;
            let options: string[];
        }
        namespace indicatorType {
            let control_1: string;
            export { control_1 as control };
            let options_1: string[];
            export { options_1 as options };
        }
        namespace currentValue {
            let control_2: string;
            export { control_2 as control };
        }
        namespace targetValue {
            let control_3: string;
            export { control_3 as control };
        }
        namespace notSetText {
            let control_4: string;
            export { control_4 as control };
        }
    }
    export namespace parameters {
        namespace docs {
            namespace description {
                let component: string;
            }
        }
    }
}
export default _default;
export namespace Playground {
    function render(args: any): {
        components: {
            SvnProGraphTarget: import("vue").DefineComponent<{}, {
                progress: string;
                indicatorType: string;
                notSetText: string;
                currentValue?: string | undefined;
                targetValue?: string | undefined;
                $props: {
                    readonly progress?: string | undefined;
                    readonly indicatorType?: string | undefined;
                    readonly notSetText?: string | undefined;
                    readonly currentValue?: string | undefined;
                    readonly targetValue?: string | undefined;
                };
            }, {}, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").PublicProps, Readonly<{}> & Readonly<{}>, {}, {}, {}, {}, string, import("vue").ComponentProvideOptions, true, {}, any>;
        };
        setup(): {
            args: any;
        };
        template: string;
    };
    namespace args {
        let progress_1: string;
        export { progress_1 as progress };
        let currentValue_1: string;
        export { currentValue_1 as currentValue };
        let targetValue_1: string;
        export { targetValue_1 as targetValue };
        let notSetText_1: string;
        export { notSetText_1 as notSetText };
        let indicatorType_1: string;
        export { indicatorType_1 as indicatorType };
    }
}
import SvnProGraphTarget from "../components/progress/SvnProGraphTarget.vue";
