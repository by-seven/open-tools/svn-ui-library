declare namespace _default {
    export let title: string;
    export { SvnProIconButton as component };
    export let tags: string[];
    export namespace argTypes {
        namespace icon {
            let control: string;
        }
        namespace color {
            let control_1: string;
            export { control_1 as control };
        }
        namespace disabled {
            let control_2: string;
            export { control_2 as control };
        }
        namespace toggle {
            let control_3: string;
            export { control_3 as control };
        }
        namespace variant {
            let control_4: string;
            export { control_4 as control };
            export let options: string[];
        }
    }
    export namespace parameters {
        namespace docs {
            namespace description {
                let component: string;
            }
        }
    }
}
export default _default;
export namespace Playground {
    function render(args: any): {
        components: {
            SvnProIconButton: import("vue").DefineComponent<{}, {
                toggle: boolean;
                variant: string;
                icon: string;
                color: string;
                disabled: boolean;
                rounded: string;
                $props: {
                    readonly toggle?: boolean | undefined;
                    readonly variant?: string | undefined;
                    readonly icon?: string | undefined;
                    readonly color?: string | undefined;
                    readonly disabled?: boolean | undefined;
                    readonly rounded?: string | undefined;
                };
            }, {}, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").PublicProps, Readonly<{}> & Readonly<{}>, {}, {}, {}, {}, string, import("vue").ComponentProvideOptions, true, {}, any>;
        };
        setup(): {
            args: any;
        };
        template: string;
    };
    namespace args {
        let icon_1: string;
        export { icon_1 as icon };
        let variant_1: string;
        export { variant_1 as variant };
        let color_1: string;
        export { color_1 as color };
        let disabled_1: boolean;
        export { disabled_1 as disabled };
        let toggle_1: boolean;
        export { toggle_1 as toggle };
    }
}
import SvnProIconButton from "../components/button/SvnProIconButton.vue";
