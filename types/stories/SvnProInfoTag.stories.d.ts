declare namespace _default {
    export let title: string;
    export { SvnProInfoTag as component };
    export let tags: string[];
    export namespace argTypes {
        namespace text {
            let control: string;
        }
        namespace tagType {
            let control_1: string;
            export { control_1 as control };
            export let options: string[];
        }
        namespace tagSize {
            let control_2: string;
            export { control_2 as control };
            let options_1: string[];
            export { options_1 as options };
        }
        namespace tagStyle {
            let control_3: string;
            export { control_3 as control };
            let options_2: string[];
            export { options_2 as options };
        }
        namespace leadingItem {
            let control_4: string;
            export { control_4 as control };
            let options_3: (string | undefined)[];
            export { options_3 as options };
        }
        namespace icon {
            let control_5: string;
            export { control_5 as control };
        }
    }
    export namespace parameters {
        namespace docs {
            namespace description {
                let component: string;
            }
        }
    }
}
export default _default;
export namespace Playground {
    function render(args: any): {
        components: {
            SvnProInfoTag: import("vue").DefineComponent<{}, {
                tagType: string;
                tagSize: string;
                tagStyle: string;
                breakAll: boolean;
                text?: string | undefined;
                icon?: string | undefined;
                leadingItem?: string | undefined;
                $props: {
                    readonly tagType?: string | undefined;
                    readonly tagSize?: string | undefined;
                    readonly tagStyle?: string | undefined;
                    readonly breakAll?: boolean | undefined;
                    readonly text?: string | undefined;
                    readonly icon?: string | undefined;
                    readonly leadingItem?: string | undefined;
                };
            }, {}, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").PublicProps, Readonly<{}> & Readonly<{}>, {}, {}, {}, {}, string, import("vue").ComponentProvideOptions, true, {}, any>;
        };
        setup(): {
            args: any;
        };
        template: string;
    };
    namespace args {
        let text_1: string;
        export { text_1 as text };
        let tagType_1: string;
        export { tagType_1 as tagType };
        let tagSize_1: string;
        export { tagSize_1 as tagSize };
        let tagStyle_1: string;
        export { tagStyle_1 as tagStyle };
        let leadingItem_1: undefined;
        export { leadingItem_1 as leadingItem };
        let icon_1: undefined;
        export { icon_1 as icon };
    }
}
import SvnProInfoTag from "../components/chip/SvnProInfoTag.vue";
