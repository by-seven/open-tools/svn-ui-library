declare namespace _default {
    export let title: string;
    export { SvnProLearnBadge as component };
    export let tags: string[];
    export namespace argTypes {
        namespace count {
            let control: string;
        }
        namespace levels {
            let control_1: string;
            export { control_1 as control };
        }
        namespace badgeType {
            let control_2: string;
            export { control_2 as control };
            export let options: string[];
        }
    }
    export namespace parameters {
        namespace docs {
            namespace description {
                let component: string;
            }
        }
    }
}
export default _default;
export namespace Playground {
    function render(args: any): {
        components: {
            SvnProLearnBadge: import("vue").DefineComponent<{}, {
                levels: Record<string, any>;
                badgeType: string;
                count?: number | undefined;
                $props: {
                    readonly levels?: Record<string, any> | undefined;
                    readonly badgeType?: string | undefined;
                    readonly count?: number | undefined;
                };
            }, {}, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").PublicProps, Readonly<{}> & Readonly<{}>, {}, {}, {}, {}, string, import("vue").ComponentProvideOptions, true, {}, any>;
        };
        setup(): {
            args: any;
        };
        template: string;
    };
    namespace args {
        let count_1: number;
        export { count_1 as count };
        let badgeType_1: string;
        export { badgeType_1 as badgeType };
        export namespace levels_1 {
            let levelOne: number;
            let levelTwo: number;
            let levelThree: number;
            let levelFour: number;
            let titleOne: string;
            let titleTwo: string;
            let titleThree: string;
            let titleFour: string;
        }
        export { levels_1 as levels };
    }
}
import SvnProLearnBadge from "../components/chip/SvnProLearnBadge.vue";
