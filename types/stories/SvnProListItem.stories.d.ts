declare namespace _default {
    export let title: string;
    export { SvnProListItem as component };
    export let tags: string[];
    export namespace argTypes {
        export namespace title_1 {
            let control: string;
        }
        export { title_1 as title };
        export namespace subtitle {
            let control_1: string;
            export { control_1 as control };
        }
        export namespace value {
            let control_2: string;
            export { control_2 as control };
        }
        export namespace density {
            let control_3: string;
            export { control_3 as control };
            export let options: string[];
        }
        export namespace minHeight {
            let control_4: string;
            export { control_4 as control };
        }
        export namespace lines {
            let control_5: string;
            export { control_5 as control };
            let options_1: string[];
            export { options_1 as options };
        }
        export namespace leading {
            let control_6: string;
            export { control_6 as control };
            let options_2: string[];
            export { options_2 as options };
        }
        export namespace trailing {
            let control_7: string;
            export { control_7 as control };
            let options_3: string[];
            export { options_3 as options };
        }
        export namespace leadingIcon {
            let control_8: string;
            export { control_8 as control };
        }
        export namespace trailingIcon {
            let control_9: string;
            export { control_9 as control };
        }
        export namespace leadingColor {
            let control_10: string;
            export { control_10 as control };
        }
        export namespace trailingColor {
            let control_11: string;
            export { control_11 as control };
        }
        export namespace leadingMedia {
            let control_12: string;
            export { control_12 as control };
        }
        export namespace leadingFirstname {
            let control_13: string;
            export { control_13 as control };
        }
    }
    export namespace parameters {
        namespace docs {
            namespace description {
                let component: string;
            }
        }
    }
}
export default _default;
export namespace Playground {
    function render(args: any): {
        components: {
            SvnProListItem: import("vue").DefineComponent<{}, {
                minHeight: number;
                start: boolean;
                density: string;
                leading: string;
                trailing: string;
                leadingIcon: string;
                trailingIcon: string;
                leadingColor: string;
                trailingColor: string;
                title?: string | undefined;
                subtitle?: string | undefined;
                value?: undefined;
                lines?: string | boolean | undefined;
                leadingMedia?: string | undefined;
                leadingFirstname?: string | undefined;
                $props: {
                    readonly minHeight?: number | undefined;
                    readonly start?: boolean | undefined;
                    readonly density?: string | undefined;
                    readonly leading?: string | undefined;
                    readonly trailing?: string | undefined;
                    readonly leadingIcon?: string | undefined;
                    readonly trailingIcon?: string | undefined;
                    readonly leadingColor?: string | undefined;
                    readonly trailingColor?: string | undefined;
                    readonly title?: string | undefined;
                    readonly subtitle?: string | undefined;
                    readonly value?: undefined;
                    readonly lines?: string | boolean | undefined;
                    readonly leadingMedia?: string | undefined;
                    readonly leadingFirstname?: string | undefined;
                };
            }, {}, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").PublicProps, Readonly<{}> & Readonly<{}>, {}, {}, {}, {}, string, import("vue").ComponentProvideOptions, true, {}, any>;
        };
        setup(): {
            args: any;
        };
        template: string;
    };
    namespace args {
        let title_2: undefined;
        export { title_2 as title };
        let subtitle_1: undefined;
        export { subtitle_1 as subtitle };
        let value_1: undefined;
        export { value_1 as value };
        let lines_1: undefined;
        export { lines_1 as lines };
        let leadingMedia_1: undefined;
        export { leadingMedia_1 as leadingMedia };
        let leadingIcon_1: string;
        export { leadingIcon_1 as leadingIcon };
        let leadingFirstname_1: undefined;
        export { leadingFirstname_1 as leadingFirstname };
        let leading_1: string;
        export { leading_1 as leading };
        let trailing_1: string;
        export { trailing_1 as trailing };
        let trailingIcon_1: string;
        export { trailingIcon_1 as trailingIcon };
        let trailingColor_1: string;
        export { trailingColor_1 as trailingColor };
    }
}
export namespace ListItemWithList {
    export function render_1(args: any): {
        components: {
            SvnListTest: import("vue").DefineComponent<{}, {
                items: unknown[];
                $props: {
                    readonly items?: unknown[] | undefined;
                };
            }, {}, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").PublicProps, Readonly<{}> & Readonly<{}>, {}, {}, {}, {}, string, import("vue").ComponentProvideOptions, true, {}, any>;
        };
        setup(): {
            args: any;
        };
        template: string;
    };
    export { render_1 as render };
    export namespace args_1 {
        let items: ({
            title: string;
            subtitle: string;
            value: string;
            lines: string;
            density: string;
            minHeight: number;
            leading: string;
            leadingColor: string;
            leadingIcon: string;
            leadingMedia: string;
            leadingFirstname: string;
            trailing: string;
            trailingColor: string;
            trailingIcon: string;
            trailingMedia: string;
            trailingFirstname: string;
            type?: undefined;
        } | {
            type: string;
            title?: undefined;
            subtitle?: undefined;
            value?: undefined;
            lines?: undefined;
            density?: undefined;
            minHeight?: undefined;
            leading?: undefined;
            leadingColor?: undefined;
            leadingIcon?: undefined;
            leadingMedia?: undefined;
            leadingFirstname?: undefined;
            trailing?: undefined;
            trailingColor?: undefined;
            trailingIcon?: undefined;
            trailingMedia?: undefined;
            trailingFirstname?: undefined;
        })[];
    }
    export { args_1 as args };
}
import SvnProListItem from "../components/list/SvnProListItem.vue";
