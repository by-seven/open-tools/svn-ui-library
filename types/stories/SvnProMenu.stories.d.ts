declare namespace _default {
    export let title: string;
    export { SvnProMenu as component };
    export let tags: string[];
    export namespace argTypes {
        namespace menuColor {
            let control: string;
        }
        namespace listDensity {
            let control_1: string;
            export { control_1 as control };
            export let options: string[];
        }
        namespace closeOnContentClick {
            let control_2: string;
            export { control_2 as control };
        }
        namespace items {
            let contorl: string;
        }
        namespace offset {
            let control_3: string;
            export { control_3 as control };
        }
        namespace menuClass {
            let control_4: string;
            export { control_4 as control };
        }
    }
    export namespace parameters {
        namespace docs {
            namespace description {
                let component: string;
            }
        }
    }
}
export default _default;
export namespace Playground {
    function render(args: any): {
        components: {
            SvnProMenu: {
                new (...args: any[]): import("vue").CreateComponentPublicInstanceWithMixins<Readonly<{}> & Readonly<{}>, {
                    origin: string;
                    location: string;
                    closeOnContentClick: boolean;
                    offset: number;
                    items: unknown[];
                    menuColor: string;
                    listDensity: string;
                    menuClass?: string | undefined;
                    $props: {
                        readonly origin?: string | undefined;
                        readonly location?: string | undefined;
                        readonly closeOnContentClick?: boolean | undefined;
                        readonly offset?: number | undefined;
                        readonly items?: unknown[] | undefined;
                        readonly menuColor?: string | undefined;
                        readonly listDensity?: string | undefined;
                        readonly menuClass?: string | undefined;
                    };
                }, {}, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps & Readonly<{}> & Readonly<{}>, {}, true, {}, {}, import("vue").GlobalComponents, import("vue").GlobalDirectives, string, {}, any, import("vue").ComponentProvideOptions, {
                    P: {};
                    B: {};
                    D: {};
                    C: {};
                    M: {};
                    Defaults: {};
                }, Readonly<{}> & Readonly<{}>, {
                    origin: string;
                    location: string;
                    closeOnContentClick: boolean;
                    offset: number;
                    items: unknown[];
                    menuColor: string;
                    listDensity: string;
                    menuClass?: string | undefined;
                    $props: {
                        readonly origin?: string | undefined;
                        readonly location?: string | undefined;
                        readonly closeOnContentClick?: boolean | undefined;
                        readonly offset?: number | undefined;
                        readonly items?: unknown[] | undefined;
                        readonly menuColor?: string | undefined;
                        readonly listDensity?: string | undefined;
                        readonly menuClass?: string | undefined;
                    };
                }, {}, {}, {}, {}>;
                __isFragment?: never;
                __isTeleport?: never;
                __isSuspense?: never;
            } & import("vue").ComponentOptionsBase<Readonly<{}> & Readonly<{}>, {
                origin: string;
                location: string;
                closeOnContentClick: boolean;
                offset: number;
                items: unknown[];
                menuColor: string;
                listDensity: string;
                menuClass?: string | undefined;
                $props: {
                    readonly origin?: string | undefined;
                    readonly location?: string | undefined;
                    readonly closeOnContentClick?: boolean | undefined;
                    readonly offset?: number | undefined;
                    readonly items?: unknown[] | undefined;
                    readonly menuColor?: string | undefined;
                    readonly listDensity?: string | undefined;
                    readonly menuClass?: string | undefined;
                };
            }, {}, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, {}, {}, string, {}, import("vue").GlobalComponents, import("vue").GlobalDirectives, string, import("vue").ComponentProvideOptions> & import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps & (new () => {
                $slots: {
                    activator?(_: {
                        isActive: any;
                    }): any;
                    dropdown?(_: {}): any;
                };
            });
        };
        setup(): {
            args: any;
        };
        template: string;
    };
    namespace args {
        export { items };
        let offset_1: undefined;
        export { offset_1 as offset };
        let menuColor_1: string;
        export { menuColor_1 as menuColor };
        let menuClass_1: undefined;
        export { menuClass_1 as menuClass };
        let listDensity_1: string;
        export { listDensity_1 as listDensity };
        let closeOnContentClick_1: boolean;
        export { closeOnContentClick_1 as closeOnContentClick };
    }
}
import SvnProMenu from "../components/menu/SvnProMenu.vue";
declare const items_1: {
    id: number;
    title: string;
}[];
