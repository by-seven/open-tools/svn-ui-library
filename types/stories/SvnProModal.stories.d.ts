declare namespace _default {
    export let title: string;
    export { SvnProModal as component };
    export let tags: string[];
    export namespace argTypes {
        export namespace title_1 {
            let control: string;
        }
        export { title_1 as title };
        export namespace contentHeight {
            let control_1: string;
            export { control_1 as control };
        }
        export namespace actionOneTitle {
            let control_2: string;
            export { control_2 as control };
        }
        export namespace actionTwoTitle {
            let control_3: string;
            export { control_3 as control };
        }
        export namespace contentText {
            let control_4: string;
            export { control_4 as control };
        }
        export namespace stickyBottom {
            let control_5: string;
            export { control_5 as control };
        }
    }
    export namespace parameters {
        namespace docs {
            namespace description {
                let component: string;
            }
        }
    }
}
export default _default;
export namespace Playground {
    function render(args: any): {
        components: {
            SvnProModal: {
                new (...args: any[]): import("vue").CreateComponentPublicInstanceWithMixins<Readonly<{}> & Readonly<{}>, {
                    dialog: import("vue").Ref<boolean, boolean>;
                    $emit: (event: "click-primary-button" | "click-secondary-button" | "click-outside" | "click-close", ...args: any[]) => void;
                    title: string;
                    primaryLoading: boolean;
                    primaryDisabled: boolean;
                    closeButtonClosesModal: boolean;
                    stickyBottom: boolean;
                    horizontalButtons: boolean;
                    contentText?: string | undefined;
                    contentHeight?: number | undefined;
                    actionOneTitle?: string | undefined;
                    actionTwoTitle?: string | undefined;
                    $props: {
                        readonly title?: string | undefined;
                        readonly primaryLoading?: boolean | undefined;
                        readonly primaryDisabled?: boolean | undefined;
                        readonly closeButtonClosesModal?: boolean | undefined;
                        readonly stickyBottom?: boolean | undefined;
                        readonly horizontalButtons?: boolean | undefined;
                        readonly contentText?: string | undefined;
                        readonly contentHeight?: number | undefined;
                        readonly actionOneTitle?: string | undefined;
                        readonly actionTwoTitle?: string | undefined;
                    };
                }, {}, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps & Readonly<{}> & Readonly<{}>, {}, true, {}, {}, import("vue").GlobalComponents, import("vue").GlobalDirectives, string, {}, any, import("vue").ComponentProvideOptions, {
                    P: {};
                    B: {};
                    D: {};
                    C: {};
                    M: {};
                    Defaults: {};
                }, Readonly<{}> & Readonly<{}>, {
                    dialog: import("vue").Ref<boolean, boolean>;
                    $emit: (event: "click-primary-button" | "click-secondary-button" | "click-outside" | "click-close", ...args: any[]) => void;
                    title: string;
                    primaryLoading: boolean;
                    primaryDisabled: boolean;
                    closeButtonClosesModal: boolean;
                    stickyBottom: boolean;
                    horizontalButtons: boolean;
                    contentText?: string | undefined;
                    contentHeight?: number | undefined;
                    actionOneTitle?: string | undefined;
                    actionTwoTitle?: string | undefined;
                    $props: {
                        readonly title?: string | undefined;
                        readonly primaryLoading?: boolean | undefined;
                        readonly primaryDisabled?: boolean | undefined;
                        readonly closeButtonClosesModal?: boolean | undefined;
                        readonly stickyBottom?: boolean | undefined;
                        readonly horizontalButtons?: boolean | undefined;
                        readonly contentText?: string | undefined;
                        readonly contentHeight?: number | undefined;
                        readonly actionOneTitle?: string | undefined;
                        readonly actionTwoTitle?: string | undefined;
                    };
                }, {}, {}, {}, {}>;
                __isFragment?: never;
                __isTeleport?: never;
                __isSuspense?: never;
            } & import("vue").ComponentOptionsBase<Readonly<{}> & Readonly<{}>, {
                dialog: import("vue").Ref<boolean, boolean>;
                $emit: (event: "click-primary-button" | "click-secondary-button" | "click-outside" | "click-close", ...args: any[]) => void;
                title: string;
                primaryLoading: boolean;
                primaryDisabled: boolean;
                closeButtonClosesModal: boolean;
                stickyBottom: boolean;
                horizontalButtons: boolean;
                contentText?: string | undefined;
                contentHeight?: number | undefined;
                actionOneTitle?: string | undefined;
                actionTwoTitle?: string | undefined;
                $props: {
                    readonly title?: string | undefined;
                    readonly primaryLoading?: boolean | undefined;
                    readonly primaryDisabled?: boolean | undefined;
                    readonly closeButtonClosesModal?: boolean | undefined;
                    readonly stickyBottom?: boolean | undefined;
                    readonly horizontalButtons?: boolean | undefined;
                    readonly contentText?: string | undefined;
                    readonly contentHeight?: number | undefined;
                    readonly actionOneTitle?: string | undefined;
                    readonly actionTwoTitle?: string | undefined;
                };
            }, {}, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, {}, {}, string, {}, import("vue").GlobalComponents, import("vue").GlobalDirectives, string, import("vue").ComponentProvideOptions> & import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps & (new () => {
                $slots: {
                    activator?(_: {
                        props: any;
                    }): any;
                    text?(_: {}): any;
                };
            });
        };
        setup(): {
            args: any;
        };
        template: string;
    };
    namespace args {
        let title_2: string;
        export { title_2 as title };
        let stickyBottom_1: boolean;
        export { stickyBottom_1 as stickyBottom };
        let contentText_1: string;
        export { contentText_1 as contentText };
        let contentHeight_1: undefined;
        export { contentHeight_1 as contentHeight };
        let actionOneTitle_1: undefined;
        export { actionOneTitle_1 as actionOneTitle };
        let actionTwoTitle_1: undefined;
        export { actionTwoTitle_1 as actionTwoTitle };
    }
}
import SvnProModal from "../components/modal/SvnProModal.vue";
