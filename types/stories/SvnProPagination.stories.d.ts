declare namespace _default {
    export let title: string;
    export { SvnProPagination as component };
    export let tags: string[];
    export namespace argTypes {
        namespace previousText {
            let control: string;
        }
        namespace nextText {
            let control_1: string;
            export { control_1 as control };
        }
        namespace variant {
            let control_2: string;
            export { control_2 as control };
            export let options: string[];
        }
        namespace length {
            let control_3: string;
            export { control_3 as control };
        }
        namespace page {
            let control_4: string;
            export { control_4 as control };
        }
    }
    export namespace parameters {
        namespace docs {
            namespace description {
                let component: string;
            }
        }
    }
}
export default _default;
export namespace Playground {
    function render(args: any): {
        components: {
            SvnProPagination: import("vue").DefineComponent<{}, {
                $emit: (event: "update:modelValue", ...args: any[]) => void;
                length: number;
                page: number;
                $props: {
                    readonly length?: number | undefined;
                    readonly page?: number | undefined;
                };
            }, {}, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").PublicProps, Readonly<{}> & Readonly<{}>, {}, {}, {}, {}, string, import("vue").ComponentProvideOptions, true, {}, any>;
        };
        setup(): {
            args: any;
        };
        template: string;
    };
    namespace args {
        let variant_1: string;
        export { variant_1 as variant };
        let previousText_1: string;
        export { previousText_1 as previousText };
        let nextText_1: string;
        export { nextText_1 as nextText };
        let length_1: number;
        export { length_1 as length };
        let page_1: number;
        export { page_1 as page };
    }
}
import SvnProPagination from "../components/pagination/SvnProPagination.vue";
