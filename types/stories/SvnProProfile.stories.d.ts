declare namespace _default {
    export let title: string;
    export { SvnProProfile as component };
    export let tags: string[];
    export namespace argTypes {
        export namespace size {
            let control: string;
            let options: string[];
        }
        export namespace avatar {
            let control_1: string;
            export { control_1 as control };
        }
        export namespace firstname {
            let control_2: string;
            export { control_2 as control };
        }
        export namespace lastname {
            let control_3: string;
            export { control_3 as control };
        }
        export namespace email {
            let control_4: string;
            export { control_4 as control };
        }
        export namespace tags_1 {
            let control_5: string;
            export { control_5 as control };
        }
        export { tags_1 as tags };
        export namespace showBadges {
            let control_6: string;
            export { control_6 as control };
        }
        export namespace pictureEditable {
            let control_7: string;
            export { control_7 as control };
        }
        export namespace badgeTypes {
            let control_8: string;
            export { control_8 as control };
        }
    }
    export namespace parameters {
        namespace docs {
            namespace description {
                let component: string;
            }
        }
    }
}
export default _default;
export namespace Playground {
    function render(args: any): {
        components: {
            SvnProProfile: {
                new (...args: any[]): import("vue").CreateComponentPublicInstanceWithMixins<Readonly<{}> & Readonly<{}>, {
                    $emit: (event: "edit-picture" | "image-selected" | "open-badges-modal", ...args: any[]) => void;
                    size: string;
                    firstname: string;
                    lastname: string;
                    email: string;
                    tags: unknown[];
                    showBadges: boolean;
                    pictureEditable: boolean;
                    badgeTypes: unknown[];
                    avatar?: string | undefined;
                    $props: {
                        readonly size?: string | undefined;
                        readonly firstname?: string | undefined;
                        readonly lastname?: string | undefined;
                        readonly email?: string | undefined;
                        readonly tags?: unknown[] | undefined;
                        readonly showBadges?: boolean | undefined;
                        readonly pictureEditable?: boolean | undefined;
                        readonly badgeTypes?: unknown[] | undefined;
                        readonly avatar?: string | undefined;
                    };
                }, {}, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps & Readonly<{}> & Readonly<{}>, {}, true, {}, {}, import("vue").GlobalComponents, import("vue").GlobalDirectives, string, {}, any, import("vue").ComponentProvideOptions, {
                    P: {};
                    B: {};
                    D: {};
                    C: {};
                    M: {};
                    Defaults: {};
                }, Readonly<{}> & Readonly<{}>, {
                    $emit: (event: "edit-picture" | "image-selected" | "open-badges-modal", ...args: any[]) => void;
                    size: string;
                    firstname: string;
                    lastname: string;
                    email: string;
                    tags: unknown[];
                    showBadges: boolean;
                    pictureEditable: boolean;
                    badgeTypes: unknown[];
                    avatar?: string | undefined;
                    $props: {
                        readonly size?: string | undefined;
                        readonly firstname?: string | undefined;
                        readonly lastname?: string | undefined;
                        readonly email?: string | undefined;
                        readonly tags?: unknown[] | undefined;
                        readonly showBadges?: boolean | undefined;
                        readonly pictureEditable?: boolean | undefined;
                        readonly badgeTypes?: unknown[] | undefined;
                        readonly avatar?: string | undefined;
                    };
                }, {}, {}, {}, {}>;
                __isFragment?: never;
                __isTeleport?: never;
                __isSuspense?: never;
            } & import("vue").ComponentOptionsBase<Readonly<{}> & Readonly<{}>, {
                $emit: (event: "edit-picture" | "image-selected" | "open-badges-modal", ...args: any[]) => void;
                size: string;
                firstname: string;
                lastname: string;
                email: string;
                tags: unknown[];
                showBadges: boolean;
                pictureEditable: boolean;
                badgeTypes: unknown[];
                avatar?: string | undefined;
                $props: {
                    readonly size?: string | undefined;
                    readonly firstname?: string | undefined;
                    readonly lastname?: string | undefined;
                    readonly email?: string | undefined;
                    readonly tags?: unknown[] | undefined;
                    readonly showBadges?: boolean | undefined;
                    readonly pictureEditable?: boolean | undefined;
                    readonly badgeTypes?: unknown[] | undefined;
                    readonly avatar?: string | undefined;
                };
            }, {}, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, {}, {}, string, {}, import("vue").GlobalComponents, import("vue").GlobalDirectives, string, import("vue").ComponentProvideOptions> & import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps & (new () => {
                $slots: {
                    settings?(_: {}): any;
                    settings?(_: {}): any;
                };
            });
        };
        setup(): {
            args: any;
        };
        template: string;
    };
    namespace args {
        let size_1: string;
        export { size_1 as size };
        let avatar_1: undefined;
        export { avatar_1 as avatar };
        let firstname_1: string;
        export { firstname_1 as firstname };
        let lastname_1: string;
        export { lastname_1 as lastname };
        let email_1: string;
        export { email_1 as email };
        let tags_2: string[];
        export { tags_2 as tags };
        let showBadges_1: boolean;
        export { showBadges_1 as showBadges };
        let pictureEditable_1: boolean;
        export { pictureEditable_1 as pictureEditable };
        let badgeTypes_1: {
            name: string;
            count: number;
        }[];
        export { badgeTypes_1 as badgeTypes };
    }
}
import SvnProProfile from "../components/profile/SvnProProfile.vue";
