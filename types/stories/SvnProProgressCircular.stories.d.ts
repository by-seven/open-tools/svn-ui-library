declare namespace _default {
    export let title: string;
    export { SvnProProgressCircular as component };
    export let tags: string[];
    export namespace argTypes {
        namespace bgColor {
            let control: string;
        }
        namespace color {
            let control_1: string;
            export { control_1 as control };
        }
        namespace indeterminate {
            let control_2: string;
            export { control_2 as control };
        }
        namespace modelValue {
            let control_3: string;
            export { control_3 as control };
        }
        namespace size {
            let control_4: string;
            export { control_4 as control };
        }
        namespace width {
            let control_5: string;
            export { control_5 as control };
        }
    }
    export namespace parameters {
        namespace docs {
            namespace description {
                let component: string;
            }
        }
    }
}
export default _default;
export namespace Playground {
    function render(args: any): {
        components: {
            SvnProProgressCircular: {
                new (...args: any[]): import("vue").CreateComponentPublicInstanceWithMixins<Readonly<{}> & Readonly<{}>, {
                    color: string;
                    width: number;
                    size: number;
                    indeterminate: boolean;
                    modelValue?: number | undefined;
                    bgColor?: string | undefined;
                    $props: {
                        readonly color?: string | undefined;
                        readonly width?: number | undefined;
                        readonly size?: number | undefined;
                        readonly indeterminate?: boolean | undefined;
                        readonly modelValue?: number | undefined;
                        readonly bgColor?: string | undefined;
                    };
                }, {}, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps & Readonly<{}> & Readonly<{}>, {}, true, {}, {}, import("vue").GlobalComponents, import("vue").GlobalDirectives, string, {}, any, import("vue").ComponentProvideOptions, {
                    P: {};
                    B: {};
                    D: {};
                    C: {};
                    M: {};
                    Defaults: {};
                }, Readonly<{}> & Readonly<{}>, {
                    color: string;
                    width: number;
                    size: number;
                    indeterminate: boolean;
                    modelValue?: number | undefined;
                    bgColor?: string | undefined;
                    $props: {
                        readonly color?: string | undefined;
                        readonly width?: number | undefined;
                        readonly size?: number | undefined;
                        readonly indeterminate?: boolean | undefined;
                        readonly modelValue?: number | undefined;
                        readonly bgColor?: string | undefined;
                    };
                }, {}, {}, {}, {}>;
                __isFragment?: never;
                __isTeleport?: never;
                __isSuspense?: never;
            } & import("vue").ComponentOptionsBase<Readonly<{}> & Readonly<{}>, {
                color: string;
                width: number;
                size: number;
                indeterminate: boolean;
                modelValue?: number | undefined;
                bgColor?: string | undefined;
                $props: {
                    readonly color?: string | undefined;
                    readonly width?: number | undefined;
                    readonly size?: number | undefined;
                    readonly indeterminate?: boolean | undefined;
                    readonly modelValue?: number | undefined;
                    readonly bgColor?: string | undefined;
                };
            }, {}, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, {}, {}, string, {}, import("vue").GlobalComponents, import("vue").GlobalDirectives, string, import("vue").ComponentProvideOptions> & import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps & (new () => {
                $slots: {
                    default?(_: {}): any;
                };
            });
        };
        setup(): {
            args: any;
        };
        template: string;
    };
    namespace args {
        let size_1: number;
        export { size_1 as size };
        let width_1: number;
        export { width_1 as width };
        let color_1: string;
        export { color_1 as color };
        let bgColor_1: undefined;
        export { bgColor_1 as bgColor };
        let indeterminate_1: boolean;
        export { indeterminate_1 as indeterminate };
        let modelValue_1: undefined;
        export { modelValue_1 as modelValue };
    }
}
import SvnProProgressCircular from "../components/progress/SvnProProgressCircular.vue";
