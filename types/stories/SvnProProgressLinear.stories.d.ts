declare namespace _default {
    export let title: string;
    export { SvnProProgressLinear as component };
    export let tags: string[];
    export namespace argTypes {
        namespace bgColor {
            let control: string;
        }
        namespace color {
            let control_1: string;
            export { control_1 as control };
        }
        namespace indeterminate {
            let control_2: string;
            export { control_2 as control };
        }
        namespace modelValue {
            let control_3: string;
            export { control_3 as control };
        }
        namespace size {
            let control_4: string;
            export { control_4 as control };
        }
        namespace height {
            let control_5: string;
            export { control_5 as control };
        }
        namespace steps {
            let control_6: string;
            export { control_6 as control };
        }
    }
    export namespace parameters {
        namespace docs {
            namespace description {
                let component: string;
            }
        }
    }
}
export default _default;
export namespace Playground {
    function render(args: any): {
        components: {
            SvnProProgressLinear: import("vue").DefineComponent<{}, {
                color: string;
                height: number;
                size: number;
                indeterminate: boolean;
                steps: unknown[];
                modelValue?: number | undefined;
                bgColor?: string | undefined;
                $props: {
                    readonly color?: string | undefined;
                    readonly height?: number | undefined;
                    readonly size?: number | undefined;
                    readonly indeterminate?: boolean | undefined;
                    readonly steps?: unknown[] | undefined;
                    readonly modelValue?: number | undefined;
                    readonly bgColor?: string | undefined;
                };
            }, {}, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").PublicProps, Readonly<{}> & Readonly<{}>, {}, {}, {}, {}, string, import("vue").ComponentProvideOptions, true, {}, any>;
        };
        setup(): {
            args: any;
        };
        template: string;
    };
    namespace args {
        let size_1: number;
        export { size_1 as size };
        let height_1: number;
        export { height_1 as height };
        export { steps };
        let color_1: string;
        export { color_1 as color };
        let bgColor_1: undefined;
        export { bgColor_1 as bgColor };
        let indeterminate_1: boolean;
        export { indeterminate_1 as indeterminate };
        let modelValue_1: undefined;
        export { modelValue_1 as modelValue };
    }
}
import SvnProProgressLinear from "../components/progress/SvnProProgressLinear.vue";
declare const steps_1: {
    id: number;
    progress: number;
}[];
