declare namespace _default {
    export let title: string;
    export { SvnProQuizAnswerLearnItem as component };
    export let tags: string[];
    export namespace argTypes {
        namespace leftLetter {
            let control: string;
        }
        namespace optionText {
            let control_1: string;
            export { control_1 as control };
        }
        namespace state {
            let control_2: string;
            export { control_2 as control };
            export let options: any[];
        }
        namespace link {
            let control_3: string;
            export { control_3 as control };
        }
    }
    export namespace parameters {
        namespace docs {
            namespace description {
                let component: string;
            }
        }
    }
}
export default _default;
export namespace Playground {
    function render(args: any): {
        components: {
            SvnProQuizAnswerLearnItem: any;
        };
        setup(): {
            args: any;
        };
        template: string;
    };
    namespace args {
        let leftLetter_1: string;
        export { leftLetter_1 as leftLetter };
        let optionText_1: string;
        export { optionText_1 as optionText };
        let state_1: string;
        export { state_1 as state };
        let link_1: boolean;
        export { link_1 as link };
    }
}
