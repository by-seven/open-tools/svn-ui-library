declare namespace _default {
    export let title: string;
    export { SvnProRadioButton as component };
    export let tags: string[];
    export namespace argTypes {
        namespace disabled {
            let control: string;
        }
        namespace readonly {
            let control_1: string;
            export { control_1 as control };
        }
        namespace value {
            let control_2: string;
            export { control_2 as control };
        }
        namespace modelValue {
            let control_3: string;
            export { control_3 as control };
        }
        namespace color {
            let control_4: string;
            export { control_4 as control };
        }
        namespace label {
            let control_5: string;
            export { control_5 as control };
        }
        namespace density {
            let control_6: string;
            export { control_6 as control };
            export let options: string[];
        }
    }
    export namespace parameters {
        namespace docs {
            namespace description {
                let component: string;
            }
        }
    }
}
export default _default;
export namespace Playground {
    function render(args: any): {
        components: {
            SvnProRadioButton: import("vue").DefineComponent<{}, {
                color: string;
                disabled: boolean;
                readonly: boolean;
                density: string;
                value: boolean;
                label?: string | undefined;
                modelValue?: undefined;
                $props: {
                    readonly color?: string | undefined;
                    readonly disabled?: boolean | undefined;
                    readonly readonly?: boolean | undefined;
                    readonly density?: string | undefined;
                    readonly value?: boolean | undefined;
                    readonly label?: string | undefined;
                    readonly modelValue?: undefined;
                };
            }, {}, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").PublicProps, Readonly<{}> & Readonly<{}>, {}, {}, {}, {}, string, import("vue").ComponentProvideOptions, true, {}, any>;
        };
        setup(): {
            args: any;
        };
        template: string;
    };
    namespace args {
        let disabled_1: boolean;
        export { disabled_1 as disabled };
        let readonly_1: boolean;
        export { readonly_1 as readonly };
        let value_1: boolean;
        export { value_1 as value };
        let modelValue_1: boolean;
        export { modelValue_1 as modelValue };
        let density_1: string;
        export { density_1 as density };
        let color_1: string;
        export { color_1 as color };
        let label_1: undefined;
        export { label_1 as label };
    }
}
import SvnProRadioButton from "../components/input/SvnProRadioButton.vue";
