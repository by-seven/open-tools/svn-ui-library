declare namespace _default {
    export let title: string;
    export { SvnProReactionTag as component };
    export let tags: string[];
    export namespace argTypes {
        namespace reactionCount {
            let control: string;
        }
        namespace reactionList {
            let control_1: string;
            export { control_1 as control };
        }
    }
    export namespace parameters {
        namespace docs {
            namespace description {
                let component: string;
            }
        }
    }
}
export default _default;
export namespace Playground {
    function render(args: any): {
        components: {
            SvnProReactionTag: import("vue").DefineComponent<{}, {
                reactionList: unknown[];
                reactionCount?: number | undefined;
                $props: {
                    readonly reactionList?: unknown[] | undefined;
                    readonly reactionCount?: number | undefined;
                };
            }, {}, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").PublicProps, Readonly<{}> & Readonly<{}>, {}, {}, {}, {}, string, import("vue").ComponentProvideOptions, true, {}, any>;
        };
        setup(): {
            args: any;
        };
        template: string;
    };
    namespace args {
        let reactionCount_1: undefined;
        export { reactionCount_1 as reactionCount };
        let reactionList_1: string[];
        export { reactionList_1 as reactionList };
    }
}
import SvnProReactionTag from "../components/chip/SvnProReactionTag.vue";
