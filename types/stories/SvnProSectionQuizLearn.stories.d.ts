declare namespace _default {
    export namespace components {
        export { SvnProQuizAnswerLearnItem };
    }
    export let title: string;
    export { SvnProSectionQuizLearnSkt as component };
    export let tags: string[];
    export namespace argTypes {
        export namespace correctionType {
            let control: string;
            let options: any[];
        }
        export namespace title_1 {
            let control_1: string;
            export { control_1 as control };
        }
        export { title_1 as title };
        export namespace nbrOfItems {
            let control_2: string;
            export { control_2 as control };
        }
    }
    export namespace parameters {
        namespace docs {
            namespace description {
                let component: string;
            }
        }
    }
}
export default _default;
export namespace Playground {
    function render(args: any): {
        components: {
            SvnProSectionQuizLearnSkt: any;
            SvnProQuizAnswerLearnItem: any;
        };
        setup(): {
            args: any;
        };
        template: string;
    };
    namespace args {
        let nbrOfItems_1: number;
        export { nbrOfItems_1 as nbrOfItems };
    }
}
