declare namespace _default {
    export let title: string;
    export { SvnProSlider as component };
    export let tags: string[];
    export namespace argTypes {
        namespace color {
            let control: string;
        }
        namespace modelValue {
            let control_1: string;
            export { control_1 as control };
        }
        namespace step {
            let control_2: string;
            export { control_2 as control };
        }
        namespace min {
            let control_3: string;
            export { control_3 as control };
        }
        namespace max {
            let control_4: string;
            export { control_4 as control };
        }
        namespace disabled {
            let control_5: string;
            export { control_5 as control };
        }
        namespace thumbLabel {
            let control_6: string;
            export { control_6 as control };
        }
        namespace showTicks {
            let control_7: string;
            export { control_7 as control };
        }
    }
    export namespace parameters {
        namespace docs {
            namespace description {
                let component: string;
            }
        }
    }
}
export default _default;
export namespace Playground {
    function render(args: any): {
        components: {
            SvnProSlider: import("vue").DefineComponent<{}, {
                $emit: (event: "update:modelValue", ...args: any[]) => void;
                color: string;
                disabled: boolean;
                modelValue: number;
                max: number;
                min: number;
                step: number;
                thumbLabel: boolean;
                showTicks: boolean;
                $props: {
                    readonly color?: string | undefined;
                    readonly disabled?: boolean | undefined;
                    readonly modelValue?: number | undefined;
                    readonly max?: number | undefined;
                    readonly min?: number | undefined;
                    readonly step?: number | undefined;
                    readonly thumbLabel?: boolean | undefined;
                    readonly showTicks?: boolean | undefined;
                };
            }, {}, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").PublicProps, Readonly<{}> & Readonly<{}>, {}, {}, {}, {}, string, import("vue").ComponentProvideOptions, true, {}, any>;
        };
        setup(): {
            args: any;
        };
        template: string;
    };
    namespace args {
        let step_1: number;
        export { step_1 as step };
        let min_1: number;
        export { min_1 as min };
        let max_1: number;
        export { max_1 as max };
        let modelValue_1: number;
        export { modelValue_1 as modelValue };
        let showTicks_1: boolean;
        export { showTicks_1 as showTicks };
        let disabled_1: boolean;
        export { disabled_1 as disabled };
        let color_1: string;
        export { color_1 as color };
        let thumbLabel_1: boolean;
        export { thumbLabel_1 as thumbLabel };
    }
}
import SvnProSlider from "../components/input/SvnProSlider.vue";
