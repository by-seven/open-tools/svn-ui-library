declare namespace _default {
    export let title: string;
    export { SvnProSnackbar as component };
    export let tags: string[];
    export namespace argTypes {
        namespace modelValue {
            let control: string;
        }
        namespace action {
            let control_1: string;
            export { control_1 as control };
        }
        namespace vertical {
            let control_2: string;
            export { control_2 as control };
        }
        namespace close {
            let control_3: string;
            export { control_3 as control };
        }
        namespace twoLines {
            let control_4: string;
            export { control_4 as control };
        }
        namespace actionText {
            let control_5: string;
            export { control_5 as control };
        }
        namespace supportingText {
            let control_6: string;
            export { control_6 as control };
        }
        namespace color {
            let control_7: string;
            export { control_7 as control };
        }
    }
    export namespace parameters {
        namespace docs {
            namespace description {
                let component: string;
            }
        }
    }
}
export default _default;
export namespace Playground {
    function render(args: any): {
        components: {
            SvnProSnackbar: import("vue").DefineComponent<{}, {
                snackbar: import("vue").Ref<null, null>;
                $emit: (event: "update:modelValue" | "action-clicked" | "close-clicked", ...args: any[]) => void;
                close: boolean;
                color: string;
                modelValue: boolean;
                vertical: boolean;
                supportingText: string;
                action: boolean;
                twoLines: boolean;
                actionText: string;
                $props: {
                    readonly close?: boolean | undefined;
                    readonly color?: string | undefined;
                    readonly modelValue?: boolean | undefined;
                    readonly vertical?: boolean | undefined;
                    readonly supportingText?: string | undefined;
                    readonly action?: boolean | undefined;
                    readonly twoLines?: boolean | undefined;
                    readonly actionText?: string | undefined;
                };
            }, {}, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").PublicProps, Readonly<{}> & Readonly<{}>, {}, {}, {}, {}, string, import("vue").ComponentProvideOptions, true, {}, any>;
        };
        setup(): {
            args: any;
        };
        template: string;
    };
    namespace args {
        let modelValue_1: boolean;
        export { modelValue_1 as modelValue };
        let action_1: boolean;
        export { action_1 as action };
        let vertical_1: boolean;
        export { vertical_1 as vertical };
        let close_1: boolean;
        export { close_1 as close };
        let twoLines_1: boolean;
        export { twoLines_1 as twoLines };
        let color_1: string;
        export { color_1 as color };
        let actionText_1: string;
        export { actionText_1 as actionText };
        let supportingText_1: string;
        export { supportingText_1 as supportingText };
    }
}
import SvnProSnackbar from "../components/snackbar/SvnProSnackbar.vue";
