declare namespace _default {
    export let title: string;
    export { SvnProSwitch as component };
    export let tags: string[];
    export namespace argTypes {
        namespace modelValue {
            let control: string;
        }
        namespace disabled {
            let control_1: string;
            export { control_1 as control };
        }
        namespace readonly {
            let control_2: string;
            export { control_2 as control };
        }
        namespace icon {
            let control_3: string;
            export { control_3 as control };
        }
        namespace color {
            let control_4: string;
            export { control_4 as control };
        }
        namespace density {
            let control_5: string;
            export { control_5 as control };
            export let options: string[];
        }
    }
    export namespace parameters {
        namespace docs {
            namespace description {
                let component: string;
            }
        }
    }
}
export default _default;
export namespace Playground {
    function render(args: any): {
        components: {
            SvnProSwitch: import("vue").DefineComponent<{}, {
                $emit: (event: "update:modelValue", ...args: any[]) => void;
                icon: boolean;
                color: string;
                disabled: boolean;
                modelValue: boolean;
                readonly: boolean;
                density: string;
                switchId: string;
                $props: {
                    readonly icon?: boolean | undefined;
                    readonly color?: string | undefined;
                    readonly disabled?: boolean | undefined;
                    readonly modelValue?: boolean | undefined;
                    readonly readonly?: boolean | undefined;
                    readonly density?: string | undefined;
                    readonly switchId?: string | undefined;
                };
            }, {}, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").PublicProps, Readonly<{}> & Readonly<{}>, {}, {}, {}, {}, string, import("vue").ComponentProvideOptions, true, {}, any>;
        };
        setup(): {
            args: any;
        };
        template: string;
    };
    namespace args {
        let modelValue_1: boolean;
        export { modelValue_1 as modelValue };
        let disabled_1: boolean;
        export { disabled_1 as disabled };
        let readonly_1: boolean;
        export { readonly_1 as readonly };
        let density_1: string;
        export { density_1 as density };
        let color_1: string;
        export { color_1 as color };
        let icon_1: boolean;
        export { icon_1 as icon };
    }
}
import SvnProSwitch from "../components/input/SvnProSwitch.vue";
