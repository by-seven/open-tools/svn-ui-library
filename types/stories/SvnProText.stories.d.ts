declare namespace _default {
    export let title: string;
    export { SvnProText as component };
    export let tags: string[];
    export namespace argTypes {
        namespace color {
            let control: string;
        }
        namespace text {
            let control_1: string;
            export { control_1 as control };
        }
        namespace bodyLarge {
            let control_2: string;
            export { control_2 as control };
        }
        namespace textarea {
            let control_3: string;
            export { control_3 as control };
        }
        namespace bodyMedium {
            let control_4: string;
            export { control_4 as control };
        }
        namespace subtitleMedium {
            let control_5: string;
            export { control_5 as control };
        }
        namespace subtitleLarge {
            let control_6: string;
            export { control_6 as control };
        }
        namespace regular {
            let control_7: string;
            export { control_7 as control };
        }
        namespace medium {
            let control_8: string;
            export { control_8 as control };
        }
        namespace bold {
            let control_9: string;
            export { control_9 as control };
        }
        namespace caption {
            let control_10: string;
            export { control_10 as control };
        }
        namespace overline {
            let control_11: string;
            export { control_11 as control };
        }
        namespace button {
            let control_12: string;
            export { control_12 as control };
        }
    }
    export namespace parameters {
        namespace docs {
            namespace description {
                let component: string;
            }
        }
    }
}
export default _default;
export namespace Playground {
    function render(args: any): {
        components: {
            SvnProText: {
                new (...args: any[]): import("vue").CreateComponentPublicInstanceWithMixins<Readonly<{}> & Readonly<{}>, {
                    button: boolean;
                    caption: boolean;
                    textarea: boolean;
                    text: string;
                    color: string;
                    bold: boolean;
                    medium: boolean;
                    regular: boolean;
                    bodyLarge: boolean;
                    bodyMedium: boolean;
                    subtitleLarge: boolean;
                    subtitleMedium: boolean;
                    overline: boolean;
                    $props: {
                        readonly button?: boolean | undefined;
                        readonly caption?: boolean | undefined;
                        readonly textarea?: boolean | undefined;
                        readonly text?: string | undefined;
                        readonly color?: string | undefined;
                        readonly bold?: boolean | undefined;
                        readonly medium?: boolean | undefined;
                        readonly regular?: boolean | undefined;
                        readonly bodyLarge?: boolean | undefined;
                        readonly bodyMedium?: boolean | undefined;
                        readonly subtitleLarge?: boolean | undefined;
                        readonly subtitleMedium?: boolean | undefined;
                        readonly overline?: boolean | undefined;
                    };
                }, {}, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps & Readonly<{}> & Readonly<{}>, {}, true, {}, {}, import("vue").GlobalComponents, import("vue").GlobalDirectives, string, {}, any, import("vue").ComponentProvideOptions, {
                    P: {};
                    B: {};
                    D: {};
                    C: {};
                    M: {};
                    Defaults: {};
                }, Readonly<{}> & Readonly<{}>, {
                    button: boolean;
                    caption: boolean;
                    textarea: boolean;
                    text: string;
                    color: string;
                    bold: boolean;
                    medium: boolean;
                    regular: boolean;
                    bodyLarge: boolean;
                    bodyMedium: boolean;
                    subtitleLarge: boolean;
                    subtitleMedium: boolean;
                    overline: boolean;
                    $props: {
                        readonly button?: boolean | undefined;
                        readonly caption?: boolean | undefined;
                        readonly textarea?: boolean | undefined;
                        readonly text?: string | undefined;
                        readonly color?: string | undefined;
                        readonly bold?: boolean | undefined;
                        readonly medium?: boolean | undefined;
                        readonly regular?: boolean | undefined;
                        readonly bodyLarge?: boolean | undefined;
                        readonly bodyMedium?: boolean | undefined;
                        readonly subtitleLarge?: boolean | undefined;
                        readonly subtitleMedium?: boolean | undefined;
                        readonly overline?: boolean | undefined;
                    };
                }, {}, {}, {}, {}>;
                __isFragment?: never;
                __isTeleport?: never;
                __isSuspense?: never;
            } & import("vue").ComponentOptionsBase<Readonly<{}> & Readonly<{}>, {
                button: boolean;
                caption: boolean;
                textarea: boolean;
                text: string;
                color: string;
                bold: boolean;
                medium: boolean;
                regular: boolean;
                bodyLarge: boolean;
                bodyMedium: boolean;
                subtitleLarge: boolean;
                subtitleMedium: boolean;
                overline: boolean;
                $props: {
                    readonly button?: boolean | undefined;
                    readonly caption?: boolean | undefined;
                    readonly textarea?: boolean | undefined;
                    readonly text?: string | undefined;
                    readonly color?: string | undefined;
                    readonly bold?: boolean | undefined;
                    readonly medium?: boolean | undefined;
                    readonly regular?: boolean | undefined;
                    readonly bodyLarge?: boolean | undefined;
                    readonly bodyMedium?: boolean | undefined;
                    readonly subtitleLarge?: boolean | undefined;
                    readonly subtitleMedium?: boolean | undefined;
                    readonly overline?: boolean | undefined;
                };
            }, {}, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, {}, {}, string, {}, import("vue").GlobalComponents, import("vue").GlobalDirectives, string, import("vue").ComponentProvideOptions> & import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps & (new () => {
                $slots: {
                    default?(_: {}): any;
                };
            });
        };
        setup(): {
            args: any;
        };
        template: string;
    };
    namespace args {
        let color_1: string;
        export { color_1 as color };
        let text_1: string;
        export { text_1 as text };
        let bodyLarge_1: boolean;
        export { bodyLarge_1 as bodyLarge };
        let bodyMedium_1: boolean;
        export { bodyMedium_1 as bodyMedium };
        let caption_1: boolean;
        export { caption_1 as caption };
        let subtitleMedium_1: boolean;
        export { subtitleMedium_1 as subtitleMedium };
        let subtitleLarge_1: boolean;
        export { subtitleLarge_1 as subtitleLarge };
        let overline_1: boolean;
        export { overline_1 as overline };
        let button_1: boolean;
        export { button_1 as button };
        let regular_1: boolean;
        export { regular_1 as regular };
        let medium_1: boolean;
        export { medium_1 as medium };
        let textarea_1: boolean;
        export { textarea_1 as textarea };
        let bold_1: boolean;
        export { bold_1 as bold };
    }
}
import SvnProText from "../components/styles/SvnProText.vue";
