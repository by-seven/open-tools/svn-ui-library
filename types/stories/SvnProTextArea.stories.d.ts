declare namespace _default {
    export let title: string;
    export { SvnProTextArea as component };
    export let tags: string[];
    export namespace argTypes {
        namespace modelValue {
            let control: string;
        }
        namespace label {
            let control_1: string;
            export { control_1 as control };
        }
        namespace color {
            let control_2: string;
            export { control_2 as control };
        }
        namespace variant {
            let control_3: string;
            export { control_3 as control };
            export let options: string[];
        }
        namespace rows {
            let control_4: string;
            export { control_4 as control };
        }
        namespace maxRows {
            let control_5: string;
            export { control_5 as control };
        }
        namespace maxLength {
            let control_6: string;
            export { control_6 as control };
        }
        namespace error {
            let control_7: string;
            export { control_7 as control };
        }
        namespace disabled {
            let control_8: string;
            export { control_8 as control };
        }
        namespace counter {
            let control_9: string;
            export { control_9 as control };
        }
        namespace readonly {
            let control_10: string;
            export { control_10 as control };
        }
        namespace persistentCounter {
            let control_11: string;
            export { control_11 as control };
        }
        namespace messages {
            let control_12: string;
            export { control_12 as control };
        }
        namespace rules {
            let control_13: string;
            export { control_13 as control };
        }
    }
    export namespace parameters {
        namespace docs {
            namespace description {
                let component: string;
            }
        }
    }
}
export default _default;
export namespace Playground {
    function render(args: any): {
        components: {
            SvnProTextArea: import("vue").DefineComponent<{}, {
                $emit: (event: "clear" | "update:modelValue", ...args: any[]) => void;
                error: boolean;
                variant: string;
                color: string;
                disabled: boolean;
                modelValue: string;
                readonly: boolean;
                rules: unknown[];
                counter: boolean;
                persistentCounter: boolean;
                rows: number;
                label?: string | undefined;
                errorMessages?: string | undefined;
                messages?: string | undefined;
                placeholder?: string | undefined;
                maxLength?: number | undefined;
                maxRows?: number | undefined;
                $props: {
                    readonly error?: boolean | undefined;
                    readonly variant?: string | undefined;
                    readonly color?: string | undefined;
                    readonly disabled?: boolean | undefined;
                    readonly modelValue?: string | undefined;
                    readonly readonly?: boolean | undefined;
                    readonly rules?: unknown[] | undefined;
                    readonly counter?: boolean | undefined;
                    readonly persistentCounter?: boolean | undefined;
                    readonly rows?: number | undefined;
                    readonly label?: string | undefined;
                    readonly errorMessages?: string | undefined;
                    readonly messages?: string | undefined;
                    readonly placeholder?: string | undefined;
                    readonly maxLength?: number | undefined;
                    readonly maxRows?: number | undefined;
                };
            }, {}, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").PublicProps, Readonly<{}> & Readonly<{}>, {}, {}, {}, {}, string, import("vue").ComponentProvideOptions, true, {}, any>;
        };
        setup(): {
            args: any;
        };
        template: string;
    };
    namespace args {
        let modelValue_1: string;
        export { modelValue_1 as modelValue };
        let label_1: string;
        export { label_1 as label };
        let color_1: string;
        export { color_1 as color };
        let variant_1: string;
        export { variant_1 as variant };
        let rows_1: number;
        export { rows_1 as rows };
        let maxRows_1: undefined;
        export { maxRows_1 as maxRows };
        let error_1: boolean;
        export { error_1 as error };
        let disabled_1: boolean;
        export { disabled_1 as disabled };
        let counter_1: boolean;
        export { counter_1 as counter };
        let readonly_1: boolean;
        export { readonly_1 as readonly };
        let persistentCounter_1: boolean;
        export { persistentCounter_1 as persistentCounter };
        let messages_1: undefined;
        export { messages_1 as messages };
        let rules_1: never[];
        export { rules_1 as rules };
        let maxLength_1: undefined;
        export { maxLength_1 as maxLength };
    }
}
import SvnProTextArea from "../components/input/SvnProTextArea.vue";
