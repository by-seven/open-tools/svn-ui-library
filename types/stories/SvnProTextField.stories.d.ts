declare namespace _default {
    export let title: string;
    export { SvnProTextField as component };
    export let tags: string[];
    export namespace argTypes {
        namespace label {
            let control: string;
        }
        namespace appendInnerIcon {
            let control_1: string;
            export { control_1 as control };
        }
        namespace prependInnerIcon {
            let control_2: string;
            export { control_2 as control };
        }
        namespace variant {
            let control_3: string;
            export { control_3 as control };
            export let options: (string | undefined)[];
        }
        namespace autofocus {
            let control_4: string;
            export { control_4 as control };
        }
        namespace clearable {
            let control_5: string;
            export { control_5 as control };
        }
        namespace color {
            let control_6: string;
            export { control_6 as control };
        }
        namespace disabled {
            let control_7: string;
            export { control_7 as control };
        }
        namespace hint {
            let control_8: string;
            export { control_8 as control };
        }
        namespace placeholder {
            let control_9: string;
            export { control_9 as control };
        }
        namespace messages {
            let control_10: string;
            export { control_10 as control };
        }
        namespace error {
            let control_11: string;
            export { control_11 as control };
        }
        namespace counter {
            let control_12: string;
            export { control_12 as control };
        }
        namespace persistentCounter {
            let control_13: string;
            export { control_13 as control };
        }
        namespace maxLength {
            let control_14: string;
            export { control_14 as control };
        }
        namespace persistentHint {
            let control_15: string;
            export { control_15 as control };
        }
        namespace rules {
            let control_16: string;
            export { control_16 as control };
        }
    }
    export namespace parameters {
        namespace docs {
            namespace description {
                let component: string;
            }
        }
    }
}
export default _default;
export namespace Playground {
    function render(args: any): {
        components: {
            SvnProTextField: {
                new (...args: any[]): import("vue").CreateComponentPublicInstanceWithMixins<Readonly<{}> & Readonly<{}>, {
                    $emit: (event: "clear" | "update:modelValue", ...args: any[]) => void;
                    error: boolean;
                    label: string;
                    variant: string;
                    color: string;
                    disabled: boolean;
                    modelValue: null;
                    rules: unknown[];
                    persistentHint: boolean;
                    clearable: boolean;
                    prependInnerIcon: string;
                    autofocus: boolean;
                    counter: boolean;
                    persistentCounter: boolean;
                    append: boolean;
                    errorMessages?: string | undefined;
                    messages?: string | undefined;
                    hint?: string | undefined;
                    appendInnerIcon?: string | undefined;
                    placeholder?: string | undefined;
                    maxLength?: number | undefined;
                    $props: {
                        readonly error?: boolean | undefined;
                        readonly label?: string | undefined;
                        readonly variant?: string | undefined;
                        readonly color?: string | undefined;
                        readonly disabled?: boolean | undefined;
                        readonly modelValue?: null | undefined;
                        readonly rules?: unknown[] | undefined;
                        readonly persistentHint?: boolean | undefined;
                        readonly clearable?: boolean | undefined;
                        readonly prependInnerIcon?: string | undefined;
                        readonly autofocus?: boolean | undefined;
                        readonly counter?: boolean | undefined;
                        readonly persistentCounter?: boolean | undefined;
                        readonly append?: boolean | undefined;
                        readonly errorMessages?: string | undefined;
                        readonly messages?: string | undefined;
                        readonly hint?: string | undefined;
                        readonly appendInnerIcon?: string | undefined;
                        readonly placeholder?: string | undefined;
                        readonly maxLength?: number | undefined;
                    };
                }, {}, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps & Readonly<{}> & Readonly<{}>, {}, true, {}, {}, import("vue").GlobalComponents, import("vue").GlobalDirectives, string, {}, any, import("vue").ComponentProvideOptions, {
                    P: {};
                    B: {};
                    D: {};
                    C: {};
                    M: {};
                    Defaults: {};
                }, Readonly<{}> & Readonly<{}>, {
                    $emit: (event: "clear" | "update:modelValue", ...args: any[]) => void;
                    error: boolean;
                    label: string;
                    variant: string;
                    color: string;
                    disabled: boolean;
                    modelValue: null;
                    rules: unknown[];
                    persistentHint: boolean;
                    clearable: boolean;
                    prependInnerIcon: string;
                    autofocus: boolean;
                    counter: boolean;
                    persistentCounter: boolean;
                    append: boolean;
                    errorMessages?: string | undefined;
                    messages?: string | undefined;
                    hint?: string | undefined;
                    appendInnerIcon?: string | undefined;
                    placeholder?: string | undefined;
                    maxLength?: number | undefined;
                    $props: {
                        readonly error?: boolean | undefined;
                        readonly label?: string | undefined;
                        readonly variant?: string | undefined;
                        readonly color?: string | undefined;
                        readonly disabled?: boolean | undefined;
                        readonly modelValue?: null | undefined;
                        readonly rules?: unknown[] | undefined;
                        readonly persistentHint?: boolean | undefined;
                        readonly clearable?: boolean | undefined;
                        readonly prependInnerIcon?: string | undefined;
                        readonly autofocus?: boolean | undefined;
                        readonly counter?: boolean | undefined;
                        readonly persistentCounter?: boolean | undefined;
                        readonly append?: boolean | undefined;
                        readonly errorMessages?: string | undefined;
                        readonly messages?: string | undefined;
                        readonly hint?: string | undefined;
                        readonly appendInnerIcon?: string | undefined;
                        readonly placeholder?: string | undefined;
                        readonly maxLength?: number | undefined;
                    };
                }, {}, {}, {}, {}>;
                __isFragment?: never;
                __isTeleport?: never;
                __isSuspense?: never;
            } & import("vue").ComponentOptionsBase<Readonly<{}> & Readonly<{}>, {
                $emit: (event: "clear" | "update:modelValue", ...args: any[]) => void;
                error: boolean;
                label: string;
                variant: string;
                color: string;
                disabled: boolean;
                modelValue: null;
                rules: unknown[];
                persistentHint: boolean;
                clearable: boolean;
                prependInnerIcon: string;
                autofocus: boolean;
                counter: boolean;
                persistentCounter: boolean;
                append: boolean;
                errorMessages?: string | undefined;
                messages?: string | undefined;
                hint?: string | undefined;
                appendInnerIcon?: string | undefined;
                placeholder?: string | undefined;
                maxLength?: number | undefined;
                $props: {
                    readonly error?: boolean | undefined;
                    readonly label?: string | undefined;
                    readonly variant?: string | undefined;
                    readonly color?: string | undefined;
                    readonly disabled?: boolean | undefined;
                    readonly modelValue?: null | undefined;
                    readonly rules?: unknown[] | undefined;
                    readonly persistentHint?: boolean | undefined;
                    readonly clearable?: boolean | undefined;
                    readonly prependInnerIcon?: string | undefined;
                    readonly autofocus?: boolean | undefined;
                    readonly counter?: boolean | undefined;
                    readonly persistentCounter?: boolean | undefined;
                    readonly append?: boolean | undefined;
                    readonly errorMessages?: string | undefined;
                    readonly messages?: string | undefined;
                    readonly hint?: string | undefined;
                    readonly appendInnerIcon?: string | undefined;
                    readonly placeholder?: string | undefined;
                    readonly maxLength?: number | undefined;
                };
            }, {}, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, {}, {}, string, {}, import("vue").GlobalComponents, import("vue").GlobalDirectives, string, import("vue").ComponentProvideOptions> & import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps & (new () => {
                $slots: {
                    append?(_: {}): any;
                };
            });
        };
        setup(): {
            args: any;
        };
        template: string;
    };
    namespace args {
        let label_1: string;
        export { label_1 as label };
        let appendInnerIcon_1: undefined;
        export { appendInnerIcon_1 as appendInnerIcon };
        let prependInnerIcon_1: string;
        export { prependInnerIcon_1 as prependInnerIcon };
        let variant_1: undefined;
        export { variant_1 as variant };
        let placeholder_1: undefined;
        export { placeholder_1 as placeholder };
        let autofocus_1: boolean;
        export { autofocus_1 as autofocus };
        let persistentHint_1: boolean;
        export { persistentHint_1 as persistentHint };
        let clearable_1: boolean;
        export { clearable_1 as clearable };
        let color_1: string;
        export { color_1 as color };
        let disabled_1: boolean;
        export { disabled_1 as disabled };
        let hint_1: undefined;
        export { hint_1 as hint };
        let error_1: boolean;
        export { error_1 as error };
        let messages_1: undefined;
        export { messages_1 as messages };
        let counter_1: boolean;
        export { counter_1 as counter };
        let persistentCounter_1: boolean;
        export { persistentCounter_1 as persistentCounter };
        let rules_1: never[];
        export { rules_1 as rules };
        let maxLength_1: undefined;
        export { maxLength_1 as maxLength };
    }
}
import SvnProTextField from "../components/input/SvnProTextField.vue";
