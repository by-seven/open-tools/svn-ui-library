declare namespace _default {
    export let title: string;
    export { SvnProTitle as component };
    export let tags: string[];
    export namespace argTypes {
        namespace color {
            let control: string;
        }
        namespace h1 {
            let control_1: string;
            export { control_1 as control };
        }
        namespace h2 {
            let control_2: string;
            export { control_2 as control };
        }
        namespace h3 {
            let control_3: string;
            export { control_3 as control };
        }
        namespace h4 {
            let control_4: string;
            export { control_4 as control };
        }
        namespace h5 {
            let control_5: string;
            export { control_5 as control };
        }
        namespace h6 {
            let control_6: string;
            export { control_6 as control };
        }
        namespace regular {
            let control_7: string;
            export { control_7 as control };
        }
        namespace medium {
            let control_8: string;
            export { control_8 as control };
        }
        namespace bold {
            let control_9: string;
            export { control_9 as control };
        }
    }
    export namespace parameters {
        namespace docs {
            namespace description {
                let component: string;
            }
        }
    }
}
export default _default;
export namespace Playground {
    function render(args: any): {
        components: {
            SvnProTitle: {
                new (...args: any[]): import("vue").CreateComponentPublicInstanceWithMixins<Readonly<{}> & Readonly<{}>, {
                    h1: boolean;
                    h2: boolean;
                    h3: boolean;
                    h4: boolean;
                    h5: boolean;
                    h6: boolean;
                    color: string;
                    bold: boolean;
                    medium: boolean;
                    regular: boolean;
                    $props: {
                        readonly h1?: boolean | undefined;
                        readonly h2?: boolean | undefined;
                        readonly h3?: boolean | undefined;
                        readonly h4?: boolean | undefined;
                        readonly h5?: boolean | undefined;
                        readonly h6?: boolean | undefined;
                        readonly color?: string | undefined;
                        readonly bold?: boolean | undefined;
                        readonly medium?: boolean | undefined;
                        readonly regular?: boolean | undefined;
                    };
                }, {}, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps & Readonly<{}> & Readonly<{}>, {}, true, {}, {}, import("vue").GlobalComponents, import("vue").GlobalDirectives, string, {}, any, import("vue").ComponentProvideOptions, {
                    P: {};
                    B: {};
                    D: {};
                    C: {};
                    M: {};
                    Defaults: {};
                }, Readonly<{}> & Readonly<{}>, {
                    h1: boolean;
                    h2: boolean;
                    h3: boolean;
                    h4: boolean;
                    h5: boolean;
                    h6: boolean;
                    color: string;
                    bold: boolean;
                    medium: boolean;
                    regular: boolean;
                    $props: {
                        readonly h1?: boolean | undefined;
                        readonly h2?: boolean | undefined;
                        readonly h3?: boolean | undefined;
                        readonly h4?: boolean | undefined;
                        readonly h5?: boolean | undefined;
                        readonly h6?: boolean | undefined;
                        readonly color?: string | undefined;
                        readonly bold?: boolean | undefined;
                        readonly medium?: boolean | undefined;
                        readonly regular?: boolean | undefined;
                    };
                }, {}, {}, {}, {}>;
                __isFragment?: never;
                __isTeleport?: never;
                __isSuspense?: never;
            } & import("vue").ComponentOptionsBase<Readonly<{}> & Readonly<{}>, {
                h1: boolean;
                h2: boolean;
                h3: boolean;
                h4: boolean;
                h5: boolean;
                h6: boolean;
                color: string;
                bold: boolean;
                medium: boolean;
                regular: boolean;
                $props: {
                    readonly h1?: boolean | undefined;
                    readonly h2?: boolean | undefined;
                    readonly h3?: boolean | undefined;
                    readonly h4?: boolean | undefined;
                    readonly h5?: boolean | undefined;
                    readonly h6?: boolean | undefined;
                    readonly color?: string | undefined;
                    readonly bold?: boolean | undefined;
                    readonly medium?: boolean | undefined;
                    readonly regular?: boolean | undefined;
                };
            }, {}, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, {}, {}, string, {}, import("vue").GlobalComponents, import("vue").GlobalDirectives, string, import("vue").ComponentProvideOptions> & import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps & (new () => {
                $slots: {
                    default?(_: {}): any;
                    default?(_: {}): any;
                    default?(_: {}): any;
                    default?(_: {}): any;
                    default?(_: {}): any;
                    default?(_: {}): any;
                };
            });
        };
        setup(): {
            args: any;
        };
        template: string;
    };
    namespace args {
        let color_1: string;
        export { color_1 as color };
        let h1_1: boolean;
        export { h1_1 as h1 };
        let h2_1: boolean;
        export { h2_1 as h2 };
        let h3_1: boolean;
        export { h3_1 as h3 };
        let h4_1: boolean;
        export { h4_1 as h4 };
        let h5_1: boolean;
        export { h5_1 as h5 };
        let h6_1: boolean;
        export { h6_1 as h6 };
        let regular_1: boolean;
        export { regular_1 as regular };
        let medium_1: boolean;
        export { medium_1 as medium };
        let bold_1: boolean;
        export { bold_1 as bold };
    }
}
import SvnProTitle from "../components/styles/SvnProTitle.vue";
