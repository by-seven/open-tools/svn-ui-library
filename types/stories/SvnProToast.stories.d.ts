declare namespace _default {
    export let title: string;
    export { SvnProToast as component };
    export let tags: string[];
    export namespace argTypes {
        namespace toastTitle {
            let control: string;
        }
        namespace toastDescription {
            let control_1: string;
            export { control_1 as control };
        }
        namespace actionText {
            let control_2: string;
            export { control_2 as control };
        }
        namespace progressBar {
            let control_3: string;
            export { control_3 as control };
        }
        namespace action {
            let control_4: string;
            export { control_4 as control };
        }
        namespace badge {
            let control_5: string;
            export { control_5 as control };
            export let options: string[];
        }
        namespace toastType {
            let control_6: string;
            export { control_6 as control };
            let options_1: string[];
            export { options_1 as options };
        }
    }
    export namespace parameters {
        namespace docs {
            namespace description {
                let component: string;
            }
        }
    }
}
export default _default;
export namespace Playground {
    function render(args: any): {
        components: {
            SvnProToast: import("vue").DefineComponent<{}, {
                badge: string;
                action: boolean;
                actionText: string;
                toastType: string;
                toastTitle: string;
                toastDescription: string;
                progressBar: boolean;
                $props: {
                    readonly badge?: string | undefined;
                    readonly action?: boolean | undefined;
                    readonly actionText?: string | undefined;
                    readonly toastType?: string | undefined;
                    readonly toastTitle?: string | undefined;
                    readonly toastDescription?: string | undefined;
                    readonly progressBar?: boolean | undefined;
                };
            }, {}, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").PublicProps, Readonly<{}> & Readonly<{}>, {}, {}, {}, {}, string, import("vue").ComponentProvideOptions, true, {}, any>;
        };
        setup(): {
            args: any;
        };
        template: string;
    };
    namespace args {
        let toastTitle_1: string;
        export { toastTitle_1 as toastTitle };
        let toastDescription_1: string;
        export { toastDescription_1 as toastDescription };
        let actionText_1: string;
        export { actionText_1 as actionText };
        let progressBar_1: boolean;
        export { progressBar_1 as progressBar };
        let action_1: boolean;
        export { action_1 as action };
        let badge_1: string;
        export { badge_1 as badge };
        let toastType_1: string;
        export { toastType_1 as toastType };
    }
}
import SvnProToast from "../components/snackbar/SvnProToast.vue";
