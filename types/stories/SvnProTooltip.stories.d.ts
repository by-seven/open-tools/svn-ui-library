declare namespace _default {
    export let title: string;
    export { SvnProTooltip as component };
    export let tags: string[];
    export namespace argTypes {
        namespace width {
            let control: string;
        }
        namespace maxWidth {
            let control_1: string;
            export { control_1 as control };
        }
        namespace location {
            let control_2: string;
            export { control_2 as control };
        }
        namespace origin {
            let control_3: string;
            export { control_3 as control };
        }
        namespace text {
            let control_4: string;
            export { control_4 as control };
        }
    }
    export namespace parameters {
        namespace docs {
            namespace description {
                let component: string;
            }
        }
    }
}
export default _default;
export namespace Playground {
    function render(args: any): {
        components: {
            SvnProTooltip: {
                new (...args: any[]): import("vue").CreateComponentPublicInstanceWithMixins<Readonly<{}> & Readonly<{}>, {
                    text: string;
                    origin: string;
                    location: string;
                    maxWidth: number;
                    width?: number | undefined;
                    $props: {
                        readonly text?: string | undefined;
                        readonly origin?: string | undefined;
                        readonly location?: string | undefined;
                        readonly maxWidth?: number | undefined;
                        readonly width?: number | undefined;
                    };
                }, {}, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps & Readonly<{}> & Readonly<{}>, {}, true, {}, {}, import("vue").GlobalComponents, import("vue").GlobalDirectives, string, {}, any, import("vue").ComponentProvideOptions, {
                    P: {};
                    B: {};
                    D: {};
                    C: {};
                    M: {};
                    Defaults: {};
                }, Readonly<{}> & Readonly<{}>, {
                    text: string;
                    origin: string;
                    location: string;
                    maxWidth: number;
                    width?: number | undefined;
                    $props: {
                        readonly text?: string | undefined;
                        readonly origin?: string | undefined;
                        readonly location?: string | undefined;
                        readonly maxWidth?: number | undefined;
                        readonly width?: number | undefined;
                    };
                }, {}, {}, {}, {}>;
                __isFragment?: never;
                __isTeleport?: never;
                __isSuspense?: never;
            } & import("vue").ComponentOptionsBase<Readonly<{}> & Readonly<{}>, {
                text: string;
                origin: string;
                location: string;
                maxWidth: number;
                width?: number | undefined;
                $props: {
                    readonly text?: string | undefined;
                    readonly origin?: string | undefined;
                    readonly location?: string | undefined;
                    readonly maxWidth?: number | undefined;
                    readonly width?: number | undefined;
                };
            }, {}, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, {}, {}, string, {}, import("vue").GlobalComponents, import("vue").GlobalDirectives, string, import("vue").ComponentProvideOptions> & import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps & (new () => {
                $slots: {
                    activator?(_: {
                        props: any;
                    }): any;
                };
            });
        };
        setup(): {
            args: any;
        };
        template: string;
    };
    namespace args {
        let width_1: undefined;
        export { width_1 as width };
        let maxWidth_1: number;
        export { maxWidth_1 as maxWidth };
        let location_1: string;
        export { location_1 as location };
        let origin_1: string;
        export { origin_1 as origin };
        let text_1: string;
        export { text_1 as text };
    }
}
import SvnProTooltip from "../components/tooltip/SvnProTooltip.vue";
