declare namespace _default {
    export let title: string;
    export { SvnProTopBarLearnModule as component };
    export let tags: string[];
    export namespace argTypes {
        namespace type {
            let control: string;
            let options: ("show" | "edit" | "draft" | "quiz" | "view_mode" | "face_to_face")[];
        }
        namespace isFavorited {
            let control_1: string;
            export { control_1 as control };
        }
        namespace publishText {
            let control_2: string;
            export { control_2 as control };
        }
        namespace saveText {
            let control_3: string;
            export { control_3 as control };
        }
        namespace updateText {
            let control_4: string;
            export { control_4 as control };
        }
        namespace saveMode {
            let control_5: string;
            export { control_5 as control };
            let options_1: ("auto" | "manual")[];
            export { options_1 as options };
        }
        namespace creatorOrAdmin {
            let control_6: string;
            export { control_6 as control };
        }
    }
}
export default _default;
export namespace Playground {
    function render(args: any): {
        components: {
            SvnProTopBarLearnModule: import("vue").DefineComponent<{}, {
                $emit: (event: "toggle-comments" | "toggle-favorite" | "copy-link" | "edit-module" | "save-module" | "preview-module" | "close-and-go-back" | "closeAndGoBack" | "publish-module" | "download-pdf", ...args: any[]) => void;
                type: string;
                isFavorited: boolean;
                saveMode: string;
                saveText: string;
                menuItems: unknown[];
                updateText: string;
                publishText: string;
                creatorOrAdmin: boolean;
                $props: {
                    readonly type?: string | undefined;
                    readonly isFavorited?: boolean | undefined;
                    readonly saveMode?: string | undefined;
                    readonly saveText?: string | undefined;
                    readonly menuItems?: unknown[] | undefined;
                    readonly updateText?: string | undefined;
                    readonly publishText?: string | undefined;
                    readonly creatorOrAdmin?: boolean | undefined;
                };
            }, {}, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").PublicProps, Readonly<{}> & Readonly<{}>, {}, {}, {}, {}, string, import("vue").ComponentProvideOptions, true, {}, any>;
        };
        setup(): {
            args: any;
        };
        template: string;
    };
    namespace args {
        let type_1: string;
        export { type_1 as type };
        let saveMode_1: string;
        export { saveMode_1 as saveMode };
        let saveText_1: string;
        export { saveText_1 as saveText };
        let isFavorited_1: boolean;
        export { isFavorited_1 as isFavorited };
        let updateText_1: string;
        export { updateText_1 as updateText };
        let creatorOrAdmin_1: boolean;
        export { creatorOrAdmin_1 as creatorOrAdmin };
        let publishText_1: string;
        export { publishText_1 as publishText };
    }
}
import SvnProTopBarLearnModule from "../components/menu/SvnProTopBarLearnModule.vue";
