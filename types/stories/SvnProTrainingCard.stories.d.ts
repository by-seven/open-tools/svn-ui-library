declare namespace _default {
    export let title: string;
    export { SvnProTrainingCard as component };
    export let tags: string[];
    export namespace argTypes {
        export namespace cardSize {
            let control: string;
            let options: string[];
        }
        export namespace status {
            let control_1: string;
            export { control_1 as control };
            let options_1: string[];
            export { options_1 as options };
        }
        export namespace image {
            let control_2: string;
            export { control_2 as control };
        }
        export namespace title_1 {
            let control_3: string;
            export { control_3 as control };
        }
        export { title_1 as title };
        export namespace duration {
            let control_4: string;
            export { control_4 as control };
        }
        export namespace progress {
            let control_5: string;
            export { control_5 as control };
        }
        export namespace primaryButtonText {
            let control_6: string;
            export { control_6 as control };
        }
    }
    export namespace parameters {
        namespace docs {
            namespace description {
                let component: string;
            }
        }
    }
}
export default _default;
export namespace Playground {
    function render(args: any): {
        components: {
            SvnProTrainingCard: import("vue").DefineComponent<{}, {
                $emit: (event: "click-primary-button" | "go-to-item-show", ...args: any[]) => void;
                primaryButtonText: string;
                cardSize: string;
                status: string;
                progress?: number | undefined;
                title?: string | undefined;
                image?: string | undefined;
                duration?: string | undefined;
                $props: {
                    readonly primaryButtonText?: string | undefined;
                    readonly cardSize?: string | undefined;
                    readonly status?: string | undefined;
                    readonly progress?: number | undefined;
                    readonly title?: string | undefined;
                    readonly image?: string | undefined;
                    readonly duration?: string | undefined;
                };
            }, {}, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").PublicProps, Readonly<{}> & Readonly<{}>, {}, {}, {}, {}, string, import("vue").ComponentProvideOptions, true, {}, any>;
        };
        setup(): {
            args: any;
        };
        template: string;
    };
    namespace args {
        let cardSize_1: string;
        export { cardSize_1 as cardSize };
        let status_1: string;
        export { status_1 as status };
        let image_1: string;
        export { image_1 as image };
        let title_2: string;
        export { title_2 as title };
        let duration_1: string;
        export { duration_1 as duration };
        let progress_1: number;
        export { progress_1 as progress };
        let primaryButtonText_1: string;
        export { primaryButtonText_1 as primaryButtonText };
    }
}
import SvnProTrainingCard from "../components/card/SvnProTrainingCard.vue";
