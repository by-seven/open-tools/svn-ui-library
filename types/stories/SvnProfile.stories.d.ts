declare namespace _default {
    export let title: string;
    export { SvnProfile as component };
    export let tags: string[];
    export namespace argTypes {
        namespace appVariant {
            let control: string;
            let options: string[];
        }
    }
    export namespace parameters {
        namespace docs {
            namespace description {
                let component: string;
            }
        }
    }
}
export default _default;
export namespace Playground {
    function render(args: any): {
        components: {
            SvnProfile: import("vue").DefineComponent<{}, {
                $emit: (event: "uploadFile" | "deletePicture", ...args: any[]) => void;
                dialogCustomClass: string;
                role: string;
                avatar: string;
                firstname: string;
                lastname: string;
                email: string;
                tags: unknown[];
                appVariant: string;
                avatarSize: string;
                edit: boolean;
                $props: {
                    readonly dialogCustomClass?: string | undefined;
                    readonly role?: string | undefined;
                    readonly avatar?: string | undefined;
                    readonly firstname?: string | undefined;
                    readonly lastname?: string | undefined;
                    readonly email?: string | undefined;
                    readonly tags?: unknown[] | undefined;
                    readonly appVariant?: string | undefined;
                    readonly avatarSize?: string | undefined;
                    readonly edit?: boolean | undefined;
                };
            }, {}, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").PublicProps, Readonly<{}> & Readonly<{}>, {}, {}, {}, {}, string, import("vue").ComponentProvideOptions, true, {}, any>;
        };
        setup(): {
            args: any;
        };
        template: string;
    };
    namespace args {
        export { tags };
        export let firstname: string;
        export let lastname: string;
        export let email: string;
        export let role: string;
        let appVariant_1: string;
        export { appVariant_1 as appVariant };
    }
}
import SvnProfile from "../components/profile/SvnProfile.vue";
declare const tags_1: string[];
