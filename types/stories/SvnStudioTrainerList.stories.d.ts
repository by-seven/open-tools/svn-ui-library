declare namespace _default {
    export let title: string;
    export { SvnStudioTrainerList as component };
    export let tags: string[];
    export namespace parameters {
        namespace docs {
            namespace description {
                let component: string;
            }
        }
    }
}
export default _default;
export namespace Playground {
    function render(args: any): {
        components: {
            SvnStudioTrainerList: import("vue").DefineComponent<{}, {
                edit: boolean;
                trainersList: unknown[];
                $props: {
                    readonly edit?: boolean | undefined;
                    readonly trainersList?: unknown[] | undefined;
                };
            }, {}, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").PublicProps, Readonly<{}> & Readonly<{}>, {}, {}, {}, {}, string, import("vue").ComponentProvideOptions, true, {}, any>;
        };
        setup(): {
            args: any;
        };
        template: string;
    };
    namespace args {
        export { trainerList as trainersList };
        export let edit: boolean;
    }
}
import SvnStudioTrainerList from "../components/list/SvnStudioTrainerList.vue";
declare const trainerList: {
    avatar: string;
    firstname: string;
    lastname: string;
}[];
