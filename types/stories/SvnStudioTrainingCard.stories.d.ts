declare namespace _default {
    export let title: string;
    export { SvnStudioTrainingCard as component };
    export let tags: string[];
    export namespace parameters {
        namespace docs {
            namespace description {
                let component: string;
            }
        }
    }
}
export default _default;
export namespace Playground {
    function render(args: any): {
        components: {
            SvnStudioTrainingCard: import("vue").DefineComponent<{}, {
                card: Record<string, any>;
                dateNotSetText: string;
                $props: {
                    readonly card?: Record<string, any> | undefined;
                    readonly dateNotSetText?: string | undefined;
                };
            }, {}, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").PublicProps, Readonly<{}> & Readonly<{}>, {}, {}, {}, {}, string, import("vue").ComponentProvideOptions, true, {}, any>;
        };
        setup(): {
            args: any;
        };
        template: string;
    };
    namespace args {
        export { card };
        export let dateNotSetText: string;
    }
}
import SvnStudioTrainingCard from "../components/card/SvnStudioTrainingCard.vue";
declare namespace card {
    export let startline: string;
    export let endline: string;
    let title_1: string;
    export { title_1 as title };
    let description_1: string;
    export { description_1 as description };
    export let logo: string;
    export let creator: string;
    export let session_number: number;
}
