declare namespace _default {
    export let title: string;
    export { SvnSwitch as component };
    export let tags: string[];
    export namespace parameters {
        namespace docs {
            namespace description {
                let component: string;
            }
        }
    }
}
export default _default;
export namespace Playground {
    function render(args: any): {
        components: {
            SvnSwitch: import("vue").DefineComponent<{}, {
                $emit: (event: "update:modelValue", ...args: any[]) => void;
                label: string;
                color: string;
                modelValue: string;
                density: string;
                ripple: boolean;
                inset: boolean;
                $props: {
                    readonly label?: string | undefined;
                    readonly color?: string | undefined;
                    readonly modelValue?: string | undefined;
                    readonly density?: string | undefined;
                    readonly ripple?: boolean | undefined;
                    readonly inset?: boolean | undefined;
                };
            }, {}, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").PublicProps, Readonly<{}> & Readonly<{}>, {}, {}, {}, {}, string, import("vue").ComponentProvideOptions, true, {}, any>;
        };
        setup(): {
            args: any;
        };
        template: string;
    };
    namespace args {
        let readOnly: boolean;
        let density: string;
        let color: string;
        let label: string;
        let inset: boolean;
        let ripple: boolean;
    }
}
import SvnSwitch from "../components/input/SvnSwitch.vue";
