declare namespace _default {
    export let title: string;
    export { SvnText as component };
    export let tags: string[];
    export namespace argTypes {
        namespace color {
            let control: string;
        }
        namespace base {
            let control_1: string;
            export { control_1 as control };
        }
    }
    export namespace parameters {
        namespace docs {
            namespace description {
                let component: string;
            }
        }
    }
}
export default _default;
export namespace Playground {
    function render(args: any): {
        components: {
            SvnText: {
                new (...args: any[]): import("vue").CreateComponentPublicInstanceWithMixins<Readonly<{}> & Readonly<{}>, {
                    base: boolean;
                    textarea: boolean;
                    text: string;
                    sm: boolean;
                    color: string;
                    bold: boolean;
                    semiBold: boolean;
                    medium: boolean;
                    regular: boolean;
                    italic: boolean;
                    xs: boolean;
                    xxs: boolean;
                    $props: {
                        readonly base?: boolean | undefined;
                        readonly textarea?: boolean | undefined;
                        readonly text?: string | undefined;
                        readonly sm?: boolean | undefined;
                        readonly color?: string | undefined;
                        readonly bold?: boolean | undefined;
                        readonly semiBold?: boolean | undefined;
                        readonly medium?: boolean | undefined;
                        readonly regular?: boolean | undefined;
                        readonly italic?: boolean | undefined;
                        readonly xs?: boolean | undefined;
                        readonly xxs?: boolean | undefined;
                    };
                }, {}, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps & Readonly<{}> & Readonly<{}>, {}, true, {}, {}, import("vue").GlobalComponents, import("vue").GlobalDirectives, string, {}, any, import("vue").ComponentProvideOptions, {
                    P: {};
                    B: {};
                    D: {};
                    C: {};
                    M: {};
                    Defaults: {};
                }, Readonly<{}> & Readonly<{}>, {
                    base: boolean;
                    textarea: boolean;
                    text: string;
                    sm: boolean;
                    color: string;
                    bold: boolean;
                    semiBold: boolean;
                    medium: boolean;
                    regular: boolean;
                    italic: boolean;
                    xs: boolean;
                    xxs: boolean;
                    $props: {
                        readonly base?: boolean | undefined;
                        readonly textarea?: boolean | undefined;
                        readonly text?: string | undefined;
                        readonly sm?: boolean | undefined;
                        readonly color?: string | undefined;
                        readonly bold?: boolean | undefined;
                        readonly semiBold?: boolean | undefined;
                        readonly medium?: boolean | undefined;
                        readonly regular?: boolean | undefined;
                        readonly italic?: boolean | undefined;
                        readonly xs?: boolean | undefined;
                        readonly xxs?: boolean | undefined;
                    };
                }, {}, {}, {}, {}>;
                __isFragment?: never;
                __isTeleport?: never;
                __isSuspense?: never;
            } & import("vue").ComponentOptionsBase<Readonly<{}> & Readonly<{}>, {
                base: boolean;
                textarea: boolean;
                text: string;
                sm: boolean;
                color: string;
                bold: boolean;
                semiBold: boolean;
                medium: boolean;
                regular: boolean;
                italic: boolean;
                xs: boolean;
                xxs: boolean;
                $props: {
                    readonly base?: boolean | undefined;
                    readonly textarea?: boolean | undefined;
                    readonly text?: string | undefined;
                    readonly sm?: boolean | undefined;
                    readonly color?: string | undefined;
                    readonly bold?: boolean | undefined;
                    readonly semiBold?: boolean | undefined;
                    readonly medium?: boolean | undefined;
                    readonly regular?: boolean | undefined;
                    readonly italic?: boolean | undefined;
                    readonly xs?: boolean | undefined;
                    readonly xxs?: boolean | undefined;
                };
            }, {}, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, {}, {}, string, {}, import("vue").GlobalComponents, import("vue").GlobalDirectives, string, import("vue").ComponentProvideOptions> & import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps & (new () => {
                $slots: {
                    default?(_: {}): any;
                    default?(_: {}): any;
                    default?(_: {}): any;
                    default?(_: {}): any;
                };
            });
        };
        setup(): {
            args: any;
        };
        template: string;
    };
    namespace args {
        let color_1: string;
        export { color_1 as color };
        let base_1: boolean;
        export { base_1 as base };
        export let sm: boolean;
        export let xs: boolean;
        export let xxs: boolean;
        export let bold: boolean;
        export let semiBold: boolean;
        export let medium: boolean;
        export let regular: boolean;
        export let italic: boolean;
        export let text: string;
        export let textarea: boolean;
    }
}
import SvnText from "../components/styles/SvnText.vue";
