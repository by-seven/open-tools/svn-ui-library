declare namespace _default {
    export let title: string;
    export { SvnTimeline as component };
    export let tags: string[];
    export namespace argTypes {
        namespace truncateLine {
            let control: string;
        }
        namespace lineThickness {
            export namespace control_1 {
                let type: string;
            }
            export { control_1 as control };
        }
        namespace size {
            let control_2: string;
            export { control_2 as control };
            export let options: string[];
        }
    }
    export namespace parameters {
        namespace docs {
            namespace description {
                let component: string;
            }
        }
    }
}
export default _default;
export namespace Playground {
    function render(args: any): {
        components: {
            SvnTimeline: {
                new (...args: any[]): import("vue").CreateComponentPublicInstanceWithMixins<Readonly<{}> & Readonly<{}>, {
                    size: string;
                    items: Record<string, any>;
                    truncateLine: string;
                    lineThickness: number;
                    lineInset: number;
                    dotColor: string;
                    $props: {
                        readonly size?: string | undefined;
                        readonly items?: Record<string, any> | undefined;
                        readonly truncateLine?: string | undefined;
                        readonly lineThickness?: number | undefined;
                        readonly lineInset?: number | undefined;
                        readonly dotColor?: string | undefined;
                    };
                }, {}, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps & Readonly<{}> & Readonly<{}>, {}, true, {}, {}, import("vue").GlobalComponents, import("vue").GlobalDirectives, string, {}, any, import("vue").ComponentProvideOptions, {
                    P: {};
                    B: {};
                    D: {};
                    C: {};
                    M: {};
                    Defaults: {};
                }, Readonly<{}> & Readonly<{}>, {
                    size: string;
                    items: Record<string, any>;
                    truncateLine: string;
                    lineThickness: number;
                    lineInset: number;
                    dotColor: string;
                    $props: {
                        readonly size?: string | undefined;
                        readonly items?: Record<string, any> | undefined;
                        readonly truncateLine?: string | undefined;
                        readonly lineThickness?: number | undefined;
                        readonly lineInset?: number | undefined;
                        readonly dotColor?: string | undefined;
                    };
                }, {}, {}, {}, {}>;
                __isFragment?: never;
                __isTeleport?: never;
                __isSuspense?: never;
            } & import("vue").ComponentOptionsBase<Readonly<{}> & Readonly<{}>, {
                size: string;
                items: Record<string, any>;
                truncateLine: string;
                lineThickness: number;
                lineInset: number;
                dotColor: string;
                $props: {
                    readonly size?: string | undefined;
                    readonly items?: Record<string, any> | undefined;
                    readonly truncateLine?: string | undefined;
                    readonly lineThickness?: number | undefined;
                    readonly lineInset?: number | undefined;
                    readonly dotColor?: string | undefined;
                };
            }, {}, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, {}, {}, string, {}, import("vue").GlobalComponents, import("vue").GlobalDirectives, string, import("vue").ComponentProvideOptions> & import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps & (new () => {
                $slots: {
                    title?(_: {
                        item: any;
                    }): any;
                    centerItem?(_: {
                        item: any;
                    }): any;
                    block?(_: {
                        item: any;
                    }): any;
                };
            });
        };
        setup(): {
            args: any;
        };
        template: string;
    };
    namespace args {
        export { items };
        let truncateLine_1: string;
        export { truncateLine_1 as truncateLine };
        let lineThickness_1: number;
        export { lineThickness_1 as lineThickness };
        let size_1: string;
        export { size_1 as size };
    }
}
import SvnTimeline from "../components/SvnTimeline.vue";
declare const items: ({
    title: string;
    state: null;
    dotColor: string;
} | {
    title: string;
    state: string;
    dotColor: string;
})[];
