declare namespace _default {
    export let title: string;
    export { SvnTiptapTest as component };
    export let tags: string[];
    export namespace argTypes {
        namespace isEditable {
            let control: string;
        }
        namespace withBorder {
            let control_1: string;
            export { control_1 as control };
        }
        namespace withPadding {
            let control_2: string;
            export { control_2 as control };
        }
        namespace extensionLeftMenu {
            let control_3: string;
            export { control_3 as control };
        }
        namespace size {
            let control_4: string;
            export { control_4 as control };
            export let options: string[];
        }
    }
    export namespace parameters {
        namespace docs {
            namespace description {
                let component: string;
            }
        }
    }
}
export default _default;
export namespace Playground {
    function render(args: any): {
        components: {
            SvnTiptapTest: import("vue").DefineComponent<{}, {
                size: string;
                htmlData: string;
                withBorder: boolean;
                withPadding: boolean;
                isEditable: boolean;
                extensionLeftMenu: boolean;
                $props: {
                    readonly size?: string | undefined;
                    readonly htmlData?: string | undefined;
                    readonly withBorder?: boolean | undefined;
                    readonly withPadding?: boolean | undefined;
                    readonly isEditable?: boolean | undefined;
                    readonly extensionLeftMenu?: boolean | undefined;
                };
            }, {}, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").PublicProps, Readonly<{}> & Readonly<{}>, {}, {}, {}, {}, string, import("vue").ComponentProvideOptions, true, {}, any>;
        };
        setup(): {
            args: any;
        };
        template: string;
    };
    namespace args {
        let size_1: string;
        export { size_1 as size };
        let isEditable_1: boolean;
        export { isEditable_1 as isEditable };
        let withBorder_1: boolean;
        export { withBorder_1 as withBorder };
        let withPadding_1: boolean;
        export { withPadding_1 as withPadding };
        let extensionLeftMenu_1: boolean;
        export { extensionLeftMenu_1 as extensionLeftMenu };
        export let htmlData: string;
    }
}
import SvnTiptapTest from "./Components/SvnTiptapTest.vue";
