declare namespace _default {
    export let title: string;
    export { SvnTitle as component };
    export let tags: string[];
    export namespace argTypes {
        namespace color {
            let control: string;
        }
        namespace h1 {
            let control_1: string;
            export { control_1 as control };
        }
    }
    export namespace parameters {
        namespace docs {
            namespace description {
                let component: string;
            }
        }
    }
}
export default _default;
export namespace Playground {
    function render(args: any): {
        components: {
            SvnTitle: {
                new (...args: any[]): import("vue").CreateComponentPublicInstanceWithMixins<Readonly<{}> & Readonly<{}>, {
                    h1: boolean;
                    h2: boolean;
                    h3: boolean;
                    color: string;
                    bold: boolean;
                    semiBold: boolean;
                    medium: boolean;
                    regular: boolean;
                    italic: boolean;
                    customClass: string;
                    $props: {
                        readonly h1?: boolean | undefined;
                        readonly h2?: boolean | undefined;
                        readonly h3?: boolean | undefined;
                        readonly color?: string | undefined;
                        readonly bold?: boolean | undefined;
                        readonly semiBold?: boolean | undefined;
                        readonly medium?: boolean | undefined;
                        readonly regular?: boolean | undefined;
                        readonly italic?: boolean | undefined;
                        readonly customClass?: string | undefined;
                    };
                }, {}, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps & Readonly<{}> & Readonly<{}>, {}, true, {}, {}, import("vue").GlobalComponents, import("vue").GlobalDirectives, string, {}, any, import("vue").ComponentProvideOptions, {
                    P: {};
                    B: {};
                    D: {};
                    C: {};
                    M: {};
                    Defaults: {};
                }, Readonly<{}> & Readonly<{}>, {
                    h1: boolean;
                    h2: boolean;
                    h3: boolean;
                    color: string;
                    bold: boolean;
                    semiBold: boolean;
                    medium: boolean;
                    regular: boolean;
                    italic: boolean;
                    customClass: string;
                    $props: {
                        readonly h1?: boolean | undefined;
                        readonly h2?: boolean | undefined;
                        readonly h3?: boolean | undefined;
                        readonly color?: string | undefined;
                        readonly bold?: boolean | undefined;
                        readonly semiBold?: boolean | undefined;
                        readonly medium?: boolean | undefined;
                        readonly regular?: boolean | undefined;
                        readonly italic?: boolean | undefined;
                        readonly customClass?: string | undefined;
                    };
                }, {}, {}, {}, {}>;
                __isFragment?: never;
                __isTeleport?: never;
                __isSuspense?: never;
            } & import("vue").ComponentOptionsBase<Readonly<{}> & Readonly<{}>, {
                h1: boolean;
                h2: boolean;
                h3: boolean;
                color: string;
                bold: boolean;
                semiBold: boolean;
                medium: boolean;
                regular: boolean;
                italic: boolean;
                customClass: string;
                $props: {
                    readonly h1?: boolean | undefined;
                    readonly h2?: boolean | undefined;
                    readonly h3?: boolean | undefined;
                    readonly color?: string | undefined;
                    readonly bold?: boolean | undefined;
                    readonly semiBold?: boolean | undefined;
                    readonly medium?: boolean | undefined;
                    readonly regular?: boolean | undefined;
                    readonly italic?: boolean | undefined;
                    readonly customClass?: string | undefined;
                };
            }, {}, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, {}, {}, string, {}, import("vue").GlobalComponents, import("vue").GlobalDirectives, string, import("vue").ComponentProvideOptions> & import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps & (new () => {
                $slots: {
                    default?(_: {}): any;
                    default?(_: {}): any;
                    default?(_: {}): any;
                };
            });
        };
        setup(): {
            args: any;
        };
        template: string;
    };
    namespace args {
        let color_1: string;
        export { color_1 as color };
        let h1_1: boolean;
        export { h1_1 as h1 };
        export let h2: boolean;
        export let h3: boolean;
        export let bold: boolean;
        export let semiBold: boolean;
        export let medium: boolean;
        export let regular: boolean;
        export let italic: boolean;
    }
}
import SvnTitle from "../components/styles/SvnTitle.vue";
