declare namespace _default {
    export let title: string;
    export { SvnTooltip as component };
    export let tags: string[];
    export namespace parameters {
        namespace docs {
            namespace description {
                let component: string;
            }
        }
    }
}
export default _default;
export namespace Playground {
    function render(args: any): {
        components: {
            SvnTooltip: {
                new (...args: any[]): import("vue").CreateComponentPublicInstanceWithMixins<Readonly<{}> & Readonly<{}>, {
                    origin: string;
                    location: string;
                    contentClass: string;
                    tooltipText: string;
                    $props: {
                        readonly origin?: string | undefined;
                        readonly location?: string | undefined;
                        readonly contentClass?: string | undefined;
                        readonly tooltipText?: string | undefined;
                    };
                }, {}, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps & Readonly<{}> & Readonly<{}>, {}, true, {}, {}, import("vue").GlobalComponents, import("vue").GlobalDirectives, string, {}, any, import("vue").ComponentProvideOptions, {
                    P: {};
                    B: {};
                    D: {};
                    C: {};
                    M: {};
                    Defaults: {};
                }, Readonly<{}> & Readonly<{}>, {
                    origin: string;
                    location: string;
                    contentClass: string;
                    tooltipText: string;
                    $props: {
                        readonly origin?: string | undefined;
                        readonly location?: string | undefined;
                        readonly contentClass?: string | undefined;
                        readonly tooltipText?: string | undefined;
                    };
                }, {}, {}, {}, {}>;
                __isFragment?: never;
                __isTeleport?: never;
                __isSuspense?: never;
            } & import("vue").ComponentOptionsBase<Readonly<{}> & Readonly<{}>, {
                origin: string;
                location: string;
                contentClass: string;
                tooltipText: string;
                $props: {
                    readonly origin?: string | undefined;
                    readonly location?: string | undefined;
                    readonly contentClass?: string | undefined;
                    readonly tooltipText?: string | undefined;
                };
            }, {}, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, {}, {}, string, {}, import("vue").GlobalComponents, import("vue").GlobalDirectives, string, import("vue").ComponentProvideOptions> & import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps & (new () => {
                $slots: {
                    "activator-content"?(_: {}): any;
                    "tooltip-content"?(_: {}): any;
                };
            });
        };
        setup(): {
            args: any;
        };
        template: string;
    };
    namespace args {
        let tooltipText: string;
    }
}
import SvnTooltip from "../components/SvnTooltip.vue";
