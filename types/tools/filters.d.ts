declare namespace _default {
    function capitalize(value: any): any;
    function upcase(value: any): any;
    function downcase(value: any): any;
    function plural(value: any): "" | "s";
    function cleanUnderscore(value: any): any;
    function convert_hr(value: any): any;
    function truncateN(value: any, n?: number): any;
    function compareDates(startDate: any, endDate: any, formatStr?: string): boolean;
    function formatDate(value: any, formatStr?: string, initialFormatStr?: string): string | undefined;
    function formatDateHours(value: any, formatStr?: string): string;
    function formatHours(value: any, formatStr?: string): string;
    function formatDateHoursWithoutSeconds(value: any, formatStr?: string): string;
    function formatDateHoursWithAmPm(value: any, formatStr?: string, initialFormatStr?: string): string;
    function formatSpecificDate(value: any, initialFormat: any, resultFormat: any): string;
    function formatSpecificDateCalendar(value: any, initialFormat: any): string;
    function generateUniqueId(): string;
}
export default _default;
