declare const _default: import("vue").DefineComponent<{}, {
    activatorText: string;
    activatorVariant: string;
    activatorIcon: string;
    activatorColor: string;
    activatorHeight: number;
    mockData: unknown[];
    $props: {
        readonly activatorText?: string | undefined;
        readonly activatorVariant?: string | undefined;
        readonly activatorIcon?: string | undefined;
        readonly activatorColor?: string | undefined;
        readonly activatorHeight?: number | undefined;
        readonly mockData?: unknown[] | undefined;
    };
}, {}, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").PublicProps, Readonly<{}> & Readonly<{}>, {}, {}, {}, {}, string, import("vue").ComponentProvideOptions, true, {}, any>;
export default _default;
