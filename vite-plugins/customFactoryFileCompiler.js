export function customFactoryFileCompiler() {
  return {
    name: 'custom-factory-file-compiler',
    transform(src, id) {
      if (id.endsWith('.factory.js')) {
        console.log(`Transforming ${id}`);

        const transformedCode =
          `// Transformed by customFactoryFileCompiler\n` + src;

        return {
          code: transformedCode,
          map: null,
        };
      }
      return null;
    },
  };
}
