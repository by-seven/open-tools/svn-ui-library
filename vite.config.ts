import { defineConfig } from 'vite';
import vue from '@vitejs/plugin-vue';
import vueJsx from '@vitejs/plugin-vue-jsx';
import { fileURLToPath, URL } from 'url';
import { customFactoryFileCompiler } from './vite-plugins/customFactoryFileCompiler';

// Configuration Vite
export default defineConfig({
  build: {
    lib: {
      entry: {
        components: './src/plugins/svn-ui-library.js',
        extensions: './src/components/editor/extensions/index.ts',
      },
    },
    rollupOptions: {
      external: ['vue'],
      output: {
        globals: {
          vue: 'Vue',
        },
      },
    },
  },
  plugins: [
    vue({
      template: {
        compilerOptions: {
          isCustomElement: (tag) => ['hex-color-picker'].includes(tag),
        },
      },
    }),
    vueJsx({}),
    customFactoryFileCompiler(),
  ],
  resolve: {
    alias: {
      '@': fileURLToPath(new URL('./src', import.meta.url)),
    },
  },
});
